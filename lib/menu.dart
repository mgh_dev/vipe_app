import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/settimgs_artist_page/ui/settings_artist_page.dart';
import 'package:vibe_app/futures/venue_flow/calender_venus_page/ui/calender_calender_page.dart';
import 'package:vibe_app/futures/venue_flow/gig_request_venus/ui/gig_request_venus_page.dart';
import 'package:vibe_app/futures/venue_flow/request_list_page/request_list_page.dart';
import 'package:vibe_app/futures/venue_flow/settimgs_venus_page/ui/settings_venus_page.dart';
import 'package:vibe_app/futures/venue_flow/status_request_page/status_request_page.dart';
import 'package:vibe_app/incoming_requests_tab.dart';

import 'date_create_event_page.dart';
import 'futures/venue_flow/buy_subscription_page/ui/buy_subscription_page.dart';
import 'futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'futures/venue_flow/messages_page_venus/ui/messages_page_venus.dart';


class Menussss extends StatefulWidget {

  var btn;

  Menussss({@required this.btn});

  @override
  State<Menussss> createState() => _MenussssState();
}


class _MenussssState extends State<Menussss> {
  bool btn1 = false;
  bool btn2 = false;
  bool btn3 = false;
  bool btn4 = false;
  bool btn5 = false;
  bool btn6 = false;


  @override
  Widget build(BuildContext context) {
    widget.btn == 1 ? btn1 = true : btn1 = false;
    widget.btn == 2 ? btn2 = true : btn2 = false;
    widget.btn == 3 ? btn3 = true : btn3 = false;
    widget.btn == 4 ? btn4 = true : btn4 = false;
    widget.btn == 5 ? btn5 = true : btn5 = false;
    widget.btn == 6 ? btn6 = true : btn6 = false;

    return Drawer(
      width: 270.w,
      backgroundColor: AppColor.white.withOpacity(00),
      child:

      ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 15,
            sigmaY: 15,
          ),
          child: Container(
            width: 228.0,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.3),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 63.h, bottom: 53.h, left: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 63.h, left: 16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          ImagesApp.profileMenu,
                          width: 48.w,
                          height: 48.h,
                        ),
                        SizedBox(height: 12.h,),
                        Row(
                          children: [
                            Text(HiveServices.getNameUser.toString(),
                              style: poppins.copyWith(fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: AppColor.white),),
                            SizedBox(width: 5.w,),
                            Icon(Icons.check_circle, color: AppColor.greyBlue2,)
                          ],
                        ),
                        SizedBox(height: 4.h,),
                        Text(HiveServices.getEmailUser.toString(),
                          style: poppins.copyWith(fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: AppColor.white),),
                      ],
                    ),
                  ),
                  Container(margin: EdgeInsets.only(top: 20.h, bottom: 32.h),
                    color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const GigRequestVenus()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuRequests),
                          SizedBox(width: 8.w,),
                          Text(
                            "Gig Requests",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => const CalenderVenusPage()));
                        },
                        child: Row(
                          children: [
                            SvgPicture.asset(ImagesApp.menuCalendar),
                            SizedBox(width: 8.w,),
                            Text(
                              "Calendar",
                              style: poppins.copyWith(
                                  fontSize: 14, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        btn3 = true;
                        if (btn3 == true) {
                          btn6 = false;
                          btn5 = false;
                          btn4 = false;
                          btn2 = false;
                          btn1 = false;
                          // Navigator.push(context,MaterialPageRoute(builder: (context)=>));
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => MessagesPageVenus()));
                        }
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuMessage),
                          SizedBox(width: 8.w,),
                          Text(
                            "Messages",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        btn4 = true;
                        if (btn4 == true) {
                          btn6 = false;
                          btn5 = false;
                          btn3 = false;
                          btn2 = false;
                          btn1 = false;
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => BuySubscriptionPage()));
                        }
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuSubscription),
                          SizedBox(width: 8.w,),
                          Text(
                            "Subscription",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        btn5 = true;
                        if (btn5 == true) {
                          btn6 = false;
                          btn4 = false;
                          btn3 = false;
                          btn2 = false;
                          btn1 = false;
                        }
                      });
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const SettingVenusPage()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuSetting),
                          SizedBox(width: 8.w,),
                          Text(
                            "Settings",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(color: AppColor.white.withOpacity(0.07),
                    height: 0.5,
                    width: double.infinity,),
                  const Spacer(),
                  GestureDetector(
                    onTap: () {
                      HiveServices.clear();
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      margin: EdgeInsets.only(bottom: 53.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuLogout),
                          SizedBox(width: 8.w,),
                          Text(
                            "Logout",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),


    );
  }
}
