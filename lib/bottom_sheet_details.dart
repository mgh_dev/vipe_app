
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetDetails extends StatelessWidget {
  BottomSheetDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _userEmailController = TextEditingController();
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                    },
                    child: SvgPicture.asset(ImagesApp.icBack)),
                Text(
                  "Subscription",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                SvgPicture.asset(ImagesApp.icNotification),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                ),
                backgroundColor: AppColor.darkBlue,
                isScrollControlled: true,
                builder: (BuildContext context) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 54.h,
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.r),
                              topRight: Radius.circular(20.r)),
                          child: Image.asset(
                            ImagesApp.ellipse,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 18.5.h,bottom: 13.h),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(child: Padding(
                                      padding: EdgeInsets.only(left: 28.w),
                                      child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                          },
                                          child: Text("Cancel",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w400,color: AppColor.blue),)
                                      ),
                                    ),),
                                    Expanded(child: Text("Event Summary",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),)),
                                    Expanded(
                                      child: Container(
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.07),),
                              Container(
                                margin: EdgeInsets.only(right: 16.w,left: 16.w,top: 14.5.h,bottom: 12.h),
                                padding: EdgeInsets.symmetric(vertical: 16.h),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.r),
                                    color: AppColor.white.withOpacity(0.09)
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(left: 16.w),
                                      child: Text("Performance length.",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Container(margin: EdgeInsets.only(top: 11.h,bottom: 13.h),height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.07),),
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                                      decoration: BoxDecoration(
                                          color: AppColor.white.withOpacity(0.07),
                                          borderRadius: BorderRadius.circular(15.r)
                                      ),
                                      child: CustomTextField(
                                        controller: _userEmailController,
                                        hint: "1.5 Hrs",
                                        hintTextDirection: TextDirection.rtl,
                                        hintStyle: poppins.copyWith(fontSize:44.sp,fontWeight: FontWeight.w500,color: AppColor.white ),
                                        align: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 16.w,left: 16.w,top: 14.5.h,bottom: 12.h),
                                padding: EdgeInsets.symmetric(vertical: 11.23.h,horizontal: 11.w),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.r),
                                    color: AppColor.white.withOpacity(0.09)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Offer Food & Beverage?",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                                    CustomSwitch(
                                      isToggled: true,
                                      onToggled: (isToggled) {
                                      },),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            },
            child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16)
                ),
                child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

          ),
        ],
      ),
    );
  }
}
