import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';



class TabCalIncomingRequestsVenus extends StatefulWidget {
  TabCalIncomingRequestsVenus({required this.scrollController}) : super();
  ScrollController scrollController;

  @override
  State<TabCalIncomingRequestsVenus> createState() => _TabCalIncomingRequestsVenusState();
}

class _TabCalIncomingRequestsVenusState extends State<TabCalIncomingRequestsVenus> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal:16.w  ),
      child: Container(
        width: 332,
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(18.r),
        ),
        child: Column(
          children: [
            SizedBox(height: 27.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.h),
              child: Text(
                "View and manage all incoming requests from artists.",
                style: poppins.copyWith(
                    fontSize: 11.sp,
                    fontWeight: FontWeight.w500,
                    color: AppColor.greyHint),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: ListView.builder(
                controller: widget.scrollController,
                physics: const BouncingScrollPhysics(),
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:  EdgeInsets.only(bottom: 10.h,right: 10.w,left: 10.w),
                    child: Container(
                      width: double.infinity,
                      height: 180.h,
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(12.r)),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 22.w,
                                      top: 17.5.h,
                                      bottom: 14.5.h),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(ImagesApp.icClock),
                                      SizedBox(
                                        width: 8.w,
                                      ),
                                      Text(
                                        "24 Aug",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: AppColor.white.withOpacity(0.07),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 46.6.w,
                                      top: 15.5.h,
                                      bottom: 14.5.h),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        ImagesApp.profileRequest,
                                        width: 48.w,
                                        height: 48.r,
                                      ),
                                      SizedBox(
                                        width: 16.w,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Audrey Soul",
                                            style: poppins.copyWith(
                                                fontSize: 16.sp,
                                                fontWeight:
                                                FontWeight.w500),
                                          ),
                                          Text(
                                            "\$599",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.grey2),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: AppColor.white.withOpacity(0.20),
                                ),
                                SizedBox(
                                  height: 46.h,
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                    const ChatRoomPage()));
                                          },
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                top: 12.h, bottom: 13.h),
                                            decoration: BoxDecoration(
                                                color: Colors.transparent,
                                                border: Border(
                                                    right: BorderSide(
                                                        color: AppColor
                                                            .grey5b))),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(
                                                    ImagesApp.icMessage),
                                                SizedBox(
                                                  width: 8.w,
                                                ),
                                                Text(
                                                  "Message",
                                                  style: poppins.copyWith(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight.w500,
                                                      color:
                                                      AppColor.whiteF8),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                top: 12.h, bottom: 13.h),
                                            color: Colors.transparent,
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(
                                                    ImagesApp.check),
                                                SizedBox(
                                                  width: 8.w,
                                                ),
                                                Text(
                                                  "Decline",
                                                  style: poppins.copyWith(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight.w500,
                                                      color: AppColor
                                                          .blueText2),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
