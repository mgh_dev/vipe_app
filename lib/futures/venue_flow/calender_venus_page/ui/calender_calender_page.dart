import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:glass/glass.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/tab/tab_cal_incoming_requests_artist.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/tab/tab_cal_open_postings_artist.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/tab/tab_cal_outgoing_requests_artist.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/tab/tab_cal_post_submissions_artist.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/tab/tab_cal_upcoming_events_artist.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/notifications_list/notifications_list.dart';
import 'package:vibe_app/incoming_requests_tab.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import 'tab/tab_cal_incoming_requests_venus.dart';
import 'tab/tab_cal_open_postings_venus.dart';
import 'tab/tab_cal_outgoing_requests_venus.dart';
import 'tab/tab_cal_post_submissions_venus.dart';
import 'tab/tab_cal_upcoming_events_venus.dart';

class CalenderVenusPage extends StatefulWidget {
  const CalenderVenusPage({Key? key}) : super(key: key);

  @override
  State<CalenderVenusPage> createState() => _CalenderVenusPageState();
}

class _CalenderVenusPageState extends State<CalenderVenusPage>
    with SingleTickerProviderStateMixin {
  late StreamController<DateTime> _weekDatesController;
  SelectItemViewModel selectItemViewModel = SelectItemViewModel();
  SelectItemViewModel selectDate = SelectItemViewModel();
  SelectItemViewModel viewDate = SelectItemViewModel();
  late TabController tabController;
  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();
  List<DateTime> weekDates = [];
  int currentYear = DateTime.now().year;
  int currentMonth = DateTime.now().month;
  List<String> lsDay = [
    "M",
    "T",
    "W",
    "T",
    "F",
    "S",
    "S",
  ];
  List<String> months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  late int indexDayWeek;

  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDate;

  Map<String, List<String>> mySelectedEvents = {};

  loadPreviousEvents() {
    mySelectedEvents = {
      "2023-07-05": [
        "cdcd",
      ],
      "2023-07-19": [
        "cdcd",
      ],
      "2023-07-29": [
        "cdcd",
      ]
    };
  }

  List _listOfDayEvents(DateTime dateTime) {
    if (mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)] != null) {
      return mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)]!;
    } else {
      return [];
    }
  }

  List<Widget> lsTab = [];

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      length: 4,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });
    print(currentMonth);
    _selectedDate = _focusedDay;
    loadPreviousEvents();
    print(months[currentMonth - 1]);
    DateTime now = DateTime.now();
    int weekday = now.weekday;

    // Calculate the start date of the current week
    DateTime startDate = now.subtract(Duration(days: weekday - 1));
    _weekDatesController = StreamController<DateTime>.broadcast();

    Timer.periodic(const Duration(days: 1), (timer) {
      DateTime currentDate = DateTime.now();
      if (currentDate.weekday == 1 && currentDate.day != startDate.day) {
        startDate = currentDate;
        _weekDatesController.add(startDate);
      }
    });

    // Generate a list of dates for the week
    for (int i = 0; i < 7; i++) {
      DateTime date = startDate.add(Duration(days: i));
      weekDates.add(date);
      if (date.day == DateTime.now().day) {
        indexDayWeek = i;
        print(date);
      }
      print("object${i}");
    }
    lsTab = [
      TabCalIncomingRequestsVenus(scrollController: secondController),
      TabCalOutgoingRequestsVenus(scrollController: secondController),
      TabCalOpenPostingsVenus(scrollController: secondController),
      TabCalPostSubmissionsVenus(scrollController: secondController),
      TabCalUpcomingEventsVenus(scrollController: secondController),
    ];
  }

  @override
  void dispose() {
    _weekDatesController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    secondController.addListener(() {
      if (secondController.offset <=
          secondController.position.minScrollExtent) {
        firstController.animateTo(0,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      } else if (secondController.offset >=
          secondController.position.minScrollExtent) {
        firstController.animateTo(firstController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      }
    });
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: CustomScrollView(
        controller: firstController,
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 50.h, left: 20.w, right: 20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: SvgPicture.asset(ImagesApp.icBack)),
                      Image.asset(
                        ImagesApp.logoApp,
                        width: 60.w,
                        height: 60.h,
                      ),
                      GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>NotificationsList()));
                          },
                          child: SvgPicture.asset(ImagesApp.icNotification)),
                    ],
                  ),
                ),
                BlocBuilder(
                  bloc: viewDate,
                  builder: (context, state) {
                    if (state is ItemChangeState) {
                      if (state.index == 1) {
                        return Container(
                          padding: EdgeInsets.only(
                              left: 13.w, right: 13.w, top: 27.h, bottom: 10.h),
                          width: 344.w,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        months[currentMonth - 1],
                                        style: poppins.copyWith(
                                          fontSize: 16.sp,
                                        ),
                                      ),
                                      Text(
                                        currentYear.toString(),
                                        style: poppins.copyWith(
                                          fontSize: 16.sp,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 40.w,
                                        height: 40.r,
                                        decoration: BoxDecoration(
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                            borderRadius:
                                                BorderRadius.circular(10.r)),
                                        child: Center(
                                            child: SvgPicture.asset(
                                                ImagesApp.calendar)),
                                      ),
                                      SizedBox(width: 7.w),
                                      GestureDetector(
                                        onTap: () => viewDate.changeItem(0),
                                        child: Container(
                                          width: 40.w,
                                          height: 40.r,
                                          decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.07),
                                              borderRadius:
                                                  BorderRadius.circular(10.r)),
                                          child: Center(
                                              child: SvgPicture.asset(
                                                  ImagesApp.icDoubleArrow)),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(height: 35.h),
                              BlocBuilder(
                                  bloc: selectDate..changeItem(indexDayWeek),
                                  builder: (context, state) {
                                    if (state is ItemChangeState) {
                                      return SizedBox(
                                        height: 60,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          physics:
                                              const BouncingScrollPhysics(),
                                          itemCount: weekDates.length,
                                          itemBuilder: (context, index) {
                                            DateTime date = weekDates[index];
                                            String formattedDate =
                                                DateFormat('dd').format(date);

                                            return Padding(
                                              padding:
                                                  EdgeInsets.only(right: 10.w),
                                              child: GestureDetector(
                                                onTap: () {
                                                  indexDayWeek = index;
                                                  selectDate
                                                      .changeItem(indexDayWeek);
                                                  print(indexDayWeek);
                                                },
                                                child: Container(
                                                  width: 45,
                                                  // height: 60,
                                                  decoration: BoxDecoration(
                                                    color: state.index == index
                                                        ? AppColor.green3
                                                        : Colors.transparent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            16.r),
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      SizedBox(height: 2.5.h),
                                                      Text(
                                                        lsDay[index],
                                                        style: poppins.copyWith(
                                                            fontSize: 12.sp,
                                                            color:
                                                                AppColor.white),
                                                      ),
                                                      SizedBox(height: 10.h),
                                                      Text(
                                                        formattedDate,
                                                        style: poppins.copyWith(
                                                          fontSize: 16.sp,
                                                        ),
                                                      ),
                                                      SizedBox(height: 2.5.h),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                    }
                                    return const SizedBox();
                                  })
                            ],
                          ),
                        );
                      } else {
                        return Container(
                          margin: EdgeInsets.only(
                            left: 13.w,
                            right: 13.w,
                            bottom: 10.h,
                          ),
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(20.r),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 15.h, left: 13.w, right: 13.w),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          months[currentMonth - 1],
                                          style: poppins.copyWith(
                                            fontSize: 16.sp,
                                          ),
                                        ),
                                        Text(
                                          currentYear.toString(),
                                          style: poppins.copyWith(
                                            fontSize: 16.sp,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 40.w,
                                          height: 40.r,
                                          decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.07),
                                              borderRadius:
                                                  BorderRadius.circular(10.r)),
                                          child: Center(
                                              child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                // _focusedDay = DateTime.now();
                                              });
                                            },
                                            child: SvgPicture.asset(
                                                ImagesApp.calendar),
                                          )),
                                        ),
                                        SizedBox(width: 7.w),
                                        GestureDetector(
                                          onTap: () => viewDate.changeItem(1),
                                          child: Container(
                                            width: 40.w,
                                            height: 40.r,
                                            decoration: BoxDecoration(
                                                color: AppColor.white
                                                    .withOpacity(0.07),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.r)),
                                            child: Center(
                                                child: SvgPicture.asset(
                                                    ImagesApp.icDoubleArrow)),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 17.5.h,
                                  bottom: 19.h,
                                  left: 16.w,
                                  right: 16.w,
                                ),
                                child: TableCalendar(
                                  headerVisible: false,
                                  startingDayOfWeek: StartingDayOfWeek.monday,
                                  daysOfWeekHeight: 20,
                                  daysOfWeekStyle: DaysOfWeekStyle(
                                    weekdayStyle:
                                        TextStyle(color: AppColor.grey2),
                                    weekendStyle:
                                        TextStyle(color: AppColor.grey2),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: AppColor.grey2,
                                            width: 0.5.w),
                                      ),
                                    ),
                                  ),
                                  firstDay: DateTime.utc(2010, 10, 16),
                                  lastDay: DateTime.utc(2030, 3, 14),
                                  focusedDay: _focusedDay,
                                  calendarStyle: CalendarStyle(
                                    markerDecoration: BoxDecoration(
                                      color: AppColor.green3,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    selectedDecoration: BoxDecoration(
                                      color: AppColor.green3,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    todayDecoration: BoxDecoration(
                                      color: AppColor.green3.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10.r),
                                    ),
                                    outsideDaysVisible: false,
                                    defaultTextStyle:
                                        TextStyle(color: AppColor.white),
                                    selectedTextStyle:
                                        TextStyle(color: AppColor.white),
                                    todayTextStyle:
                                        TextStyle(color: AppColor.white),
                                    weekendTextStyle:
                                        TextStyle(color: AppColor.white),
                                  ),
                                  calendarFormat: CalendarFormat.month,
                                  onDaySelected: (selectedDay, focusedDay) {
                                    print(selectedDay);
                                    if (!isSameDay(
                                        _selectedDate, selectedDay)) {
                                      setState(() {
                                        _selectedDate = selectedDay;
                                        _focusedDay = focusedDay;
                                      });
                                    }
                                  },
                                  selectedDayPredicate: (day) {
                                    return isSameDay(_selectedDate, day);
                                  },
                                  onFormatChanged: (format) {
                                    if (_calendarFormat != format) {
                                      setState(() {
                                        _calendarFormat = format;
                                      });
                                    }
                                  },
                                  onPageChanged: (focusedDay) {
                                    _focusedDay = focusedDay;
                                  },
                                  eventLoader: _listOfDayEvents,
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    return const SizedBox();
                  },
                )
              ],
            ),
          ),
          SliverToBoxAdapter(
              child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              BlocBuilder(
                  bloc: selectItemViewModel,
                  builder: (context, state) {
                    if (state is ItemChangeState) {
                      return Padding(
                        padding: EdgeInsets.only(
                            top: 8.h, bottom: 8.h, left: 16.w, right: 16.w),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50.r),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Container(
                              height: 54.h,
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(50.r),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5.w),
                                child: Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        selectItemViewModel.changeItem(0);
                                      },
                                      child: Container(
                                        width: 149.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 0
                                              ? AppColor.white.withOpacity(0.1)
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Incoming Requests",
                                            style: poppins.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () =>
                                          selectItemViewModel.changeItem(1),
                                      child: Container(
                                        width: 149.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 1
                                              ? AppColor.white.withOpacity(0.1)
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Outgoing Requests",
                                            style: poppins.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () =>
                                          selectItemViewModel.changeItem(2),
                                      child: Container(
                                        width: 149.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 2
                                              ? AppColor.white.withOpacity(0.1)
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Open gig postings",
                                            style: poppins.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () =>
                                          selectItemViewModel.changeItem(3),
                                      child: Container(
                                        width: 149.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 3
                                              ? AppColor.white.withOpacity(0.1)
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Post Submissions",
                                            style: poppins.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () =>
                                          selectItemViewModel.changeItem(4),
                                      child: Container(
                                        width: 149.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 4
                                              ? AppColor.white.withOpacity(0.1)
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Upcoming Events",
                                            style: poppins.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                    return const SizedBox();
                  }),
            ],
          )),
          SliverFillRemaining(
            child: BlocBuilder(
              bloc: selectItemViewModel,
              builder: (context, state) {
                if (state is ItemChangeState) {
                  return lsTab[state.index];
                }
                return SizedBox();
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: 84.h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 165.w,
              height: 49.h,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: AppColor.grey1.withOpacity(0.4),
                ),
                borderRadius: BorderRadius.circular(50.r),
              ),
              child: Center(
                child: Text(
                  "Manage Calendar",
                  style: poppins.copyWith(
                    fontSize: 14.sp,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            const CreateEventDetailsVenuePage()));
              },
              child: Container(
                width: 165.w,
                height: 49.h,
                decoration: BoxDecoration(
                  color: AppColor.green3,
                  borderRadius: BorderRadius.circular(50.r),
                ),
                child: Center(
                  child: Text(
                    "Create a Event",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ).asGlass(),
    );
  }
}
