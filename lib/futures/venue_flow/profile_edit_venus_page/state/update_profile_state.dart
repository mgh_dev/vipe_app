import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class UpdateProfileBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class UpdateProfileInitialState extends UpdateProfileBaseState {}

class UpdateProfileLoadingState extends UpdateProfileBaseState {}

class UpdateProfileSuccessState extends UpdateProfileBaseState {
  UpdateProfileSuccessState();
}

class UpdateProfileFailState extends UpdateProfileBaseState {
  final String message;
  UpdateProfileFailState({required this.message});
}
