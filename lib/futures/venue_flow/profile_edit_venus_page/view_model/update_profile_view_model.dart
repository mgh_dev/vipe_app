import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_edit_venus_page/state/update_profile_state.dart';

import '../../../../../core/config/web_service.dart';
import '../../create_profile_venue/logic/view_model/send_information_view_model.dart';
import '../../create_profile_venue/logic/view_model/send_vibe_view_model.dart';
import '../../create_profile_venue/model/request_model/request_profile_model_venus.dart';

class UpdateProfileViewModel extends Cubit<UpdateProfileBaseState> {
  UpdateProfileViewModel() : super(UpdateProfileInitialState());
  final SendInformationViewModel updateProfileViewModel =
      SendInformationViewModel();
  final SendVibeViewModel sendVibeViewModel = SendVibeViewModel();
  final UploadAvatarViewModel uploadAvatarViewModel = UploadAvatarViewModel();

  void updateWithAvatar(RequestCreateProfileVenus requestCreateProfileVenus,
      List<int>? lsVibe, File avatar) async {
    print("1");
    emit(UpdateProfileLoadingState());
    print("3");
    try {
      print("4");
    await  uploadAvatarViewModel.uploadAvatar(avatar.path);
      await  updateProfileViewModel.send(requestCreateProfileVenus);
      lsVibe!.isNotEmpty?
      await  sendVibeViewModel.send(lsVibe):null;

      emit(UpdateProfileSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(UpdateProfileFailState(message: e.toString()));
      print("10");
    }
  }

void update(RequestCreateProfileVenus requestCreateProfileVenus,
    List<int>? lsVibe) async {
  print("1");
  emit(UpdateProfileLoadingState());
  print("3");
  try {
    print("4");
    await updateProfileViewModel.send(requestCreateProfileVenus);
    lsVibe!.isNotEmpty?
    await  sendVibeViewModel.send(lsVibe):null;

    emit(UpdateProfileSuccessState());
    print("8");
  } catch (e) {
    print("9");
    emit(UpdateProfileFailState(message: e.toString()));
    print("10");
  }
}
}