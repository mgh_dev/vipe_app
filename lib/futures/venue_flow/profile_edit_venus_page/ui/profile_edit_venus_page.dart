import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/get_vibe_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_information_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_vibe_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_edit_venus_page/state/update_profile_state.dart';
import 'package:vibe_app/futures/venue_flow/profile_edit_venus_page/view_model/update_profile_view_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/state/get_profile_venus_state.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/view_model/get_profile_venus_view_model.dart';
import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../create_profile_venue/logic/state/get_vibe_state.dart';
import '../../profile_venus_page/ui/profile_venus_page.dart';

class ProfileEditVenusPage extends StatefulWidget {
  static List<int> lsVibe = [];
  static List<String> lsVibeName = [];
  static TextEditingController textCntTiktok = TextEditingController(text: "");
  static TextEditingController textCnTwitter = TextEditingController(text: "");
  static TextEditingController textCntYouTub = TextEditingController(text: "");
  static TextEditingController textCntWeb = TextEditingController(text: "");
  static TextEditingController textCntAdditionalDetail =
      TextEditingController(text: "");
  static List<Links>? myList;
  static List<Links> lsLink = [];
  static List<Links> lsLinkNotUpdate = [];
  static String? selectType;
  static late int role;

  const ProfileEditVenusPage() : super();

  @override
  State<ProfileEditVenusPage> createState() => _ProfileEditVenusPageState();
}

class _ProfileEditVenusPageState extends State<ProfileEditVenusPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  final List<String> ls = [
    "Eclectic",
    "Intimate",
    "High Energy",
    "Chill",
    "Nostalgic",
    "Bluesy",
    "Jazzy",
    "Folksy",
    "Rocking",
    "Reggae",
    "Hip-hop",
    "Classical",
    "World music",
  ];

  static String? seletEquipment;
  final getProfileViewModel = GetProfileVenusViewModel();
  final UploadAvatarViewModel uploadAvatarViewModel = UploadAvatarViewModel();
  File? image;

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
        print("<<<<IMAGE>>>>");

        ;
      });
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  FocusNode? _focusNodeNameBusines;
  final textCtnNameBusines = TextEditingController();

  FocusNode? textFocusNodeAgeLimit;

  final textCntAgeLimit = TextEditingController();

  final textCtnAbout = TextEditingController();

  FocusNode? _focusNodeNumberStage;
  final textCtnNumberStage = TextEditingController();

  FocusNode? _focusNodeSits;
  final textCtnSits = TextEditingController();
  GetVibeViewModel getVibeViewModel = GetVibeViewModel();
  final SendInformationViewModel updateProfileViewModel =
      SendInformationViewModel();
  final SendVibeViewModel sendVibeViewModel = SendVibeViewModel();
  final UpdateProfileViewModel updateProfileViewModels =
      UpdateProfileViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _focusNodeNameBusines = FocusNode();
    textFocusNodeAgeLimit = FocusNode();
    _focusNodeNumberStage = FocusNode();
    _focusNodeSits = FocusNode();
    getVibeViewModel.getVibe();
    tabController = TabController(
      length: 3,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });
    getProfileViewModel.get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: BlocConsumer(
        listener: (ctx, state) {
          if (state is GetProfileVenusSuccessState) {
            textCtnNameBusines.text =
                state.response.data.extraData.d.stageName;
            textCtnAbout.text = state.response.data.extraData.about;
            textCntAgeLimit.text = state.response.data.extraData.d.ageLimit;
            textCtnNumberStage.text =
                state.response.data.extraData.d.numberOfStages.toString();
            textCtnSits.text =
                state.response.data.extraData.d.venueCapacity.toString();
            ProfileEditVenusPage.selectType =
                ls[state.response.data.extraData.role];
            seletEquipment = state.response.data.extraData.d.inHouseEquipment;
            ProfileEditVenusPage.textCntAdditionalDetail.text =
                state.response.data.extraData.d.additionalDetailedInformation;
            ProfileEditVenusPage.role = state.response.data.extraData.role;
            for (int i = 0;
                i < state.response.data.extraData.links.length;
                i++) {
              String type = state.response.data.extraData.links[i].type;
              String title = state.response.data.extraData.links[i].type;
              String link = state.response.data.extraData.links[i].type;
              ProfileEditVenusPage.lsLinkNotUpdate
                  .add(Links(title: title, type: type, link: link));
            }
            print(state.response.data.gallery[0].path);
          }
        },
        bloc: getProfileViewModel,
        builder: (ctx, state) {
          if (state is GetProfileVenusLoadingState) {
            return const CustomLoading();
          }
          if (state is GetProfileVenusSuccessState) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 301.h,
                    decoration: BoxDecoration(
                      image: image == null
                          ? DecorationImage(
                              image: NetworkImage(
                                  "http://46.249.102.65:8080${state.response.data.avatar}"),
                              fit: BoxFit.fill)
                          : DecorationImage(
                              image: FileImage(image!), fit: BoxFit.fill),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: 53.h, left: 20.w, right: 20.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: SvgPicture.asset(ImagesApp.icBack)),
                              Image.asset(
                                ImagesApp.logoApp,
                                width: 60.w,
                                height: 60.h,
                              ),
                              SvgPicture.asset(ImagesApp.icShared),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 20.h, right: 24.w),
                          child: GestureDetector(
                            onTap: () {
                              pickImage(ImageSource.gallery);
                            },
                            child: Container(
                              width: 56.w,
                              height: 56.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColor.blue,
                              ),
                              child: Center(
                                child: SvgPicture.asset(
                                  ImagesApp.icPen,
                                  color: AppColor.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 10.h, bottom: 0.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    width: 300.w,
                                    child: CustomTextField(
                                      controller: textCtnNameBusines,
                                      fontSize: 14.sp,
                                      focusNode: _focusNodeNameBusines,
                                    ),
                                  ),
                                ],
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (_focusNodeNameBusines!.hasFocus ==
                                      false) {
                                    FocusScope.of(context)
                                        .requestFocus(_focusNodeNameBusines);
                                  } else {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                  }
                                },
                                child: SvgPicture.asset(
                                  ImagesApp.icPen,
                                  color: AppColor.blue,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 300.w,
                              child: CustomTextField(
                                controller: textCtnAbout,
                                fontSize: 14.sp,
                                line: 3,
                              ),
                            ),
                          ],
                        ),
                        const DividerWidget(),
                        Text(
                          "Business Profile information",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 34.h, bottom: 34.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                width: 40.w,
                                height: 40,
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.07),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                    child: SvgPicture.asset(ImagesApp.icGps)),
                              ),
                              SizedBox(width: 10.w),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Business type",
                                    style: poppins.copyWith(
                                      fontSize: 11.sp,
                                      color: AppColor.greyHint,
                                    ),
                                  ),
                                  Text(
                                    ProfileEditVenusPage.selectType!,
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              const Spacer(),
                              GestureDetector(
                                onTap: () {
                                  selectBuisinseType();
                                },
                                child: SvgPicture.asset(
                                  ImagesApp.icPen,
                                  color: AppColor.blue,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 40.w,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.07),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                  child: SvgPicture.asset(
                                      ImagesApp.icAddLocation)),
                            ),
                            SizedBox(width: 10.w),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Location",
                                  style: poppins.copyWith(
                                    fontSize: 11.sp,
                                    color: AppColor.greyHint,
                                  ),
                                ),
                                Text(
                                  "Concert Hall",
                                  style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            const Spacer(),
                            SvgPicture.asset(
                              ImagesApp.icPen,
                              color: AppColor.blue,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
                    child: Container(
                      width: 343.w,
                      height: 156.h,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(9.r),
                        image: DecorationImage(
                          image: AssetImage(ImagesApp.imgMap),
                        ),
                      ),
                      child: Center(
                        child: Container(
                          width: 110.w,
                          height: 110.r,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColor.blue.withOpacity(0.3),
                          ),
                          child: Center(
                            child: Container(
                              width: 58.w,
                              height: 58.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColor.blue,
                              ),
                              child: Center(
                                  child: SvgPicture.asset(
                                      ImagesApp.icHomeLocation)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 40.w,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            shape: BoxShape.circle,
                          ),
                          child:
                              Center(child: SvgPicture.asset(ImagesApp.icGame)),
                        ),
                        SizedBox(width: 10.w),
                        Text(
                          "Performance equipment",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const Spacer(),
                        GestureDetector(
                          onTap: () {
                            performanceEquipmentBottomSheet();
                          },
                          child: SvgPicture.asset(
                            ImagesApp.icPen,
                            color: AppColor.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30.h),
                  const DividerWidget(),
                  Padding(
                    padding: EdgeInsets.only(left: 16.w, top: 10.h),
                    child: Text(
                      "Capacity & Genre",
                      style: poppins.copyWith(
                          fontSize: 14.sp, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 16.w, right: 16.w, top: 15.h, bottom: 15.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 40.w,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.icSitting)),
                        ),
                        SizedBox(width: 10.w),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.only(left: 15.w, bottom: 10.h),
                              child: Text(
                                "sits",
                                style: poppins.copyWith(
                                  fontSize: 11.sp,
                                  color: AppColor.greyHint,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.h,
                              width: 250,
                              child: CustomTextField(
                                controller: textCtnSits,
                                fontSize: 14.sp,
                                focusNode: _focusNodeSits,
                              ),
                            )
                          ],
                        ),
                        const Spacer(),
                        GestureDetector(
                          onTap: () {
                            if (_focusNodeSits!.hasFocus == false) {
                              FocusScope.of(context)
                                  .requestFocus(_focusNodeSits);
                            } else {
                              FocusManager.instance.primaryFocus?.unfocus();
                            }
                          },
                          child: SvgPicture.asset(
                            ImagesApp.icPen,
                            color: AppColor.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 16.w, right: 16.w, top: 15.h, bottom: 15.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 40.w,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.icMicropon)),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.only(left: 15.w, bottom: 10.h),
                              child: Text(
                                "stage",
                                style: poppins.copyWith(
                                  fontSize: 11.sp,
                                  color: AppColor.greyHint,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.h,
                              width: 250,
                              child: CustomTextField(
                                controller: textCtnNumberStage,
                                fontSize: 14.sp,
                                focusNode: _focusNodeNumberStage,
                              ),
                            )
                          ],
                        ),
                        const Spacer(),
                        GestureDetector(
                          onTap: () {
                            if (_focusNodeNumberStage!.hasFocus == false) {
                              FocusScope.of(context)
                                  .requestFocus(_focusNodeNumberStage);
                            } else {
                              FocusManager.instance.primaryFocus?.unfocus();
                            }
                          },
                          child: SvgPicture.asset(
                            ImagesApp.icPen,
                            color: AppColor.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 16.w, right: 16.w, top: 15.h, bottom: 15.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 40.w,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.icLimit)),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 20.w, bottom: 5.h),
                              child: Text(
                                "Age limit",
                                style: poppins.copyWith(
                                  fontSize: 11.sp,
                                  color: AppColor.greyHint,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.h,
                              width: 250,
                              child: CustomTextField(
                                controller: textCntAgeLimit,
                                fontSize: 14.sp,
                                focusNode: textFocusNodeAgeLimit,
                              ),
                            )
                          ],
                        ),
                        const Spacer(),
                        GestureDetector(
                          onTap: () {
                            if (textFocusNodeAgeLimit!.hasFocus == false) {
                              FocusScope.of(context)
                                  .requestFocus(textFocusNodeAgeLimit);
                            } else {
                              FocusManager.instance.primaryFocus?.unfocus();
                            }
                          },
                          child: SvgPicture.asset(
                            ImagesApp.icPen,
                            color: AppColor.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 16.w, right: 16.w, top: 15.h, bottom: 15.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 40.w,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.07),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                  child: SvgPicture.asset(ImagesApp.icSaz)),
                            ),
                            SizedBox(width: 10.w),
                            Text(
                              "Your vibe",
                              style: poppins.copyWith(
                                fontSize: 11.sp,
                                color: AppColor.greyHint,
                              ),
                            ),
                            const Spacer(),
                            GestureDetector(
                              onTap: () {
                                selectVibe();
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icPen,
                                color: AppColor.blue,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20.h),
                        ProfileEditVenusPage.lsVibeName.isEmpty
                            ? Wrap(
                                children: state.response.data.vibe
                                    .map(
                                      (text) => Container(
                                        height: 30,
                                        padding: EdgeInsets.only(
                                            top: 5.h, right: 10.w, left: 10.w),
                                        margin: EdgeInsets.only(right: 5.w),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                          border: Border.all(
                                              width: 0.5,
                                              color: AppColor.white
                                                  .withOpacity(0.3)),
                                          color:
                                              AppColor.white.withOpacity(0.07),
                                        ),
                                        child: Text(
                                          text.name,
                                          style: poppins.copyWith(
                                            color: Colors.white,
                                            fontSize: 12.sp,
                                          ),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              )
                            : Wrap(
                                children: ProfileEditVenusPage.lsVibeName
                                    .map(
                                      (text) => Container(
                                        height: 30,
                                        padding: EdgeInsets.only(
                                            top: 5.h, right: 10.w, left: 10.w),
                                        margin: EdgeInsets.only(right: 5.w),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50.r),
                                          border: Border.all(
                                              width: 0.5,
                                              color: AppColor.white
                                                  .withOpacity(0.3)),
                                          color:
                                              AppColor.white.withOpacity(0.07),
                                        ),
                                        child: Text(
                                          text,
                                          style: poppins.copyWith(
                                            color: Colors.white,
                                            fontSize: 12.sp,
                                          ),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30.h),
                  const DividerWidget(),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 16.h, bottom: 24.h, left: 16.w, right: 16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Social",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        SizedBox(height: 8.h),
                        Row(
                          children: [
                            ProfileEditVenusPage.lsLink.isEmpty
                                ? Wrap(
                                    children:
                                        state.response.data.extraData.links
                                            .map(
                                              (link) => Padding(
                                                padding:
                                                    EdgeInsets.only(right: 7.w),
                                                child: Container(
                                                  width: 40.w,
                                                  height: 40.r,
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.white
                                                        .withOpacity(0.07),
                                                  ),
                                                  child: Center(
                                                      child: SvgPicture.asset(
                                                          "assets/svg/${link.type}.svg")),
                                                ),
                                              ),
                                            )
                                            .toList(),
                                  )
                                : Wrap(
                                    children: ProfileEditVenusPage.lsLink
                                        .map(
                                          (link) => Padding(
                                            padding:
                                                EdgeInsets.only(right: 7.w),
                                            child: Container(
                                              width: 40.w,
                                              height: 40.r,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white
                                                    .withOpacity(0.07),
                                              ),
                                              child: Center(
                                                  child: SvgPicture.asset(
                                                      "assets/svg/${link.type}.svg")),
                                            ),
                                          ),
                                        )
                                        .toList(),
                                  ),
                            const Spacer(),
                            GestureDetector(
                              onTap: () {
                                editSocial();
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icPen,
                                color: AppColor.blue,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const DividerWidget(),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 24.h, bottom: 12.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Media Gallery",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              SvgPicture.asset(
                                ImagesApp.icPen,
                                color: AppColor.blue,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 343.w,
                          height: 86.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.r),
                            image: DecorationImage(
                              image: AssetImage(ImagesApp.editProf4),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 6.h, bottom: 24.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 110.w,
                                height: 86.h,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12.r),
                                  image: DecorationImage(
                                    image: AssetImage(ImagesApp.editProf3),
                                  ),
                                ),
                              ),
                              Container(
                                width: 110.w,
                                height: 86.h,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12.r),
                                  image: DecorationImage(
                                    image: AssetImage(ImagesApp.editProf2),
                                  ),
                                ),
                              ),
                              Container(
                                width: 110.w,
                                height: 86.h,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12.r),
                                  image: DecorationImage(
                                    image: AssetImage(ImagesApp.editProf1),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    "+3",
                                    style: poppins.copyWith(
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
          return const SizedBox();
        },
      ),
      bottomNavigationBar: BlocBuilder(
        bloc: getProfileViewModel,
        builder: (ctx, sts) {
          if (sts is GetProfileVenusSuccessState) {
            return ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 15,
                  sigmaY: 15,
                ),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.07),
                      border: BorderDirectional(
                        top: BorderSide(
                          width: 1,
                          color: AppColor.grey1.withOpacity(0.4),
                        ),
                      )),
                  height: 84.h,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 165.w,
                        height: 49.h,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: AppColor.grey1.withOpacity(0.4),
                          ),
                          borderRadius: BorderRadius.circular(50.r),
                        ),
                        child: Center(
                          child: Text(
                            "Save",
                            style: poppins.copyWith(
                              fontSize: 14.sp,
                            ),
                          ),
                        ),
                      ),
                      BlocConsumer(
                          bloc: updateProfileViewModels,
                          listener: (ctx, state) {
                            if (state is UpdateProfileSuccessState) {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const BottomNavVenusPages(index: 4)));
                            }
                          },
                          builder: (context, state) {
                            return MyCustomButton(
                              onTap: () {
                                image != null
                                    ? updateProfileViewModels.updateWithAvatar(
                                        RequestCreateProfileVenus(
                                          role: ProfileEditVenusPage.role,
                                          about: textCtnAbout.text,
                                          links: ProfileEditVenusPage
                                                  .lsLink.isNotEmpty
                                              ? ProfileEditVenusPage.lsLink
                                              : null,
                                          data: DataProfile(
                                            inHouseEquipment: seletEquipment!,
                                            numberOfStages: int.parse(
                                                textCtnNumberStage.text),
                                            venueCapacity:
                                                int.parse(textCtnSits.text),
                                            ageLimit: textCntAgeLimit.text,
                                            additionalDetailedInformation:
                                                ProfileEditVenusPage
                                                    .textCntAdditionalDetail
                                                    .text,
                                            stageName: textCtnNameBusines.text,
                                          ),
                                        ),
                                        ProfileEditVenusPage.lsVibe,
                                        image!,
                                      )
                                    : updateProfileViewModels.update(
                                        RequestCreateProfileVenus(
                                          role: ProfileEditVenusPage.role,
                                          about: textCtnAbout.text,
                                          links: ProfileEditVenusPage
                                                  .lsLink.isNotEmpty
                                              ? ProfileEditVenusPage.lsLink
                                              : null,
                                          data: DataProfile(
                                            inHouseEquipment: seletEquipment!,
                                            numberOfStages: int.parse(
                                                textCtnNumberStage.text),
                                            venueCapacity:
                                                int.parse(textCtnSits.text),
                                            ageLimit: textCntAgeLimit.text,
                                            additionalDetailedInformation:
                                                ProfileEditVenusPage
                                                    .textCntAdditionalDetail
                                                    .text,
                                            stageName: textCtnNameBusines.text,
                                          ),
                                        ),
                                        ProfileEditVenusPage.lsVibe,
                                      );
                              },
                              title: "Publish",
                              fontSize: 14.sp,
                              fontWeight: FontWeight.normal,
                              loading: state is UpdateProfileLoadingState,
                              width: 165.w,
                              height: 49.h,
                            );
                          }),
                    ],
                  ),
                ),
              ),
            );
          }
          return SizedBox();
        },
      ),
    );
  }

  void selectBuisinseType() {
    final selectItem = SelectItemViewModel();
    final List<String> ls = [
      "Rok",
      "Pop",
      "Jazz",
      "RnB",
      "Hip-hop",
      "Blues",
      "country",
      "folksy",
      "Classical orchestra",
      "Indie",
      "Heavy",
      "punk",
      "other",
    ];
    ProfileEditVenusPage.selectType = ls[0];

    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 34.h,
                                  left: 16.w,
                                  right: 16.w,
                                  bottom: 19.h),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.07),
                                    borderRadius: BorderRadius.circular(12.r)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 13.h, top: 16.h),
                                      child: Text(
                                        "Select your performance equipment",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                    const DividerWidget(),
                                    SizedBox(
                                      height: 15.h,
                                    ),
                                    BlocBuilder(
                                        bloc: selectItem,
                                        builder: (context, state) {
                                          if (state is ItemChangeState) {
                                            return ListView.builder(
                                              controller: scrollController,
                                              shrinkWrap: true,
                                              itemCount: ls.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 30.h,
                                                      bottom: 30.h,
                                                      left: 28.w,
                                                      right: 28.w),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      selectItem
                                                          .changeItem(index);
                                                      ProfileEditVenusPage
                                                              .selectType =
                                                          ls[index];
                                                      ProfileEditVenusPage
                                                          .role = index;
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 12.w),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            ls[index],
                                                            style: poppins.copyWith(
                                                                fontSize: 14.sp,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          const Spacer(),
                                                          Container(
                                                            height: 16.6.h,
                                                            width: 16.6.w,
                                                            decoration:
                                                                BoxDecoration(
                                                              color: state.index ==
                                                                      index
                                                                  ? AppColor
                                                                      .green3
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: state.index ==
                                                                          index
                                                                      ? Colors
                                                                          .transparent
                                                                      : AppColor
                                                                          .grey5b),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50),
                                                            ),
                                                            child: Center(
                                                              child: state.index ==
                                                                      index
                                                                  ? Icon(
                                                                      Icons
                                                                          .check,
                                                                      size: 16,
                                                                      color: AppColor
                                                                          .backgroundApp,
                                                                    )
                                                                  : const SizedBox(),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          }
                                          return const SizedBox();
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 84.h,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                ),
                                child: Center(
                                    child: MyCustomButton(
                                  title: "Done",
                                  loading: false,
                                  onTap: () {
                                    Navigator.pop(context);
                                    setState(() {});
                                  },
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void performanceEquipmentBottomSheet() {
    final selectItem = SelectItemViewModel();
    final List<String> ls = [
      "Rok",
      "Pop",
      "Jazz",
      "RnB",
      "Hip-hop",
      "Blues",
      "country",
      "folksy",
      "Classical orchestra",
      "Indie",
      "Heavy",
      "punk",
      "other",
    ];
    seletEquipment = ls[0];
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 34.h,
                                  left: 16.w,
                                  right: 16.w,
                                  bottom: 19.h),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.07),
                                    borderRadius: BorderRadius.circular(12.r)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 13.h, top: 16.h),
                                      child: Text(
                                        "Select your performance equipment",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                    const DividerWidget(),
                                    SizedBox(
                                      height: 15.h,
                                    ),
                                    BlocBuilder(
                                        bloc: selectItem,
                                        builder: (context, state) {
                                          if (state is ItemChangeState) {
                                            return ListView.builder(
                                              controller: scrollController,
                                              shrinkWrap: true,
                                              itemCount: ls.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 30.h,
                                                      bottom: 30.h,
                                                      left: 28.w,
                                                      right: 28.w),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      selectItem
                                                          .changeItem(index);
                                                      seletEquipment =
                                                          ls[index];
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 12.w),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            ls[index],
                                                            style: poppins.copyWith(
                                                                fontSize: 14.sp,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          const Spacer(),
                                                          Container(
                                                            height: 16.6.h,
                                                            width: 16.6.w,
                                                            decoration:
                                                                BoxDecoration(
                                                              color: state.index ==
                                                                      index
                                                                  ? AppColor
                                                                      .green3
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: state.index ==
                                                                          index
                                                                      ? Colors
                                                                          .transparent
                                                                      : AppColor
                                                                          .grey5b),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50),
                                                            ),
                                                            child: Center(
                                                              child: state.index ==
                                                                      index
                                                                  ? Icon(
                                                                      Icons
                                                                          .check,
                                                                      size: 16,
                                                                      color: AppColor
                                                                          .backgroundApp,
                                                                    )
                                                                  : const SizedBox(),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          }
                                          return const SizedBox();
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 84.h,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                ),
                                child: Center(
                                    child: MyCustomButton(
                                  title: "Done",
                                  loading: false,
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void selectVibe() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SVibe(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 84.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                          ),
                          child: Center(
                              child: MyCustomButton(
                            title: "Done",
                            loading: false,
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {});
                            },
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                    setState(() {});
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void editSocial() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      CardSocial(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 84.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                          ),
                          child: Center(
                              child: MyCustomButton(
                            title: "Done",
                            loading: false,
                            onTap: () {
                              if (ProfileEditVenusPage
                                  .textCntWeb.text.isNotEmpty) {
                                ProfileEditVenusPage.lsLink.add(Links(
                                    title: "WebSite",
                                    type: "webSite",
                                    link:
                                        ProfileEditVenusPage.textCntWeb.text));
                              }
                              if (ProfileEditVenusPage
                                  .textCntTiktok.text.isNotEmpty) {
                                ProfileEditVenusPage.lsLink.add(Links(
                                    title: "Tiktok",
                                    type: "tiktok",
                                    link: ProfileEditVenusPage
                                        .textCntTiktok.text));
                              }
                              if (ProfileEditVenusPage
                                  .textCnTwitter.text.isNotEmpty) {
                                ProfileEditVenusPage.lsLink.add(Links(
                                    title: "Twitter",
                                    type: "twitter",
                                    link: ProfileEditVenusPage
                                        .textCnTwitter.text));
                              }
                              if (ProfileEditVenusPage
                                  .textCntYouTub.text.isNotEmpty) {
                                ProfileEditVenusPage.lsLink.add(Links(
                                    title: "YouTub",
                                    type: "youTub",
                                    link: ProfileEditVenusPage
                                        .textCntYouTub.text));
                              }
                              Navigator.pop(context);
                              ProfileEditVenusPage.textCntYouTub.clear();
                              ProfileEditVenusPage.textCnTwitter.clear();
                              ProfileEditVenusPage.textCntTiktok.clear();
                              ProfileEditVenusPage.textCntWeb.clear();

                              setState(() {});
                            },
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                    setState(() {});
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class SVibe extends StatefulWidget {
  SVibe({Key? key}) : super(key: key);

  @override
  State<SVibe> createState() => _SVibeState();
}

class _SVibeState extends State<SVibe> {
  final controllerVibeSearch = TextEditingController();

  bool visible = true;

  SelectItemViewModel changeItemBloc = SelectItemViewModel();

  GetVibeViewModel getVibeViewModel = GetVibeViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getVibeViewModel.getVibe();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(16.r),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(top: 40.h),
                    width: 320.w,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: BlocConsumer(
                      bloc: getVibeViewModel,
                      listener: (context, state) {
                        if (state is GetVibeInitialState) {
                          print(
                              "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<GetVibeInitialState>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                        if (state is GetVibeLoadingState) {
                          print(
                              "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<LODING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                        if (state is GetVibeSuccessState) {
                          print("*************************");
                          print(state.response.data[0].name);
                          print("*************************");
                        }
                        if (state is GetVibeFailState) {
                          print(
                              "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<ERROR>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                          print(state.message);
                        }
                      },
                      builder: (context, sts) {
                        if (sts is GetVibeLoadingState) {
                          return const CustomLoading();
                        }
                        if (sts is GetVibeSuccessState) {
                          return BlocBuilder(
                            bloc: changeItemBloc,
                            builder: (context, state) {
                              if (state is ItemChangeState) {
                                return ListView.builder(
                                  padding: EdgeInsets.zero,
                                  physics: const BouncingScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: sts.response.data.length,
                                  itemBuilder: (context, index) {
                                    int item = sts.response.data[index].id;
                                    String itemName =
                                        sts.response.data[index].name;
                                    return Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ProfileEditVenusPage
                                                  .lsVibe
                                                  .contains(item)
                                              ? 16.w
                                              : 28.w,
                                          vertical: 5),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (ProfileEditVenusPage.lsVibe
                                                .contains(item)) {
                                              ProfileEditVenusPage.lsVibe
                                                  .remove(item);
                                              ProfileEditVenusPage.lsVibeName
                                                  .remove(itemName);
                                            } else {
                                              ProfileEditVenusPage.lsVibe
                                                  .add(item);
                                              ProfileEditVenusPage.lsVibeName
                                                  .add(itemName);
                                            }
                                          });
                                        },
                                        child: Container(
                                          height: 68.h,
                                          // width: 311,
                                          decoration: BoxDecoration(
                                              color: ProfileEditVenusPage.lsVibe
                                                      .contains(item)
                                                  ? AppColor.white
                                                      .withOpacity(0.09)
                                                  : Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(12.r)),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: ProfileEditVenusPage
                                                        .lsVibe
                                                        .contains(item)
                                                    ? 12.w
                                                    : 0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Visibility(
                                                  visible: ProfileEditVenusPage
                                                      .lsVibe
                                                      .contains(item),
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        right:
                                                            ProfileEditVenusPage
                                                                    .lsVibe
                                                                    .contains(
                                                                        item)
                                                                ? 12.w
                                                                : 0),
                                                    child: Container(
                                                      width: 44.w,
                                                      height: 44.r,
                                                      decoration: BoxDecoration(
                                                          color: AppColor.white
                                                              .withOpacity(
                                                                  0.09),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10.r)),
                                                      child: Center(
                                                          child: SvgPicture
                                                              .asset(ImagesApp
                                                                  .icTiktook)),
                                                    ),
                                                  ),
                                                ),
                                                Text(
                                                  sts.response.data[index].name,
                                                  style: poppins.copyWith(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                const Spacer(),
                                                Container(
                                                  height: 16.6.h,
                                                  width: 16.6.w,
                                                  decoration: BoxDecoration(
                                                    color: ProfileEditVenusPage
                                                            .lsVibe
                                                            .contains(item)
                                                        ? AppColor.green3
                                                        : Colors.transparent,
                                                    border: Border.all(
                                                        width: 1,
                                                        color:
                                                            ProfileEditVenusPage
                                                                    .lsVibe
                                                                    .contains(
                                                                        item)
                                                                ? Colors
                                                                    .transparent
                                                                : AppColor
                                                                    .grey5b),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            50),
                                                  ),
                                                  child: Center(
                                                    child: ProfileEditVenusPage
                                                            .lsVibe
                                                            .contains(item)
                                                        ? Icon(
                                                            Icons.check,
                                                            size: 16,
                                                            color: AppColor
                                                                .backgroundApp,
                                                          )
                                                        : const SizedBox(),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }
                              return const SizedBox();
                            },
                          );
                        }
                        return const SizedBox();
                      },
                    )),
                const SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class CardSocial extends StatefulWidget {
  const CardSocial({Key? key}) : super(key: key);

  @override
  State<CardSocial> createState() => _CardSocialState();
}

class _CardSocialState extends State<CardSocial> {
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: 343.w,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                borderRadius: BorderRadius.circular(16.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(left: 16.w, top: 16.h, bottom: 12.h),
                    child: Text(
                      "Contact Information",
                      style: poppins.copyWith(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  SizedBox(height: 22.h),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        visible = !visible;
                      });
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 16.w, right: 12.w),
                          child: Container(
                            width: 44.w,
                            height: 44.r,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.07),
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: SvgPicture.asset(
                                ImagesApp.icSelectVibe,
                                width: 24.w,
                                height: 24.r,
                              ),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Website",
                              style: poppins.copyWith(
                                fontSize: 11.sp,
                                color: AppColor.greyHint,
                              ),
                            ),
                            SizedBox(height: 8.h),
                            Text(
                              "Add your website",
                              style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Text(
                          "Add",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            color: AppColor.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.w),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: visible,
                    child: Padding(
                      padding:
                          EdgeInsets.only(left: 12.w, top: 12.h, right: 12.w),
                      child: Container(
                        width: 311.w,
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        child: CustomTextField(
                          controller: ProfileEditVenusPage.textCntWeb,
                          prefixIcon: ImagesApp.icWeb,
                          hint: "mahady.com",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 12.5.h),
                  Padding(
                    padding: EdgeInsets.only(left: 16.w, right: 12.w),
                    child: Text(
                      "Social media links",
                      style: poppins.copyWith(
                        fontSize: 13.sp,
                        color: AppColor.greyText,
                      ),
                    ),
                  ),
                  SizedBox(height: 8.h),
                  Padding(
                    padding: EdgeInsets.only(left: 12.w, right: 12.w),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: ProfileEditVenusPage.textCntTiktok,
                        prefixIcon: ImagesApp.icTiktok,
                        hint: "Tiktok.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: ProfileEditVenusPage.textCnTwitter,
                        prefixIcon: ImagesApp.icTiwett,
                        hint: "twitter.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 12.w, right: 12.w, bottom: 16.h),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: ProfileEditVenusPage.textCntYouTub,
                        prefixIcon: ImagesApp.icTiktok,
                        hint: "youtube.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
