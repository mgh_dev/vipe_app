import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_validator_text_filde/state/validate_state.dart';
import 'package:vibe_app/constant_bloc/logic_validator_text_filde/view_model/view_model_validate.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/buy_subscription_page/ui/buy_subscription_page.dart';
import 'package:vibe_app/loadind.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_state.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class PaymentMethodPage extends StatelessWidget {
  PaymentMethodPage({Key? key}) : super(key: key);
  SelectItemViewModel changeItemRolesBloc = SelectItemViewModel();
  SelectItemViewModel selectPaymentBloc = SelectItemViewModel();
  ViewModelValidate validateBloc =ViewModelValidate();
  final cartController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>BuySubscriptionPage()));
                          },
                          child: SvgPicture.asset(ImagesApp.icBack)),
                      Text(
                        "Subscription",
                        style: poppins.copyWith(
                            fontSize: 16.sp, fontWeight: FontWeight.w500),
                      ),
                      SvgPicture.asset(ImagesApp.icNotification),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.w, top: 22.h),
                  child: Text(
                    "Payment method",
                    style: poppins.copyWith(
                        fontSize: 24.sp, fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(15.r)),
                  margin: EdgeInsets.only(left: 16.w, right: 16.w, top: 18.h),
                  padding: EdgeInsets.only(top: 24.h, bottom: 26.h),
                  width: double.infinity,
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.w),
                        child: Row(
                          children: [
                            Container(
                              width: 22.w,
                              height: 22.r,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1.w,
                                  color: AppColor.white,
                                ),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: Container(
                                  width: 8.w,
                                  height: 8.r,
                                  decoration: BoxDecoration(
                                    color: AppColor.greyBlue3,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 12.w,
                            ),
                            Text(
                              "Credit or debit card",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w600),
                            ),
                            const Spacer(),
                            SvgPicture.asset(ImagesApp.card),
                          ],
                        ),
                      ),
                      Container(
                        color: AppColor.white.withOpacity(0.07),
                        width: double.infinity,
                        height: 1,
                        margin: EdgeInsets.only(top: 16.h, bottom: 20.h),
                      ),
                      BlocBuilder(
                        bloc: validateBloc,
                        builder: (context,state){
                          if(state is checkValueState){
                            return Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 16.w,
                              ),
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  border: Border.all(width: 0.5.w,color: state.value?AppColor.red:Colors.transparent),
                                  borderRadius: BorderRadius.circular(10.r)),
                              width: double.infinity,
                              height: 48.h,
                              child: CustomTextField(
                                controller: cartController,
                                onChanged: (val){
                                  if(val.length<12){
                                    validateBloc.check(true);
                                  }
                                  else{
                                    validateBloc.check(false);
                                  }
                                },
                                line: 1,
                                suffixIcon: ImagesApp.card,
                                hint: "Card Number",
                              ),
                            );
                          }
                          return SizedBox();
                        },
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Row(
                        children: [
                          Container(

                            margin: EdgeInsets.only(
                              left: 16.w,
                            ),
                            decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10.r)),
                            //width: double.infinity.w,
                            width: 176.w,
                            height: 48.h,
                            child: CustomTextField(
                              controller: TextEditingController(),
                              line: 1,
                              suffixIcon: ImagesApp.calendar,
                              hint: "MM/YY",
                            )
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Container(

                            margin: EdgeInsets.only(
                              right: 16.w,
                            ),
                            decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10.r)),
                            width: 127.w,
                            height: 48.h,
                            child:  CustomTextField(
                              controller: TextEditingController(),
                              line: 1,
                              hint: "CVC",
                            )
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Container(

                        margin: EdgeInsets.symmetric(
                          horizontal: 16.w,
                        ),
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r)),
                        width: double.infinity,
                        height: 48.h,
                        child:  CustomTextField(
                          controller: TextEditingController(),
                          line: 1,

                          hint: "Zip Code",
                        )
                      ),
                      SizedBox(
                        height: 30.h,
                      ),
                      BlocBuilder(
                          bloc: selectPaymentBloc,
                          builder: (context, state) {
                            if (state is ItemChangeState) {
                              return Column(
                                children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16.w),
                                    child: GestureDetector(
                                      onTap: () =>
                                          selectPaymentBloc.changeItem(0),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 22.w,
                                            height: 22.r,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1.w,
                                                color: state.index == 0
                                                    ? AppColor.greyBlue3
                                                    : AppColor.grey5b,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 14.w,
                                                height: 14.r,
                                                decoration: BoxDecoration(
                                                  color: state.index == 0
                                                      ? AppColor.greyBlue3
                                                      : Colors.transparent,
                                                  shape: BoxShape.circle,
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                  text:
                                                      "Buy now and pay later\nwith PayPal.",
                                                  style: poppins.copyWith(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                TextSpan(
                                                  text: " Learn more",
                                                  style: inter.copyWith(
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      foreground: Paint()
                                                        ..shader =
                                                            LinearGradient(
                                                          colors: <Color>[
                                                            AppColor
                                                                .gradientBlue1,
                                                            AppColor
                                                                .gradientBlue2
                                                          ],
                                                        ).createShader(const Rect
                                                                    .fromLTWH(
                                                                120.0,
                                                                0.0,
                                                                200.0,
                                                                0.0))),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const Spacer(),
                                          SvgPicture.asset(ImagesApp.paypal),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 20.h),
                                    child: const DividerWidget(),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16.w),
                                    child: GestureDetector(
                                      onTap: () =>
                                          selectPaymentBloc.changeItem(1),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 22.w,
                                            height: 22.r,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1.w,
                                                color: state.index == 1
                                                    ? AppColor.greyBlue3
                                                    : AppColor.grey5b,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 14.w,
                                                height: 14.r,
                                                decoration: BoxDecoration(
                                                  color: state.index == 1
                                                      ? AppColor.greyBlue3
                                                      : Colors.transparent,
                                                  shape: BoxShape.circle,
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Text(
                                            "Apple Pay",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          const Spacer(),
                                          SvgPicture.asset(ImagesApp.appLepay),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }
                            return const SizedBox();
                          })
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 16.w, left: 16.w, top: 11.h),
                  child: BlocBuilder(
                    bloc: changeItemRolesBloc,
                    builder: (context, state) {
                      if (state is ItemChangeState) {
                        return GestureDetector(
                          onTap: () {
                            if (state.index == 1) {
                              changeItemRolesBloc.changeItem(0);
                            } else {
                              changeItemRolesBloc.changeItem(1);
                            }
                          },
                          child: Row(
                            children: [
                              Container(
                                width: 22.w,
                                height: 22.r,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: state.index == 0
                                          ? AppColor.greyText
                                          : Colors.transparent,
                                    ),
                                    borderRadius: BorderRadius.circular(2.5.r),
                                    color: state.index == 1
                                        ? AppColor.greyBlue
                                        : Colors.transparent),
                                child: state.index == 1
                                    ? Center(
                                        child: Icon(
                                          Icons.check,
                                          color: AppColor.backgroundApp,
                                          size: 18,
                                        ),
                                      )
                                    : null,
                              ),
                              SizedBox(
                                width: 12.w,
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "I accept the",
                                      style: inter.copyWith(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.white),
                                    ),
                                    TextSpan(
                                      text:
                                          " Vibe’s Terms of Service, Community\nGuidelines ",
                                      style: inter.copyWith(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          foreground: Paint()
                                            ..shader = LinearGradient(
                                              colors: <Color>[
                                                AppColor.gradientBlue1,
                                                AppColor.gradientBlue2
                                              ],
                                            ).createShader(const Rect.fromLTWH(
                                                120.0, 0.0, 200.0, 0.0))),
                                    ),
                                    TextSpan(
                                      text: " & ",
                                      style: inter.copyWith(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.white),
                                    ),
                                    TextSpan(
                                      text: "Privacy Policy. ",
                                      style: inter.copyWith(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          foreground: Paint()
                                            ..shader = LinearGradient(
                                              colors: <Color>[
                                                AppColor.gradientBlue1,
                                                AppColor.gradientBlue2
                                              ],
                                            ).createShader(const Rect.fromLTWH(
                                                120.0, 0.0, 200.0, 0.0))),
                                    ),
                                    TextSpan(
                                      text: "(Required)",
                                      style: inter.copyWith(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.white),
                                    ),
                                  ],
                                ),
                              ),

                              /*Text("I accept the Vibe’s Terms of Service, Community\nGuidelines & Privacy Policy. (Required)",
                          style: inter.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              foreground: Paint()..shader = LinearGradient(
                                colors: <Color>[AppColor.gradientblue1, AppColor.gradientblue2],
                              ).createShader(Rect.fromLTWH(120.0, 0.0, 200.0, 0.0))
                          ),
                      ),*/
                            ],
                          ),
                        );
                      }
                      return const SizedBox();
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 15,
            sigmaY: 15,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                border: BorderDirectional(
                  top: BorderSide(
                    width: 1,
                    color: AppColor.grey1.withOpacity(0.4),
                  ),
                )),
            height: 162.h,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 17.5.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImagesApp.icCart),
                      Text(
                        "99.00",
                        style: poppins.copyWith(
                          fontSize: 18.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 29.h),
                MyCustomButton(
                  onTap: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => Loading()));
                  },
                  title: "Subscribe",
                  fontSize: 14.sp,
                  fontWeight: FontWeight.normal,
                  loading: false,
                  color: AppColor.green,
                  width: 343.w,
                  height: 49.h,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
