import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class SubscriptionPurchaseAdvertisementPage extends StatelessWidget {
  SubscriptionPurchaseAdvertisementPage({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
            top: -100.h,
              left: -100.w,
              child: ContainerBlur(color: AppColor.purple,height: 100.h,)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(
                alignment: Alignment.topRight,
                children: [
                  Container(
                    height: 258.h,
                    width: double.infinity,
                    child: Image.asset(
                      fit: BoxFit.cover,
                      ImagesApp.unlockTheFull,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 22.w,top: 40.h),
                    padding: EdgeInsets.all(12.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.r),
                      color:AppColor.greyBtnClose
                    ),
                    child:Icon(Icons.close,color: AppColor.white,size: 20,),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 28.w),
                child: Text(
                  "Unlock the full potential of\n Vibe by subscribing to one \nof our plans today!",
                  style: poppins.copyWith(fontSize: 24.sp),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 26.h, bottom: 24.h),
                color: AppColor.white.withOpacity(0.14),
                height: 0.5,
              ),
              Expanded(child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.w),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: AppColor.blue,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Text(
                              "Discover and book eclectic talent.",
                              style: poppins.copyWith(
                                  color: AppColor.greyCE, fontSize: 14.sp),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: AppColor.blue,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Text(
                              "Create immerse events.",
                              style: poppins.copyWith(
                                  color: AppColor.greyCE, fontSize: 14.sp),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: AppColor.blue,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Text(
                              "Send and receive gig requests and\n messages.",
                              style: poppins.copyWith(
                                  color: AppColor.greyCE, fontSize: 14.sp),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: AppColor.blue,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Text(
                              "Use our in-app “manage calendar”\n feature.",
                              style: poppins.copyWith(
                                  color: AppColor.greyCE, fontSize: 14.sp),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: AppColor.blue,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Text(
                              "Real-time communication to discuss\n booking details.",
                              style: poppins.copyWith(
                                  color: AppColor.greyCE, fontSize: 14.sp),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 24.w, left: 24.w, top: 8.h),
                    child: MyCustomButton(
                      title: "Start 1 month free trial",
                      loading: false,
                      color: AppColor.green1,
                      width: 327.w,
                      height: 49.h,
                      fontSize: 14.sp,
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(right: 24.w, left: 24.w, bottom: 56.h, top: 8.h),
                    child: GestureDetector(
                      child: Container(
                        height: 49.h,
                        width: 327.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50.r),
                            border: Border.all(
                                width: 0.5,
                                color: AppColor.white.withOpacity(0.14))),
                        child: Center(
                          child: Text("No thanks",
                              style: poppins.copyWith(
                                  color: AppColor.green1, fontSize: 14.sp)),
                        ),
                      ),
                    ),
                  ) //mmd
                ]),
              ))

            ],
          ),
        ],
      ),
    );
  }
}
