import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

class TabIncomingRequestVenus extends StatefulWidget {
  const TabIncomingRequestVenus({Key? key}) : super(key: key);

  @override
  State<TabIncomingRequestVenus> createState() => _TabIncomingRequestVenusState();
}

class _TabIncomingRequestVenusState extends State<TabIncomingRequestVenus> {
  @override
  Widget build(BuildContext context) {
    return  ListView.builder(
      itemCount: 4,
      physics: const BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        return Padding(
          padding:
          EdgeInsets.only(left: 22.w, right: 22.w, bottom: 8.h),
          child: GestureDetector(
            onTap: () {
              showDetailsGigBottomSheet();
            },
            child: Container(
              width: double.infinity,
              height: 180.h,
              decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.07),
                  borderRadius: BorderRadius.circular(12.r)),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 22.w,
                              top: 17.5.h,
                              bottom: 14.5.h),
                          child: Row(
                            children: [
                              SvgPicture.asset(ImagesApp.icClock),
                              SizedBox(
                                width: 8.w,
                              ),
                              Text(
                                "24 Aug",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          width: double.infinity,
                          color: AppColor.white.withOpacity(0.07),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 46.6.w,
                              top: 15.5.h,
                              bottom: 14.5.h),
                          child: Row(
                            children: [
                              Image.asset(
                                ImagesApp.profileRequest,
                                width: 48.w,
                                height: 48.r,
                              ),
                              SizedBox(
                                width: 16.w,
                              ),
                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Audrey Soul",
                                    style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight:
                                        FontWeight.w500),
                                  ),
                                  Text(
                                    "\$599",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: AppColor.grey2),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          width: double.infinity,
                          color: AppColor.white.withOpacity(0.20),
                        ),
                        SizedBox(
                          height: 46.h,
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const ChatRoomPage()));
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 12.h, bottom: 13.h),
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        border: Border(
                                            right: BorderSide(
                                                color: AppColor
                                                    .grey5b))),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                            ImagesApp.icMessage),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        Text(
                                          "Message",
                                          style: poppins.copyWith(
                                              fontSize: 14.sp,
                                              fontWeight:
                                              FontWeight.w500,
                                              color:
                                              AppColor.whiteF8),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 12.h, bottom: 13.h),
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                            ImagesApp.check),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        Text(
                                          "Decline",
                                          style: poppins.copyWith(
                                              fontSize: 14.sp,
                                              fontWeight:
                                              FontWeight.w500,
                                              color: AppColor
                                                  .blueText2),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void showDetailsGigBottomSheet() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(left: 16.w),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "Cancel",
                                    style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w400,
                                        color: AppColor.blue),
                                  )),
                            ),
                          ),
                          Expanded(
                              child: Text(
                                "Request Details",
                                style: poppins.copyWith(
                                    fontSize: 16.sp, fontWeight: FontWeight.w500),
                              )),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: AppColor.white.withOpacity(0.07),
                    ),
                    Container(
                      width: double.infinity,
                      margin:
                      EdgeInsets.only(top: 20.h, right: 14.w, left: 18.w),
                      padding: EdgeInsets.only(
                          right: 10.w, left: 12.w, top: 12.h, bottom: 12.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.r),
                        color: AppColor.white.withOpacity(0.07),
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 4.w),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Open Mic Night",
                                        style: poppins.copyWith(
                                            fontSize: 20.sp,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 4.h,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Thursday ",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.grey81),
                                          ),
                                          Text(
                                            "at 7:30pm",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.blue),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 12.5.w, vertical: 6.h),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(14.r),
                                      color: AppColor.black12,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "24",
                                          style: poppins.copyWith(
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.w600,
                                              color: AppColor.white),
                                        ),
                                        Text(
                                          "Dec",
                                          style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500,
                                              color: AppColor.grey2),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(4.r),
                              margin: EdgeInsets.only(top: 8.h, bottom: 16.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.r),
                                color: AppColor.blackBack,
                              ),
                              width: double.infinity,
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(8.r),
                                    child: Image.asset(
                                      width: 52.w,
                                      height: 56.8.h,
                                      ImagesApp.backHouseofmusic,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8.w,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            "House of Music",
                                            style: poppins.copyWith(
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            width: 6.w,
                                          ),
                                          Icon(
                                            Icons.check_circle,
                                            color: AppColor.greyBlue2,
                                            size: 16.r,
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 4.h,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(4.5.r),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(4.r),
                                              color: AppColor.white
                                                  .withOpacity(0.09),
                                            ),
                                            child: Container(
                                                height: 7.h,
                                                width: 7.w,
                                                child: SvgPicture.asset(
                                                  ImagesApp.icLocation,
                                                  color: AppColor.white,
                                                  fit: BoxFit.cover,
                                                )),
                                          ),
                                          SizedBox(
                                            width: 6.w,
                                          ),
                                          Text(
                                            "273-296 Geary St, Richmond, VA 09584",
                                            style: inter.copyWith(
                                                fontSize: 10.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.greyTxt),
                                          )
                                        ],
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Performance length:",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                                Text(
                                  "1.5 hrs",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.greenText),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 12.h, bottom: 8.h),
                              color: AppColor.white.withOpacity(0.07),
                              height: 1,
                              width: double.infinity,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Guarantee amount:",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                                Text(
                                  " \$300",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.greenText),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 12.h, bottom: 8.h),
                              color: AppColor.white.withOpacity(0.07),
                              height: 1,
                              width: double.infinity,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Food and beverage included: ",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                                Text(
                                  "Yes",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.greenText),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 8.h),
                              color: AppColor.white.withOpacity(0.07),
                              height: 1,
                              width: double.infinity,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 11.w, vertical: 12.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                color: AppColor.white.withOpacity(0.07),
                              ),
                              width: double.infinity,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Response time for this request: ",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Text(
                                    "48 hrs",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: AppColor.blueText),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 14.h, bottom: 6.h),
                              child: Text(
                                "Message",
                                style: poppins.copyWith(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                    color: AppColor.grey7E),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10.w, vertical: 8.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                color: AppColor.white.withOpacity(0.07),
                              ),
                              width: double.infinity,
                              child: Text(
                                "We are interested in having you performing at our venue. I believe that your music would be a perfect fit for our audience. Are you interested?",
                                style: poppins.copyWith(
                                    fontSize: 11.sp,
                                    fontWeight: FontWeight.w400),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ]),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 21.h, bottom: 25.h),
                      child: Container(
                        width: 343.w,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.r),
                          color: AppColor.white.withOpacity(0.07),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 18.h,
                                  bottom: 18.h,
                                  left: 16.w,
                                  right: 16.w),
                              child: MyCustomButton(
                                title: "Accept",
                                onTap: () {
                                  Navigator.pop(context);
                                  showMessageAccept();
                                },
                                loading: false,
                                color: AppColor.green3,
                                width: double.infinity,
                                height: 50.h,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            const DividerWidget(),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 18.h,
                                  bottom: 18.h,
                                  left: 16.w,
                                  right: 16.w),
                              child: MyCustomButton(
                                title: "Message",
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                          const ChatRoomPage()));
                                },
                                loading: false,
                                color: AppColor.black,
                                width: double.infinity,
                                borderColor: Colors.white,
                                height: 50.h,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Text(
                      "Decline",
                      style: poppins.copyWith(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w500,
                          color: AppColor.red),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void showMessageAccept() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.h),
                ///sumryPhoto
                child: Image(image: AssetImage(ImagesApp.p1)),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 16.w),
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        "Cancel",
                                        style: poppins.copyWith(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.blue),
                                      )),
                                ),
                              ),
                              Expanded(
                                  child: Text(
                                    "Request Details",
                                    style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w500),
                                  )),
                              Expanded(
                                child: Container(),
                              ),
                            ],
                          ),
                        ),
                        const DividerWidget(),
                      ],
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Well done!\nYou’ve accepted the gig \nrequest!",
                      style: poppins.copyWith(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold,
                        color: AppColor.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "We look forward to hearing all about your\nexperience at the venue.",
                      style: poppins.copyWith(
                        fontSize: 14.sp,
                        // fontWeight: FontWeight.bold,
                        color: AppColor.grey2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 76.h),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 18.h, bottom: 8.h, left: 16.w, right: 16.w),
                      child: MyCustomButton(
                        title: "Back to Home",
                        onTap: () {
                          Navigator.pop(context);
                          // Navigator.pushReplacement(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             BottomNavArtistVenusPages(index: 0)));
                        },
                        loading: false,
                        color: AppColor.blue,
                        width: double.infinity,
                        height: 50.h,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(top: 18.h, left: 16.w, right: 16.w),
                      child: MyCustomButton(
                        title: "Share With Friends",
                        onTap: () {},
                        loading: false,
                        color: AppColor.white,
                        textColor: AppColor.black,
                        width: double.infinity,
                        height: 50.h,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
