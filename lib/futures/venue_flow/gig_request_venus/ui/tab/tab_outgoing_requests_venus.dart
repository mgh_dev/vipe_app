import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

class TabOutgoingRequestsVenus extends StatefulWidget {
  const TabOutgoingRequestsVenus({Key? key}) : super(key: key);

  @override
  State<TabOutgoingRequestsVenus> createState() => _TabOutgoingRequestsVenusState();
}

class _TabOutgoingRequestsVenusState extends State<TabOutgoingRequestsVenus> {
  @override
  Widget build(BuildContext context) {
    return  ListView.builder(
      itemCount:3,
      itemBuilder: (context, index) {
        return Container(
          decoration: BoxDecoration(
              color: AppColor.white.withOpacity(0.09),
              borderRadius: BorderRadius.circular(8.r)),
          margin:
          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
          padding:
          EdgeInsets.symmetric(horizontal: 8.w),
          child: Stack(
            children: [

              Padding(
                padding:  EdgeInsets.only(top: 15.h,bottom: 15.h),
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.r),
                      child: Image.asset(
                        ImagesApp.showsHouseofmusic,
                        width: 52.w,
                        height: 56.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Rock & Roll",
                          style: poppins.copyWith(
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          "\$350",
                          style: inter.copyWith(
                              fontSize: 10.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.greyTxt),
                        ),
                      ],
                    ),
                    const Spacer(),
                    Container(
                      padding: EdgeInsets.only(
                          left: 13.w,
                          right: 13.w,
                          top: 4.h,
                          bottom: 17.h),
                      decoration: BoxDecoration(
                          color: AppColor.black12,
                          borderRadius: BorderRadius.circular(8.r)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "03",
                            style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "Aug",
                            style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.grey81),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(top: 5.h),
                  width: 53.w,
                  height: 20.h,
                  decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.09),
                      borderRadius: BorderRadius.circular(6.r)),
                  child:  Center(
                    child: Text(
                      "Gig request",
                      style: inter.copyWith(
                          fontSize: 9.sp,
                          fontWeight: FontWeight.w500,
                          color: AppColor.greyTxt),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

}
