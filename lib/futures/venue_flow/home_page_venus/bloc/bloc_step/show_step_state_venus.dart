abstract class ShowStepBaseVenus {}

class StatusStep extends ShowStepBaseVenus {
  final int index;

  StatusStep({required this.index});
}
