import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_step/show_step_state_venus.dart';

class ShowStepViewModelVenus extends Cubit<ShowStepBaseVenus> {
  ShowStepViewModelVenus() : super(StatusStep(index: 0));

  void changeIndex(int index) {
    emit(StatusStep(index: index));
  }
}
