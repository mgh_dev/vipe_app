import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/fan_flow/home_page/bloc/bloc_tabs/tabs_state.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_tabs/tabs_state_venus.dart';

class TabsViewModelVenus extends Cubit<TabsBaseVenus> {
  TabsViewModelVenus() : super(TabsIndexStateVenus(index: 0));

  void changeIndex(int index) {
    emit(TabsIndexStateVenus(index: index));
  }
}
