abstract class TabsBaseVenus {}

class TabsIndexStateVenus extends TabsBaseVenus {
  final int index;

  TabsIndexStateVenus({required this.index});
}
