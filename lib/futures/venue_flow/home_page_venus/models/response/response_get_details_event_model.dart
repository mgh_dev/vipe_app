class ResponseGetDetailsEventModel {
  ResponseGetDetailsEventModel({
    required this.message,
    required this.data,
  });
  late final String message;
  late final Data data;

  ResponseGetDetailsEventModel.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.status,
    required this.image,
    required this.ageLimit,
    required this.offerFoodAndBeverage,
    required this.ticketLink,
    required this.ticketPrice,
    required this.respondExpireTime,
    required this.asGigPost,
    required this.amount,
    required this.additionalInformation,
    required this.calendar,
    required this.address,
    required this.user,
    required this.gigRequest,
    required this.vibe,
  });
  late final int id;
  late final String status;
  late final String image;
  late final String ageLimit;
  late final int offerFoodAndBeverage;
  late final String ticketLink;
  late final String ticketPrice;
  late final int respondExpireTime;
  late final int asGigPost;
  late final int amount;
  late final String additionalInformation;
  late final Calendar calendar;
  late final Address address;
  late final User user;
  late final List<GigRequest> gigRequest;
  late final List<Vibe> vibe;

  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    status = json['status'];
    image = json['image'];
    ageLimit = json['ageLimit'];
    offerFoodAndBeverage = json['offerFoodAndBeverage'];
    ticketLink = json['ticketLink'];
    ticketPrice = json['ticketPrice'];
    respondExpireTime = json['respondExpireTime'];
    asGigPost = json['asGigPost'];
    amount = json['amount'];
    additionalInformation = json['additionalInformation'];
    calendar = Calendar.fromJson(json['calendar']);
    address = Address.fromJson(json['address']);
    user = User.fromJson(json['user']);
    gigRequest = List.from(json['gigRequest']).map((e)=>GigRequest.fromJson(e)).toList();
    vibe = List.from(json['vibe']).map((e)=>Vibe.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['status'] = status;
    _data['image'] = image;
    _data['ageLimit'] = ageLimit;
    _data['offerFoodAndBeverage'] = offerFoodAndBeverage;
    _data['ticketLink'] = ticketLink;
    _data['ticketPrice'] = ticketPrice;
    _data['respondExpireTime'] = respondExpireTime;
    _data['asGigPost'] = asGigPost;
    _data['amount'] = amount;
    _data['additionalInformation'] = additionalInformation;
    _data['calendar'] = calendar.toJson();
    _data['address'] = address.toJson();
    _data['user'] = user.toJson();
    _data['gigRequest'] = gigRequest.map((e)=>e.toJson()).toList();
    _data['vibe'] = vibe.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class Calendar {
  Calendar({
    required this.id,
    required this.status,
    required this.date,
    required this.startTime,
    required this.endTime,
  });
  late final int id;
  late final String status;
  late final String date;
  late final String startTime;
  late final String endTime;

  Calendar.fromJson(Map<String, dynamic> json){
    id = json['id'];
    status = json['status'];
    date = json['date'];
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['status'] = status;
    _data['date'] = date;
    _data['startTime'] = startTime;
    _data['endTime'] = endTime;
    return _data;
  }
}

class Address {
  Address({
    required this.id,
    required this.address,
    required this.longitude,
    required this.latitude,
    required this.city,
    required this.state,
    required this.country,
    this.postalCode,
  });
  late final int id;
  late final String address;
  late final double longitude;
  late final double latitude;
  late final String city;
  late final String state;
  late final String country;
  late final Null postalCode;

  Address.fromJson(Map<String, dynamic> json){
    id = json['id'];
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    postalCode = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['address'] = address;
    _data['longitude'] = longitude;
    _data['latitude'] = latitude;
    _data['city'] = city;
    _data['state'] = state;
    _data['country'] = country;
    _data['postalCode'] = postalCode;
    return _data;
  }
}

class User {
  User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.type,
    required this.email,
    required this.avatar,
  });
  late final int id;
  late final String firstName;
  late final String lastName;
  late final String type;
  late final String email;
   String? avatar;

  User.fromJson(Map<String, dynamic> json){
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    type = json['type'];
    email = json['email'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['type'] = type;
    _data['email'] = email;
    _data['avatar'] = avatar;
    return _data;
  }
}

class GigRequest {
  GigRequest({
    required this.id,
    required this.status,
    required this.expireDate,
    required this.additionalInformation,
    required this.amount,
    required this.user,
  });
  late final int id;
  late final String status;
  late final String expireDate;
  late final String additionalInformation;
  late final int amount;
  late final User user;

  GigRequest.fromJson(Map<String, dynamic> json){
    id = json['id'];
    status = json['status'];
    expireDate = json['expireDate'];
    additionalInformation = json['additionalInformation'];
    amount = json['amount'];
    user = User.fromJson(json['user']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['status'] = status;
    _data['expireDate'] = expireDate;
    _data['additionalInformation'] = additionalInformation;
    _data['amount'] = amount;
    _data['user'] = user.toJson();
    return _data;
  }
}

class Vibe {
  Vibe({
    required this.id,
    required this.name,
    required this.description,
    required this.image,
    this.icon,
  });
  late final int id;
  late final String name;
  late final String description;
  late final String image;
  late final Null icon;

  Vibe.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    icon = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['image'] = image;
    _data['icon'] = icon;
    return _data;
  }
}