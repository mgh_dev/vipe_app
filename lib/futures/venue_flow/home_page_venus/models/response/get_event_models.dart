class ResponseGetEventModel {
  ResponseGetEventModel({
    required this.data,
    required this.meta,
  });
  late final List<Data> data;
  late final Meta meta;

  ResponseGetEventModel.fromJson(Map<String, dynamic> json){
    data = List.from(json['data']).map((e)=>Data.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.map((e)=>e.toJson()).toList();
    _data['meta'] = meta.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.title,
    required this.status,
    required this.image,
    required this.ageLimit,
    required this.offerFoodAndBeverage,
    required this.ticketLink,
    required this.ticketPrice,
    required this.respondExpireTime,
    required this.asGigPost,
    required this.amount,
    required this.additionalInformation,
  });
  late final int id;
  late final String title;
  late final String status;
  late final String image;
  late final String ageLimit;
  late final int offerFoodAndBeverage;
  late final String ticketLink;
  late final String ticketPrice;
  late final int respondExpireTime;
  late final int asGigPost;
  late final int amount;
  late final String additionalInformation;

  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    title = json['title'];
    status = json['status'];
    image = json['image'];
    ageLimit = json['ageLimit'];
    offerFoodAndBeverage = json['offerFoodAndBeverage'];
    ticketLink = json['ticketLink'];
    ticketPrice = json['ticketPrice'];
    respondExpireTime = json['respondExpireTime'];
    asGigPost = json['asGigPost'];
    amount = json['amount'];
    additionalInformation = json['additionalInformation'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['title'] = title;
    _data['status'] = status;
    _data['image'] = image;
    _data['ageLimit'] = ageLimit;
    _data['offerFoodAndBeverage'] = offerFoodAndBeverage;
    _data['ticketLink'] = ticketLink;
    _data['ticketPrice'] = ticketPrice;
    _data['respondExpireTime'] = respondExpireTime;
    _data['asGigPost'] = asGigPost;
    _data['amount'] = amount;
    _data['additionalInformation'] = additionalInformation;
    return _data;
  }
}

class Meta {
  Meta({
    required this.pagination,
  });
  late final Pagination pagination;

  Meta.fromJson(Map<String, dynamic> json){
    pagination = Pagination.fromJson(json['pagination']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pagination'] = pagination.toJson();
    return _data;
  }
}

class Pagination {
  Pagination({
    required this.total,
    required this.count,
    required this.perPage,
    required this.currentPage,
    required this.totalPages,
    required this.links,
  });
  late final int total;
  late final int count;
  late final int perPage;
  late final int currentPage;
  late final int totalPages;
  late final Links links;

  Pagination.fromJson(Map<String, dynamic> json){
    total = json['total'];
    count = json['count'];
    perPage = json['perPage'];
    currentPage = json['currentPage'];
    totalPages = json['totalPages'];
    links = Links.fromJson(json['links']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total'] = total;
    _data['count'] = count;
    _data['perPage'] = perPage;
    _data['currentPage'] = currentPage;
    _data['totalPages'] = totalPages;
    _data['links'] = links.toJson();
    return _data;
  }
}

class Links {
  Links();

  Links.fromJson(Map json);

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    return _data;
  }
}