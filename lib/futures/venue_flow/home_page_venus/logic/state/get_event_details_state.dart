import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/response_model/get_address_map_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/get_event_models.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/response_get_details_event_model.dart';

abstract class GetEventDetailsBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetEventDetailsInitialState extends GetEventDetailsBaseState {}

class GetEventDetailsLoadingState extends GetEventDetailsBaseState {}

class GetEventDetailsSuccessState extends GetEventDetailsBaseState {
  final ResponseGetDetailsEventModel getDetailsEventModel;

  GetEventDetailsSuccessState({required this.getDetailsEventModel});
}

class GetEventDetailsFailState extends GetEventDetailsBaseState {
  final String message;


  GetEventDetailsFailState({required this.message});
}
