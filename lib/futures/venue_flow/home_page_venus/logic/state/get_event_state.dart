import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/response_model/get_address_map_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/get_event_models.dart';

abstract class GetEventBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetEventInitialState extends GetEventBaseState {}

class GetEventLoadingState extends GetEventBaseState {}

class GetEventSuccessState extends GetEventBaseState {
  final ResponseGetEventModel getEventModel;

  GetEventSuccessState({required this.getEventModel});
}

class GetEventFailState extends GetEventBaseState {
  final String message;


  GetEventFailState({required this.message});
}
