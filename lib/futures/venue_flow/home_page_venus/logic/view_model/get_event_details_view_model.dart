

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/logic/state/get_event_details_state.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/get_event_models.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/response_get_details_event_model.dart';

import '../../../../../core/config/web_service.dart';
import '../../../create_event_details_venue_page/logic/state/get_address_state.dart';
import '../state/get_event_state.dart';

class GetEventDetailsViewModel extends Cubit<GetEventDetailsBaseState> {
  GetEventDetailsViewModel() : super(GetEventDetailsInitialState());

  Future getEvent(int eventId) async {
    print("1");
    emit(GetEventDetailsLoadingState());
    print("3");
    try {

      print("4");
      var response =
      await WebService().dio.get("events/$eventId");

      print("5");
      ResponseGetDetailsEventModel getDetailsEventModel=ResponseGetDetailsEventModel.fromJson(response.data);
      print("6");

      print("7");
      emit(GetEventDetailsSuccessState(getDetailsEventModel:getDetailsEventModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetEventDetailsFailState(message: e.toString()));
      print("10");
    }
  }
}

