

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/models/response/get_event_models.dart';

import '../../../../../core/config/web_service.dart';
import '../../../create_event_details_venue_page/logic/state/get_address_state.dart';
import '../state/get_event_state.dart';

class GetEventViewModel extends Cubit<GetEventBaseState> {
  GetEventViewModel() : super(GetEventInitialState());

  Future getEvent() async {
    print("1");
    emit(GetEventLoadingState());
    print("3");
    try {

      print("4");
      var response =
      await WebService().dio.get("events?page=1");
      ResponseGetEventModel getEventModel=ResponseGetEventModel.fromJson(response.data);
      print("5");
      print("6");

      print("7");
      emit(GetEventSuccessState(getEventModel:getEventModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetEventFailState(message: e.toString()));
      print("10");
    }
  }
}

