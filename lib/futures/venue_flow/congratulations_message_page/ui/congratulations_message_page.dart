import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class CongratulationsMessagePage extends StatelessWidget {
  CongratulationsMessagePage({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              Padding(
              padding: EdgeInsets.only(
                  bottom: 20.h, right: 62.w, left: 62.w,top: 80.h),
              child: Image.asset(
                ImagesApp.congratulations,
                width: 250.w,
                height: 250.h,
              ),
              ),
              Text(
              "Congratulations!",
              style: poppins.copyWith(
                  fontSize: 24.sp,
                  fontWeight: FontWeight.w600,
                  color: AppColor.green3),
              ),
              Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 21.w, vertical: 12.h),
              child: Text(
                "Your monthly subscription plan \nhas been confirmed. You are now able to \nbook artists, promote your venue & \nelevate your local live music ecosystem.",
                textAlign: TextAlign.center,
                style: poppins.copyWith(
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500,
                    color: AppColor.white),
              ),
              ),
              Text(
              "Oh, let’s vibe together!",
              style: poppins.copyWith(
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w400,
                  color: AppColor.grey73),
              ),
              Spacer(),
              Padding(
              padding: EdgeInsets.only(bottom: 27.h),
              child: MyCustomButton(
                title: "Back To Home",
                loading: false,
                color: AppColor.blue,
                width: 343.w,
                height: 49.h,
                fontSize: 14.sp,
              ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
