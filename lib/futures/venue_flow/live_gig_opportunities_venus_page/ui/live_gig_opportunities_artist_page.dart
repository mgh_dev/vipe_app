import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/artist_flow/gig_request_artist/ui/tab/tab_incoming_equest_artist.dart';
import 'package:vibe_app/futures/artist_flow/gig_request_artist/ui/tab/tab_outgoing_requests_artist.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';
import 'package:vibe_app/futures/venue_flow/payment_method_page/ui/payment_method_page.dart';

class LiveGigOpportunitiesArtistPage extends StatefulWidget {
  const LiveGigOpportunitiesArtistPage({Key? key}) : super(key: key);

  @override
  State<LiveGigOpportunitiesArtistPage> createState() =>
      _LiveGigOpportunitiesArtistPageState();
}

class _LiveGigOpportunitiesArtistPageState
    extends State<LiveGigOpportunitiesArtistPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 53.h, bottom: 25.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Live gig opportunities",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    GestureDetector(
                        onTap: () {},
                        child: SvgPicture.asset(ImagesApp.icNotification)),
                  ],
                ),
              ),
              Text(
                "Review open gig posts  and submit your interest.",
                style: poppins.copyWith(
                  fontSize: 12.sp,
                  color: AppColor.greyHint,
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return Container(
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.09),
                          borderRadius: BorderRadius.circular(8.r)),
                      margin:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      padding: EdgeInsets.only(left: 8.w,right: 8.w,top: 7.h),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.r),
                            child: Image.asset(
                              ImagesApp.showsHouseofmusic,
                              width: 52.w,
                              height: 56.h,
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Open Mic Night",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 5.h),
                                child: Text(
                                  "Guarantee: \$700",
                                  style: inter.copyWith(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    color: AppColor.greenText2,
                                  ),
                                ),
                              ),
                              Text(
                                "Type of artist needed: Jazz Band",
                                style: inter.copyWith(
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.white,
                                ),
                              ),
                              SizedBox(
                                height:9.h,
                              ),
                              GenerateChipe(
                                texts: const ["Nostalgic", "Bluesy", "Jazzy"],
                                h: 20.h,
                                borderRadius: 4.r,
                                borderColor: Colors.white,
                                padding: EdgeInsets.symmetric(horizontal: 8.w),
                              ),
                              SizedBox(
                                height:9.h,
                              ),
                            ],
                          ),
                          const Spacer(),
                          Container(
                            padding: EdgeInsets.only(
                                left: 13.w,
                                right: 13.w,
                                top: 4.h,
                                bottom: 17.h),
                            decoration: BoxDecoration(
                                color: AppColor.black12,
                                borderRadius: BorderRadius.circular(8.r)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "03",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  "Aug",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.grey81),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
