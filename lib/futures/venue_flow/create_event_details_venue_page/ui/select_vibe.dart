import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:glass/glass.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/darop_down.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/widget/create_event_bottom_sheet.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/get_vibe_view_model.dart';

import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/helper/custom_text_fild.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class SelectVibeCreateEventPage extends StatefulWidget {
  SelectVibeCreateEventPage({Key? key}) : super(key: key);

  @override
  State<SelectVibeCreateEventPage> createState() => _SelectVibeCreateEventPageState();
}

class _SelectVibeCreateEventPageState extends State<SelectVibeCreateEventPage> {
  final controllerVibeSearch = TextEditingController();

  final List<String> ls = [
    "Eclectic",
    "Intimate",
    "High Energy",
    "Chill",
    "Nostalgic",
    "Bluesy",
    "Jazzy",
    "Folksy",
    "Rocking",
    "Reggae",
    "Hip-hop",
    "Classical",
    "World music",
  ];

  SelectItemViewModel changeItemBloc = SelectItemViewModel();

  ScrollController scrollController = ScrollController();

  GetVibeViewModel getVibeViewModel=GetVibeViewModel();
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getVibeViewModel.getVibe();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: BlocBuilder(
        bloc: getVibeViewModel,
        builder: (context,state){
          if(state is GetVibeLoadingState){
            return const CustomLoading();
          }
          if(state is GetVibeSuccessState){
            return  Stack(
              children: [
                Positioned(
                    left: -100.w,
                    top: -100.h,
                    child: ContainerBlur(color: AppColor.purple)),
                SingleChildScrollView(
                  controller: scrollController,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: SvgPicture.asset(ImagesApp.icBack)),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            SvgPicture.asset(ImagesApp.icShared),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 31.h,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(16.r),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.w, right: 16.w, top: 23.h, bottom: 24.h),
                              child: ClassDropDown(
                                data: state.response.data,
                                scrollController: scrollController,
                                callBack: (_) {
                                  print(ls[_]);
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          }
          return const SizedBox();
        },
      ),
      bottomNavigationBar: SizedBox(
        height: 84.h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 165.w,
              height: 49.h,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: AppColor.grey1.withOpacity(0.4),
                ),
                borderRadius: BorderRadius.circular(50.r),
              ),
              child: Center(
                child: Text(
                  "Skip",
                  style: poppins.copyWith(
                    fontSize: 14.sp,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                if(Review.vibe==1){
                  Review.vibe=0;
                  CreateEventBottomSheets.reviewYourRequest(context);
                }
                else{
                  CreateEventBottomSheets.setProfEvent(context);
                }
              },
              child: Container(
                width: 165.w,
                height: 49.h,
                decoration: BoxDecoration(
                  color: AppColor.blue,
                  borderRadius: BorderRadius.circular(50.r),
                ),
                child: Center(
                  child: Text(
                    "Next",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ).asGlass(),
    );
  }
}
