import 'dart:io';
import 'dart:ui';

import 'package:bottom_picker/bottom_picker.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:getwidget/components/progress_bar/gf_progress_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/logic/state/get_users_state.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/logic/view_model/get_users_view_model.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/core/helper/divider.dart';

import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/select_vibe.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/get_address_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_address_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_event_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/view_model/get_address_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/view_model/send_create_address_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/view_model/send_create_event_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_create_address_event_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_send_create_event_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/select_vibe.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/profile_venus_page.dart';
import 'package:vibe_app/test_map.dart';

import '../../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../../core/helper/custom_button.dart';
import '../../../../../core/helper/custom_text_fild.dart';
import '../../../../../core/utils/app_color.dart';
import '../../../../../dat.dart';
import '../../../../constant_pages/sgin_in_page/logic/state/check_box_state.dart';
import '../../../../constant_pages/sgin_in_page/logic/view_model/view_model_check_box.dart';
import '../../model/select_users_prefomance_model.dart';

class CreateEventBottomSheets {
  static void showSelectDateTime(BuildContext context) {
    var time;
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 160.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              ListView(
                reverse: true,
                children: [
                  ClipRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 15,
                        sigmaY: 15,
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            border: BorderDirectional(
                              top: BorderSide(
                                width: 1,
                                color: AppColor.grey1.withOpacity(0.4),
                              ),
                            )),
                        height: 84.h,
                        child: Center(
                          child: MyCustomButton(
                            title: "Continue",
                            loading: false,
                            onTap: () {
                              Navigator.pop(context);
                            },
                            color: AppColor.blue,
                            width: 319.w,
                            height: 49.h,
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                  BottomPicker.time(
                    title: '',
                    titleStyle: const TextStyle(fontSize: 0),
                    backgroundColor: Colors.transparent,
                    height: 300,
                    onSubmit: (date) {
                      print(date);
                    },
                    onChange: (date) {},
                    pickerTextStyle: poppins.copyWith(
                      fontSize: 23.sp,
                      color: AppColor.white.withOpacity(0.5),
                    ),
                    displayButtonIcon: false,
                    displaySubmitButton: false,
                    displayCloseIcon: false,
                    initialDateTime: DateTime(2021, 5, 1),
                  ),
                  const Da(),
                  SizedBox(height: 25.h),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Select the time and date of the event",
                      style: poppins.copyWith(
                          fontSize: 14.sp, color: AppColor.greyHint),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Date & Time",
                      style: poppins.copyWith(
                          fontSize: 28.sp, color: AppColor.white),
                    ),
                  ),
                  SizedBox(height: 25.h),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  static void selectPreforms(BuildContext context) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              const SelectPreforms(),
            ],
          ),
        );
      },
    );
  }

  static void prefomanceDetails(
      {required BuildContext context,
      required List<int> lsSelected,
      required List<String> lsName,
      required List<String> lsImg}) {
    List<TextEditingController> amount =
        List.generate(lsSelected.length, (index) => TextEditingController());
    List<TextEditingController> additionalInformation =
        List.generate(lsSelected.length, (index) => TextEditingController());
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                  child: Image.asset(
                    ImagesApp.ellipse,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              CustomScrollView(
                controller: scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(top: 19.h, bottom: 14.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 16.w),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Cancel",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Text(
                              "Select performers",
                              style: poppins.copyWith(
                                  fontSize: 16.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: DividerWidget(),
                  ),
                  SliverToBoxAdapter(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.w, right: 16.w, top: 10.h),
                          child: Container(
                            width: 343.w,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: 16.w, right: 16.w, top: 16.h),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Select performers",
                                    style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 12.h),
                                  const DividerWidget(),
                                  SizedBox(height: 12.h),
                                  ListView.builder(
                                    itemCount: lsSelected.length,
                                    controller: scrollController,
                                    shrinkWrap: true,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: [
                                          Container(
                                            width: 312.w,
                                            height: 65.h,
                                            decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.07),
                                              borderRadius:
                                                  BorderRadius.circular(10.r),
                                            ),
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: 52.w,
                                                  height: 52.r,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.r),
                                                    image:
                                                        lsImg[index] == "null"
                                                            ? DecorationImage(
                                                                image: AssetImage(
                                                                    ImagesApp
                                                                        .profileTest1),
                                                              )
                                                            : DecorationImage(
                                                                image: NetworkImage(
                                                                    "http://46.249.102.65:8080/${lsImg[index]}"),
                                                              ),
                                                  ),
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      lsName[index],
                                                      style: poppins.copyWith(
                                                        fontSize: 16.sp,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          width: 16.w,
                                                          height: 16.h,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: AppColor
                                                                .white
                                                                .withOpacity(
                                                                    0.1),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4.r),
                                                          ),
                                                          child: Center(
                                                              child: SvgPicture
                                                                  .asset(
                                                            ImagesApp
                                                                .icLocation,
                                                            width: 10.w,
                                                            height: 10.h,
                                                          )),
                                                        ),
                                                        SizedBox(width: 6.w),
                                                        Text(
                                                          "Elgin St. Celina, Delaware",
                                                          style: inter.copyWith(
                                                            fontSize: 10.sp,
                                                            color:
                                                                AppColor.grey2,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 2.h),
                                          Container(
                                            width: 312.w,
                                            decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.07),
                                              borderRadius:
                                                  BorderRadius.circular(10.r),
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 16.w,
                                                      top: 8.h,
                                                      bottom: 8.h),
                                                  child: Text(
                                                    "Set your price",
                                                    style: poppins.copyWith(
                                                        fontSize: 12.sp,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                                const DividerWidget(),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 16.w,
                                                      right: 16.w,
                                                      top: 4.h,
                                                      bottom: 12.h),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        "Write guarantee amount",
                                                        style: poppins.copyWith(
                                                          fontSize: 10.sp,
                                                          color:
                                                              AppColor.greyText,
                                                        ),
                                                      ),
                                                      SizedBox(height: 6.h),
                                                      Container(
                                                        width: 280.w,
                                                        height: 49.h,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: AppColor
                                                              .greyText
                                                              .withOpacity(
                                                                  0.08),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.r),
                                                        ),
                                                        child: CustomTextField(
                                                          hintTextDirection:
                                                              TextDirection.ltr,
                                                          enable: true,
                                                          controller:
                                                              amount[index],
                                                          hint: "0.00",
                                                          contentPadding:
                                                              EdgeInsets.only(
                                                                  bottom: 5.h,
                                                                  top: 10.h),
                                                          hintStyle:
                                                              poppins.copyWith(
                                                            fontSize: 24.sp,
                                                            color: AppColor
                                                                .greyText,
                                                          ),
                                                          fontSize: 24.sp,
                                                          align:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                      SizedBox(height: 6.h),
                                                      Text(
                                                        "Any additional Information? ",
                                                        style: poppins.copyWith(
                                                          fontSize: 10.sp,
                                                          color:
                                                              AppColor.greyText,
                                                        ),
                                                      ),
                                                      SizedBox(height: 6.h),
                                                      Container(
                                                        width: 280.w,
                                                        height: 61.h,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: AppColor
                                                              .greyText
                                                              .withOpacity(
                                                                  0.08),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.r),
                                                        ),
                                                        child: CustomTextField(
                                                          hintTextDirection:
                                                              TextDirection.ltr,
                                                          controller:
                                                              additionalInformation[
                                                                  index],
                                                          hint:
                                                              "We are interested in having you\nperforming at our venue.",
                                                          enable: true,
                                                          line: 100,
                                                          type: TextInputType
                                                              .multiline,
                                                          hintStyle:
                                                              poppins.copyWith(
                                                                  fontSize:
                                                                      12.sp,
                                                                  color: AppColor
                                                                      .greyText),
                                                          fontSize: 12.sp,
                                                          align:
                                                              TextAlign.start,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: 8.h),
                                        ],
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 8.h),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                            selectPreforms(context);
                          },
                          child: Container(
                            height: 48.h,
                            width: 348.w,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 11.w),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Add more artists to this event",
                                    style: poppins.copyWith(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Container(
                                    width: 51.w,
                                    height: 23.h,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(6.r),
                                        color: AppColor.blue),
                                    child: Center(
                                      child: Text(
                                        "Add",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          height: 48.h,
                          width: 348.w,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 11.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Offer food & beverage?",
                                  style: poppins.copyWith(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                CustomSwitch(
                                  isToggled: true,
                                  onToggled: (isToggled) {},
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 28.h),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: 15,
                                sigmaY: 15,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(0.07),
                                    border: BorderDirectional(
                                      top: BorderSide(
                                        width: 1,
                                        color: AppColor.grey1.withOpacity(0.4),
                                      ),
                                    )),
                                height: 84.h,
                                child: Center(
                                  child: MyCustomButton(
                                    title: "Save & Continue",
                                    loading: false,
                                    onTap: () {
                                      if (Review.preform == 1) {
                                        Review.preform = 0;
                                        for (int i = 0;
                                            i < amount.length;
                                            i++) {
                                          if (amount[i].text.isNotEmpty &&
                                              additionalInformation[i]
                                                  .text
                                                  .isNotEmpty) {
                                            CreateEventDetailsVenuePage.lsUsers
                                                .add(
                                              Users(
                                                id: lsSelected[i],
                                                amount: int.parse(
                                                  amount[i].text,
                                                ),
                                                additionalInformation:
                                                    additionalInformation[i]
                                                        .text,
                                              ),
                                            );
                                            Navigator.pop(context);
                                            reviewYourRequest(context);
                                          } else {
                                            CustomToast.show(
                                                "please Complete Fields");
                                          }
                                        }
                                      } else {
                                        for (int i = 0;
                                            i < amount.length;
                                            i++) {
                                          if (amount[i].text.isNotEmpty &&
                                              additionalInformation[i]
                                                  .text
                                                  .isNotEmpty) {
                                            CreateEventDetailsVenuePage.lsUsers
                                                .add(
                                              Users(
                                                id: lsSelected[i],
                                                amount: int.parse(
                                                  amount[i].text,
                                                ),
                                                additionalInformation:
                                                    additionalInformation[i]
                                                        .text,
                                              ),
                                            );
                                            Navigator.pop(context);
                                            setLimit(context);
                                          } else {
                                            CustomToast.show(
                                                "please Complete Fields");
                                          }
                                        }
                                      }
                                    },
                                    color: AppColor.blue,
                                    width: 319.w,
                                    height: 49.h,
                                    fontSize: 14.sp,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  static void setLimit(BuildContext context) {
    TextEditingController textEditingController = TextEditingController();
    final checkBoxViewModel = CheckBoxViewModel();

    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                  child: Image.asset(
                    ImagesApp.ellipse,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              CustomScrollView(
                controller: scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(top: 19.h, bottom: 14.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 16.w),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Cancel",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Text(
                              "Select performers",
                              style: poppins.copyWith(
                                  fontSize: 16.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: DividerWidget(),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: 14.h, left: 16.w, right: 16.w),
                      child: Container(
                        width: 343.w,
                        decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.r),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.w, vertical: 16.h),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  BlocBuilder(
                                    bloc: checkBoxViewModel,
                                    builder: (context, state) {
                                      if (state is ChangeState) {
                                        return GestureDetector(
                                          onTap: () {
                                            checkBoxViewModel
                                                .changeState(!state.status);
                                          },
                                          child: Container(
                                            width: 24,
                                            height: 24,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(4.r),
                                              border: Border.all(
                                                  color: state.status
                                                      ? Colors.transparent
                                                      : AppColor.greyHint),
                                              color: state.status
                                                  ? AppColor.greyBlue
                                                  : Colors.transparent,
                                            ),
                                            child: state.status
                                                ? Center(
                                                    child: Icon(
                                                    Icons.check,
                                                    color:
                                                        AppColor.backgroundApp,
                                                    size: 18,
                                                  ))
                                                : const SizedBox(),
                                          ),
                                        );
                                      }
                                      return const SizedBox();
                                    },
                                  ),
                                  SizedBox(width: 10.w),
                                  Text(
                                    "Set a time limit for invited performers\nto respond.",
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.white,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 12.h),
                              Container(
                                height: 114.h,
                                decoration: BoxDecoration(
                                  color: AppColor.greyText.withOpacity(0.08),
                                  borderRadius: BorderRadius.circular(15.r),
                                ),
                                child: CustomTextField(
                                  hintTextDirection: TextDirection.ltr,
                                  controller: textEditingController,
                                  cursorHeight: 34,
                                  hint: "48",
                                  line: 100,
                                  type: TextInputType.number,
                                  hintStyle: poppins.copyWith(
                                    fontSize: 34.sp,
                                    color: AppColor.greyText,
                                  ),
                                  fontSize: 34.sp,
                                  align: TextAlign.center,
                                  contentPadding:
                                      EdgeInsets.only(bottom: 30.h, top: 30.h),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 15,
                      sigmaY: 15,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          border: BorderDirectional(
                            top: BorderSide(
                              width: 1,
                              color: AppColor.grey1.withOpacity(0.4),
                            ),
                          )),
                      height: 84.h,
                      child: Center(
                        child: MyCustomButton(
                          title: "Save & Continue",
                          loading: false,
                          onTap: () {
                            if (Review.responseTime == 1) {
                              Review.responseTime = 0;
                              if (textEditingController.text.isNotEmpty) {
                                CreateEventDetailsVenuePage.timeLimit =
                                    textEditingController.text;
                                Navigator.pop(context);
                                reviewYourRequest(context);
                              } else {
                                CustomToast.show("Please Complete Fields");
                              }
                            } else {
                              if (textEditingController.text.isNotEmpty) {
                                CreateEventDetailsVenuePage.timeLimit =
                                    textEditingController.text;
                                Navigator.pop(context);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SelectVibeCreateEventPage()));
                              } else {
                                CustomToast.show("Please Complete Fields");
                              }
                            }
                          },
                          color: AppColor.blue,
                          width: 319.w,
                          height: 49.h,
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void setProfEvent(BuildContext context) {
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                  child: Image.asset(
                    ImagesApp.ellipse,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              CustomScrollView(
                controller: scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(top: 19.h, bottom: 14.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 16.w),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Cancel",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text(
                              "Details",
                              style: poppins.copyWith(
                                  fontSize: 16.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: DividerWidget(),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: 14.h, left: 16.w, right: 16.w),
                      child: Container(
                        width: 343.w,
                        decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.r),
                        ),
                        child: Column(
                          children: [
                            Container(
                              width: 343.w,
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.01),
                                borderRadius: BorderRadius.circular(20.r),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.w, top: 16.h, bottom: 13.h),
                                    child: Text(
                                      "Add a photo for event",
                                      style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 349.w,
                                    height: 1.h,
                                    decoration: BoxDecoration(
                                      color: AppColor.white.withOpacity(0.05),
                                      borderRadius: BorderRadius.circular(20.r),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.w, top: 11.h, bottom: 8.h),
                                    child: Text(
                                      "Upload photo from your library",
                                      style: poppins.copyWith(
                                        fontSize: 13.sp,
                                        color: AppColor.greyText,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  const Avatar(),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.w, right: 16.w, bottom: 16.h),
                                    child: Container(
                                      height: 48.h,
                                      width: 348.w,
                                      decoration: BoxDecoration(
                                        color: AppColor.white.withOpacity(0.05),
                                        borderRadius:
                                            BorderRadius.circular(10.r),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 11.w),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Use your venue photo",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            CustomSwitch(
                                              isToggled: true,
                                              onToggled: (isToggled) {},
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 15,
                      sigmaY: 15,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          border: BorderDirectional(
                            top: BorderSide(
                              width: 1,
                              color: AppColor.grey1.withOpacity(0.4),
                            ),
                          )),
                      height: 84.h,
                      child: Center(
                        child: MyCustomButton(
                          title: "Save & Continue",
                          loading: false,
                          onTap: () {
                            if (Review.avatar == 1) {
                              Review.avatar = 0;
                              if (CreateEventDetailsVenuePage
                                  .avatar.isNotEmpty) {
                                Navigator.pop(context);
                                reviewYourRequest(context);
                              } else {
                                CustomToast.show("Please Choose Avatar");
                              }
                            } else {
                              if (CreateEventDetailsVenuePage
                                  .avatar.isNotEmpty) {
                                Navigator.pop(context);
                                ticketDetails(context);
                              } else {
                                CustomToast.show("Please Choose Avatar");
                              }
                            }
                          },
                          color: AppColor.blue,
                          width: 319.w,
                          height: 49.h,
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void ticketDetails(BuildContext context) {
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                  child: Image.asset(
                    ImagesApp.ellipse,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              CustomScrollView(
                controller: scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(top: 19.h, bottom: 14.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 16.w),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Cancel",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Text(
                              "Details",
                              style: poppins.copyWith(
                                  fontSize: 16.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: DividerWidget(),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: 14.h, left: 16.w, right: 16.w),
                      child: Container(
                        width: 343.w,
                        decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.r),
                        ),
                        child: Column(
                          children: [
                            Container(
                              width: 343.w,
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.01),
                                borderRadius: BorderRadius.circular(20.r),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.w, top: 16.h, bottom: 13.h),
                                    child: Text(
                                      "Ticket Details",
                                      style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  const DividerWidget(),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.w, bottom: 16.h),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 11.h, bottom: 8.h),
                                          child: Text(
                                            "Add your ticket Purchase Link",
                                            style: poppins.copyWith(
                                              fontSize: 13.sp,
                                              color: AppColor.greyText,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 311.w,
                                          height: 48.h,
                                          decoration: BoxDecoration(
                                            color:
                                                AppColor.white.withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(10.r),
                                          ),
                                          child: CustomTextField(
                                            controller:
                                                CreateEventDetailsVenuePage
                                                    .textCntEventTicket,
                                            hint:
                                                "Eventbrite.com/jfurjry4h/ltot",
                                            prefixIcon: ImagesApp.icTiktook,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 11.h, bottom: 8.h),
                                          child: Text(
                                            "Add ticket price",
                                            style: poppins.copyWith(
                                              fontSize: 13.sp,
                                              color: AppColor.greyText,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 311.w,
                                          height: 78.h,
                                          decoration: BoxDecoration(
                                            color:
                                                AppColor.white.withOpacity(0.1),
                                            borderRadius:
                                                BorderRadius.circular(10.r),
                                          ),
                                          child: CustomTextField(
                                            controller:
                                                CreateEventDetailsVenuePage
                                                    .textCntEventPrice,
                                            hint: "\$55",
                                            align: TextAlign.center,
                                            contentPadding: EdgeInsets.only(
                                                top: 18.h, bottom: 18.h),
                                            hintFontSize: 32.sp,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 15,
                      sigmaY: 15,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          border: BorderDirectional(
                            top: BorderSide(
                              width: 1,
                              color: AppColor.grey1.withOpacity(0.4),
                            ),
                          )),
                      height: 84.h,
                      child: Center(
                        child: MyCustomButton(
                          title: "Save & Continue",
                          loading: false,
                          onTap: () {
                            if (Review.ticketPrice == 1 || Review.ticket == 1) {
                              Review.ticketPrice = 0;
                              Review.ticket = 0;
                              Navigator.pop(context);
                              reviewYourRequest(context);
                            } else {
                              Navigator.pop(context);
                              reviewYourRequest(context);
                            }
                          },
                          color: AppColor.blue,
                          width: 319.w,
                          height: 49.h,
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void reviewYourRequest(BuildContext context) {
    SendCreateEventViewModel createEventViewModel = SendCreateEventViewModel();

    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              const Review(),
              Align(
                alignment: Alignment.bottomCenter,
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 15,
                      sigmaY: 15,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          border: BorderDirectional(
                            top: BorderSide(
                              width: 1,
                              color: AppColor.grey1.withOpacity(0.4),
                            ),
                          )),
                      height: 84.h,
                      child: Center(
                        child: BlocConsumer(
                          bloc: createEventViewModel,
                          listener: (ctx, state) {
                            if (state is SendCreateEventSuccessState) {
                              CreateEventDetailsVenuePage.textCntEventTitle
                                  .clear();
                              CreateEventDetailsVenuePage.idAddress =0;
                              CreateEventDetailsVenuePage.dateAndTime = "";
                              CreateEventDetailsVenuePage.avatar = "";
                              CreateEventDetailsVenuePage.limitAge = "";
                              CreateEventDetailsVenuePage.textCntEventTicket
                                  .clear();
                              CreateEventDetailsVenuePage.textCntEventPrice
                                  .clear();
                              CreateEventDetailsVenuePage.timeLimit = "";
                              CreateEventDetailsVenuePage.textCntEventPrice
                                  .clear();
                              CreateEventDetailsVenuePage.textCntEventAbout
                                  .clear();
                              CreateEventDetailsVenuePage.lsUsers.clear();
                              CreateEventDetailsVenuePage.selectItemVibe
                                  .clear();
                              CreateEventDetailsVenuePage.imageSelectPreformance
                                  .clear();
                              CreateEventDetailsVenuePage.nameSelectPreformance
                                  .clear();

                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const BottomNavVenusPages(index: 4),
                                ),
                              );
                            }
                          },
                          builder: (ctx, state) {
                            return MyCustomButton(
                              title: "Save & Continue",
                              loading: state is SendCreateEventLoadingState,
                              enable: state is! SendCreateEventLoadingState,
                              onTap: () async {
                                print(CreateEventDetailsVenuePage.avatar);
                                createEventViewModel.send(
                                    eventTitle: CreateEventDetailsVenuePage
                                        .textCntEventTitle.text,
                                    idAddress:
                                        CreateEventDetailsVenuePage.idAddress!,
                                    calendar:
                                        CreateEventDetailsVenuePage.dateAndTime,
                                    avatar: CreateEventDetailsVenuePage.avatar,
                                    ageLimit:
                                        CreateEventDetailsVenuePage.limitAge,
                                    offerFoodAndBeverage: 0,
                                    ticketLink: CreateEventDetailsVenuePage
                                        .textCntEventTicket.text,
                                    ticketPrice: CreateEventDetailsVenuePage
                                        .textCntEventPrice.text,
                                    respondExpireTime: int.parse(
                                        CreateEventDetailsVenuePage.timeLimit),
                                    asGigPost: 0,
                                    amount: int.parse(
                                        CreateEventDetailsVenuePage
                                            .textCntEventPrice.text),
                                    additionalInformation:
                                        CreateEventDetailsVenuePage
                                            .textCntEventAbout.text,
                                    users: CreateEventDetailsVenuePage.lsUsers,
                                    vibe: CreateEventDetailsVenuePage
                                        .selectItemVibe);
                              },
                              color: AppColor.blue,
                              width: 319.w,
                              height: 49.h,
                              fontSize: 14.sp,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void showMap(BuildContext context) {
    showModalBottomSheet(
      enableDrag: false,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 101.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          topRight: Radius.circular(20.r)),
                    ),
                    height: MediaQuery.of(context).size.height - 101.h,
                    child: MapApp(
                      center: LatLong(33, 54),
                      isVisiblBtn: true,
                      onPicked: (pickedData) {
                        print(pickedData.latLong.latitude);
                        print(pickedData.latLong.longitude);
                        print(pickedData.address);
                      },
                    )),
              ),
            ],
          ),
        );
      },
    );
  }

  static void showDetailsMap(
      {required BuildContext context, required var lat, required var long}) {
    GetAddressViewModel getAddressViewModel = GetAddressViewModel();
    SendCreateAddressViewModel createAddressViewModel =
        SendCreateAddressViewModel();
    getAddressViewModel.getAddress(lat: lat, long: long);
    showModalBottomSheet(
      enableDrag: false,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return BlocBuilder(
            bloc: getAddressViewModel,
            builder: (context, state) {
              if (state is GetAddressLoadingState) {
                return const CustomLoading();
              }
              if (state is GetAddressSuccessState) {
                CreateEventDetailsVenuePage.textCntOptional.text =
                    state.getResponseAddressModel.displayName;
                CreateEventDetailsVenuePage.textCntCountry.text =
                    state.getResponseAddressModel.address.country;

                return SizedBox(
                  height: MediaQuery.of(context).size.height - 101.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Column(
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                                physics: const BouncingScrollPhysics(),
                                child: Column(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color:
                                              AppColor.white.withOpacity(0.07),
                                          borderRadius:
                                              BorderRadius.circular(10.r),
                                          border: Border.all(
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                            width: 1,
                                          )),
                                      width: double.infinity,
                                      margin: EdgeInsets.only(
                                          right: 16.w,
                                          left: 16.w,
                                          top: 33.h,
                                          bottom: 27.5.h),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          CustomTextField(
                                            controller:
                                                CreateEventDetailsVenuePage
                                                    .textCntStreet,
                                            hint: "Street",
                                            line: 2,
                                          ),
                                          Container(
                                            width: double.infinity,
                                            height: 1,
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                          ),
                                          CustomTextField(
                                            controller:
                                                CreateEventDetailsVenuePage
                                                    .textCntOptional,
                                            line: 3,
                                          ),
                                          Container(
                                            width: double.infinity,
                                            height: 1,
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                          ),
                                          CustomTextField(
                                              controller:
                                                  CreateEventDetailsVenuePage
                                                      .textCntCity,
                                              hint: "City"),
                                          Container(
                                            width: double.infinity,
                                            height: 1,
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                    decoration: BoxDecoration(
                                                      border: Border(
                                                        right: BorderSide(
                                                          color: AppColor.white
                                                              .withOpacity(
                                                                  0.07),
                                                          width: 1,
                                                        ),
                                                      ),
                                                    ),
                                                    child: CustomTextField(
                                                      controller:
                                                          CreateEventDetailsVenuePage
                                                              .textCntState,
                                                      hint: "State",
                                                    )),
                                              ),
                                              //Container(color: AppColor.white.withOpacity(0.07),width: 1,height: 163.h,),
                                              Expanded(
                                                child: CustomTextField(
                                                  controller:
                                                      CreateEventDetailsVenuePage
                                                          .textCntZipCode,
                                                  hint: "Zip Code",
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            width: double.infinity,
                                            height: 1,
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                          ),
                                          CustomTextField(
                                            controller:
                                                CreateEventDetailsVenuePage
                                                    .textCntCountry,
                                            hint: "Country / Region",
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 1,
                                      color: AppColor.black48,
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 23.75.w),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 25.5.h,
                                          bottom: 21.75.h,
                                          right: 21.w,
                                          left: 16.w),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            children: [
                                              Text(
                                                "Show your specific location",
                                                style: poppins.copyWith(
                                                    fontSize: 16.sp,
                                                    fontWeight: FontWeight.w500,
                                                    color: AppColor.whiteEB),
                                              ),
                                              SizedBox(
                                                height: 5.h,
                                              ),
                                              Text(
                                                "Make it more clear to guests where your\nplace is located. We’ll never shere your\naddress untill a guest is booked",
                                                style: poppins.copyWith(
                                                    fontSize: 11.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: AppColor.grey2),
                                              ),
                                            ],
                                          ),
                                          CustomSwitch(
                                              onToggled: (isToggled) {},
                                              isToggled: true)
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          Column(
                            children: [
                              Container(
                                width: double.infinity,
                                height: 1,
                                color: AppColor.black48,
                              ),
                              BlocConsumer(
                                bloc: createAddressViewModel,
                                listener: (context, stateSendAddress) {
                                  if (stateSendAddress
                                      is SendCreateAddressSuccessState) {
                                    CreateEventDetailsVenuePage.idAddress =
                                        stateSendAddress
                                            .responseCreateAddressModel.data.id;
                                    Navigator.pop(context);
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const CreateEventDetailsVenuePage()));
                                  }
                                },
                                builder: (context, stateSendAddress) {
                                  return Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16.w, vertical: 17.5.h),
                                    color: AppColor.white.withOpacity(0.07),
                                    child: MyCustomButton(
                                      title: "Next",
                                      loading: stateSendAddress
                                          is SendCreateAddressLoadingState,
                                      enable: stateSendAddress
                                          is! SendCreateAddressLoadingState,
                                      onTap: () {
                                        if (CreateEventDetailsVenuePage
                                                .textCntStreet.text.isEmpty ||
                                            CreateEventDetailsVenuePage
                                                .textCntCountry.text.isEmpty ||
                                            CreateEventDetailsVenuePage
                                                .textCntZipCode.text.isEmpty ||
                                            CreateEventDetailsVenuePage
                                                .textCntState.text.isEmpty ||
                                            CreateEventDetailsVenuePage
                                                .textCntOptional.text.isEmpty ||
                                            CreateEventDetailsVenuePage
                                                .textCntCity.text.isEmpty) {
                                          CustomToast.show(
                                              "Please Complete Fields");
                                        } else {
                                          createAddressViewModel.send(
                                              requestCreateAddressEventModel:
                                                  RequestCreateAddressEventModel(
                                            address:
                                                "${CreateEventDetailsVenuePage.textCntCity.text},${CreateEventDetailsVenuePage.textCntStreet.text},${CreateEventDetailsVenuePage.textCntState.text}",
                                            longitude: 37.4219983,
                                            latitude: -122.084,
                                            city: CreateEventDetailsVenuePage
                                                .textCntCity.text,
                                            state: CreateEventDetailsVenuePage
                                                .textCntState.text,
                                            country: CreateEventDetailsVenuePage
                                                .textCntCountry.text,
                                            postalCode:
                                                CreateEventDetailsVenuePage
                                                    .textCntZipCode.text,
                                          ));
                                        }
                                      },
                                      color: AppColor.blue,
                                      width: double.infinity,
                                      height: 49.h,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                      borderRadius: 48.r,
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              }
              return const SizedBox();
            });
      },
    );
  }
}

class SelectPreforms extends StatefulWidget {
  const SelectPreforms({Key? key}) : super(key: key);

  @override
  State<SelectPreforms> createState() => _SelectPreformsState();
}

class _SelectPreformsState extends State<SelectPreforms> {
  final getUsersViewModel = GetUsersViewModel();

  // final List<int> select = [];
  // final List<String> img = [];
  // final List<String> lsName = [];

  void initState() {
    // TODO: implement initState
    super.initState();
    getUsersViewModel.get(2);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: getUsersViewModel,
      builder: (context, state) {
        if (state is GetUsersLoadingState) {
          return const CustomLoading();
        }
        if (state is GetUsersSuccessState) {
          return SingleChildScrollView(
            child: Column(
              children: [
                SingleChildScrollView(
                  reverse: true,
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 16.w),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "Cancel",
                                    style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w400,
                                        color: AppColor.blue),
                                  )),
                            ),
                            Text(
                              "Select performers",
                              style: poppins.copyWith(
                                  fontSize: 16.sp, fontWeight: FontWeight.w500),
                            ),
                            Container(
                              width: 90.h,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 1,
                        width: double.infinity,
                        color: AppColor.white.withOpacity(0.07),
                      ),
                      SizedBox(
                        height: 25.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller: TextEditingController(),
                              hint: "Event Name*",
                              prefixIcon: ImagesApp.icSearchBlue,
                              prefixTitle: "|",
                              prefixTitleStyle:
                                  poppins.copyWith(color: AppColor.greyHint),
                            ),
                          ),
                          SizedBox(width: 8.w),
                          Container(
                            width: 44.w,
                            height: 44.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: Center(
                                child: SvgPicture.asset(ImagesApp.icFilter)),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.h, left: 16.w, right: 16.w, bottom: 15.h),
                        child: Container(
                          height: 48.h,
                          width: 348.w,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 11.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Create as gig post",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                CustomSwitch(
                                  isToggled: true,
                                  onToggled: (isToggled) {},
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 26.w),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "By selecting this option, artists will be able to \nsubmit their interest to perform at this event.",
                            style: poppins.copyWith(
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.greyHint),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 26.w, top: 15.h, bottom: 12.h),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Selected",
                            style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                color: AppColor.white),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 26.w),
                        child: SizedBox(
                          height: 80.h,
                          child: ListView.builder(
                            itemCount: CreateEventDetailsVenuePage
                                .idSelectPreformance.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    CreateEventDetailsVenuePage
                                        .idSelectPreformance
                                        .removeAt(index);
                                    CreateEventDetailsVenuePage
                                        .imageSelectPreformance
                                        .removeAt(index);
                                    CreateEventDetailsVenuePage
                                        .nameSelectPreformance
                                        .removeAt(index);
                                  });
                                },
                                child: Padding(
                                  padding: EdgeInsets.only(right: 14.w),
                                  child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Container(
                                        width: 42.w,
                                        height: 42.r,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: CreateEventDetailsVenuePage
                                                          .imageSelectPreformance[
                                                      index] ==
                                                  "null"
                                              ? DecorationImage(
                                                  image: AssetImage(
                                                      ImagesApp.prof))
                                              : DecorationImage(
                                                  image: NetworkImage(
                                                      "http://46.249.102.65:8080/${CreateEventDetailsVenuePage.imageSelectPreformance[index]}"),
                                                ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          width: 11.w,
                                          height: 11.r,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: AppColor.black48,
                                          ),
                                          child: Center(
                                            child: Icon(
                                              Icons.close,
                                              size: 12,
                                              color: AppColor.white,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 16.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 22.w),
                        child: SizedBox(
                          height: 300,
                          child: ListView.builder(
                            itemCount: state.response.data.length,
                            itemBuilder: (context, index) {
                              int item = state.response.data[index].id;
                              String imgAddress =
                                  state.response.data[index].avatar != null
                                      ? state.response.data[index].avatar!
                                      : "null";
                              String name =
                                  state.response.data[index].firstName +
                                      state.response.data[index].lastName;
                              return Padding(
                                padding: EdgeInsets.only(bottom: 8.h),
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (CreateEventDetailsVenuePage
                                          .idSelectPreformance
                                          .contains(item)) {
                                        CreateEventDetailsVenuePage
                                            .idSelectPreformance
                                            .remove(item);
                                        CreateEventDetailsVenuePage
                                            .imageSelectPreformance
                                            .remove(imgAddress);
                                        CreateEventDetailsVenuePage
                                            .nameSelectPreformance
                                            .remove(name);
                                      } else {
                                        CreateEventDetailsVenuePage
                                            .idSelectPreformance
                                            .add(item);
                                        CreateEventDetailsVenuePage
                                            .imageSelectPreformance
                                            .add(imgAddress);
                                        CreateEventDetailsVenuePage
                                            .nameSelectPreformance
                                            .add(name);
                                      }
                                    });
                                  },
                                  child: Container(
                                    width: 335.w,
                                    height: 65.h,
                                    decoration: BoxDecoration(
                                      color: AppColor.white.withOpacity(0.07),
                                      borderRadius: BorderRadius.circular(10.r),
                                    ),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 52.w,
                                          height: 52.r,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            image: state.response.data[index]
                                                        .avatar ==
                                                    null
                                                ? DecorationImage(
                                                    image: AssetImage(
                                                        ImagesApp.profileTest1),
                                                  )
                                                : DecorationImage(
                                                    image: NetworkImage(
                                                        "http://46.249.102.65:8080/${state.response.data[index].avatar}"),
                                                  ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                state.response.data[index]
                                                        .firstName +
                                                    state.response.data[index]
                                                        .lastName,
                                                style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 16.w,
                                                    height: 16.h,
                                                    decoration: BoxDecoration(
                                                      color: AppColor.white
                                                          .withOpacity(0.1),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              4.r),
                                                    ),
                                                    child: Center(
                                                        child: SvgPicture.asset(
                                                      ImagesApp.icLocation,
                                                      width: 10.w,
                                                      height: 10.h,
                                                    )),
                                                  ),
                                                  SizedBox(width: 6.w),
                                                  Text(
                                                    "Elgin St. Celina, Delaware",
                                                    style: inter.copyWith(
                                                      fontSize: 10.sp,
                                                      color: AppColor.grey2,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: 51.w,
                                          height: 23.h,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6.r),
                                              color: CreateEventDetailsVenuePage
                                                      .idSelectPreformance
                                                      .contains(item)
                                                  ? AppColor.black48
                                                  : AppColor.blue),
                                          child: Center(
                                            child: Text(
                                              CreateEventDetailsVenuePage
                                                      .idSelectPreformance
                                                      .contains(item)
                                                  ? "Cansel"
                                                  : "Add",
                                              style: poppins.copyWith(
                                                  fontSize: 9.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 16.w),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ClipRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 15,
                        sigmaY: 15,
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            border: BorderDirectional(
                              top: BorderSide(
                                width: 1,
                                color: AppColor.grey1.withOpacity(0.4),
                              ),
                            )),
                        height: 84.h,
                        child: Center(
                          child: MyCustomButton(
                            title: "Continue",
                            loading: false,
                            onTap: () {
                              if (CreateEventDetailsVenuePage
                                  .idSelectPreformance.isNotEmpty) {
                                Navigator.pop(context);
                                CreateEventBottomSheets.prefomanceDetails(
                                  context: context,
                                  lsSelected: CreateEventDetailsVenuePage
                                      .idSelectPreformance,
                                  lsName: CreateEventDetailsVenuePage
                                      .nameSelectPreformance,
                                  lsImg: CreateEventDetailsVenuePage
                                      .imageSelectPreformance,
                                );
                              } else {
                                CustomToast.show("Please Select Artist");
                              }
                            },
                            color: AppColor.blue,
                            width: 319.w,
                            height: 49.h,
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}

class Avatar extends StatefulWidget {
  const Avatar({Key? key}) : super(key: key);

  @override
  State<Avatar> createState() => _AvatarState();
}

class _AvatarState extends State<Avatar> {
  File? image;

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
        CreateEventDetailsVenuePage.avatar = this.image!.path;
      });
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 16.h),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: 311.w,
          height: 232.h,
          decoration: BoxDecoration(
            color: AppColor.white.withOpacity(0.05),
            borderRadius: BorderRadius.circular(20.r),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                alignment: Alignment.bottomRight,
                children: [
                  Container(
                    width: 112.w,
                    height: 112.h,
                    decoration: BoxDecoration(
                      color: AppColor.darkBlue,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: image != null
                          ? Container(
                              width: 112.w,
                              height: 112.h,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: FileImage(image!), fit: BoxFit.fill),
                                shape: BoxShape.circle,
                              ),
                            )
                          : SvgPicture.asset(
                              ImagesApp.icUser,
                              width: 22.w,
                              height: 28.h,
                            ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      pickImage(ImageSource.gallery);
                      print("object");
                    },
                    child: Container(
                      width: 42.w,
                      height: 42.r,
                      decoration: BoxDecoration(
                        color: AppColor.black54,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Container(
                          width: 35.w,
                          height: 35.r,
                          decoration: BoxDecoration(
                            color: AppColor.darkPurple,
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.icCamera)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 17.h),
              Text(
                "Upload Image ",
                style: poppins.copyWith(
                  fontSize: 14.sp,
                  color: AppColor.darkPurple,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Review extends StatefulWidget {
  static int preform = 0;
  static int title = 0;
  static int nameEvent = 0;
  static int dateTime = 0;
  static int location = 0;
  static int ticket = 0;
  static int ticketPrice = 0;
  static int about = 0;
  static int responseTime = 0;
  static int vibe = 0;
  static int avatar = 0;

  const Review({Key? key}) : super(key: key);

  @override
  State<Review> createState() => _ReviewState();
}

class _ReviewState extends State<Review> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 16.w),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Cancel",
                          style: poppins.copyWith(
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w400,
                              color: AppColor.blue),
                        )),
                  ),
                ),
                Text(
                  "Review your request",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                Expanded(child: Container()),
              ],
            ),
          ),
          Container(
            height: 1,
            width: double.infinity,
            color: AppColor.white.withOpacity(0.07),
          ),
          SizedBox(height: 20.h),
          SizedBox(
            width: double.infinity,
            height: 170,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 17.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Performance details",
                        style: poppins.copyWith(
                          fontSize: 12.sp,
                          color: Colors.white,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          CreateEventBottomSheets.selectPreforms(context);
                          Review.preform = 1;
                        },
                        child: SizedBox(
                            width: 20.w,
                            height: 20.7.r,
                            child: SvgPicture.asset(ImagesApp.editText,
                                fit: BoxFit.cover)),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.h),
                  Expanded(
                    child: ListView.builder(
                        itemCount: CreateEventDetailsVenuePage
                            .idSelectPreformance.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(right: 25.w),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 72.w,
                                  height: 72.r,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: CreateEventDetailsVenuePage
                                                      .imageSelectPreformance[
                                                  index] ==
                                              "null"
                                          ? DecorationImage(
                                              image: AssetImage(
                                                ImagesApp.prof,
                                              ),
                                              fit: BoxFit.fill)
                                          : DecorationImage(
                                              image: NetworkImage(
                                                "http://46.249.102.65:8080/${CreateEventDetailsVenuePage.imageSelectPreformance[index]}",
                                              ),
                                              fit: BoxFit.fill)),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(top: 16.h, bottom: 4.h),
                                  child: SizedBox(
                                    width: 100,
                                    child: Text(
                                      CreateEventDetailsVenuePage
                                          .nameSelectPreformance[index],
                                      overflow: TextOverflow.ellipsis,
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(12.r),
            margin: EdgeInsets.only(
                top: 0.h, bottom: 10.h, right: 16.w, left: 16.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14.r),
              color: AppColor.white.withOpacity(0.07),
            ),
            width: double.infinity,
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(6.r),
                  child: Image.asset(
                    width: 44.w,
                    height: 43.h,
                    ImagesApp.backHouseofmusic,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  width: 10.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Location",
                      style: poppins.copyWith(
                          fontSize: 11.sp,
                          fontWeight: FontWeight.w400,
                          color: AppColor.white.withOpacity(0.5)),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    Text(
                      "House of Music",
                      style: poppins.copyWith(
                          fontSize: 14.sp, fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            color: AppColor.white.withOpacity(0.07),
            height: 1,
            width: double.infinity,
          ),
          Container(
            margin: EdgeInsets.only(
                top: 10.h, bottom: 12.h, right: 15.5.w, left: 15.5.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14.r),
              color: AppColor.white.withOpacity(0.07),
            ),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 12.h, left: 12.w),
                  child: Text(
                    "Event Details",
                    style: poppins.copyWith(
                        fontSize: 25.sp,
                        fontWeight: FontWeight.w500,
                        color: AppColor.white),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 12.h),
                  color: AppColor.white.withOpacity(0.07),
                  height: 1,
                  width: double.infinity,
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 11.w, vertical: 13.5.h),
                          child: Row(
                            children: [
                              Text(
                                CreateEventDetailsVenuePage
                                    .textCntEventTitle.text,
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 21.w),
                        child: GestureDetector(
                          onTap: () {
                            Review.title = 1;
                            Navigator.pop(context);
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const CreateEventDetailsVenuePage(),
                              ),
                            );
                          },
                          child: SizedBox(
                              width: 20.w,
                              height: 20.7.r,
                              child: SvgPicture.asset(ImagesApp.editText,
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 11.w, vertical: 13.5.h),
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 20.w,
                                  height: 20.7.r,
                                  child: SvgPicture.asset(ImagesApp.icCalendar,
                                      color: AppColor.white,
                                      fit: BoxFit.cover)),
                              SizedBox(
                                width: 8.w,
                              ),
                              Text(
                                "Date",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              const Spacer(),
                              Text(
                                "${Da.month}-${Da.day}",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.greyHint),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 21.w),
                        child: GestureDetector(
                          onTap: () {
                            Review.dateTime = 1;
                            Navigator.pop(context);
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const CreateEventDetailsVenuePage(),
                              ),
                            );
                          },
                          child: SizedBox(
                              width: 20.w,
                              height: 20.7.r,
                              child: SvgPicture.asset(ImagesApp.editText,
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 11.w, vertical: 13.5.h),
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 17.w,
                                  height: 17.r,
                                  child: SvgPicture.asset(ImagesApp.icClock,
                                      color: AppColor.white,
                                      fit: BoxFit.cover)),
                              SizedBox(
                                width: 8.w,
                              ),
                              Text(
                                "Time",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              const Spacer(),
                              Text(
                                CreateEventDetailsVenuePage.time,
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.greyHint),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 21.w),
                        child: GestureDetector(
                          onTap: () {
                            Review.dateTime = 1;
                            Navigator.pop(context);
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const CreateEventDetailsVenuePage(),
                              ),
                            );
                          },
                          child: SizedBox(
                              width: 20.w,
                              height: 20.7.r,
                              child: SvgPicture.asset(ImagesApp.editText,
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(right: 12.w, left: 12.w, bottom: 12.h),
                  decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 11.w, vertical: 13.5.h),
                  child: Row(
                    children: [
                      SizedBox(
                          width: 17.w,
                          height: 17.r,
                          child: SvgPicture.asset(ImagesApp.locationRound,
                              color: AppColor.white, fit: BoxFit.cover)),
                      SizedBox(
                        width: 8.w,
                      ),
                      Text(
                        "Location",
                        style: poppins.copyWith(
                            fontSize: 14.sp, fontWeight: FontWeight.w500),
                      ),
                      const Spacer(),
                      Text(
                        "273-296 Geary St, Rich...",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greyHint),
                      ),
                      SizedBox(
                        width: 6.w,
                      ),
                      SizedBox(
                          width: 16.w,
                          height: 16.r,
                          child: GestureDetector(
                            onTap: () {
                              Review.location = 1;
                              Navigator.pop(context);
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const CreateEventDetailsVenuePage(),
                                ),
                              );
                            },
                            child: SvgPicture.asset(ImagesApp.arrowForward,
                                color: AppColor.white, fit: BoxFit.cover),
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 11.w, vertical: 13.5.h),
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 17.w,
                                  height: 17.r,
                                  child: SvgPicture.asset(ImagesApp.icTiktook,
                                      color: AppColor.white,
                                      fit: BoxFit.cover)),
                              SizedBox(
                                width: 8.w,
                              ),
                              Text(
                                "Ticket Link",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              const Spacer(),
                              Text(
                                "7Eventbrite.com/jfurjry4h/ltot",
                                style: poppins.copyWith(
                                    fontSize: 9.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 21.w),
                        child: GestureDetector(
                          onTap: () {
                            Review.ticket = 1;
                            Navigator.pop(context);
                            CreateEventBottomSheets.ticketDetails(context);
                          },
                          child: SizedBox(
                              width: 20.w,
                              height: 20.7.r,
                              child: SvgPicture.asset(ImagesApp.editText,
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 11.w, vertical: 13.5.h),
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 17.w,
                                  height: 17.r,
                                  child: SvgPicture.asset(ImagesApp.icTiktoke,
                                      color: AppColor.white,
                                      fit: BoxFit.cover)),
                              SizedBox(
                                width: 8.w,
                              ),
                              Text(
                                "Ticket price",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              const Spacer(),
                              Text(
                                "\$55",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.greyHint),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 21.w),
                        child: GestureDetector(
                          onTap: () {
                            Review.ticketPrice = 1;
                            Navigator.pop(context);
                            CreateEventBottomSheets.ticketDetails(context);
                          },
                          child: SizedBox(
                              width: 20.w,
                              height: 20.7.r,
                              child: SvgPicture.asset(ImagesApp.editText,
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12.w, bottom: 2.h),
                  child: Text(
                    "About this event",
                    style: poppins.copyWith(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400,
                        color: AppColor.grey7E),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: 8.h, bottom: 10.h, left: 16.w, right: 16.w),
                  child: Container(
                    height: 95.h,
                    decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: CustomTextField(
                      controller: CreateEventDetailsVenuePage.textCntEventAbout,
                      type: TextInputType.multiline,
                      line: 10,
                      hintStyle: poppins.copyWith(
                          color: AppColor.grey81, fontSize: 11.sp),
                      hint:
                          "Join us at Music Fest and let the music take you on a journey. This show is all about exploring the various genres and styles of music and finding the connections that bring them together.",
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 22.w, left: 13.w, bottom: 8.h),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    padding: EdgeInsets.symmetric(
                        horizontal: 11.w, vertical: 13.5.h),
                    child: Row(
                      children: [
                        Text(
                          "Response Time",
                          style: poppins.copyWith(
                              fontSize: 14.sp, fontWeight: FontWeight.w500),
                        ),
                        const Spacer(),
                        Text(
                          "48 hrs",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w400,
                              color: AppColor.greyHint),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 21.w),
                  child: GestureDetector(
                    onTap: () {
                      Review.responseTime = 1;
                      Navigator.pop(context);
                      CreateEventBottomSheets.setLimit(context);
                    },
                    child: SizedBox(
                        width: 20.w,
                        height: 20.7.r,
                        child: SvgPicture.asset(ImagesApp.editText,
                            fit: BoxFit.cover)),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 16.w, left: 16.w),
            padding: EdgeInsets.symmetric(horizontal: 11.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.r),
              color: AppColor.white.withOpacity(0.07),
            ),
            width: double.infinity,
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 15.5.h),
                  child: Text(
                    "Offer Food & Beverage?",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                const Spacer(),
                CustomSwitch(onToggled: (isToggled) {}, isToggled: true)
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 30.w, right: 30.w, top: 10.h, bottom: 10.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Vibe",
                  style: poppins.copyWith(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Review.vibe = 1;
                    Navigator.pop(context);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SelectVibeCreateEventPage()));
                  },
                  child: SizedBox(
                      width: 20.w,
                      height: 20.7.r,
                      child: SvgPicture.asset(ImagesApp.editText,
                          fit: BoxFit.cover)),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 30.w, left: 15.w, bottom: 8.h),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    padding: EdgeInsets.symmetric(
                        horizontal: 11.w, vertical: 13.5.h),
                    child: Row(
                      children: [
                        Text(
                          "Event photo",
                          style: poppins.copyWith(
                              fontSize: 14.sp, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 21.w),
                  child: SizedBox(
                      width: 20.w,
                      height: 20.7.r,
                      child: SvgPicture.asset(ImagesApp.editText,
                          fit: BoxFit.cover)),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 16.w, left: 16.w),
            padding: EdgeInsets.symmetric(horizontal: 11.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.r),
              color: AppColor.white.withOpacity(0.07),
            ),
            width: double.infinity,
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 15.5.h),
                  child: Text(
                    "Create as gig post",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                const Spacer(),
                CustomSwitch(onToggled: (isToggled) {}, isToggled: true)
              ],
            ),
          ),
          SizedBox(height: 6.h),
          Padding(
            padding: EdgeInsets.only(left: 29.w),
            child: Text(
              "Age limit",
              style:
                  poppins.copyWith(fontSize: 13.sp, color: AppColor.greyText),
            ),
          ),
          // Padding(
          //   padding: EdgeInsets.only(
          //       left: 37.w, right: 68.w, bottom: 13.h),
          //   child: BlocBuilder(
          //       bloc: changeItemBloc,
          //       builder: (context, sts) {
          //         if (sts is ItemChangeState) {
          //           return Row(
          //             children: [
          //               GestureDetector(
          //                 onTap: () => changeItemBloc.changeItem(0),
          //                 child: Row(
          //                   children: [
          //                     Container(
          //                       width: 15.w,
          //                       height: 15.r,
          //                       decoration: BoxDecoration(
          //                         border: Border.all(
          //                           width: 1.w,
          //                           color: sts.index == 0
          //                               ? AppColor.greyBlue3
          //                               : AppColor.black48,
          //                         ),
          //                         shape: BoxShape.circle,
          //                       ),
          //                       child: Center(
          //                         child: Container(
          //                           width: 9.w,
          //                           height: 9.r,
          //                           decoration: BoxDecoration(
          //                             color: sts.index == 0
          //                                 ? AppColor.greyBlue3
          //                                 : Colors.transparent,
          //                             shape: BoxShape.circle,
          //                           ),
          //                         ),
          //                       ),
          //                     ),
          //                     SizedBox(
          //                       width: 8.w,
          //                     ),
          //                     Text(
          //                       "No Age Limit",
          //                       style: poppins.copyWith(
          //                         fontSize: 12.sp,
          //                         fontWeight: FontWeight.bold,
          //                       ),
          //                     ),
          //                   ],
          //                 ),
          //               ),
          //               const Spacer(),
          //               Row(
          //                 children: [
          //                   GestureDetector(
          //                     onTap: () =>
          //                         changeItemBloc.changeItem(1),
          //                     child: Row(
          //                       children: [
          //                         Container(
          //                           width: 15.w,
          //                           height: 15.r,
          //                           decoration: BoxDecoration(
          //                             border: Border.all(
          //                               width: 1.w,
          //                               color: sts.index == 1
          //                                   ? AppColor.greyBlue3
          //                                   : AppColor.black48,
          //                             ),
          //                             shape: BoxShape.circle,
          //                           ),
          //                           child: Center(
          //                             child: Container(
          //                               width: 9.w,
          //                               height: 9.r,
          //                               decoration: BoxDecoration(
          //                                 color: sts.index == 1
          //                                     ? AppColor.greyBlue3
          //                                     : Colors.transparent,
          //                                 shape: BoxShape.circle,
          //                               ),
          //                             ),
          //                           ),
          //                         ),
          //                         SizedBox(
          //                           width: 8.w,
          //                         ),
          //                         Text(
          //                           "18+",
          //                           style: poppins.copyWith(
          //                             fontSize: 12.sp,
          //                             fontWeight: FontWeight.bold,
          //                           ),
          //                         ),
          //                       ],
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     width: 8.w,
          //                   ),
          //                   GestureDetector(
          //                     onTap: () =>
          //                         changeItemBloc.changeItem(2),
          //                     child: Row(
          //                       children: [
          //                         Container(
          //                           width: 15.w,
          //                           height: 15.r,
          //                           decoration: BoxDecoration(
          //                             border: Border.all(
          //                               width: 1.w,
          //                               color: sts.index == 2
          //                                   ? AppColor.greyBlue3
          //                                   : AppColor.black48,
          //                             ),
          //                             shape: BoxShape.circle,
          //                           ),
          //                           child: Center(
          //                             child: Container(
          //                               width: 9.w,
          //                               height: 9.r,
          //                               decoration: BoxDecoration(
          //                                 color: sts.index == 2
          //                                     ? AppColor.greyBlue3
          //                                     : Colors.transparent,
          //                                 shape: BoxShape.circle,
          //                               ),
          //                             ),
          //                           ),
          //                         ),
          //                         SizedBox(
          //                           width: 8.w,
          //                         ),
          //                         Text(
          //                           "21+",
          //                           style: poppins.copyWith(
          //                             fontSize: 12.sp,
          //                             fontWeight: FontWeight.bold,
          //                           ),
          //                         ),
          //                       ],
          //                     ),
          //                   ),
          //                 ],
          //               ),
          //             ],
          //           );
          //         }
          //         return const SizedBox();
          //       }),
          // ),
          SizedBox(height: 85.h),
        ],
      ),
    );
  }
}
