import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:bottom_picker/bottom_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/dat.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/logic/view_model/auth_register_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_send_create_event_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/widget/create_event_bottom_sheet.dart';
import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';

class CreateEventDetailsVenuePage extends StatefulWidget {
  static String dateAndTime = "";
  static TextEditingController textCntEventTitle = TextEditingController();
  static TextEditingController textCntEventAbout = TextEditingController();
  static String limitAge = "No Age Limit";
  static List<Users> lsUsers = [];
  static List<String> imageSelectPreformance = [];
  static List<String> nameSelectPreformance = [];
  static List<int> idSelectPreformance = [];
  static String timeLimit = "";
  static List<int> selectItemVibe = [];
  static String avatar = "";
  static File? m;
  static TextEditingController textCntEventTicket =
      TextEditingController(text: "");
  static TextEditingController textCntEventPrice =
      TextEditingController(text: "");
  static String time = "";

  static TextEditingController textCntStreet = TextEditingController(text: "");
  static TextEditingController textCntCity = TextEditingController(text: "");
  static TextEditingController textCntState = TextEditingController(text: "");
  static TextEditingController textCntZipCode = TextEditingController(text: "");
  static TextEditingController textCntCountry = TextEditingController(text: "");
  static TextEditingController textCntOptional =
      TextEditingController(text: "");
  static int? idAddress;

  const CreateEventDetailsVenuePage();

  @override
  State<CreateEventDetailsVenuePage> createState() =>
      _CreateEventDetailsVenuePageState();
}

class _CreateEventDetailsVenuePageState
    extends State<CreateEventDetailsVenuePage> {
  final viewModelRegister = LoginSignupViewModel();
  final viewChaneCheckBox = SelectItemViewModel();
  var dateNow = DateTime.now().hour;
  late StreamController<DateTime> _weekDatesController;

  static final DateTime _focusedDay = DateTime.now();

  static DateTime? _selectedDate;

  Map<String, List<String>> mySelectedEvents = {};

  loadPreviousEvents() {
    mySelectedEvents = {
      "2023-07-05": [
        "cdcd",
      ],
      "2023-07-19": [
        "cdcd",
      ],
      "2023-07-29": [
        "cdcd",
      ]
    };
  }

  List _listOfDayEvents(DateTime dateTime) {
    if (mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)] != null) {
      return mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)]!;
    } else {
      return [];
    }
  }

  List<DateTime> weekDates = [];

  @override
  void initState() {
    super.initState();
    CreateEventDetailsVenuePage.dateAndTime =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
    CreateEventDetailsVenuePage.time =
        DateFormat('HH:mm:ss').format(DateTime.now());
    _selectedDate = _focusedDay;
    loadPreviousEvents();
    DateTime now = DateTime.now();
    int weekday = now.weekday;

    // Calculate the start date of the current week
    DateTime startDate = now.subtract(Duration(days: weekday - 1));
    _weekDatesController = StreamController<DateTime>.broadcast();

    Timer.periodic(const Duration(days: 1), (timer) {
      DateTime currentDate = DateTime.now();
      if (currentDate.weekday == 1 && currentDate.day != startDate.day) {
        startDate = currentDate;
        _weekDatesController.add(startDate);
      }
    });

    // Generate a list of dates for the week
    for (int i = 0; i < 7; i++) {
      DateTime date = startDate.add(Duration(days: i));
      weekDates.add(date);
    }
  }

  @override
  void dispose() {
    _weekDatesController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Calendar",
                      style: poppins.copyWith(
                          fontSize: 16.sp, color: AppColor.white),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              SizedBox(
                height: 31.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16.w),
                child: Text(
                  "Event Details",
                  style: poppins.copyWith(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                      color: AppColor.white),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 28.h, bottom: 12.h),
                          child: Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller:
                                  CreateEventDetailsVenuePage.textCntEventTitle,
                              hint: "Event Name*",
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 12.h),
                          child: GestureDetector(
                            onTap: () {
                              selectTime();
                            },
                            child: Container(
                              width: 319.w,
                              height: 49.h,
                              padding: EdgeInsets.symmetric(horizontal: 11.w),
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(10.r),
                              ),
                              child: Row(
                                children: [
                                  SvgPicture.asset(ImagesApp.icWatch),
                                  SizedBox(width: 8.w),
                                  Text(
                                    "Date and time",
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.white,
                                    ),
                                  ),
                                  const Spacer(),
                                  Text(
                                    CreateEventDetailsVenuePage.dateAndTime,
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.grey1,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: AppColor.white,
                                    size: 13,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            CreateEventBottomSheets.showMap(context);
                          },
                          child: Container(
                              width: 319.w,
                              height: 49.h,
                              padding: EdgeInsets.symmetric(horizontal: 11.w),
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(10.r),
                              ),
                              child: Row(
                                children: [
                                  SvgPicture.asset(ImagesApp.icMiniLocation),
                                  SizedBox(width: 8.w),
                                  Text(
                                    "Location",
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.white,
                                    ),
                                  ),
                                  const Spacer(),
                                  Text(
                                    "${CreateEventDetailsVenuePage.textCntCity.text},${CreateEventDetailsVenuePage.textCntStreet.text}",
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.grey1,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: AppColor.white,
                                    size: 13,
                                  ),
                                ],
                              )),
                        ),
                        SizedBox(height: 12.h),
                        Text(
                          "About this event",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            color: AppColor.grey1,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8.h, bottom: 16.h),
                          child: Container(
                            width: 319.w,
                            height: 149.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller:
                                  CreateEventDetailsVenuePage.textCntEventAbout,
                              type: TextInputType.multiline,
                              line: 100,
                              hintStyle: poppins.copyWith(
                                  color: AppColor.grey81, fontSize: 11.sp),
                              hint:
                                  "Join us at Music Fest and let the music take you on a journey. This show is all about exploring the various genres and styles of music and finding the connections that bring them together.",
                            ),
                          ),
                        ),
                        Text(
                          "Age limit",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        SizedBox(
                          height: 15.h,
                        ),
                        BlocBuilder(
                            bloc: viewChaneCheckBox,
                            builder: (context, state) {
                              if (state is ItemChangeState) {
                                return Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        viewChaneCheckBox.changeItem(0);
                                        CreateEventDetailsVenuePage.limitAge =
                                            "No Age Limit";
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 22.w,
                                            height: 22.r,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1.w,
                                                color: state.index == 0
                                                    ? AppColor.greyBlue3
                                                    : AppColor.black48,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 14.w,
                                                height: 14.r,
                                                decoration: BoxDecoration(
                                                  color: state.index == 0
                                                      ? AppColor.greyBlue3
                                                      : Colors.transparent,
                                                  shape: BoxShape.circle,
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          Text(
                                            "No Age Limit",
                                            style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Spacer(),
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            viewChaneCheckBox.changeItem(1);
                                            CreateEventDetailsVenuePage
                                                .limitAge = "+18";
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 22.w,
                                                height: 22.r,
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1.w,
                                                    color: state.index == 1
                                                        ? AppColor.greyBlue3
                                                        : AppColor.black48,
                                                  ),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                  child: Container(
                                                    width: 14.w,
                                                    height: 14.r,
                                                    decoration: BoxDecoration(
                                                      color: state.index == 1
                                                          ? AppColor.greyBlue3
                                                          : Colors.transparent,
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8.w,
                                              ),
                                              Text(
                                                "18+",
                                                style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            viewChaneCheckBox.changeItem(2);
                                            CreateEventDetailsVenuePage
                                                .limitAge = "+21";
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 22.w,
                                                height: 22.r,
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1.w,
                                                    color: state.index == 2
                                                        ? AppColor.greyBlue3
                                                        : AppColor.black48,
                                                  ),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                  child: Container(
                                                    width: 14.w,
                                                    height: 14.r,
                                                    decoration: BoxDecoration(
                                                      color: state.index == 2
                                                          ? AppColor.greyBlue3
                                                          : Colors.transparent,
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8.w,
                                              ),
                                              Text(
                                                "21+",
                                                style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }
                              return const SizedBox();
                            })
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 84.h,
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.01),
        ),
        child: Center(
            child: MyCustomButton(
          title: "Continue",
          loading: false,
          onTap: () {
            if (Review.title == 1 ||
                Review.dateTime == 1 ||
                Review.location == 1) {
              Review.title = 0;
              Review.dateTime = 0;
              Review.location = 0;
              CreateEventBottomSheets.reviewYourRequest(context);
              print(Review.title);
              print(Review.dateTime);
              print(Review.location);
            } else {
              CreateEventBottomSheets.selectPreforms(context);
            }
          },
        )),
      ),
    );
  }

  void selectTime() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 160.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              ListView(
                reverse: true,
                children: [
                  ClipRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 15,
                        sigmaY: 15,
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            border: BorderDirectional(
                              top: BorderSide(
                                width: 1,
                                color: AppColor.grey1.withOpacity(0.4),
                              ),
                            )),
                        height: 84.h,
                        child: Center(
                          child: MyCustomButton(
                            title: "Continue",
                            loading: false,
                            onTap: () {
                              setState(() {
                                CreateEventDetailsVenuePage.dateAndTime =
                                    "${Da.years}-${Da.month}-${Da.day} ${CreateEventDetailsVenuePage.time}";
                                print(CreateEventDetailsVenuePage.dateAndTime);
                              });
                              Navigator.pop(context);
                            },
                            color: AppColor.blue,
                            width: 319.w,
                            height: 49.h,
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                  BottomPicker.time(
                    title: '',
                    titleStyle: const TextStyle(fontSize: 0),
                    backgroundColor: Colors.transparent,
                    height: 300,
                    onSubmit: (date) {
                      print(date);
                    },
                    onChange: (date) {
                      CreateEventDetailsVenuePage.time =
                          DateFormat('HH:mm:ss').format(date);
                    },
                    pickerTextStyle: poppins.copyWith(
                      fontSize: 23.sp,
                      color: AppColor.white.withOpacity(0.5),
                    ),
                    displayButtonIcon: false,
                    displaySubmitButton: false,
                    displayCloseIcon: false,
                    initialDateTime: DateTime(2021, 5, 1),
                  ),
                  const Da(),
                  SizedBox(height: 25.h),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Select the time and date of the event",
                      style: poppins.copyWith(
                          fontSize: 14.sp, color: AppColor.greyHint),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Date & Time",
                      style: poppins.copyWith(
                          fontSize: 28.sp, color: AppColor.white),
                    ),
                  ),
                  SizedBox(height: 25.h),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
