import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/response_model/get_address_map_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class GetAddressBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetAddressInitialState extends GetAddressBaseState {}

class GetAddressLoadingState extends GetAddressBaseState {}

class GetAddressSuccessState extends GetAddressBaseState {
final GetResponseAddressModel getResponseAddressModel;

  GetAddressSuccessState({required this.getResponseAddressModel});
}

class GetAddressFailState extends GetAddressBaseState {
  final String message;


  GetAddressFailState({required this.message});
}
