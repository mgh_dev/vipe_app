import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendCreateEventBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendCreateEventInitialState extends SendCreateEventBaseState {}

class SendCreateEventLoadingState extends SendCreateEventBaseState {}

class SendCreateEventSuccessState extends SendCreateEventBaseState {

}

class SendCreateEventFailState extends SendCreateEventBaseState {
  final String message;


  SendCreateEventFailState({required this.message});
}
