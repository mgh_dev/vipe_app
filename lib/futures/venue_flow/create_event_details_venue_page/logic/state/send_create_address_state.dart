import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/response_model/response_create_address_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendCreateAddressBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendCreateAddressInitialState extends SendCreateAddressBaseState {}

class SendCreateAddressLoadingState extends SendCreateAddressBaseState {}

class SendCreateAddressSuccessState extends SendCreateAddressBaseState {
  final ResponseCreateAddressModel responseCreateAddressModel;

  SendCreateAddressSuccessState({required this.responseCreateAddressModel});

}

class SendCreateAddressFailState extends SendCreateAddressBaseState {
  final String message;


  SendCreateAddressFailState({required this.message});
}
