import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_address_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_event_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_send_create_event_model.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/response_model/get_address_map_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

import '../../../../../core/config/web_service.dart';
import '../state/get_address_state.dart';

class GetAddressViewModel extends Cubit<GetAddressBaseState> {
  GetAddressViewModel() : super(GetAddressInitialState());

  Future getAddress({required var lat, required var long}) async {
    print("1");
    emit(GetAddressLoadingState());
    print("3");
    try {

      print("4");
      var response =
          await WebService().dio.get("https://nominatim.openstreetmap.org/reverse?lat=37.370664647047484&lon=-122.12958588466643&format=json",);
      print("5");
GetResponseAddressModel responseAddressModel=GetResponseAddressModel.fromJson(response.data);
      print("6");

      print("7");
      emit(GetAddressSuccessState(getResponseAddressModel:responseAddressModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetAddressFailState(message: e.toString()));
      print("10");
    }
  }
}
