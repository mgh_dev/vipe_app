import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_event_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_send_create_event_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

import '../../../../../core/config/web_service.dart';

class SendCreateEventViewModel extends Cubit<SendCreateEventBaseState> {
  SendCreateEventViewModel() : super(SendCreateEventInitialState());

  Future send({
    required String eventTitle,
    required int idAddress,
    required String calendar,
    required String avatar,
    required String ageLimit,
    required int offerFoodAndBeverage,
    required String ticketLink,
    required String ticketPrice,
    required int respondExpireTime,
    required int asGigPost,
    required int amount,
    required String additionalInformation,
    required List<Users> users,
    required List<int> vibe,
  }) async {
    FormData formData = FormData.fromMap({
      "title": eventTitle,
      "addressId": idAddress,
      "calendar": calendar,
      "image": await MultipartFile.fromFile(avatar),
      "ageLimit": ageLimit,
      "offerFoodAndBeverage": offerFoodAndBeverage,
      "ticketLink": ticketLink,
      "ticketPrice": ticketPrice,
      "respondExpireTime": respondExpireTime,
      "asGigPost": asGigPost,
      "amount": amount,
      "additionalInformation": additionalInformation,
      "users": users.map((e)=>e.toJson()).toList(),
      "vibes": [vibe.map((e) => e).toList()]
    });
    print("1");
    emit(SendCreateEventLoadingState());
    print("3");
    try {

      print("4");
      var response =
          await WebService().dio.post("events", data: formData);
      print("5");

      print("6");

      print("7");
      emit(SendCreateEventSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(SendCreateEventFailState(message: e.toString()));
      print("10");
    }
  }
}
