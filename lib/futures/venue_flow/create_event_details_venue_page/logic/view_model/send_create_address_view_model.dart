import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_address_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/logic/state/send_create_event_state.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/model/request_model/request_create_address_event_model.dart';

import '../../../../../core/config/web_service.dart';
import '../../model/response_model/response_create_address_model.dart';

class SendCreateAddressViewModel extends Cubit<SendCreateAddressBaseState> {
  SendCreateAddressViewModel() : super(SendCreateAddressInitialState());

  Future send(
      {required RequestCreateAddressEventModel
          requestCreateAddressEventModel}) async {
    print("1");
    emit(SendCreateAddressLoadingState());
    print("3");
    try {
      var data = requestCreateAddressEventModel.toJson();
      print("4");
      var response = await WebService().dio.post("users/addresses", data: data);
      ResponseCreateAddressModel responseCreateAddressModel =
          ResponseCreateAddressModel.fromJson(response.data);
      print("5");

      print("6");

      print("7");
      emit(
        SendCreateAddressSuccessState(
          responseCreateAddressModel: responseCreateAddressModel,
        ),
      );
      print("8");
    } catch (e) {
      print("9");
      emit(SendCreateAddressFailState(message: e.toString()));
      print("10");
    }
  }
}
