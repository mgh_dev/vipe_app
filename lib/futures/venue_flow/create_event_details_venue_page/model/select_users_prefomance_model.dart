class SelectUsersPrefomanceModel {
  late final int id;
  late final String name;
  late final String avatar;

  SelectUsersPrefomanceModel({
    required this.id,
    required this.name,
    required this.avatar,
  });
}
