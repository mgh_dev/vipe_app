class RequestSendCreateEventModels {
  RequestSendCreateEventModels({
    required this.title,
    required this.addressId,
    required this.calendar,
    required this.image,
    required this.ageLimit,
    required this.offerFoodAndBeverage,
    required this.ticketLink,
    required this.ticketPrice,
    required this.respondExpireTime,
    required this.asGigPost,
    required this.amount,
    required this.additionalInformation,
    required this.users,
    required this.vibes,
  });
  late final String title;
  late final int addressId;
  late final String calendar;
  late final String image;
  late final String ageLimit;
  late final int offerFoodAndBeverage;
  late final String ticketLink;
  late final String ticketPrice;
  late final int respondExpireTime;
  late final int asGigPost;
  late final int amount;
  late final String additionalInformation;
  late final List<Users> users;
  late final List<int> vibes;

  RequestSendCreateEventModels.fromJson(Map<String, dynamic> json){
    title = json['title'];
    addressId = json['addressId'];
    calendar = json['calendar'];
    image = json['image'];
    ageLimit = json['ageLimit'];
    offerFoodAndBeverage = json['offerFoodAndBeverage'];
    ticketLink = json['ticketLink'];
    ticketPrice = json['ticketPrice'];
    respondExpireTime = json['respondExpireTime'];
    asGigPost = json['asGigPost'];
    amount = json['amount'];
    additionalInformation = json['additionalInformation'];
    users = List.from(json['users']).map((e)=>Users.fromJson(e)).toList();
    vibes = List.castFrom<dynamic, int>(json['vibes']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['addressId'] = addressId;
    _data['calendar'] = calendar;
    _data['image'] = image;
    _data['ageLimit'] = ageLimit;
    _data['offerFoodAndBeverage'] = offerFoodAndBeverage;
    _data['ticketLink'] = ticketLink;
    _data['ticketPrice'] = ticketPrice;
    _data['respondExpireTime'] = respondExpireTime;
    _data['asGigPost'] = asGigPost;
    _data['amount'] = amount;
    _data['additionalInformation'] = additionalInformation;
    _data['users'] = users.map((e)=>e.toJson()).toList();
    _data['vibes'] = vibes;
    return _data;
  }
}

class Users {
  Users({
    required this.id,
    required this.amount,
    required this.additionalInformation,
  });
  late final int id;
  late final int amount;
  late final String additionalInformation;

  Users.fromJson(Map<String, dynamic> json){
    id = json['id'];
    amount = json['amount'];
    additionalInformation = json['additionalInformation'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['amount'] = amount;
    _data['additionalInformation'] = additionalInformation;
    return _data;
  }
}