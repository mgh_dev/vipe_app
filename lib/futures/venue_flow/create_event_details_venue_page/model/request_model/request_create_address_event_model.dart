class RequestCreateAddressEventModel {
  RequestCreateAddressEventModel({
    required this.address,
    required this.longitude,
    required this.latitude,
    required this.city,
    required this.state,
    required this.country,
    required this.postalCode,
  });
  late final String address;
  late final double longitude;
  late final double latitude;
  late final String city;
  late final String state;
  late final String country;
  late final String postalCode;

  RequestCreateAddressEventModel.fromJson(Map<String, dynamic> json){
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    postalCode = json['postalCode'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['address'] = address;
    _data['longitude'] = longitude;
    _data['latitude'] = latitude;
    _data['city'] = city;
    _data['state'] = state;
    _data['country'] = country;
    _data['postalCode'] = postalCode;
    return _data;
  }
}