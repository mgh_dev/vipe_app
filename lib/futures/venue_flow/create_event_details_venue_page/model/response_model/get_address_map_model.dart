class GetResponseAddressModel {
  GetResponseAddressModel({
    required this.placeId,
    required this.licence,
    required this.osmType,
    required this.osmId,
    required this.lat,
    required this.lon,
    required this.displayName,
    required this.address,
    required this.boundingbox,
  });
  late final int placeId;
  late final String licence;
  late final String osmType;
  late final int osmId;
  late final String lat;
  late final String lon;
  late final String displayName;
  late final Address address;
  late final List<String> boundingbox;

  GetResponseAddressModel.fromJson(Map<String, dynamic> json){
    placeId = json['place_id'];
    licence = json['licence'];
    osmType = json['osm_type'];
    osmId = json['osm_id'];
    lat = json['lat'];
    lon = json['lon'];
    displayName = json['display_name'];
    address = Address.fromJson(json['address']);
    // boundingbox = List.castFrom<dynamic, String>(json['boundingbox']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['place_id'] = placeId;
    _data['licence'] = licence;
    _data['osm_type'] = osmType;
    _data['osm_id'] = osmId;
    _data['lat'] = lat;
    _data['lon'] = lon;
    _data['display_name'] = displayName;
    _data['address'] = address.toJson();
    _data['boundingbox'] = boundingbox;
    return _data;
  }
}

class Address {
  Address({
    required this.building,
    required this.road,
    required this.city,
    required this.county,
    required this.stateDistrict,
    required this.state,
    required this.Is,
    required this.postcode,
    required this.country,
    required this.countryCode,
  });
  late final String building;
  late final String road;
  late final String city;
  late final String county;
  late final String stateDistrict;
  late final String state;
  late final String Is;
  late final String postcode;
  late final String country;
  late final String countryCode;

  Address.fromJson(Map<String, dynamic> json){
    // building = json['building'];
    // road = json['road'];
    // city = json['city'];
    // county = json['county'];
    // stateDistrict = json['state_district'];
    // state = json['state'];
    // Is = json['ISO3166-2-lvl4'];
    // postcode = json['postcode'];
    country = json['country'];
    // countryCode = json['country_code'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['building'] = building;
    _data['road'] = road;
    _data['city'] = city;
    _data['county'] = county;
    _data['state_district'] = stateDistrict;
    _data['state'] = state;
    _data['ISO3166-2-lvl4'] = Is;
    _data['postcode'] = postcode;
    _data['country'] = country;
    _data['country_code'] = countryCode;
    return _data;
  }
}