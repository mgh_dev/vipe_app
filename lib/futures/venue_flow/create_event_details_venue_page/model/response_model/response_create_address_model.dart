class ResponseCreateAddressModel {
  ResponseCreateAddressModel({
    required this.message,
    required this.data,
  });
  late final String message;
  late final Data data;

  ResponseCreateAddressModel.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.address,
    required this.longitude,
    required this.latitude,
    required this.city,
    required this.state,
    required this.country,
    required this.postalCode,
    required this.userId,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });
  late final String address;
  late final double longitude;
  late final double latitude;
  late final String city;
  late final String state;
  late final String country;
  late final String postalCode;
  late final int userId;
  late final String updatedAt;
  late final String createdAt;
  late final int id;

  Data.fromJson(Map<String, dynamic> json){
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    postalCode = json['postalCode'];
    userId = json['userId'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['address'] = address;
    _data['longitude'] = longitude;
    _data['latitude'] = latitude;
    _data['city'] = city;
    _data['state'] = state;
    _data['country'] = country;
    _data['postalCode'] = postalCode;
    _data['userId'] = userId;
    _data['updatedAt'] = updatedAt;
    _data['createdAt'] = createdAt;
    _data['id'] = id;
    return _data;
  }
}