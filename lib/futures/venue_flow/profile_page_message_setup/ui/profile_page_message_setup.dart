import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';
import 'package:vibe_app/menu.dart';

import '../../notifications_list/notifications_list.dart';

class ProfilePageMessageSetupVenus extends StatelessWidget {
  const ProfilePageMessageSetupVenus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      drawer: Menussss(),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Builder(
                      builder: (context) => // Ensure Scaffold is in context
                          GestureDetector(
                        onTap: () {
                          Scaffold.of(context).openDrawer();
                        },
                        child: SvgPicture.asset(ImagesApp.icMenu),
                      ),
                    ),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NotificationsList()));
                        },
                        child: SvgPicture.asset(ImagesApp.icNotification)),
                  ],
                ),
              ),
              Expanded(
                  child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 23.w),
                      child: Stack(
                        alignment: Alignment.bottomLeft,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20.r),
                            child: Image(
                              fit: BoxFit.fitWidth,
                              image: AssetImage(
                                ImagesApp.createProfile,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 23.h, left: 23.w),
                            child: GestureDetector(
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.r),
                                    color: AppColor.white),
                                height: 41.h,
                                width: 151.w,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    SvgPicture.asset(ImagesApp.icPlay),
                                    const Text("Watch Full Video"),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 23.w, top: 23),
                      child: Text(
                        "Create your profile \n in  7 easy steps.",
                        style: poppins.copyWith(
                          fontSize: 24.sp,
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
              ))
            ],
          )
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 17.5.h),
        color: AppColor.black48.withOpacity(0.25),
        child: MyCustomButton(
          title: "Next",
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => CreateProfileVenuePage()));
          },
          loading: false,
          color: AppColor.blue,
          width: 319.w,
          height: 49.h,
          fontSize: 14.sp,
        ),
      ),
    );
  }
}
