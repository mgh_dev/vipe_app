import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/constant_bloc/upload_file_logic/state/upload_file_state.dart';
import 'package:vibe_app/constant_bloc/upload_file_logic/view_model/upload_file_view_model.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_information_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_vibe_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/steps/add_about_and_select_type.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/steps/add_contact_information.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/steps/add_name_and_location.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/steps/upload_avatar_and_base_photo.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/page_view/stepper_page_view.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../../menu.dart';
import '../../notifications_list/notifications_list.dart';
import '../steps/select_business_type_step.dart';
import '../../profile_venus_page/ui/profile_venus_page.dart';
import '../steps/add_details_step.dart';
import '../steps/select_vibe_step.dart';

class CreateProfileVenuePage extends StatefulWidget {
  static String? imageAvatar;

  static String equipment = "";
  static String ageLimit = "No Age Limit";
  static String additionalDetailedInformation = "";
  static int role = 0;

  static String nameStage = "";

  static List<String> lsImageGallery = [];
  static List<int> lsVibe = [];
  static List<Links> lsLink = [];
  static TextEditingController textCntTiktok = TextEditingController(text: "");
  static TextEditingController textCnTwitter = TextEditingController(text: "");
  static TextEditingController textCntYouTub = TextEditingController(text: "");
  static TextEditingController textCntWeb = TextEditingController(text: "");
  static TextEditingController textCntNumberOfStage =
      TextEditingController(text: "");
  static TextEditingController textCntVenueCapacity =
      TextEditingController(text: "");
  static TextEditingController textCntAdditionalDetail =
      TextEditingController(text: "");
  static TextEditingController textCntBusinessName = TextEditingController();
  static TextEditingController about = TextEditingController(text: "");

  CreateProfileVenuePage() : super();

  @override
  State<CreateProfileVenuePage> createState() => _CreateProfileVenuePageState();
}

class _CreateProfileVenuePageState extends State<CreateProfileVenuePage> {
  final UploadAvatarViewModel uploadAvatarViewModel = UploadAvatarViewModel();
  final UploadFileViewModel uploadFileViewModel = UploadFileViewModel();
  final SendInformationViewModel createProfileViewModel =
      SendInformationViewModel();
  final SendVibeViewModel sendVibeViewModel = SendVibeViewModel();

  PageController? controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = PageController(initialPage: 0);
  }

  bool isLoadingAvatar = false;
  bool isLoadingFile = false;
  bool isLoadingSendData = false;
  bool isLoadingSendVibe = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Menussss(),
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 53.h, left: 20.w, bottom: 33.h, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Builder(
                      builder: (context) => // Ensure Scaffold is in context
                          GestureDetector(
                        onTap: () {
                          Scaffold.of(context).openDrawer();
                        },
                        child: SvgPicture.asset(ImagesApp.icMenu),
                      ),
                    ),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NotificationsList()));
                        },
                        child: SvgPicture.asset(ImagesApp.icNotification)),
                  ],
                ),
              ),
              Expanded(
                child: StepperPageView(
                  pageController: controller,
                  physics: const NeverScrollableScrollPhysics(),
                  pageSteps: [
                    PageStep(
                      title: const Text('Step 1'),
                      content: SelectBusinessType(),
                    ),
                    PageStep(
                      title: const SizedBox(),
                      content: AddNameAndLocationStep(),
                    ),
                    PageStep(
                      title: const Text('Step 2'),
                      content: UploadAvatarAndBasePhoto(),
                    ),
                    PageStep(
                      title: const Text('Step 3'),
                      content: AddAboutAndSelectTypeStep(),
                    ),
                    PageStep(
                      title: const Text('Step 4'),
                      content: SelectVibe(),
                    ),
                    PageStep(
                      title: const Text('Step 5'),
                      content: AddContactInformationStep(),
                    ),
                    PageStep(
                      title: const Text('Step 6'),
                      content: AddDetailsStep(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 15,
            sigmaY: 15,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                border: BorderDirectional(
                  top: BorderSide(
                    width: 1,
                    color: AppColor.grey1.withOpacity(0.4),
                  ),
                )),
            height: 84.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // Navigator.pushReplacement(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => const ProfileVenusPage()));
                    print(isLoadingAvatar);
                    print(isLoadingFile);
                    print(isLoadingSendVibe);
                    print(isLoadingSendData);
                  },
                  child: Container(
                    width: 165.w,
                    height: 49.h,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: AppColor.grey1.withOpacity(0.4),
                      ),
                      borderRadius: BorderRadius.circular(50.r),
                    ),
                    child: Center(
                      child: Text(
                        "Skip",
                        style: poppins.copyWith(
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  ),
                ),
                BlocConsumer(
                    bloc: createProfileViewModel,
                    listener: (context, state) {
                      if (state is SendInformationLoadingState) {
                        setState(() {
                          isLoadingSendData = true;
                        });
                      }
                      if (state is SendInformationSuccessState) {
                        setState(() {
                          isLoadingSendData = false;
                        });
                        if ((!isLoadingFile &&
                            !isLoadingAvatar &&
                            !isLoadingSendData &&
                            !isLoadingSendVibe)) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                  const BottomNavVenusPages(
                                    index: 4,
                                  )));
                        }
                      }
                      if (state is SendInformationFailState) {
                        setState(() {
                          isLoadingSendData = false;
                        });
                        CustomToast.show("Error sending information");
                      }
                    },
                    builder: (context, stateCreate) {
                      return BlocConsumer(
                          bloc: uploadAvatarViewModel,
                          listener: (context, stateAvatar) {
                            if (stateAvatar is UploadAvatarLoadingState) {
                              print("<RIDI>");
                              setState(() {
                                isLoadingAvatar = true;
                              });
                            }
                            if (stateAvatar is UploadAvatarSuccessState) {
                              setState(() {
                                print("<RIDI1>");
                                isLoadingAvatar = false;
                              });
                              if ((!isLoadingFile &&
                                  !isLoadingAvatar &&
                                  !isLoadingSendData &&
                                  !isLoadingSendVibe)) {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                        const BottomNavVenusPages(
                                          index: 4,
                                        )));
                              }
                            }
                            if (stateAvatar is UploadAvatarFailState) {
                              setState(() {
                                isLoadingAvatar = false;
                              });
                              CustomToast.show("Avatar Upload Failed");
                            }
                          },
                          builder: (context, stateAvatar) {
                            return BlocConsumer(
                              bloc: uploadFileViewModel,
                              listener: (context, state) {
                                if (state is UploadFileLoadingState) {
                                  print("<NARIDI>");
                                  setState(() {
                                    isLoadingFile = true;
                                  });
                                }
                                if (state is UploadFileSuccessState) {
                                  print("<NARIDI1>");
                                  setState(() {
                                    isLoadingFile = false;
                                  });
                                  if ((!isLoadingFile &&
                                      !isLoadingAvatar &&
                                      !isLoadingSendData &&
                                      !isLoadingSendVibe)) {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const BottomNavVenusPages(
                                              index: 4,
                                            )));
                                  }
                                }
                                if (state is UploadFileFailState) {
                                  setState(() {
                                    isLoadingFile = false;
                                  });
                                  CustomToast.show("Gallery Upload Failed");
                                }
                              },
                              builder: (context, stateFile) {
                                return BlocConsumer(
                                    listener: (ctx, sts) {

                                      if (sts is SendVibeLoadingState) {
                                        setState(() {
                                          isLoadingSendVibe = true;
                                        });
                                      }
                                      if (sts is SendVibeSuccessState) {
                                        setState(() {
                                          isLoadingSendVibe = false;
                                        });
                                        if ((!isLoadingFile &&
                                            !isLoadingAvatar &&
                                            !isLoadingSendData &&
                                            !isLoadingSendVibe)) {
                                          Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                  const BottomNavVenusPages(
                                                    index: 4,
                                                  )));
                                        }
                                      }
                                      if (sts is SendVibeFailState) {
                                        setState(() {
                                          isLoadingSendVibe = false;
                                        });
                                        CustomToast.show("Vibe Upload Failed");
                                      }
                                    },
                                    bloc: sendVibeViewModel,
                                    builder: (context, stateSendVibe) {
                                      return MyCustomButton(
                                        onTap: () {
                                          if (controller!.page == 6) {
                                            if (CreateProfileVenuePage
                                                .textCntWeb.text.isNotEmpty) {
                                              CreateProfileVenuePage.lsLink.add(
                                                  Links(
                                                      title: "WebSite",
                                                      type: "webSite",
                                                      link:
                                                          CreateProfileVenuePage
                                                              .textCntWeb
                                                              .text));
                                            }
                                            if (CreateProfileVenuePage
                                                .textCntTiktok
                                                .text
                                                .isNotEmpty) {
                                              CreateProfileVenuePage.lsLink.add(
                                                  Links(
                                                      title: "Tiktok",
                                                      type: "tiktok",
                                                      link:
                                                          CreateProfileVenuePage
                                                              .textCntTiktok
                                                              .text));
                                            }
                                            if (CreateProfileVenuePage
                                                .textCnTwitter
                                                .text
                                                .isNotEmpty) {
                                              CreateProfileVenuePage.lsLink.add(
                                                  Links(
                                                      title: "Twitter",
                                                      type: "twitter",
                                                      link:
                                                          CreateProfileVenuePage
                                                              .textCnTwitter
                                                              .text));
                                            }
                                            if (CreateProfileVenuePage
                                                .textCntYouTub
                                                .text
                                                .isNotEmpty) {
                                              CreateProfileVenuePage.lsLink.add(
                                                  Links(
                                                      title: "YouTub",
                                                      type: "youTub",
                                                      link:
                                                          CreateProfileVenuePage
                                                              .textCntYouTub
                                                              .text));
                                            }

                                            // print("<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>");
                                            // print(CreateProfileVenuePage.textCntAdditionalDetail.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntVenueCapacity.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntNumberOfStage.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.equipment);
                                            // print("------");
                                            // // print(CreateProfileVenuePage.nameStage);
                                            // // print(CreateProfileVenuePage.additionalDetailedInformation);
                                            // print(CreateProfileVenuePage.ageLimit);
                                            // print("------");
                                            // print(CreateProfileVenuePage.lsLink);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntWeb.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntBusinessName.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntYouTub.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCnTwitter.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.textCntTiktok.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.about.text);
                                            // print("------");
                                            // print(CreateProfileVenuePage.role);
                                            //
                                            // print("<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>");

                                            createProfileViewModel.send(
                                              RequestCreateProfileVenus(
                                                role:
                                                    CreateProfileVenuePage.role,
                                                about: CreateProfileVenuePage
                                                    .about.text,
                                                links: CreateProfileVenuePage
                                                    .lsLink,
                                                data: DataProfile(
                                                  inHouseEquipment:
                                                      CreateProfileVenuePage
                                                          .equipment,
                                                  numberOfStages: int.parse(
                                                      CreateProfileVenuePage
                                                          .textCntNumberOfStage
                                                          .text),
                                                  venueCapacity: int.parse(
                                                      CreateProfileVenuePage
                                                          .textCntVenueCapacity
                                                          .text),
                                                  ageLimit:
                                                      CreateProfileVenuePage
                                                          .ageLimit,
                                                  additionalDetailedInformation:
                                                      CreateProfileVenuePage
                                                          .textCntAdditionalDetail
                                                          .text,
                                                  stageName:
                                                      CreateProfileVenuePage
                                                          .textCntBusinessName
                                                          .text,
                                                ),
                                              ),
                                            );
                                            uploadAvatarViewModel.uploadAvatar(
                                                CreateProfileVenuePage
                                                    .imageAvatar!);
                                            uploadFileViewModel
                                                .uploadImageGallery(
                                                    CreateProfileVenuePage
                                                        .lsImageGallery);
                                            sendVibeViewModel.send(
                                                CreateProfileVenuePage.lsVibe);
                                          } else {
                                            controller!.nextPage(
                                                duration: const Duration(
                                                    milliseconds: 500),
                                                curve: Curves.ease);
                                          }
                                        },
                                        title: "Next",
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.normal,
                                        loading: isLoadingSendData ||
                                            isLoadingSendVibe ||
                                            isLoadingAvatar ||
                                            isLoadingFile,

                                        width: 165.w,
                                        height: 49.h,
                                      );
                                    });
                              },
                            );
                          });
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
