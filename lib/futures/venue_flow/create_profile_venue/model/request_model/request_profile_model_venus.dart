class RequestCreateProfileVenus {
  RequestCreateProfileVenus({
    required this.role,
    required this.about,
    this.links,
    required this.data,
  });

  late final int role;
  late final String about;
  List<Links>? links;
  late final DataProfile data;

  RequestCreateProfileVenus.fromJson(Map<String, dynamic> json) {
    role = json['role'];
    about = json['about'];
    links = List.from(json['links']).map((e) => Links.fromJson(e)).toList();
    data = DataProfile.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['role'] = role;
    _data['about'] = about;
    _data['links'] = links?.map((e) => e.toJson()).toList();
    _data['data'] = data.toJson();
    return _data..removeWhere((key, value) => value == null);
  }
}

class Links {
  Links({
    required this.title,
    required this.type,
    required this.link,
  });

  late final String title;
  late final String type;
  late final String link;

  Links.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    type = json['type'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['type'] = type;
    _data['link'] = link;
    return _data;
  }
}

class DataProfile {
  DataProfile({
    required this.inHouseEquipment,
    required this.numberOfStages,
    required this.venueCapacity,
    required this.ageLimit,
    required this.additionalDetailedInformation,
    required this.stageName,
  });

  late final String inHouseEquipment;
  late final int numberOfStages;
  late final int venueCapacity;
  late final String ageLimit;
  late final String additionalDetailedInformation;
  late final String stageName;

  DataProfile.fromJson(Map<String, dynamic> json) {
    inHouseEquipment = json['inHouseEquipment'];
    numberOfStages = json['numberOfStages'];
    venueCapacity = json['venueCapacity'];
    ageLimit = json['ageLimit'];
    additionalDetailedInformation = json['additionalDetailedInformation'];
    stageName = json['stageName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['inHouseEquipment'] = inHouseEquipment;
    _data['numberOfStages'] = numberOfStages;
    _data['venueCapacity'] = venueCapacity;
    _data['ageLimit'] = ageLimit;
    _data['additionalDetailedInformation'] = additionalDetailedInformation;
    _data['stageName'] = stageName;

    return _data;
  }
}
