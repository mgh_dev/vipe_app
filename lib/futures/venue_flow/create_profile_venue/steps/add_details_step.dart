import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/check_box_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';
import 'package:vibe_app/test_map.dart';
import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class AddDetailsStep extends StatefulWidget {
  AddDetailsStep({Key? key}) : super(key: key);

  @override
  State<AddDetailsStep> createState() => _AddDetailsStepState();
}

class _AddDetailsStepState extends State<AddDetailsStep> {
  final controllerAddDetailedInformation = TextEditingController();
  final numberStage = TextEditingController();
  SelectItemViewModel changeItemBloc = SelectItemViewModel();
  int maxLength = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h, bottom: 10.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: 343.w,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                borderRadius: BorderRadius.circular(16.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(left: 16.w, top: 16.h, bottom: 12.h),
                    child: Text(
                      "Additional Details",
                      style: poppins.copyWith(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  SizedBox(height: 22.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            equipmentBottomSheet();
                          },
                          child: Row(
                            children: [
                              Container(
                                width: 44.w,
                                height: 44.r,
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.07),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset(
                                    ImagesApp.icAddLocation,
                                    width: 24.w,
                                    height: 24.r,
                                  ),
                                ),
                              ),
                              SizedBox(width: 7.w),
                              Text(
                                "In-house equipment",
                                style: poppins.copyWith(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const Spacer(),
                              Text(
                                "Add",
                                style: poppins.copyWith(
                                  fontSize: 14.sp,
                                  color: AppColor.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 18.h),
                        Text(
                          "Number of stages",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          width: 311.w,
                          height: 78.h,
                          decoration: BoxDecoration(
                            color: AppColor.greyText.withOpacity(0.08),
                            borderRadius: BorderRadius.circular(15.r),
                          ),
                          child: CustomTextField(
                            hintTextDirection: TextDirection.ltr,
                            controller: CreateProfileVenuePage.textCntNumberOfStage,
                            fontSize: 32.sp,
                            align: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 12.h),
                        Text(
                          "Venue Capacity",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          width: 311.w,
                          height: 78,
                          decoration: BoxDecoration(
                            color: AppColor.greyText.withOpacity(0.08),
                            borderRadius: BorderRadius.circular(15.r),
                          ),
                          child: CustomTextField(
                            controller: CreateProfileVenuePage.textCntVenueCapacity,
                            fontSize: 32.sp,
                            align: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 12.h),
                        Text(
                          "Age limit",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        BlocBuilder(
                            bloc: changeItemBloc,
                            builder: (context, sts) {
                              if (sts is ItemChangeState) {
                                return Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        changeItemBloc.changeItem(0);
                                        CreateProfileVenuePage.ageLimit =
                                            "No Age Limit";
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 22.w,
                                            height: 22.r,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1.w,
                                                color: sts.index == 0
                                                    ? AppColor.greyBlue3
                                                    : AppColor.black48,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 14.w,
                                                height: 14.r,
                                                decoration: BoxDecoration(
                                                  color: sts.index == 0
                                                      ? AppColor.greyBlue3
                                                      : Colors.transparent,
                                                  shape: BoxShape.circle,
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          Text(
                                            "No Age Limit",
                                            style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Spacer(),
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            changeItemBloc.changeItem(1);
                                            CreateProfileVenuePage.ageLimit =
                                                "+18";
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 22.w,
                                                height: 22.r,
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1.w,
                                                    color: sts.index == 1
                                                        ? AppColor.greyBlue3
                                                        : AppColor.black48,
                                                  ),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                  child: Container(
                                                    width: 14.w,
                                                    height: 14.r,
                                                    decoration: BoxDecoration(
                                                      color: sts.index == 1
                                                          ? AppColor.greyBlue3
                                                          : Colors.transparent,
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8.w,
                                              ),
                                              Text(
                                                "18+",
                                                style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            changeItemBloc.changeItem(2);
                                            CreateProfileVenuePage.ageLimit = "+21";
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 22.w,
                                                height: 22.r,
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1.w,
                                                    color: sts.index == 2
                                                        ? AppColor.greyBlue3
                                                        : AppColor.black48,
                                                  ),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                  child: Container(
                                                    width: 14.w,
                                                    height: 14.r,
                                                    decoration: BoxDecoration(
                                                      color: sts.index == 2
                                                          ? AppColor.greyBlue3
                                                          : Colors.transparent,
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 8.w,
                                              ),
                                              Text(
                                                "21+",
                                                style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }
                              return SizedBox();
                            }),
                        Padding(
                          padding: EdgeInsets.only(top: 20.h, bottom: 12.h),
                          child: const DividerWidget(),
                        ),
                        Text(
                          "Additional Detailed Information\n(Optional)",
                          style: poppins.copyWith(
                              fontSize: 13.sp, color: AppColor.greyText),
                        ),
                        SizedBox(
                          height: 8.w,
                        ),
                        Container(
                          width: 320.w,
                          height: 78.h,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            hintTextDirection: TextDirection.ltr,
                            controller: CreateProfileVenuePage.textCntAdditionalDetail,
                            type: TextInputType.multiline,
                            line: 1000,
                            onChanged: (_) {
                              setState(() {
                                maxLength = CreateProfileVenuePage.textCntAdditionalDetail
                                    .text.length;
                              });
                            },
                            hint: "House of Music",
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8.h, bottom: 16.h),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              "${maxLength.toString()}/300",
                              style: poppins.copyWith(
                                fontSize: 10.sp,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void equipmentBottomSheet() {
    final selectItem = SelectItemViewModel();
    final List<String> ls = [
      "Rok",
      "Pop",
      "Jazz",
      "RnB",
      "Hip-hop",
      "Blues",
      "country",
      "folksy",
      "Classical orchestra",
      "Indie",
      "Heavy",
      "punk",
      "other",
    ];
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 34.h,
                                  left: 16.w,
                                  right: 16.w,
                                  bottom: 19.h),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.07),
                                    borderRadius: BorderRadius.circular(12.r)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 13.h, top: 16.h),
                                      child: Text(
                                        "Select the instruments you play",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                    const DividerWidget(),
                                    SizedBox(
                                      height: 15.h,
                                    ),
                                    BlocBuilder(
                                        bloc: selectItem,
                                        builder: (context, state) {
                                          if (state is ItemChangeState) {
                                            return ListView.builder(
                                              controller: scrollController,
                                              shrinkWrap: true,
                                              itemCount: ls.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 30.h,
                                                      bottom: 30.h,
                                                      left: 28.w,
                                                      right: 28.w),
                                                  child: GestureDetector(
                                                    onTap: (){
                                                      selectItem
                                                          .changeItem(index);
                                                      CreateProfileVenuePage.equipment=ls[index];
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 12.w),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            ls[index],
                                                            style: poppins.copyWith(
                                                                fontSize: 14.sp,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                          const Spacer(),
                                                          Container(
                                                            height: 16.6.h,
                                                            width: 16.6.w,
                                                            decoration:
                                                                BoxDecoration(
                                                              color: state.index ==
                                                                      index
                                                                  ? AppColor
                                                                      .green3
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: state.index ==
                                                                          index
                                                                      ? Colors
                                                                          .transparent
                                                                      : AppColor
                                                                          .grey5b),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50),
                                                            ),
                                                            child: Center(
                                                              child: state.index ==
                                                                      index
                                                                  ? Icon(
                                                                      Icons
                                                                          .check,
                                                                      size: 16,
                                                                      color: AppColor
                                                                          .backgroundApp,
                                                                    )
                                                                  : const SizedBox(),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          }
                                          return const SizedBox();
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: 84.h,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                ),
                                child: Center(
                                    child: MyCustomButton(
                                  title: "Done",
                                  loading: false,
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
