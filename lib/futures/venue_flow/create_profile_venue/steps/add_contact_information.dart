import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';

import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class AddContactInformationStep extends StatefulWidget {
  AddContactInformationStep({Key? key}) : super(key: key);

  @override
  State<AddContactInformationStep> createState() =>
      _AddContactInformationStepState();
}

class _AddContactInformationStepState extends State<AddContactInformationStep> {

  bool visible = false;
  int maxLength = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: 343.w,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                borderRadius: BorderRadius.circular(16.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(left: 16.w, top: 16.h, bottom: 12.h),
                    child: Text(
                      "Contact Information",
                      style: poppins.copyWith(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  SizedBox(height: 22.h),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        visible = !visible;
                      });
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 16.w, right: 12.w),
                          child: Container(
                            width: 44.w,
                            height: 44.r,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.07),
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: SvgPicture.asset(
                                ImagesApp.icSelectVibe,
                                width: 24.w,
                                height: 24.r,
                              ),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Website",
                              style: poppins.copyWith(
                                fontSize: 11.sp,
                                color: AppColor.greyHint,
                              ),
                            ),
                            SizedBox(height: 8.h),
                            Text(
                              "Add your website",
                              style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Text(
                          "Add",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            color: AppColor.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 16.w),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: visible,
                    child: Padding(
                      padding: EdgeInsets.only(left: 12.w,top: 12.h, right: 12.w),
                      child: Container(
                        width: 311.w,
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        child: CustomTextField(
                          controller: CreateProfileVenuePage.textCntWeb,
                          prefixIcon: ImagesApp.icWeb,
                          hint: "mahady.com",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 12.5.h),
                  Padding(
                    padding: EdgeInsets.only(left: 16.w, right: 12.w),
                    child: Text(
                      "Social media links",
                      style: poppins.copyWith(
                        fontSize: 13.sp,
                        color: AppColor.greyText,
                      ),
                    ),
                  ),
                  SizedBox(height: 8.h),
                  Padding(
                    padding: EdgeInsets.only(left: 12.w, right: 12.w),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: CreateProfileVenuePage.textCntTiktok,
                        prefixIcon: ImagesApp.icTiktok,
                        hint: "Tiktok.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: CreateProfileVenuePage.textCnTwitter,
                        prefixIcon: ImagesApp.icTiwett,
                        hint: "twitter.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 12.w, right: 12.w, bottom: 16.h),
                    child: Container(
                      width: 311.w,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      child: CustomTextField(
                        controller: CreateProfileVenuePage.textCntYouTub,
                        prefixIcon: ImagesApp.icTiktok,
                        hint: "youtube.com/jfurjry4h/ltot",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
