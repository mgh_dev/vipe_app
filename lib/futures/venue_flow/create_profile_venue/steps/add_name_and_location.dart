import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';
import 'package:vibe_app/test_map.dart';

import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class AddNameAndLocationStep extends StatefulWidget {
  AddNameAndLocationStep({Key? key}) : super(key: key);

  @override
  State<AddNameAndLocationStep> createState() => _AddNameAndLocationStepState();
}

class _AddNameAndLocationStepState extends State<AddNameAndLocationStep> {
  final controllerBusinessName = TextEditingController();

  int maxLength = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 343.w,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16.r),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.w, top: 16.h, bottom: 17.5.h),
                        child: Text(
                          "What is the name of your business?",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const DividerWidget(),
                      SizedBox(height: 6.5.h),
                      Padding(
                        padding: EdgeInsets.only(left: 16.w),
                        child: Container(
                          width: 320.w,
                          height: 78.h,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            hintTextDirection: TextDirection.ltr,
                            controller: CreateProfileVenuePage.textCntBusinessName,
                            type: TextInputType.multiline,
                            line: 1000,
                            onChanged: (_) {
                              setState(() {
                                maxLength = CreateProfileVenuePage.textCntBusinessName.text.length;

                              });
                            },
                            hint: "House of Music",
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            right: 10.w, top: 8.h, bottom: 16.h),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Text(
                            "${maxLength.toString()}/300",
                            style: poppins.copyWith(
                              fontSize: 10.sp,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 36.h),
                GestureDetector(
                  onTap: (){
                    showMap();
                  },
                  child: Container(
                    width: 343.w,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(16.r),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.w, top: 16.h, bottom: 12.h),
                          child: Text(
                            "Where is it located?",
                            style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        const DividerWidget(),
                        SizedBox(height: 22.h),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 16.w, right: 12.w),
                              child: Container(
                                width: 44.w,
                                height: 44.r,
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.07),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset(
                                    ImagesApp.icAddLocation,
                                    width: 24.w,
                                    height: 24.r,
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              "Add your location",
                              style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const Spacer(),
                            Text(
                              "Search",
                              style: poppins.copyWith(
                                fontSize: 14.sp,
                                color: AppColor.blue,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(width: 16.w),
                          ],
                        ),
                        SizedBox(height: 31.5.h),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void showMap(){
    showModalBottomSheet(
      enableDrag: false,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r),
            topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 101.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.r),
                        topRight: Radius.circular(20.r)),),
                    height: MediaQuery.of(context).size.height - 101.h,
                    child: MapApp(
                      center: LatLong(33, 54),
                      isVisiblBtn: true,
                      onPicked: (pickedData) {
                        print(pickedData.latLong.latitude);
                        print(pickedData.latLong.longitude);
                        print(pickedData.address);

                        setState(() {
                        });
                      },


                    )
                ),
              ),

            ],
          ),
        );
      },
    );
  }
}
