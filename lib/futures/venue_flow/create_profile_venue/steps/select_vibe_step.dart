import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/get_vibe_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';

import '../../../../core/helper/custom_text_fild.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class SelectVibe extends StatefulWidget {
  SelectVibe({Key? key}) : super(key: key);

  @override
  State<SelectVibe> createState() => _SelectVibeState();
}

class _SelectVibeState extends State<SelectVibe> {
  final controllerVibeSearch = TextEditingController();



  bool visible=true;

  SelectItemViewModel changeItemBloc = SelectItemViewModel();

  GetVibeViewModel getVibeViewModel=GetVibeViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getVibeViewModel.getVibe();
  }
  @override
  Widget build(BuildContext context) {
    return   Column(
      children: [
        Visibility(
          visible:visible,
          child: Container(
            width: 343.w,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.07),
              borderRadius: BorderRadius.circular(16.r),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: 16.w, top: 16.h, bottom: 12.h),
                  child: Text(
                    "Select the type of atmosphere your venue cultivates?",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const DividerWidget(),
                SizedBox(height: 22.h),
                GestureDetector(
                  onTap: (){
                    setState(() {
                      visible=!visible;
                    });
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 16.w, right: 12.w),
                        child: Container(
                          width: 44.w,
                          height: 44.r,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                            child: SvgPicture.asset(
                              ImagesApp.icSelectVibe,
                              width: 24.w,
                              height: 24.r,
                            ),
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "What’s your Vibe?",
                            style: poppins.copyWith(
                              fontSize: 11.sp,
                              color:AppColor.greyHint,
                            ),
                          ),
                          SizedBox(height: 8.h),
                          Text(
                            "e.g. High Energy",
                            style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      const Spacer(),
                      Text(
                        "Add",
                        style: poppins.copyWith(
                          fontSize: 14.sp,
                          color: AppColor.blue,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 16.w),
                    ],
                  ),
                ),
                SizedBox(height: 31.5.h),
              ],
            ),
          ),
        ),
        Visibility(
          visible: !visible,
          child: Expanded(
            child: Container(
              width:double.infinity,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                borderRadius: BorderRadius.circular(16.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 23.h, bottom: 12.h),
                    child: Container(
                      width: 344.w,
                      height: 48.h,
                      decoration: BoxDecoration(
                        color: AppColor.greyText.withOpacity(0.08),
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      child: CustomTextField(
                        hintTextDirection: TextDirection.ltr,
                        controller: controllerVibeSearch,
                        icon: ImagesApp.icAddLocation,
                        hintFontSize: 14.sp,
                        hint: "What’s your vibe?",
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                        width: 320.w,
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(10.r),
                        ),
                        child: BlocConsumer(
                          bloc: getVibeViewModel,
                          listener: (context,state){
                            if(state is GetVibeInitialState){
                              print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<GetVibeInitialState>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

                            }
                            if(state is GetVibeLoadingState){
                              print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<LODING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            }
                            if(state is GetVibeSuccessState){
                              print("*************************");
                              print(state.response.data[0].name);
                              print("*************************");
                            }
                            if(state is GetVibeFailState){
                              print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<ERROR>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                              print(state.message);

                            }

                          },
                          builder: (context,sts){
                            if(sts is GetVibeLoadingState){
                              return const CustomLoading();
                            }
                            if(sts is GetVibeSuccessState){
                              return BlocBuilder(
                                bloc: changeItemBloc,
                                builder: (context, state) {
                                  if (state is ItemChangeState) {
                                    return ListView.builder(
                                      padding: EdgeInsets.zero,
                                      physics: const BouncingScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: sts.response.data.length,
                                      itemBuilder: (context, index) {
                                        int item = sts.response.data[index].id;
                                        return Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: CreateProfileVenuePage.lsVibe.contains(item)? 16.w : 28.w,vertical: 5),
                                          child: GestureDetector(
                                            onTap: (){
                                              setState(() {
                                                if (CreateProfileVenuePage.lsVibe.contains(item)) {
                                                  CreateProfileVenuePage.lsVibe.remove(item);
                                                } else {
                                                  CreateProfileVenuePage.lsVibe.add(item);
                                                }
                                              });
                                            },
                                            child: Container(
                                              height: 68.h,
                                              // width: 311,
                                              decoration: BoxDecoration(
                                                  color:CreateProfileVenuePage.lsVibe.contains(item)
                                                      ? AppColor.white.withOpacity(0.09)
                                                      : Colors.transparent,
                                                  borderRadius:
                                                  BorderRadius.circular(12.r)),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                    CreateProfileVenuePage.lsVibe.contains(item)? 12.w : 0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                                  children: [
                                                    Visibility(
                                                      visible:CreateProfileVenuePage.lsVibe.contains(item),
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            right:CreateProfileVenuePage.lsVibe.contains(item)
                                                                ? 12.w
                                                                : 0),
                                                        child: Container(
                                                          width: 44.w,
                                                          height: 44.r,
                                                          decoration: BoxDecoration(
                                                              color: AppColor.white
                                                                  .withOpacity(0.09),
                                                              borderRadius:
                                                              BorderRadius.circular(
                                                                  10.r)),
                                                          child: Center(
                                                              child: SvgPicture.asset(
                                                                  ImagesApp.icTiktook)),
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      sts.response.data[index].name,
                                                      style: poppins.copyWith(
                                                          fontSize: 14.sp,
                                                          fontWeight: FontWeight.w500),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      height: 16.6.h,
                                                      width: 16.6.w,
                                                      decoration: BoxDecoration(
                                                        color: CreateProfileVenuePage.lsVibe.contains(item)
                                                            ? AppColor.green3
                                                            : Colors.transparent,
                                                        border: Border.all(
                                                            width: 1,
                                                            color:CreateProfileVenuePage.lsVibe.contains(item)
                                                                ? Colors.transparent
                                                                : AppColor.grey5b),
                                                        borderRadius:
                                                        BorderRadius.circular(50),
                                                      ),
                                                      child: Center(
                                                        child: CreateProfileVenuePage.lsVibe.contains(item)
                                                            ? Icon(
                                                          Icons.check,
                                                          size: 16,
                                                          color: AppColor
                                                              .backgroundApp,
                                                        )
                                                            : const SizedBox(),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }
                                  return const SizedBox();
                                },
                              );
                            }
                            return const SizedBox();
                          },
                        )
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
