import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';

import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class AddAboutAndSelectTypeStep extends StatefulWidget {
  AddAboutAndSelectTypeStep({Key? key}) : super(key: key);

  @override
  State<AddAboutAndSelectTypeStep> createState() => _AddAboutAndSelectTypeStepState();
}

class _AddAboutAndSelectTypeStepState extends State<AddAboutAndSelectTypeStep> {
  final controllerBusinessName = TextEditingController();

  int maxLength = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 343.w,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16.r),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.w, top: 16.h, bottom: 17.5.h),
                        child: Text(
                          "Tell us about your business.",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const DividerWidget(),
                      SizedBox(height: 6.5.h),
                      Padding(
                        padding: EdgeInsets.only(left: 16.w),
                        child: Container(
                          width: 320.w,
                          height: 78.h,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            hintTextDirection: TextDirection.ltr,
                            controller: CreateProfileVenuePage.about,
                            type: TextInputType.multiline,
                            line: 1000,
                            onChanged: (_) {
                              setState(() {
                                maxLength = CreateProfileVenuePage.about.text.length;
                              });
                            },
                            hintFontSize: 12.sp,
                            hint: "We're a premier event space that caters to a\nwide range of events....",
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            right: 10.w, top: 8.h, bottom: 16.h),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Text(
                            "${maxLength.toString()}/300",
                            style: poppins.copyWith(
                              fontSize: 10.sp,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 36.h),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
