import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';

import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';

class SelectBusinessType extends StatelessWidget {
  SelectBusinessType({Key? key}) : super(key: key);
  SelectItemViewModel changeItemBloc = SelectItemViewModel();
  final List<String> ls = [
    "Eclectic",
    "Intimate",
    "High Energy",
    "Chill",
    "Nostalgic",
    "Bluesy",
    "Jazzy",
    "Folksy",
    "Rocking",
    "Reggae",
    "Hip-hop",
    "Classical",
    "World music",
  ];
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: 26.h,
        bottom: 20.h,
      ),
      child: Container(
        decoration: BoxDecoration(
            color: AppColor.white.withOpacity(0.07),
            borderRadius: BorderRadius.circular(16.r)),
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.h),
        width: double.infinity.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Select your business type.",
              style: poppins.copyWith(
                  fontSize: 14.sp, fontWeight: FontWeight.w600),
            ),
            Container(
              height: 1.h,
              width: double.infinity,
              color: AppColor.white.withOpacity(0.07),
              margin: EdgeInsets.symmetric(vertical: 14.h),
            ),
            BlocBuilder(
              bloc: changeItemBloc,
              builder: (context, state) {
                if (state is ItemChangeState) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: ls.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 25.h, horizontal: 12.w),
                          child: GestureDetector(
                            onTap: () {
                              changeItemBloc.changeItem(index);
                              CreateProfileVenuePage.role=index;
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  ls[index],
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                Container(
                                  height: 16.6.h,
                                  width: 16.6.w,
                                  decoration: BoxDecoration(
                                      color: state.index == index
                                          ? AppColor.green
                                          : Colors.transparent,
                                      border: Border.all(
                                          width: 1,
                                          color: state.index == index
                                              ? AppColor.green
                                              : AppColor.grey5b),
                                      shape: BoxShape.circle),
                                  child: Center(
                                    child: Icon(
                                      Icons.check,
                                      size: 17,
                                      color: state.index == index
                                          ? AppColor.backgroundApp
                                          : Colors.transparent,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                return SizedBox();
              },
            )
          ],
        ),
      ),
    );
  }
}
