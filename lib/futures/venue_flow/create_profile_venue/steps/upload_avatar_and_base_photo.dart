import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:getwidget/components/progress_bar/gf_progress_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/ui/create_profile_venue_page.dart';

import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class UploadAvatarAndBasePhoto extends StatefulWidget {

  UploadAvatarAndBasePhoto() : super();

  @override
  State<UploadAvatarAndBasePhoto> createState() =>
      _UploadAvatarAndBasePhotoState();
}

class _UploadAvatarAndBasePhotoState extends State<UploadAvatarAndBasePhoto> {
  final controllerBusinessName = TextEditingController();
  File? image;


  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
        print("<<<<IMAGE>>>>");
        CreateProfileVenuePage.imageAvatar=this.image!.path
        ;});
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  int maxLength = 0;
  final List<File> _image = [];
  final List<String> _imagePath = [];

  Future getImage(int state) async {
    if (state == 0) {
      final newImage = await ImagePicker()
          .getImage(source: ImageSource.camera, imageQuality: 80);
      if (newImage != null) {
        setState(() {
          _image.add(File(newImage.path));
          _imagePath.add(newImage.path);
          CreateProfileVenuePage.lsImageGallery=_imagePath;
        });
      }
    } else {
      final newImage = await ImagePicker()
          .getImage(source: ImageSource.gallery, imageQuality: 80);
      if (newImage != null) {
        setState(() {
          _image.add(File(newImage.path));
          _imagePath.add(newImage.path);
          CreateProfileVenuePage.lsImageGallery=_imagePath;
        });
      }
    }
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (builder) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.15,
            decoration:  BoxDecoration(
                color: AppColor.darkBlue,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0))),
            child: Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(right: 15, left: 10),
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      minWidth: MediaQuery.of(context).size.width * 0.45,
                      height: 45,
                      color: AppColor.blue,
                      child: const Text(
                        "camera",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        getImage(0);
                      },
                    )),
                MaterialButton(
                    minWidth: MediaQuery.of(context).size.width * 0.45,
                    height: 45,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: AppColor.blue,
                    child: const Text(
                      "gallery",
                      style: TextStyle(
                          color: Colors.white,),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      getImage(1);
                    }),
              ],
            ),
          );
        });
  }
@override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.only(top: 26.h),
        child: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 343.w,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16.r),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.w, top: 16.h, bottom: 13.h),
                        child: Text(
                          "Let’s set up your gallery.",
                          style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const DividerWidget(),
                      SizedBox(height: 6.5.h),
                      Padding(
                        padding:
                            EdgeInsets.only(left: 16.w, top: 11.h, bottom: 8.h),
                        child: Text(
                          "Add Profile photo",
                          style: poppins.copyWith(
                            fontSize: 13.sp,
                            color: AppColor.greyText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.w, right: 16.w, bottom: 24.h),
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: 311.w,
                            height: 232.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(20.r),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Stack(
                                  alignment: Alignment.bottomRight,
                                  children: [
                                    Container(
                                      width: 112.w,
                                      height: 112.h,
                                      decoration: BoxDecoration(
                                        color: AppColor.darkBlue,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: image != null
                                            ? Container(
                                                width: 112.w,
                                                height: 112.h,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: FileImage(image!),
                                                      fit: BoxFit.fill),
                                                  shape: BoxShape.circle,
                                                ),
                                              )
                                            : SvgPicture.asset(
                                                ImagesApp.icUser,
                                                width: 22.w,
                                                height: 28.h,
                                              ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        pickImage(ImageSource.gallery);
                                        print("object");
                                      },
                                      child: Container(
                                        width: 42.w,
                                        height: 42.r,
                                        decoration: BoxDecoration(
                                          color: AppColor.black54,
                                          shape: BoxShape.circle,
                                        ),
                                        child: Center(
                                          child: Container(
                                            width: 35.w,
                                            height: 35.r,
                                            decoration: BoxDecoration(
                                              color: AppColor.darkPurple,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                                child: SvgPicture.asset(
                                                    ImagesApp.icCamera)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 17.h),
                                Text(
                                  "Upload Image ",
                                  style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    color: AppColor.darkPurple,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 21.w, right: 12.w, top: 13.h),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 225,
                                        height: 8,
                                        child: GFProgressBar(
                                          percentage: 0.2,
                                          lineHeight: 8,
                                          backgroundColor:
                                              AppColor.white.withOpacity(0.1),
                                          progressBarColor: AppColor.darkPurple,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 12.w,
                                      ),
                                      Text("10%",
                                          style:
                                              poppins.copyWith(fontSize: 12)),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16.w, bottom: 8.h),
                        child: Text(
                          "Upload your best photos (up to 5)",
                          style: poppins.copyWith(
                            fontSize: 13.sp,
                            color: AppColor.greyText,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.w, right: 16.w, bottom: 24.h),
                        child: GestureDetector(
                          onTap: () {
                            if(_image.length<5){
                              _modalBottomSheetMenu();
                            }
                            else{
                              CustomToast.show("limit 5");
                            }
                          },
                          child: Container(
                            width: 311.w,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(20.r),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 18.h,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 44.w,
                                    height: 44.r,
                                    decoration: BoxDecoration(
                                      color: AppColor.darkBlue,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                      child:
                                          SvgPicture.asset(ImagesApp.icImport),
                                    ),
                                  ),
                                  SizedBox(height: 8.h),
                                  Text(
                                    "Upload Image ",
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: AppColor.darkPurple,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 16.w, left: 16.w),
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          spacing: 12,
                          runSpacing: 12,
                          children: _buildImage(),
                        ),
                      ),
                      SizedBox(height: 15.h),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildImage() {
    List<Widget> chips = [];

    for (int i = 0; i < _image.length; i++) {
      chips.add(
        Container(
          width: 145.w,
          height: 83.h,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.r),
            image: DecorationImage(
                image: FileImage(
                  _image[i],
                ),
                fit: BoxFit.cover),
          ),
          child: Padding(
            padding: EdgeInsets.only(top: 6.h, right: 8.w),
            child: Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    chips.removeAt(i);
                    _image.removeAt(i);
                    _imagePath.removeAt(i);
                    CreateProfileVenuePage.lsImageGallery=_imagePath;
                    print(i);
                  });
                  print(_image.length);
                },
                child: Container(
                  width: 24.w,
                  height: 24.h,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white.withOpacity(0.5),
                  ),
                  child: const Center(
                    child: Icon(
                      Icons.close,
                      color: Colors.black,
                      size: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }

    return chips;
  }
}
