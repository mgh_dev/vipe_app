import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class GetVibeBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetVibeInitialState extends GetVibeBaseState {}

class GetVibeLoadingState extends GetVibeBaseState {}

class GetVibeSuccessState extends GetVibeBaseState {
  final GetResponseVibeModel response;

  GetVibeSuccessState({required this.response});
}

class GetVibeFailState extends GetVibeBaseState {
  final String message;


  GetVibeFailState({required this.message});
}
