import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendInformationBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendInformationInitialState extends SendInformationBaseState {}

class SendInformationLoadingState extends SendInformationBaseState {}

class SendInformationSuccessState extends SendInformationBaseState {

}

class SendInformationFailState extends SendInformationBaseState {
  final String message;


  SendInformationFailState({required this.message});
}
