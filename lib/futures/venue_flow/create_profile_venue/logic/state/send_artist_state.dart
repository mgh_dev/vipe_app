import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendArtistBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendArtistInitialState extends SendArtistBaseState {}

class SendArtistLoadingState extends SendArtistBaseState {}

class SendArtistSuccessState extends SendArtistBaseState {

}

class SendArtistFailState extends SendArtistBaseState {
  final String message;


  SendArtistFailState({required this.message});
}
