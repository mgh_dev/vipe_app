import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendVibeBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendVibeInitialState extends SendVibeBaseState {}

class SendVibeLoadingState extends SendVibeBaseState {}

class SendVibeSuccessState extends SendVibeBaseState {

}

class SendVibeFailState extends SendVibeBaseState {
  final String message;


  SendVibeFailState({required this.message});
}
