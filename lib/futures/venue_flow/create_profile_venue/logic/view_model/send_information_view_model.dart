import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../../../core/config/web_service.dart';


class SendInformationViewModel extends Cubit<SendInformationBaseState> {
  SendInformationViewModel() : super(SendInformationInitialState());

  Future send(RequestCreateProfileVenus requestCreateProfileVenus) async {
    print("1");
    emit(SendInformationLoadingState());
    print("3");
    var requestData=requestCreateProfileVenus.toJson();
    try {
      print("4");
      var response =
          await WebService().dio.patch("users/extra-data",data: requestData);
      print("5");

      print("6");
     
      print("7");
      emit(SendInformationSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(SendInformationFailState(message: e.toString()));
      print("10");
    }
  }

}
