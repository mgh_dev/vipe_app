import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../../../core/config/web_service.dart';


class GetVibeViewModel extends Cubit<GetVibeBaseState> {
  GetVibeViewModel() : super(GetVibeInitialState());

  void getVibe() async {
    print("1");
    emit(GetVibeLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.get("vibes");
      print("5");
      GetResponseVibeModel getResponseVibeModel =
          GetResponseVibeModel.fromJson(response.data);
      print("6");
     
      print("7");
      emit(GetVibeSuccessState(response: getResponseVibeModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetVibeFailState(message: e.toString()));
      print("10");
    }
  }

}
