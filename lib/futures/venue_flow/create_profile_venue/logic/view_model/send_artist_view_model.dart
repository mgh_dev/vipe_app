import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/request_model/request_profile_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../../../core/config/web_service.dart';
import '../state/send_artist_state.dart';


class SendArtistViewModel extends Cubit<SendArtistBaseState> {
  SendArtistViewModel() : super(SendArtistInitialState());

  Future send(List<int> lsArtistry) async {
    print("1");
    emit(SendArtistLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.post("users/assign-artistry",data:lsArtistry);
      print("5");

      print("6");
     
      print("7");
      emit(SendArtistSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(SendArtistFailState(message: e.toString()));
      print("10");
    }
  }

}
