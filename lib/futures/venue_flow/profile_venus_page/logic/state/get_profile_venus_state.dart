import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/model/response_model/response_get_profile_venus_model.dart';

abstract class GetProfileVenusBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetProfileVenusInitialState extends GetProfileVenusBaseState {}

class GetProfileVenusLoadingState extends GetProfileVenusBaseState {}

class GetProfileVenusSuccessState extends GetProfileVenusBaseState {
  final ResponseGetProfileVenusModel response;

  GetProfileVenusSuccessState({required this.response});
}

class GetProfileVenusFailState extends GetProfileVenusBaseState {
  final String message;


  GetProfileVenusFailState({required this.message});
}
