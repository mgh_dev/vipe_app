import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/state/get_profile_venus_state.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/model/response_model/response_get_profile_venus_model.dart';




import '../../../../../core/config/web_service.dart';


class GetProfileVenusViewModel extends Cubit<GetProfileVenusBaseState> {
  GetProfileVenusViewModel() : super(GetProfileVenusInitialState());

  void get() async {
    print("1");
    emit(GetProfileVenusLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.get("users/get-profile?vibe=1&extraData=1&gallery=1");
      print("5");
      ResponseGetProfileVenusModel getProfileVenusModel =
      ResponseGetProfileVenusModel.fromJson(response.data);
      print("6");
     
      print("7");
      emit(GetProfileVenusSuccessState(response: getProfileVenusModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetProfileVenusFailState(message: e.toString()));
      print("10");
    }
  }

}
