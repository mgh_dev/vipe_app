import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/dat.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/model/response_model/login_auth_register_model.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/state/get_profile_venus_state.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/view_model/get_profile_venus_view_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/shows_tab_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabgallery.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tab_media_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabreview.dart';

import '../../../../core/helper/custom_button.dart';
import '../../../../core/helper/different_size_of_gridview_item.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../../date_create_event_page.dart';
import '../../../../houseofmusic.dart';
import '../../buy_subscription_page/ui/buy_subscription_page.dart';
import '../../profile_edit_venus_page/ui/profile_edit_venus_page.dart';

class ProfileVenusPage extends StatefulWidget {
  const ProfileVenusPage({Key? key}) : super(key: key);

  @override
  State<ProfileVenusPage> createState() => _ProfileVenusPageState();
}

class _ProfileVenusPageState extends State<ProfileVenusPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();
  var texts = [
    'Intimate',
    'Chill',
    'Bluesy',
    'Eclectic',
    'High Energy',
    'Nostalgic',
    'Folksy',
    'Jazzy',
  ];
  var texts2 = [
    'Intimate',
    'Chill',
    'Bluesy',
  ];
  final getProfileViewModel = GetProfileVenusViewModel();
  final visibleWebViewModel = SelectItemViewModel();
  bool visible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(
      length: 4,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });

    // Future.delayed(const Duration(seconds: 1), () {
    //   showModalBottomSheet(
    //     context: context,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.only(
    //           topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
    //     ),
    //     backgroundColor: AppColor.darkBlue,
    //     isScrollControlled: true,
    //     builder: (BuildContext context) {
    //       return Container(
    //           child: SingleChildScrollView(
    //         child: Column(
    //           mainAxisAlignment: MainAxisAlignment.end,
    //           children: <Widget>[
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.end,
    //               children: [
    //                 GestureDetector(
    //                   onTap: () {
    //                     Navigator.of(context).pop();
    //                   },
    //                   child: Container(
    //                     padding: EdgeInsets.all(11.w),
    //                     margin: EdgeInsets.only(right: 15.w, top: 16.h),
    //                     decoration: BoxDecoration(
    //                         color: AppColor.greyBtnClose.withOpacity(0.2),
    //                         borderRadius: BorderRadius.circular(50)),
    //                     child: Icon(
    //                       Icons.close,
    //                       color: AppColor.white,
    //                     ),
    //                   ),
    //                 )
    //               ],
    //             ),
    //             Padding(
    //               padding: EdgeInsets.only(bottom: 36.h, top: 10.h),
    //               child: SvgPicture.asset(ImagesApp.bottomSheetWarning),
    //             ),
    //             Text(
    //               "Special deals & offers.",
    //               style: poppins.copyWith(
    //                   fontSize: 20,
    //                   fontWeight: FontWeight.w700,
    //                   color: AppColor.green),
    //             ),
    //             Padding(
    //               padding: EdgeInsets.only(
    //                   bottom: 16.h, top: 12.h, right: 12.w, left: 12.w),
    //               child: RichText(
    //                 textAlign: TextAlign.center,
    //                 text: TextSpan(
    //                   children: [
    //                     TextSpan(
    //                       text: "Subscribe or upgrade your plan ",
    //                       style: poppins.copyWith(
    //                           fontSize: 14.sp,
    //                           fontWeight: FontWeight.w400,
    //                           color: AppColor.blue),
    //                     ),
    //                     TextSpan(
    //                       text:
    //                           "to bookartists, promote your business & elevatethe live music and entertainmentscene in your area",
    //                       style: poppins.copyWith(
    //                           fontSize: 14.sp,
    //                           fontWeight: FontWeight.w400,
    //                           color: AppColor.greyHint),
    //                     ),
    //                   ],
    //                 ),
    //               ),
    //             ),
    //             Padding(
    //               padding:
    //                   EdgeInsets.only(right: 16.w, left: 16.w, bottom: 17.5.h),
    //               child: MyCustomButton(
    //                 title: "Choose Your Plan",
    //                 onTap: () {
    //                   Navigator.pop(context);
    //                   Navigator.push(
    //                       context,
    //                       MaterialPageRoute(
    //                           builder: (context) => BuySubscriptionPage()));
    //                 },
    //                 loading: false,
    //                 color: AppColor.greenBtn,
    //                 width: 350.w,
    //                 height: 60.h,
    //                 fontSize: 14.sp,
    //                 fontWeight: FontWeight.w500,
    //               ),
    //             ),
    //           ],
    //         ),
    //       ));
    //     },
    //   );
    // });
    getProfileViewModel.get();
  }
List<String> gallery=[];
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    getProfileViewModel.close();
  }
  @override
  Widget build(BuildContext context) {

    secondController.addListener(() {
      if (secondController.offset <=
          secondController.position.minScrollExtent) {
        firstController.animateTo(0,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      } else if (secondController.offset >=
          secondController.position.minScrollExtent) {
        firstController.animateTo(firstController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      }
    });
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder(
        bloc: getProfileViewModel,
        builder: (context, state) {
          if (state is GetProfileVenusLoadingState) {
            return const CustomLoading();
          }
          if (state is GetProfileVenusSuccessState) {
            for (int i = 0; i < state.response.data.gallery.length; i++) {
              gallery.add(state.response.data.gallery[i].path);
            }

            print("<<<<<<<<<<<object>>>>>>>>>>>");
            for (int i = 0;
                i < state.response.data.extraData.links.length;
                i++) {
              if (state.response.data.extraData.links[i].type == "webSite") {
                visibleWebViewModel.changeItem(1);
                print("****************************************");
              } else {
                visibleWebViewModel.changeItem(0);
              }
            }
            print(state.response.data.extraData.links.length);
            print("<<<<<<<<<<<object>>>>>>>>>>>");
            return CustomScrollView(
              controller: firstController,
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      Container(
                        height: 427.h,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  "http://46.249.102.65:8080${state.response.data.avatar}"),
                              fit: BoxFit.cover),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: <Color>[
                                const Color(0xff090B1B),
                                const Color(0xff090B1B).withOpacity(0.2),
                                Colors.transparent,
                                Colors.transparent,
                                Colors.transparent,
                                const Color(0xff090B1B).withOpacity(0.98),
                                const Color(0xff090B1B),
                              ],
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 53.h, left: 20.w, right: 20.w),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    GestureDetector(
                                        onTap: () {
                                          Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      const BottomNavVenusPages(
                                                        index: 0,
                                                      )));
                                        },
                                        child:
                                            SvgPicture.asset(ImagesApp.icBack)),
                                    Image.asset(
                                      ImagesApp.logoApp,
                                      width: 60.w,
                                      height: 60.h,
                                    ),
                                    SvgPicture.asset(ImagesApp.icShared),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        state.response.data.extraData.d.stageName,
                                        style: poppins.copyWith(
                                          fontSize: 26.sp,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Icon(
                                        Icons.check_circle,
                                        color: AppColor.greyBlue2,
                                        size: 21,
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "4.5* ",
                                        style: poppins.copyWith(
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                      Text(
                                        ". Concert hall .",
                                        style: poppins.copyWith(
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                      Text(
                                        " Virginia",
                                        style: poppins.copyWith(
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 15),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 24.h),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 24.h, bottom: 20.h, left: 15.w, right: 15.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const DateCreateVenusPage()));
                                });
                              },
                              child: Container(
                                width: 142.w,
                                height: 40.h,
                                decoration: BoxDecoration(
                                  color: AppColor.blue,
                                  border: Border.all(
                                    width: 0.7,
                                    color: AppColor.white,
                                  ),
                                  borderRadius: BorderRadius.circular(50.r),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Create an event",
                                        style: poppins.copyWith(
                                          fontSize: 13.sp,
                                        ),
                                      ),
                                      Text(
                                        "+ Create an event",
                                        style: poppins.copyWith(
                                          fontSize: 8.sp,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 10),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const ProfileEditVenusPage()));
                              },
                              child: Container(
                                width: 40.w,
                                height: 40.r,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColor.white.withOpacity(0.05),
                                ),
                                child: Center(
                                  child: SvgPicture.asset(
                                    ImagesApp.icPen,
                                    color: AppColor.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.w),
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  visible = !visible;
                                });
                              },
                              child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                      text: state.response.data.extraData.about,
                                      style: poppins.copyWith(
                                          fontSize: 11.sp,
                                          color: Colors.white)),
                                  TextSpan(
                                      text: "seeMore",
                                      style: poppins.copyWith(
                                          color: AppColor.blue, fontSize: 14)),
                                ]),
                              ),
                            ),
                            Visibility(
                              visible: visible,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 18.h, bottom: 16.h),
                                    child: const DividerWidget(),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 16.w, top: 4.5.h),
                                    child: Text(
                                      "vibe",
                                      style: poppins.copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.blueText),
                                    ),
                                  ),
                                  SizedBox(height: 10.h),
                                  Wrap(
                                    children: state.response.data.vibe
                                        .map(
                                          (text) => Container(
                                            height: 30,
                                            padding: EdgeInsets.only(
                                                top: 5.h,
                                                right: 10.w,
                                                left: 10.w),
                                            margin: EdgeInsets.only(right: 5.w),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50.r),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: AppColor.white
                                                      .withOpacity(0.3)),
                                              color: AppColor.white
                                                  .withOpacity(0.07),
                                            ),
                                            child: Text(
                                              text.name,
                                              style: poppins.copyWith(
                                                color: Colors.white,
                                                fontSize: 12.sp,
                                              ),
                                            ),
                                          ),
                                        )
                                        .toList(),
                                  ),

                                  BlocBuilder(
                                    bloc: visibleWebViewModel,
                                    builder: (context, stateVisible) {
                                      if (stateVisible is ItemChangeState) {
                                        return Visibility(
                                          visible: stateVisible.index == 1
                                              ? true
                                              : false,
                                          child: SizedBox(
                                            height: stateVisible.index == 1
                                                ? 90.h
                                                : 20.h,
                                            child: ListView.builder(
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                itemCount: state.response.data
                                                    .extraData.links.length,
                                                shrinkWrap: true,
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemBuilder: (ctx, index) {
                                                  return state
                                                              .response
                                                              .data
                                                              .extraData
                                                              .links[index]
                                                              .type ==
                                                          "webSite"
                                                      ? Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            SizedBox(
                                                                height: 10.h),
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      bottom:
                                                                          20.h),
                                                              child: Container(
                                                                width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                                height: 1,
                                                                color: AppColor
                                                                    .greyLine
                                                                    .withOpacity(
                                                                        0.40),
                                                              ),
                                                            ),
                                                            Row(
                                                              children: [
                                                                Text(
                                                                  "Website:  ",
                                                                  style: inter.copyWith(
                                                                      fontSize:
                                                                          13.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      color: AppColor
                                                                          .greyTxt),
                                                                ),
                                                                Text(
                                                                  state
                                                                      .response
                                                                      .data
                                                                      .extraData
                                                                      .links[
                                                                          index]
                                                                      .link,
                                                                  style: poppins.copyWith(
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        )
                                                      : const SizedBox();
                                                }),
                                          ),
                                        );
                                      }
                                      return SizedBox();
                                    },
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 22.h, bottom: 20.h),
                                    child: const DividerWidget(),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "Social Media:  ",
                                        style: poppins.copyWith(
                                            fontSize: 13.sp,
                                            fontWeight: FontWeight.w500,
                                            color: AppColor.greyTxt),
                                      ),
                                      Wrap(
                                        children:
                                        state.response.data.extraData.links
                                            .map(
                                              (link) => Padding(
                                            padding:
                                            EdgeInsets.only(right: 7.w),
                                            child: Container(
                                              width: 40.w,
                                              height: 40.r,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white
                                                    .withOpacity(0.07),
                                              ),
                                              child: Center(
                                                  child: SvgPicture.asset(
                                                      "assets/svg/${link.type}.svg")),
                                            ),
                                          ),
                                        )
                                            .toList(),
                                      )
                                      // SizedBox(width: 8.w),
                                      // Container(
                                      //   width: 40.w,
                                      //   height: 40.r,
                                      //   decoration: BoxDecoration(
                                      //     shape: BoxShape.circle,
                                      //     color: Colors.white.withOpacity(0.07),
                                      //   ),
                                      //   child: Center(
                                      //       child: SvgPicture.asset(
                                      //           ImagesApp.icInstagram)),
                                      // ),
                                      // SizedBox(width: 8.w),
                                      // Container(
                                      //   width: 40.w,
                                      //   height: 40.r,
                                      //   decoration: BoxDecoration(
                                      //     shape: BoxShape.circle,
                                      //     color: Colors.white.withOpacity(0.07),
                                      //   ),
                                      //   child: Center(
                                      //       child: SvgPicture.asset(
                                      //           ImagesApp.icLinkdin)),
                                      // ),
                                      // SizedBox(width: 8.w),
                                      // Container(
                                      //   width: 40.w,
                                      //   height: 40.r,
                                      //   decoration: BoxDecoration(
                                      //     shape: BoxShape.circle,
                                      //     color: Colors.white.withOpacity(0.07),
                                      //   ),
                                      //   child: Center(
                                      //       child: SvgPicture.asset(
                                      //           ImagesApp.icFasBook)),
                                      // )
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 22.h, bottom: 20.h),
                                    width: double.infinity,
                                    height: 0.5,
                                    color: AppColor.greyLine.withOpacity(0.40),
                                  ),
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            "Capacity: ",
                                            style: poppins.copyWith(
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w500,
                                                color: AppColor.greyTxt),
                                          ),
                                          Text(
                                            state.response.data.extraData.d
                                                .venueCapacity
                                                .toString(),
                                            style: poppins.copyWith(
                                                fontSize: 13.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Stage number: ",
                                            style: poppins.copyWith(
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w500,
                                                color: AppColor.greyTxt),
                                          ),
                                          Text(
                                            state.response.data.extraData.d
                                                .numberOfStages
                                                .toString(),
                                            style: poppins.copyWith(
                                                fontSize: 13.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Age limit: ",
                                            style: poppins.copyWith(
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w500,
                                                color: AppColor.greyTxt),
                                          ),
                                          Text(
                                            state.response.data.extraData.d
                                                .ageLimit,
                                            style: poppins.copyWith(
                                                fontSize: 13.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 20.h, bottom: 16.h),
                                    width: double.infinity,
                                    height: 0.5,
                                    color: AppColor.greyLine.withOpacity(0.40),
                                  ),
                                  Text(
                                    "In-house equipment",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w500,
                                        color: AppColor.whiteEB),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 10.h,
                                    ),
                                    child: Container(
                                      height: 30,
                                      padding: EdgeInsets.only(
                                          top: 5.h, right: 10.w, left: 10.w),
                                      margin: EdgeInsets.only(right: 5.w),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(50.r),
                                        border: Border.all(
                                            width: 0.5,
                                            color: AppColor.white
                                                .withOpacity(0.3)),
                                        color: AppColor.white.withOpacity(0.07),
                                      ),
                                      child: Text(
                                        state.response.data.extraData.d.inHouseEquipment??"",
                                        style: poppins.copyWith(
                                          color: Colors.white,
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 22.h,
                                    ),
                                    width: double.infinity,
                                    height: 0.5,
                                    color: AppColor.greyLine.withOpacity(0.40),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.w),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30.w),
                        child: Container(
                          width: double.infinity,
                          height: 1,
                          color: AppColor.greyHint,
                        ),
                      ),
                      TabBar(
                        controller: tabController,
                        // dividerColor: Colors.red,
                        indicatorColor: AppColor.blue,
                        labelColor: AppColor.white,
                        unselectedLabelColor:
                            AppColor.greyHint.withOpacity(0.5),
                        labelStyle: poppins.copyWith(
                          fontSize: 12.sp,
                        ),
                        isScrollable: true,

                        indicatorSize: TabBarIndicatorSize.tab,
                        tabs: const [
                          Tab(text: 'Events'),
                          Tab(text: 'Media'),
                          Tab(text: 'Gallery'),
                          Tab(text: 'Review'),
                        ],
                      ),
                    ],
                  ),
                )),
                SliverFillRemaining(
                  child: TabBarView(
                    controller: tabController,
                    children: [
                      ShowsTabVenus(scrollController: secondController),
                      TabMediaVenus(
                        scrollController: secondController,
                      ),
                      TabGallery(
                        scrollController: secondController,

                      ),
                      TabReview(
                        scrollController: secondController,
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          return const SizedBox();
        },
      ),
    );
  }
}
