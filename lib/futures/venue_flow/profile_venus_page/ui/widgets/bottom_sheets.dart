import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../core/helper/custom_button.dart';
import '../../../../../core/utils/app_color.dart';

class ProfileVenusBottomSheets {
  static void sendComment(
      BuildContext context, TextEditingController textController) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Padding(
                                padding: EdgeInsets.only(left: 28.w),
                                child: Text(
                                  "Cancel",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.blue),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            "Event Summary",
                            style: poppins.copyWith(
                                fontSize: 16.sp, fontWeight: FontWeight.w500),
                          )),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: AppColor.white.withOpacity(0.07),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 18.h, bottom: 26.h, right: 16.w, left: 16.w),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("VIBE",
                              style: inter.copyWith(
                                  fontSize: 20.sp,
                                  fontWeight: FontWeight.w700,
                                  color: AppColor.white)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text("Monday, 12 Dec, 18:39",
                                  style: inter.copyWith(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.white)),
                              Text(
                                "Event completed",
                                style: inter.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    color: AppColor.white),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 19.5.h),
                      height: 1,
                      width: double.infinity,
                      color: AppColor.white.withOpacity(0.07),
                    ),
                    Text(
                      "Rate your experience.",
                      style: inter.copyWith(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w500,
                          color: AppColor.white),
                    ),
                    SizedBox(
                      height: 11.h,
                    ),
                    Text(
                      "(Select one)",
                      style: inter.copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          color: AppColor.grey81),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 42.w, left: 42.w, top: 41.h, bottom: 24.h),
                      child: Stack(
                        children: [
                          Center(
                            child: Image.asset(
                              ImagesApp.happy,
                              width: 83.w,
                              height: 83.h,
                            ),
                          ),
                          Image.asset(
                            ImagesApp.star,
                          ),
                        ],
                      ),
                    ),
                    Text(
                      "Good",
                      style: inter.copyWith(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          color: AppColor.white),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 18.5.h, top: 49.h),
                      height: 1,
                      width: double.infinity,
                      color: AppColor.white.withOpacity(0.07),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 17.w, bottom: 9.h),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Tell others what you think.",
                          style: inter.copyWith(
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.white),
                        ),
                      ),
                    ),
                    Container(
                      width: 344.w,
                      height: 145.h,
                      decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      child: CustomTextField(
                        controller: textController,
                        line: 10,
                        type: TextInputType.multiline,
                        action: TextInputAction.newline,
                        hint: "Say something nice...",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 17.5.h, top: 60.h),
                      child: MyCustomButton(
                        title: "Save and Continue ",
                        onTap: () {
                          Navigator.pop(context);
                          sendCommentSuccesses(context);
                        },
                        loading: false,
                        color: AppColor.blue,
                        width: 350.w,
                        height: 60.h,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void sendCommentSuccesses(BuildContext context) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: AppColor.darkBlue,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height - 54.h,
          child: Stack(
            alignment: Alignment.topLeft,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    topRight: Radius.circular(20.r)),
                child: Image.asset(
                  ImagesApp.ellipse,
                  fit: BoxFit.fill,
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Padding(
                                padding: EdgeInsets.only(left: 28.w),
                                child: Text(
                                  "Cancel",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.blue),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            "Event Summary",
                            style: poppins.copyWith(
                                fontSize: 16.sp, fontWeight: FontWeight.w500),
                          )),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: AppColor.white.withOpacity(0.07),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 14.h),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image(
                            image: AssetImage(
                              ImagesApp.backgroundStar,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 220.h),
                            child: Column(
                              children: [
                                Text(
                                  "Thanks for submitting\nyour review!",
                                  style: poppins.copyWith(
                                      fontSize: 24.sp,
                                      fontWeight: FontWeight.w600,
                                      color: AppColor.greyD),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Text(
                                  "Your support and collaboration is crucial for\nbuilding and strengthening your local live\nentertainment community.",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.grey81),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 17.5.h),
                      child: MyCustomButton(
                        title: "Done",
                        onTap: () {
                          Navigator.pop(context);
                        },
                        loading: false,
                        color: AppColor.blue,
                        width: 350.w,
                        height: 60.h,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
