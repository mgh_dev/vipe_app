import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabMediaVenus extends StatelessWidget {
  TabMediaVenus({required this.scrollController}) : super();
  ScrollController scrollController;
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomScrollView(
          controller: scrollController,
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(left: 16.w, top: 24.h, bottom: 10.h),
                child: Text(
                  "Discography",
                  style: inter.copyWith(
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w500,
                      color: AppColor.white),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Padding(
                    padding:
                        EdgeInsets.only(bottom: 20.h, left: 16.w, right: 16.w),
                    child: Row(
                      children: [
                        Container(
                          width: 50.w,
                          height: 50.r,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.r),
                            image: DecorationImage(
                                image: AssetImage(ImagesApp.p2)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Balonku Ada 5 Meter",
                                style: inter.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.white,
                                ),
                              ),
                              SizedBox(height: 5.h),
                              Row(
                                children: [
                                  Icon(
                                    Icons.person,
                                    color: AppColor.greyTxt,
                                    size: 10,
                                  ),
                                  SizedBox(width: 3.w),
                                  Text(
                                    "Mamank",
                                    style: inter.copyWith(
                                      fontSize: 9.sp,
                                      color: AppColor.greyTxt,
                                    ),
                                  ),
                                  SizedBox(width: 6.w),
                                  Icon(
                                    Icons.play_circle,
                                    color: AppColor.blue,
                                    size: 10,
                                  ),
                                  SizedBox(width: 5.w),
                                  Text(
                                    "Mamank",
                                    style: inter.copyWith(
                                      fontSize: 9.sp,
                                      color: AppColor.greyTxt,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
                childCount: 2,
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(left: 16.w, top: 24.h, bottom: 10.h),
                child: Text(
                  "Videos",
                  style: inter.copyWith(
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w500,
                      color: AppColor.white),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Padding(
                    padding:
                        EdgeInsets.only(bottom: 20.h, left: 16.w, right: 16.w),
                    child: Row(
                      children: [
                        Container(
                          width: 129.w,
                          height: 72.r,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7.r),
                            image: DecorationImage(
                              image: AssetImage(ImagesApp.p2),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.play_circle,
                              color: AppColor.white,
                              size: 35,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "House of Music - Live DJ",
                                style: inter.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.white,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
                childCount: 2,
              ),
            ),
          ],
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ChatRoomPage(),
                ),
              );
            },
            child: Container(
              margin: EdgeInsets.only(right: 23.w, bottom: 33.h),
              width: 60.w,
              height: 60.r,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColor.blue,
              ),
              child: Center(
                child: SvgPicture.asset(ImagesApp.icChat),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

//

//
