import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/model/response_model/login_auth_register_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/view_model/get_profile_venus_view_model.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../../core/helper/custom_loading.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';
import '../../logic/state/get_profile_venus_state.dart';

class TabGallery extends StatefulWidget {
  TabGallery({required this.scrollController,y}) : super();
  ScrollController scrollController;


  @override
  State<TabGallery> createState() => _TabGalleryState();
}

class _TabGalleryState extends State<TabGallery> {
  final changeRoleBloc = ChangeRoleViewModel();
  final getProfileViewModel = GetProfileVenusViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileViewModel.get();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: getProfileViewModel,
      builder: (context, state) {
        if (state is GetProfileVenusLoadingState) {
          return const CustomLoading();
        }
        if (state is GetProfileVenusSuccessState) {
print(state.response.data.gallery[0].path);
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
          return Padding(
            padding:  EdgeInsets.only(top:24.h),
            child: Stack(
              children: [
                SingleChildScrollView(
                  controller: widget.scrollController,
                  child: Padding(
                    padding:  EdgeInsets.symmetric(horizontal:16.w),
                    child: Column(
                      children: [
                        Row  (
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Visibility(
                                  visible: state.response.data.gallery.length>=1,
                                  child: Container(
                                    width: 168.w,
                                    height: 276.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.r),
                                      image: DecorationImage(
                                        image: NetworkImage("http://46.249.102.65:8080/storage/${state.response.data.gallery[0].path}"),
                                        // image: AssetImage(ImagesApp.profileTest1),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 6.w),
                                state.response.data.gallery.length>=2
                                ? Container(
                                  width: 168.w,
                                  height: 109.h,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.r),
                                    image: DecorationImage(
                                      image: NetworkImage("http://46.249.102.65:8080/storage/${state.response.data.gallery[1].path}"),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ):SizedBox(),

                              ],
                            ),
                            SizedBox(width: 6.w),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                  state.response.data.gallery.length>=3
                                  ?Container(
                                    width: 168.w,
                                    height: 139.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.r),
                                      image: DecorationImage(
                                        image: NetworkImage("http://46.249.102.65:8080/storage/${state.response.data.gallery[2].path}"),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  )
                                :SizedBox(),

                                SizedBox(height: 6.w),
                                state.response.data.gallery.length>=4?
                                 Container(
                                    width: 168.w,
                                    height: 246.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.r),
                                      image: DecorationImage(
                                        image:NetworkImage("http://46.249.102.65:8080/storage/${state.response.data.gallery[3].path}"),
                                        fit: BoxFit.fill,
                                      ),

                                  ),
                                ):SizedBox(),
                              ],
                            ),

                          ],
                        ),
                        SizedBox(height: 6.h),
                       state.response.data.gallery.length>=5?
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 150.h,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.r),
                              image: DecorationImage(
                                image:NetworkImage("http://46.249.102.65:8080/storage/${state.response.data.gallery[4].path}"),
                                fit: BoxFit.fill,
                              ),

                          ),
                        ):SizedBox(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }

        return const SizedBox();
      },);
  }
}
