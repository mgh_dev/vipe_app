import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/nav_state.dart';



class NavViewModel extends Cubit<NavBaseState> {
  NavViewModel() : super(NavChangeState(index: 0));

  void changeNav(int i) {
    emit(NavChangeState(index: i));
  }
}
