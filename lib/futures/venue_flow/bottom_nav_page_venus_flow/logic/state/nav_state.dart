abstract class NavBaseState {}

class NavChangeState extends NavBaseState{
  final int index;

  NavChangeState({required this.index});
}
