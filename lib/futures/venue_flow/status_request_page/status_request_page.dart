import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class StatusRequestPage extends StatelessWidget {
  StatusRequestPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Request",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              Expanded(child: ListView.builder(
                itemCount: 13,
                  itemBuilder: (context, index) {
                    return Container(
                      decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.09),
                        borderRadius: BorderRadius.circular(8.r)
                      ),
                      margin: EdgeInsets.only(right: 16.w,left: 16.w,bottom: 8.h),
                      padding: EdgeInsets.symmetric(horizontal: 8.w,vertical: 4.h),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(7.r),
                            child: Image.asset(
                              ImagesApp.showsHouseofmusic,
                              width: 52.w,
                              height: 56.h,
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(width: 8.w,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Music  Fest ",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),),
                              Text("Waiting for Confirmation",style: inter.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w500,color: AppColor.greyTxt),),
                            ],
                          ),
                          Spacer(),
                          Container(
                            decoration: BoxDecoration(
                              color: AppColor.black12,
                              borderRadius: BorderRadius.circular(8.r)
                            ),
                            width: 52.w,
                            height: 52.h,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("03",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                Text("Aug",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey81),),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
              )
              )
            ],
          ),
        ],
      ),
    );
  }
}
