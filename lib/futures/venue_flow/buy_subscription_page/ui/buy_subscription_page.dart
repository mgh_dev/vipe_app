import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/payment_method_page/ui/payment_method_page.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';


class BuySubscriptionPage extends StatelessWidget {
  BuySubscriptionPage({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();
  SelectItemViewModel changeItemBloc = SelectItemViewModel();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: SvgPicture.asset(ImagesApp.icBack)),
                      Text(
                        "Subscription",
                        style: poppins.copyWith(
                            fontSize: 16.sp, fontWeight: FontWeight.w500),
                      ),
                      GestureDetector(
                          onTap: () {},
                          child: SvgPicture.asset(ImagesApp.icNotification)),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 25.h),
                  child: Text(
                    "Choose Your Plan",
                    style: poppins.copyWith(
                        fontSize: 20.sp, fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 15.h),
                  child: Text(
                    "Subscribe today & become part of a vibrant,\neclectic and inclusive local live music ecosystem.",
                    textAlign: TextAlign.center,
                    style: poppins.copyWith(
                      color: AppColor.greyHint,
                      fontSize: 11.sp,
                    ),
                  ),
                ),
                BlocBuilder(
                  bloc:changeItemBloc,
                  builder: (context, state) {
                    if(state is ItemChangeState){
                      return Container(
                        width: 343.w,
                        height: 54.h,
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(50)),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  GestureDetector(
                                    onTap: ()=>changeItemBloc.changeItem(0),
                                    child: Container(
                                      width: 180,
                                      height: 45.h,
                                      decoration: BoxDecoration(
                                          color:state.index==0?AppColor.blue:Colors.transparent,
                                          borderRadius: BorderRadius.circular(50)),
                                      child: Center(
                                        child: Text(
                                          "Monthly",
                                          style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  GestureDetector(
                                    onTap: ()=>changeItemBloc.changeItem(1),
                                    child: Container(
                                      width: 180,
                                      height: 45.h,
                                      decoration: BoxDecoration(
                                          color:state.index==1?AppColor.blue:Colors.transparent,
                                          borderRadius: BorderRadius.circular(50)),
                                      child: Center(
                                        child: Text(
                                          "Yearly",
                                          style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                margin: EdgeInsets.only(right: 27.w),
                                width: 28.w,
                                height: 35.h,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [
                                          const Color(0xff090B1B),
                                          AppColor.green,
                                          AppColor.green,
                                        ]),
                                    borderRadius: const BorderRadius.only(
                                        bottomLeft: Radius.circular(50),
                                        bottomRight: Radius.circular(50))),
                                child: Center(
                                    child: Text(
                                      "40%\nOFF",
                                      style: inter.copyWith(
                                          color: AppColor.white,
                                          fontSize: 8.sp,
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    )),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return SizedBox();
                  },
                ),
                SizedBox(height: 12.h),
                Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 6, left: 6),
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 6.w),
                        padding: EdgeInsets.only(top: 16.h),
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(children: [
                          Text(
                            "PREMIUM",
                            style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w600,
                                color: AppColor.green3),
                          ),
                          SizedBox(
                            height: 16.h,
                          ),
                          Text(
                            "Get 1 month free",
                            style: inter.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.white),
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          Text(
                            "\$179.99/month",
                            style: inter.copyWith(
                                fontSize: 32.sp,
                                fontWeight: FontWeight.w500,
                                color: AppColor.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 16.h, bottom: 17.h),
                            color: AppColor.white.withOpacity(0.14),
                            height: 0.5,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 21.w),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Ability to book unlimited gigs/month",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 14.sp),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Sponsored search",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 14.sp),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Ability to access the feed page / homepage & \nmap feature to view upcoming shows and event \ndetails.",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 11.sp),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Ability to create venue profile and add media kit \n(video/audio samples, social media integration, \nanalytics)",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 11.sp),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Ability to send messages to artists",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 11.sp),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.blue,
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "Access to artists’ calendars to view upcoming \nshows and request/book gigs.",
                                      style: poppins.copyWith(
                                          color: AppColor.greyCE,
                                          fontSize: 11.sp),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                right: 16.w, left: 16.w, top: 20.h),
                            child: MyCustomButton(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PaymentMethodPage()));
                              },
                              title: "Start 1 month free trial",
                              loading: false,
                              color: AppColor.green1,
                              width: 327.w,
                              height: 49.h,
                              fontSize: 14.sp,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                right: 16.w,
                                left: 16.w,
                                bottom: 24.h,
                                top: 8.h),
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Container(
                                height: 49.h,
                                width: 327.w,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.r),
                                    border: Border.all(
                                        width: 0.5,
                                        color:
                                            AppColor.white.withOpacity(0.14))),
                                child: Center(
                                  child: Text("No thanks",
                                      style: poppins.copyWith(
                                          color: AppColor.green1,
                                          fontSize: 14.sp)),
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 27.w),
                      width: 69.w,
                      height: 70.h,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.center,
                              colors: [
                                AppColor.green.withOpacity(0),
                                AppColor.green
                              ]),
                          borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(50),
                              bottomRight: Radius.circular(50))),
                      child: Center(
                          child: Text(
                        "MOST POPULER",
                        style: inter.copyWith(
                            color: AppColor.white,
                            fontSize: 11.sp,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      )),
                    ),
                  ],
                ),
                SizedBox(height: 20.h),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
