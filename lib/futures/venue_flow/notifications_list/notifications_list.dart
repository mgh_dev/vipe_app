import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class NotificationsList extends StatelessWidget {
  NotificationsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 12.h, left: 8.w,),
                child: Text("Notifications",style: poppins.copyWith(fontSize: 24.sp,fontWeight: FontWeight.w700),),
              ),
              Expanded(child:
              ListView.builder(
                itemCount: 12,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(bottom: 16.h,right: 16.w,left: 16.w),
                      padding: EdgeInsets.symmetric(vertical: 10.h),
                      child: Row(children: [
                        ClipRRect(
                          child: Image.asset(
                            ImagesApp.profileNotifications,
                            width: 48.w,
                            height: 48.h,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 12.w,),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Gig request received",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                              SizedBox(height: 6.h,),
                              Text("Thomas Champages would like to perform on Thuresday, march 15th",style: poppins.copyWith(fontSize: 13.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                              SizedBox(height: 8.h,),
                              Text("2h Ago",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.whiteE),),
                            ],
                          ),
                        )
                      ],),
                    );
                  },
              )
              )
            ],
          ),
        ],
      ),
    );
  }
}
