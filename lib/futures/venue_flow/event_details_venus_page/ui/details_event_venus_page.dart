import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/logic/state/get_event_details_state.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/logic/view_model/get_event_details_view_model.dart';

import '../../../../core/utils/style_text_app.dart';

class DetailsEventVenusPage extends StatefulWidget {
  final int idEvent;

  const DetailsEventVenusPage({required this.idEvent}) : super();

  @override
  State<DetailsEventVenusPage> createState() => _DetailsEventVenusPageState();
}

class _DetailsEventVenusPageState extends State<DetailsEventVenusPage> {
  final List<String> _chipList = [
    'Intimate',
    'Chill',
    'Bluesy',
  ];
  GetEventDetailsViewModel getEventDetailsViewModel =
      GetEventDetailsViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: BlocBuilder(
        bloc: getEventDetailsViewModel..getEvent(widget.idEvent),
        builder: (context, state) {
          if (state is GetEventDetailsLoadingState) {
            return const CustomLoading();
          }
          if (state is GetEventDetailsSuccessState) {
            return CustomScrollView(
              slivers: <Widget>[
                SliverToBoxAdapter(
                  child: Stack(
                    children: [
                      Container(
                        height: 383.h,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage("http://46.249.102.65:8080/storage/${state.getDetailsEventModel.data.image}"),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(20.r),
                            bottomLeft: Radius.circular(20.r),
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: SvgPicture.asset(ImagesApp.icBack)),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            SvgPicture.asset(ImagesApp.icShared),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.all(10),
                  sliver: SliverToBoxAdapter(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 27.w),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 22.h),
                                      Text(
                                        "Music Fest",
                                        style: poppins.copyWith(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        "time: ${state.getDetailsEventModel.data.calendar.date} ${state.getDetailsEventModel.data.calendar.startTime}",
                                        style: poppins.copyWith(
                                          color: AppColor.grey81,
                                          fontSize: 14,
                                        ),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: 15.w),
                                    child: Container(
                                      width: 47.w,
                                      height: 47.r,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                AppColor.green.withOpacity(0.1),
                                                AppColor.green
                                                    .withOpacity(0.35),
                                                AppColor.green
                                                    .withOpacity(0.55),
                                                AppColor.green
                                                    .withOpacity(0.65),
                                              ]),
                                          borderRadius: const BorderRadius.only(
                                              bottomLeft: Radius.circular(50),
                                              bottomRight:
                                                  Radius.circular(50))),
                                      child: Center(
                                          child: Text(
                                        "\$${state.getDetailsEventModel.data.ticketPrice}",
                                        style: inter.copyWith(
                                            color: AppColor.white,
                                            fontSize: 11,
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.center,
                                      )),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 6.h, bottom: 16.h),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child:  Wrap(
                                    children:
                                    state.getDetailsEventModel.data.vibe
                                        .map(
                                          (vibeText) => Padding(
                                        padding:
                                        EdgeInsets.only(right: 7.w),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 15.w),
                                          height: 27.h,
                                          decoration: BoxDecoration(
                                            color: AppColor.white.withOpacity(0.1),
                                            borderRadius: BorderRadius.circular(50.r),
                                            border:
                                            Border.all(color: AppColor.white.withOpacity(0.5), width: 0.3),
                                          ),
                                          child: Text(
                                            vibeText.name,
                                            style: poppins.copyWith(fontSize: 10.sp),
                                          ),
                                        ),
                                      ),
                                    )
                                        .toList(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 327.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.r),
                            border: Border.all(
                              color: AppColor.grey81.withOpacity(0.5),
                              width: 0.5.w,
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 4.h),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 4.w),
                                child: Container(
                                  width: 319.w,
                                  height: 65.h,
                                  decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.15),
                                    borderRadius: BorderRadius.circular(8.r),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 4.w, right: 8.w),
                                        child: Container(
                                          width: 52.w,
                                          height: 56.h,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  ImagesApp.venusProf),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 4.h),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "House of Music",
                                              style: poppins.copyWith(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  width: 16.w,
                                                  height: 16.r,
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white
                                                        .withOpacity(0.15),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.r),
                                                  ),
                                                  child: Center(
                                                    child: SvgPicture.asset(
                                                      ImagesApp.icLocation,
                                                      height: 10.r,
                                                      width: 10.w,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(width: 6.w),
                                                Text(
                                                  "Elgin St. Celina, Delaware",
                                                  style: inter.copyWith(
                                                    color: AppColor.greyTxt,
                                                    fontSize: 10,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 12.w, right: 12.w, top: 16.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Description",
                                      style: poppins.copyWith(
                                          color: AppColor.greyHint,
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: 8.h),
                                    RichText(
                                      textAlign: TextAlign.left,
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text:
                                                state.getDetailsEventModel.data.additionalInformation,
                                            style: poppins.copyWith(
                                              fontSize: 13.sp,
                                            ),
                                          ),
                                          TextSpan(
                                            text: "",
                                            style: poppins.copyWith(
                                              fontSize: 13.sp,
                                              color: AppColor.blue,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 16.h),
                                    DividerWidget(),
                                    SizedBox(height: 16.h),
                                    Text(
                                      "Lineup",
                                      style: poppins.copyWith(
                                          color: AppColor.greyHint,
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: 16.h),
                                    SizedBox(
                                      height: 100.h,
                                      child: Center(
                                        child: ListView.builder(
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount:state.getDetailsEventModel.data.gigRequest.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding: EdgeInsets.only(
                                                    right: 30.w),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      width: 48.w,
                                                      height: 48.r,
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        image: state.getDetailsEventModel.data.gigRequest[index].user.avatar==null
                                                        ?DecorationImage(
                                                          image: AssetImage(
                                                              ImagesApp
                                                                  .venusProf),
                                                        )
                                                        :DecorationImage(
                                                          image:NetworkImage("http://46.249.102.65:8080/${state.getDetailsEventModel.data.gigRequest[index].user.avatar!}"),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(height: 4.h),
                                                    SizedBox(
                                                      width: 100,
                                                      child: Text(
                                                        "${state.getDetailsEventModel.data.gigRequest[index].user.firstName} ${state.getDetailsEventModel.data.gigRequest[index].user.lastName}",
                                                        overflow: TextOverflow.ellipsis,
                                                        style: poppins.copyWith(
                                                            fontSize: 11.sp),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    SizedBox(height: 2.h),
                                                    Text(
                                                      "Rock & Pop",
                                                      style: poppins.copyWith(
                                                        color: AppColor.grey81,
                                                        fontSize: 10.sp,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }),
                                      ),
                                    ),
                                    SizedBox(height: 16.h),
                                    DividerWidget(),
                                    SizedBox(height: 16.h),
                                    Row(
                                      children: [
                                        Container(
                                          width: 44.w,
                                          height: 44.r,
                                          decoration: BoxDecoration(
                                            color: AppColor.white
                                                .withOpacity(0.15),
                                            borderRadius:
                                                BorderRadius.circular(12.r),
                                          ),
                                          child: Center(
                                            child: SvgPicture.asset(
                                              ImagesApp.icLocation,
                                              height: 18.r,
                                              width: 18.w,
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 12.w),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Address",
                                              style: poppins.copyWith(
                                                  fontSize: 11.sp,
                                                  color: AppColor.greyHint),
                                              textAlign: TextAlign.center,
                                            ),
                                            SizedBox(height: 8.h),
                                            Text(
                                              state.getDetailsEventModel.data.address.address,
                                              style: poppins.copyWith(
                                                  fontSize: 12.sp),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 14.h),
                                    GestureDetector(
                                      onTap: (){
                                        _launchUrl("https://maps.google.com/maps?q=31.839930836760104,54.361177637352164&ll=31.839930836760104,54.361177637352164&z=16");
                                      },
                                      child: Container(
                                        width: 304.w,
                                        height: 156.h,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(9.r),
                                          image: DecorationImage(
                                            image: AssetImage(ImagesApp.imgMap),
                                          ),
                                        ),
                                        child: Center(
                                          child: Container(
                                            width: 110.w,
                                            height: 110.r,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color:
                                                  AppColor.blue.withOpacity(0.3),
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 58.w,
                                                height: 58.r,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColor.blue,
                                                ),
                                                child: Center(
                                                    child: SvgPicture.asset(
                                                        ImagesApp
                                                            .icHomeLocation)),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 16.h),
                                    Text(
                                      "Age limit:",
                                      style: poppins.copyWith(
                                          fontSize: 11.sp,
                                          color: AppColor.greyHint),
                                    ),
                                    SizedBox(height: 8.h),
                                    Text(
                                     state.getDetailsEventModel.data.ageLimit,
                                      style: poppins.copyWith(fontSize: 12.sp),
                                    ),
                                    SizedBox(height: 8.h),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: 148.w,
                                          height: 49.h,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: 1,
                                              color: AppColor.grey1
                                                  .withOpacity(0.4),
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(50.r),
                                          ),
                                          child: Center(
                                            child: Text(
                                              "Share",
                                              style: poppins.copyWith(
                                                fontSize: 14.sp,
                                              ),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {},
                                          child: Container(
                                            width: 148.w,
                                            height: 49.h,
                                            decoration: BoxDecoration(
                                              color: AppColor.green,
                                              borderRadius:
                                                  BorderRadius.circular(50.r),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "Buy Now",
                                                style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 30.h),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }
          return const SizedBox();
        },
      ),
    );
  }

  List<Widget> _buildChips() {
    List<Widget> chips = [];

    for (int i = 0; i < _chipList.length; i++) {
      chips.add(GestureDetector(
        onTap: () {
          _chipList.removeAt(i);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 15.w),
          height: 27.h,
          decoration: BoxDecoration(
            color: AppColor.white.withOpacity(0.1),
            borderRadius: BorderRadius.circular(50.r),
            border:
                Border.all(color: AppColor.white.withOpacity(0.5), width: 0.3),
          ),
          child: Text(
            _chipList[i],
            style: poppins.copyWith(fontSize: 10.sp),
          ),
        ),
      ));
    }

    return chips;
  }
  Future<void> _launchUrl(String url) async {
    if (!await launchUrl(Uri.parse(url),mode:LaunchMode.externalNonBrowserApplication )) {
      throw 'Could not launch:: $url';
    }
  }
}
