import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/bottom_sheet_request_details.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class GigRequestListPage extends StatelessWidget {
  GigRequestListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Gig Requests",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              //tabs
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(
                      horizontal: 4.w,
                    ),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(18.r)),
                    margin: EdgeInsets.only(
                        right: 16.w,
                        left: 16.w,
                        top: 12.h),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 7.h,bottom: 4.h),
                          height: 1.5,
                          width: 21.5,
                          color: AppColor.white,
                        ),
                        Container(
                            height: 1.5,
                            width: 9.5,
                            color: AppColor.white),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 5.5.w,right: 5.5.w,top: 16.h,bottom: 16.h),
                          child: Center(child: Text("View and manage all incoming requests from artists.",style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),)),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 5.5.w),
                          child: Row(
                            children: [
                              Text(
                                "August",
                                style: inter.copyWith(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.grey7),
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Expanded(
                                  child: Container(
                                      height: 1,
                                      color: AppColor.white
                                          .withOpacity(0.2))),
                            ],
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          padding: EdgeInsets.only(top: 8.h),
                          itemCount: 2,
                          itemBuilder: (context, index) {
                            return Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(bottom: 8.h,right: 5.5.w,left: 5.5.w),
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(12.r)
                              ),
                              child: Column(children: [
                                GestureDetector(
                                  onTap: (){
                                    showModalBottomSheet(
                                      context: context,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20.r),
                                            topRight: Radius.circular(20.r)),
                                      ),
                                      backgroundColor: AppColor.darkBlue,
                                      isScrollControlled: true,
                                      builder: (BuildContext context) {
                                        return BottomSheetRequestDetailsw();
                                      },
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 22.w,top: 17.5.h,bottom: 14.5.h),
                                        child: Row(children: [
                                          SvgPicture.asset(ImagesApp.icClock),
                                          SizedBox(width: 8.w,),
                                          Text("24 Aug",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),)
                                        ],),
                                      ),
                                      Container(height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.07),),
                                      Padding(
                                        padding: EdgeInsets.only(left: 46.6.w,top: 15.5.h,bottom: 14.5.h),
                                        child: Row(children: [
                                          Image.asset(
                                            ImagesApp.profileRequest,
                                            width: 48.w,
                                            height: 48.r,
                                          ),
                                          SizedBox(width: 16.w,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("Audrey Soul",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                              Text("\$599",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                            ],
                                          )
                                        ],),
                                      ),
                                      Container(height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.20),),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 46.h,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 41.w,top: 12.h,bottom: 13.h),
                                          child: Row(children: [
                                            SvgPicture.asset(ImagesApp.icMessage),
                                            SizedBox(width: 8.w,),
                                            Text("Message",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,color: AppColor.whiteF8),),
                                          ],),
                                        ),
                                      ),
                                      Flexible(child: Container(color: AppColor.white.withOpacity(0.20),width: 1)),
                                      GestureDetector(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 12.h,bottom: 13.h,right: 41.w),
                                          child: Row(children: [
                                            SvgPicture.asset(ImagesApp.check),
                                            SizedBox(width: 8.w,),
                                            Text("Decline",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,color: AppColor.blueText2),),
                                          ],),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],),
                            );
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 5.5.w),
                          child: Row(
                            children: [
                              Text(
                                "October",
                                style: inter.copyWith(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.grey7),
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Expanded(
                                  child: Container(
                                      height: 1,
                                      color: AppColor.white
                                          .withOpacity(0.2))),
                            ],
                          ),
                        ),
                        ListView.builder(
                          padding: EdgeInsets.only(top: 8.h),
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: 1,
                          itemBuilder: (context, index) {
                            return Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(bottom: 8.h,right: 5.5.w,left: 5.5.w),
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(12.r)
                              ),
                              child: Column(children: [
                                GestureDetector(
                                  onTap: (){
                                    showModalBottomSheet(
                                      context: context,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20.r),
                                            topRight: Radius.circular(20.r)),
                                      ),
                                      backgroundColor: AppColor.darkBlue,
                                      isScrollControlled: true,
                                      builder: (BuildContext context) {
                                        return BottomSheetRequestDetailsw();
                                      },
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 22.w,top: 17.5.h,bottom: 14.5.h),
                                        child: Row(children: [
                                          SvgPicture.asset(ImagesApp.icClock),
                                          SizedBox(width: 8.w,),
                                          Text("24 Aug",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),)
                                        ],),
                                      ),
                                      Container(height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.07),),
                                      Padding(
                                        padding: EdgeInsets.only(left: 46.6.w,top: 15.5.h,bottom: 14.5.h),
                                        child: Row(children: [
                                          Image.asset(
                                            ImagesApp.profileRequest,
                                            width: 48.w,
                                            height: 48.r,
                                          ),
                                          SizedBox(width: 16.w,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("Audrey Soul",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                              Text("\$599",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                            ],
                                          )
                                        ],),
                                      ),
                                      Container(height: 1,width: double.infinity,color: AppColor.white.withOpacity(0.20),),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 46.h,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 41.w,top: 12.h,bottom: 13.h),
                                          child: Row(children: [
                                            SvgPicture.asset(ImagesApp.icMessage),
                                            SizedBox(width: 8.w,),
                                            Text("Message",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,color: AppColor.whiteF8),),
                                          ],),
                                        ),
                                      ),
                                      Flexible(child: Container(color: AppColor.white.withOpacity(0.20),width: 1)),
                                      GestureDetector(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 12.h,bottom: 13.h,right: 41.w),
                                          child: Row(children: [
                                            SvgPicture.asset(ImagesApp.check),
                                            SizedBox(width: 8.w,),
                                            Text("Decline",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,color: AppColor.blueText2),),
                                          ],),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
