import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/ui/sign_up_page.dart';
import 'package:vibe_app/futures/constant_pages/splash_page/ui/splash_page.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/Favourite%20Artist_tab.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/shows_tab.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/venues_tab_profile.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/shows_tab_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabgallery.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tab_media_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabreview.dart';

import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../profile_edit_venus_page/ui/profile_edit_venus_page.dart';

class ShowDetailsVenusPage extends StatefulWidget {
  const ShowDetailsVenusPage({Key? key}) : super(key: key);

  @override
  State<ShowDetailsVenusPage> createState() => _ShowDetailsVenusPageState();
}

class _ShowDetailsVenusPageState extends State<ShowDetailsVenusPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(
      length: 4,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    secondController.addListener(() {
      if (secondController.offset <=
          secondController.position.minScrollExtent) {
        firstController.animateTo(0,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      } else if (secondController.offset >=
          secondController.position.minScrollExtent) {
        firstController.animateTo(firstController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      }
    });
    return Scaffold(
        backgroundColor: AppColor.backgroundApp,
        body: CustomScrollView(
          controller: firstController,
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                children: [

                  Container(
                    height: 427.h,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(ImagesApp.detailsArtist),
                          fit: BoxFit.cover),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: <Color>[
                            const Color(0xff090B1B),
                            const Color(0xff090B1B).withOpacity(0.2),
                            Colors.transparent,
                            Colors.transparent,
                            Colors.transparent,
                            const Color(0xff090B1B).withOpacity(0.98),
                            const Color(0xff090B1B),
                          ],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: 53.h, left: 20.w, right: 20.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: SvgPicture.asset(ImagesApp.icBack)),
                                Image.asset(
                                  ImagesApp.logoApp,
                                  width: 60.w,
                                  height: 60.h,
                                ),
                                SvgPicture.asset(ImagesApp.icShared),
                              ],
                            ),
                          ),
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "House of Music",
                                    style: poppins.copyWith(
                                      fontSize: 26.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Icon(
                                    Icons.check_circle,
                                    color: AppColor.greyBlue2,
                                    size: 21,
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "4.5* ",
                                    style: poppins.copyWith(
                                      fontSize: 12.sp,
                                    ),
                                  ),
                                  Text(
                                    ". Concert hall .",
                                    style: poppins.copyWith(
                                      fontSize: 12.sp,
                                    ),
                                  ),
                                  Text(
                                    " Virginia",
                                    style: poppins.copyWith(
                                      fontSize: 12.sp,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 15),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 24.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.w),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                                "We're a premier event space that caters to a wide\nrange of events. Our space is equipped with state-of-\nthe-art audio and visual technology...",
                            style: poppins.copyWith(
                                fontSize: 12.sp, color: AppColor.grey1),
                          ),
                          TextSpan(
                            text: "Read more",
                            style: poppins.copyWith(
                              fontSize: 12.sp,
                              color: AppColor.blue,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
                child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: Container(
                      width: double.infinity,
                      height: 1,
                      color: AppColor.greyHint,
                    ),
                  ),
                  TabBar(
                    controller: tabController,
                    // dividerColor: Colors.red,
                    indicatorColor: AppColor.blue,
                    labelColor: AppColor.white,
                    unselectedLabelColor: AppColor.greyHint.withOpacity(0.5),
                    labelStyle: poppins.copyWith(
                      fontSize: 12.sp,
                    ),
                    isScrollable: true,

                    indicatorSize: TabBarIndicatorSize.tab,
                    tabs: const [
                      Tab(text: 'Events'),
                      Tab(text: 'Media'),
                      Tab(text: 'Gallery'),
                      Tab(text: 'Review'),
                    ],
                  ),
                ],
              ),
            )),
            SliverFillRemaining(
              child: TabBarView(
                controller: tabController,
                children: [
                  ShowsTabVenus(scrollController: secondController),
                  TabMediaVenus(scrollController: secondController),
                  TabGallery(scrollController: secondController,),
                  TabReview(scrollController: secondController),
                ],
              ),
            ),
          ],
        ));
  }
}
