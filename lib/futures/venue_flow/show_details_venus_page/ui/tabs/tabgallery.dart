import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabGallery extends StatelessWidget {
  TabGallery({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return  Expanded(
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(right: 16.w,left: 16.w,bottom: 8.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(7.r),
                      child: Image.asset(
                        ImagesApp.showsHouseofmusic,
                        width: 152.w,
                        height: 276.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: 8.h),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(7.r),
                      child: Image.asset(
                        ImagesApp.shadowHouseofmusic,
                        width: 152.w,
                        height: 139.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(7.r),
                      child: Image.asset(
                        ImagesApp.shadowHouseofmusic,
                        width: 152.w,
                        height: 139.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: 8.h),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(7.r),
                      child: Image.asset(
                        ImagesApp.showsHouseofmusic,
                        width: 152.w,
                        height: 276.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),

              ],
            ),
          );
        },
      ),
    );
  }
}
