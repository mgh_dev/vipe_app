import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabMedia extends StatelessWidget {
  TabMedia({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 16.w),
          child: Text("Videos",style: inter.copyWith(fontSize: 17.sp,fontWeight: FontWeight.w500,color: AppColor.white),),
        ),
        Expanded(
          child: ListView.builder(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: 15,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(bottom: 16.h,left: 15.w),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(7.r),
                          child: Image.asset(
                            ImagesApp.shadowHouseofmusic,
                            width: 129.w,
                            height: 72.h,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Text("12Min" " . " "Florida",style: inter.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w400,color: AppColor.greyText),)
                      ],
                    ),
                    SizedBox(
                      width: 12.w,
                    ),
                    Text("House of Music - Live DJ",style: inter.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w600,color: AppColor.white),),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(right: 23.w),
                      child: Text(":",style: inter.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w600,color: AppColor.white),),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
