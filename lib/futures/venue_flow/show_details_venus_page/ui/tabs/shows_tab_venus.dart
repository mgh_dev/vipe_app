import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

class ShowsTabVenus extends StatelessWidget {
  ShowsTabVenus({Key? key}) : super(key: key);
List<String>ls=[
  ImagesApp.show1,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
  ImagesApp.show2,
];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
        itemCount: 10,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(bottom: 12.h),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 129.w,
                  height: 79.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7.r),
                    image: DecorationImage(
                      image: AssetImage(ls[index]),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 12.w),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "R&B night",
                      style: poppins.copyWith(
                        fontSize: 14.sp,
                      ),
                    ),
                    SizedBox(height: 8.h),
                    Text(
                      "Mon, July 17. Starts at 7:30pm\nHouse of Music",
                      style: poppins.copyWith(
                        fontSize: 11.sp,
                        color: AppColor.grey1
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }
}
