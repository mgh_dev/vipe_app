import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/fan_flow/home_page/ui/home_page.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/ui/home_venus_page.dart';
import 'package:vibe_app/futures/venue_flow/profile_page_message_setup/ui/profile_page_message_setup.dart';
import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';

class MessageCompleteProfilePage extends StatefulWidget {
  const MessageCompleteProfilePage({Key? key}) : super(key: key);

  @override
  State<MessageCompleteProfilePage> createState() => _MessageCompleteProfilePageState();
}

class _MessageCompleteProfilePageState extends State<MessageCompleteProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Padding(
            padding: EdgeInsets.only(top: 57.h, bottom: 164.h),
            child: Image(
              image: AssetImage(
                ImagesApp.backgroundStar,
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 27.h),
                child: Text(
                  textAlign: TextAlign.center,
                  'Welcome to VIBE.\n You\'re now part of a\n community that loves\n and supports local live\n music, talent and venues.\n\nLet’s Vibe!',
                  style: poppins.copyWith(
                    fontSize: 24.sp,
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ),
              MyCustomButton(
                title: "Complete Profile",
                loading: false,
                onTap: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const ProfilePageMessageSetupVenus()));
                },
                width: 319.w,
                height: 49.h,
                fontSize: 14.sp,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h, bottom: 49.h),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>  BottomNavVenusPages(index: 0,)));
                  },
                  child: RichText(
                    text: TextSpan(
                      text: "Skip Now",
                      style: poppins.copyWith(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold,
                        color: AppColor.blue,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
