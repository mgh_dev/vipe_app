
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class NotificationSettingPageVenue extends StatefulWidget {
  const NotificationSettingPageVenue({super.key});


  @override
  State<NotificationSettingPageVenue> createState() => _NotificationSettingPageVenueState();
}

class _NotificationSettingPageVenueState extends State<NotificationSettingPageVenue> {
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 51.h, left: 24.w,),
                child: Text("Notification Setting",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),),
              ),
              SizedBox(height: 16.h,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.w,vertical: 16.h),
                child: Row(children: [
                  Text("Receive push notifications",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: false,
                    onToggled: (isToggled) {
                    },)
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 12.h),
                child: Row(children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Receive event updates",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                      Text("when booking has been successfully confirmed.",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                    ],
                  ),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                  },)
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 12.h),
                child: Row(children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Reminder notifications",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                      Text("the event and show up on time.",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                    ],
                  ),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 12.h),
                child: Row(children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Recommendations",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                      Text("based on the music genre, categories and vibes\nthey’ve selected.",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                    ],
                  ),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 12.h),
                child: Row(children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Reviews and feedback",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                      Text("leave a review or feedback after attending an\nevent.",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                    ],
                  ),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 12.h),
                child: Row(children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("In-app messaging",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                      Text("leave a review or feedback after attending an\nevent.",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                    ],
                  ),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 17.5.h),
                child: Row(children: [
                  Text("In-app messaging",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: false,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23.w,vertical: 17.5.h),
                child: Row(children: [
                  Text("Account activity",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400),),
                  const Spacer(),
                  CustomSwitch(
                    isToggled: true,
                    onToggled: (isToggled) {
                    },),
                ],),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
