class RequestSendRestPassModel {
  RequestSendRestPassModel({
    required this.password,
    required this.token,
    required this.email,
  });
  late final String password;
  late final String token;
  late final String email;

  RequestSendRestPassModel.fromJson(Map<String, dynamic> json){
    password = json['password'];
    token = json['token'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['password'] = password;
    _data['token'] = token;
    _data['email'] = email;
    return _data;
  }
}