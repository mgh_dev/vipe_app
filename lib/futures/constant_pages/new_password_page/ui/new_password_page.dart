import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/logic/state/send_change_pass_state.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/logic/view_model/send_change_pass_view_model.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/model/request_model/request_send_change_pass.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/ui/sgin_in_page.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';

class NewPasswordPage extends StatefulWidget {
  final String pinCode;
  final String email;

  NewPasswordPage({required this.pinCode, required this.email}) : super();

  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  final userRePassController = TextEditingController();
  final userPassController = TextEditingController();
  final viewModelChangePass = SendRestPassViewModel();
String pass="";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 39.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Enter new password",
                          style: poppins.copyWith(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 32.h, bottom: 8.h),
                          child: Text(
                            "Enter Password",
                            style: poppins.copyWith(
                                fontSize: 12.sp, color: AppColor.greyText),
                          ),
                        ),
                        Container(
                          width: 319.w,
                          height: 49.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            controller: userPassController,
                            hint: "Enter your password",
                            icon: ImagesApp.icPassT,

                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 12.h, bottom: 8.h),
                          child: Text(
                            "Re-Enter Password",
                            style: poppins.copyWith(
                                fontSize: 13.sp, color: AppColor.greyText),
                          ),
                        ),
                        Container(
                          width: 319.w,
                          height: 49.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            controller: userRePassController,
                            hint: "Enter your password",
                            icon: ImagesApp.icPassT,
                          ),
                        ),
                        SizedBox(height: 54.h),
                        BlocListener(
                          bloc: viewModelChangePass,
                          listener: (context, state) {
                            if (state is SendRestPassSuccess) {
                              CustomToast.show("Change Password Successes");
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignInPage(namePevriosPage: 'newPass', index: 0,)));
                            }
                            if (state is SendRestPassFailed) {
                              Navigator.pop(context);
                            }
                          },
                          child: MyCustomButton(
                            title: "Confirm",
                            loading: false,
                            onTap: () {
                              pass=userPassController.text;
                              print("<<<<<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>>>>>");
                              print(widget.email);
                              print(widget.pinCode);
                              print(pass);
                              print("<<<<<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>>>>>");
                              if (userPassController.text ==
                                      userRePassController.text &&
                                  userRePassController.text.isNotEmpty &&
                                  userPassController.text.isNotEmpty) {
                                userPassController.clear();
                                userRePassController.clear();
                                viewModelChangePass.sendRequestRestPass(
                                  requestSendChangePass:
                                      RequestSendRestPassModel(
                                    password:pass,
                                    token: widget.pinCode,
                                    email: widget.email,
                                  ),
                                );
                              } else {
                                CustomToast.show("Please enter the correct information");
                              }
                            },
                            color: AppColor.blue,
                            width: 319.w,
                            height: 49.h,
                            fontSize: 14.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
