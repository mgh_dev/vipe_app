abstract class SendRestPassBaseState {}

class SendRestPassInitial extends SendRestPassBaseState {}

class SendRestPassLoading extends SendRestPassBaseState {}

class SendRestPassSuccess extends SendRestPassBaseState {}

class SendRestPassFailed extends SendRestPassBaseState {}
