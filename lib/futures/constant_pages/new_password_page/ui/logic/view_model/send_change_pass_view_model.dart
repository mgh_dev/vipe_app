import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:vibe_app/core/config/web_service.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/logic/state/send_change_pass_state.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/model/request_model/request_send_change_pass.dart';

class SendRestPassViewModel extends Cubit<SendRestPassBaseState> {
  SendRestPassViewModel() : super(SendRestPassInitial());

  void sendRequestRestPass(
      {required RequestSendRestPassModel requestSendChangePass}) async {
    print("1");
    emit(SendRestPassLoading());
    print("2");
    var request = requestSendChangePass.toJson();
    print("3");
    print(request);
    try {
      print("4");
      var response =
          await WebService().dio.post("auth/reset-password", data: request);
      print("5");
      emit(SendRestPassSuccess());
    } on DioException catch (e) {
      emit(SendRestPassFailed());
    }
  }
}
