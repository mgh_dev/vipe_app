import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pinput/pinput.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../new_password_page/ui/new_password_page.dart';

class VerificationCodePage extends StatefulWidget {
  final String email;

  VerificationCodePage({required this.email}) : super();

  @override
  State<VerificationCodePage> createState() => _VerificationCodePageState();
}

class _VerificationCodePageState extends State<VerificationCodePage> {
  final defaultPinTheme = PinTheme(
    width: 60.w,
    height: 60.r,
    textStyle: poppins.copyWith(
      fontSize: 24.sp,
    ),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(25.r),
      color: AppColor.white.withOpacity(0.1),
      border: Border.all(color: Colors.transparent),
    ),
  );

  final pinController = TextEditingController();
  String pinCode="";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 39.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Enter your verification\n code",
                          style: poppins.copyWith(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 32.h),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: Pinput(
                            controller: pinController,
                            androidSmsAutofillMethod:
                                AndroidSmsAutofillMethod.smsUserConsentApi,
                            listenForMultipleSmsOnAndroid: true,
                            defaultPinTheme: defaultPinTheme,
                            length: 4,
                            hapticFeedbackType: HapticFeedbackType.lightImpact,
                            onCompleted: (pin) {
                              pinCode=pin;
                              debugPrint('onCompleted: $pin');
                            },
                            cursor: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(bottom: 9),
                                  width: 22,
                                  height: 1,
                                  color: Colors.transparent,
                                ),
                              ],
                            ),
                            focusedPinTheme: defaultPinTheme.copyWith(
                              decoration: defaultPinTheme.decoration!.copyWith(
                                  borderRadius: BorderRadius.circular(25.r),
                                  border: Border.all(color: Colors.transparent),
                                  color: AppColor.white.withOpacity(0.1)),
                              textStyle: poppins.copyWith(
                                fontSize: 24.sp,
                              ),
                            ),
                            submittedPinTheme: defaultPinTheme.copyWith(
                              decoration: defaultPinTheme.decoration!.copyWith(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(25.r),
                                border: Border.all(color: Colors.transparent),
                              ),
                              textStyle: poppins.copyWith(
                                fontSize: 24.sp,
                              ),
                            ),
                            errorPinTheme: defaultPinTheme.copyWith(
                              decoration: defaultPinTheme.decoration!.copyWith(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(25.r),
                                border: Border.all(color: Colors.transparent),
                              ),
                              textStyle: poppins.copyWith(
                                fontSize: 24.sp,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 36.h, bottom: 58.h),
                          child: MyCustomButton(
                            title: "Submit",
                            loading: false,
                            onTap: () {
                              if (pinController.length ==4) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => NewPasswordPage(
                                      pinCode: pinCode,
                                      email: widget.email,
                                    ),
                                  ),
                                );

                                pinController.clear();
                              } else {
                                CustomToast.show(
                                    "Please enter the correct pin code");
                              }
                            },
                            color: AppColor.blue,
                            width: 319.w,
                            height: 49.h,
                            fontSize: 14.sp,
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "Didn’t get any verification code? ",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp, color: AppColor.grey1),
                                ),
                                TextSpan(
                                  text: "Sent again",
                                  style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColor.blue,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
