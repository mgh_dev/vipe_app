import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/ui/sgin_in_page.dart';

import '../../../../../core/helper/custom_button.dart';
import '../../../../../core/utils/app_color.dart';
import '../../../../../core/utils/image_app.dart';
import '../../../../../core/utils/style_text_app.dart';
import '../../../../venue_flow/event_details_venus_page/ui/details_event_venus_page.dart';


class EventsGustePage extends StatelessWidget {
   EventsGustePage({Key? key}) : super(key: key);
   List<String> _chipList = [
    'Intimate',
    'Chill',
    'Bluesy',
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetailsEventVenusPage(idEvent: 0,),
            ),
          );
        },
        child: Column(
          children: [

            CarouselSlider.builder(
              itemCount: 5,
              itemBuilder:
                  (BuildContext context, int itemIndex, int pageViewIndex){
                return itemIndex<4
                    ? SizedBox(
                  height: 475.h,
                  width: 300.w,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: 300.w,
                          height: 461.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(20.r),
                          ),
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: EdgeInsets.only(left: 16.w, right: 16.w),
                              child: Column(
                                children: [
                                  SizedBox(height: 16.h),
                                  Container(
                                    width: 268.w,
                                    height: 293.h,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(ImagesApp.banner),
                                          fit: BoxFit.cover),
                                      borderRadius: BorderRadius.circular(20.r),
                                    ),
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        height: 45.h,
                                        // width: 240.w,
                                        decoration: BoxDecoration(
                                          // color: Color(0xff090B1B).withOpacity(0.5),
                                          gradient: const LinearGradient(
                                            begin: Alignment.bottomCenter,
                                            end: Alignment.topCenter,
                                            colors: <Color>[
                                              Color(0xff090B1B),
                                              Colors.transparent
                                            ],
                                          ),
                                          borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(20.r),
                                            bottomRight: Radius.circular(20.r),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 11.h,
                                              bottom: 15.h,
                                              left: 17.w),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                  ImagesApp.profileMenu),
                                              SizedBox(width: 4.w),
                                              Text(
                                                "Jerry Angel",
                                                style: poppins.copyWith(
                                                    fontWeight:
                                                    FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 11.h, bottom: 13.h),
                                            child: Text(
                                              "Music Fest",
                                              style: poppins.copyWith(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Revolution Hall",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Padding(
                                            padding:
                                            EdgeInsets.only(top: 12.h, bottom: 6.h),
                                            child: Text(
                                              "Friday, Feb 17 at 7:30pm",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  color: AppColor.blue,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          // SizedBox(height:6.h),
                                          Wrap(
                                            crossAxisAlignment: WrapCrossAlignment.start,
                                            runSpacing: 7.h,
                                            spacing: 4.w,
                                            runAlignment: WrapAlignment.start,
                                            alignment: WrapAlignment.start,
                                            children: _buildChips(),
                                          ),
                                        ],
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          margin: EdgeInsets.only(right: 25.w),
                                          width: 47.w,
                                          height: 47.h,
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    AppColor.green.withOpacity(0.1),
                                                    AppColor.green.withOpacity(0.35),
                                                    AppColor.green.withOpacity(0.55),
                                                    AppColor.green.withOpacity(0.65),
                                                  ]),
                                              borderRadius: const BorderRadius.only(
                                                  bottomLeft: Radius.circular(50),
                                                  bottomRight: Radius.circular(50))),
                                          child: Center(
                                              child: Text(
                                                "\$55",
                                                style: inter.copyWith(
                                                    color: AppColor.white,
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w600),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 32.w),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            width: 49.w,
                            height: 66.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.8),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "17",
                                  style: inter,
                                ),
                                Text(
                                  "Feb",
                                  style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    color: AppColor.black,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                    ],
                  ),
                )
                    :Container(
                  width: 300.w,
                  height: 461.h,
                  decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(20.r),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 200.h,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            Positioned(
                              top: -100,
                              left: -40,
                              child: Container(
                                width: 166.w,
                                height: 166.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.banner),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 50,
                              left: 100,
                              child: Container(
                                width: 61.w,
                                height: 61.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.icP1),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 15,
                              left: 185,
                              child: Container(
                                width: 61.w,
                                height: 61.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.icP3),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 35,
                              left: 270,
                              child: Container(
                                width: 89.w,
                                height: 89.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.icP1),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 95,
                              left: 180,
                              child: Container(
                                width: 97.w,
                                height: 97.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.icP2),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 135,
                              left: 110,
                              child: Container(
                                width: 44.w,
                                height: 44.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.venusProf),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 100,
                              left: -15,
                              child: Container(
                                width: 93.w,
                                height: 93.h,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(ImagesApp.icP4),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 15.h),
                                child: Text(
                                  "Discover, connect\n and curate live music\n & entertainment\n experiences.",
                                  textAlign: TextAlign.center,
                                  style: poppins.copyWith(
                                      fontSize: 18.sp, fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 8.h,
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                           ChooseYourRolePage()));
                                },
                                child: Text(
                                  "Sign Up to Continue",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.bold,
                                      color: AppColor.blue),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10.h, top: 20.h),
                                child: MyCustomButton(
                                  onTap: (){
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SignInPage(namePevriosPage: "role", index: 0)));
                                  },
                                  width: 259.w,
                                  height: 55.h,
                                  title: 'Sign In',
                                  loading: false,
                                  borderRadius: 16.r,
                                  fontSize: 16.sp,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
                    ;
              },
              options: CarouselOptions(
                height: 500,
                aspectRatio: 16 / 9,
                viewportFraction: 0.8,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                // autoPlay: true,
                // autoPlayInterval: const Duration(seconds: 3),
                // autoPlayAnimationDuration: const Duration(milliseconds: 800),
                // autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                enlargeFactor: 0.3,
                // onPageChanged: callbackFunction,
                scrollDirection: Axis.horizontal,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const SizedBox(height: 150),
          ],
        ),
      ),
    );
  }
  List<Widget> _buildChips() {
    List<Widget> chips = [];

    for (int i = 0; i < _chipList.length; i++) {
      chips.add(GestureDetector(
        onTap: () {
          _chipList.removeAt(i);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 15.w),
          height: 27.h,
          decoration: BoxDecoration(
            color: AppColor.white.withOpacity(0.1),
            borderRadius: BorderRadius.circular(50.r),
            border:
            Border.all(color: AppColor.white.withOpacity(0.5), width: 0.3),
          ),
          child: Text(
            _chipList[i],
            style: poppins.copyWith(fontSize: 10.sp),
          ),
        ),
      ));
    }

    return chips;
  }
}






