import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/futures/fan_flow/show_prifile_artist_details_profile/ui/show_profile_artist_details_page_fan.dart';

import '../../../../../core/utils/app_color.dart';
import '../../../../../core/utils/image_app.dart';
import '../../../../../core/utils/style_text_app.dart';

class ArtistGusteTab extends StatelessWidget {
  ArtistGusteTab({Key? key}) : super(key: key);
  final _searchController = TextEditingController();
  List<String> lsImage = [
    ImagesApp.p2,
    ImagesApp.p1,
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 289.w,
                height: 49.h,
                decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(14.r),
                ),
                child: CustomTextField(
                  controller: _searchController,
                  hint: "Search...",
                  icon: ImagesApp.icSearchBlue,
                  hintTextDirection: TextDirection.ltr,
                  suffixIcon: ImagesApp.icClose,
                ),
              ),
              SizedBox(width: 8.w),
              Container(
                width: 44.w,
                height: 44.h,
                decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(10.r),
                ),
                child: Center(child: SvgPicture.asset(ImagesApp.icFilter)),
              ),
            ],
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: lsImage.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(bottom: 8.h),
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                  const ShowProfileDetailsArtistFanPage()));
                        },
                        child: Container(
                          width: 335.w,
                          height: 65.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(8.r),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 4.w, top: 4.h, bottom: 4.h),
                                child: Container(
                                  width: 52.w,
                                  height: 56.h,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8.r),
                                    image: DecorationImage(
                                      image: AssetImage(lsImage[index]),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 8.w),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Eric Jones",
                                    style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: 16.w,
                                        height: 16.h,
                                        decoration: BoxDecoration(
                                          color: AppColor.white.withOpacity(0.1),
                                          borderRadius:
                                              BorderRadius.circular(4.r),
                                        ),
                                        child: Center(
                                            child: SvgPicture.asset(
                                          ImagesApp.icLocation,
                                          width: 10.w,
                                          height: 10.h,
                                        )),
                                      ),
                                      SizedBox(width: 6.w),
                                      Text(
                                        "Elgin St. Celina, Delaware",
                                        style: inter.copyWith(
                                          fontSize: 10.sp,
                                          color: AppColor.grey2,
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Spacer(),
                              Padding(
                                padding: EdgeInsets.only(top: 4.h, right: 4.w),
                                child: Container(
                                  width: 39.w,
                                  height: 20.h,
                                  decoration: BoxDecoration(
                                    color: AppColor.black38,
                                    borderRadius: BorderRadius.circular(4.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Singer",
                                      style: poppins.copyWith(
                                        fontSize: 9.sp,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}
