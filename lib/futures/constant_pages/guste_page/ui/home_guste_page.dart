import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/show_helper_app.dart';
import 'package:vibe_app/futures/constant_pages/guste_page/ui/tabs/venues_guste_tab.dart';
import 'package:vibe_app/futures/fan_flow/home_page/ui/tabs/events_tab.dart';
import 'package:vibe_app/futures/fan_flow/home_page/ui/tabs/venues_tab.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_drawer.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';

import '../bloc/bloc_tabs/tabs_state.dart';
import '../bloc/bloc_tabs/tabs_view_model.dart';
import 'tabs/artist_guste_tab.dart';
import 'tabs/events_guste_tab.dart';

class HomeGustePage extends StatefulWidget {
  HomeGustePage({Key? key}) : super(key: key);

  @override
  State<HomeGustePage> createState() => _HomeGustePageState();
}

class _HomeGustePageState extends State<HomeGustePage> {
  final _tabsBloc = TabsGusteViewModel();
  final List<Widget> lsTabs = [
     EventsGustePage(),
    ArtistGusteTab(),
    VenuesGusteTab(),
  ];
  bool isHelperApp = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isHelperApp = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      extendBody: true,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding:
                EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),

                  ],
                ),
              ),
              BlocBuilder(
                  bloc: _tabsBloc,
                  builder: (context, state) {
                    if (state is TabsGusteIndexState) {
                      return Expanded(
                        child: Column(
                          children: [
                            Container(
                              width: 240.w,
                              height: 46.h,
                              decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(50.r),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        _tabsBloc.changeIndex(0);
                                      },
                                      child: Container(
                                        width: 78.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 0
                                              ? AppColor.blue
                                              : Colors.transparent,
                                          borderRadius:
                                          BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Events",
                                            style: poppins,
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        _tabsBloc.changeIndex(1);
                                      },
                                      child: Container(
                                        width: 78.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 1
                                              ? AppColor.blue
                                              : Colors.transparent,
                                          borderRadius:
                                          BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Artists",
                                            style: poppins,
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        _tabsBloc.changeIndex(2);
                                      },
                                      child: Container(
                                        width: 78.w,
                                        height: 42.h,
                                        decoration: BoxDecoration(
                                          color: state.index == 2
                                              ? AppColor.blue
                                              : Colors.transparent,
                                          borderRadius:
                                          BorderRadius.circular(50.r),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Venues",
                                            style: poppins,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 39.h,
                            ),
                            Expanded(
                              child: lsTabs[state.index],
                            ),
                          ],
                        ),
                      );
                    }
                    return const SizedBox();
                  }),
            ],
          ),
          GestureDetector(
              onTap: () {
                setState(() {
                  isHelperApp = !isHelperApp;
                });
              },
              child:
              Visibility(visible: isHelperApp, child: const ShowHelperApp())),
        ],
      )

    );
  }
}
