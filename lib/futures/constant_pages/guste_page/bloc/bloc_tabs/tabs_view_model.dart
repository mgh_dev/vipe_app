import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/constant_pages/guste_page/bloc/bloc_tabs/tabs_state.dart';
import 'package:vibe_app/futures/fan_flow/home_page/bloc/bloc_tabs/tabs_state.dart';

class TabsGusteViewModel extends Cubit<TabsGusteBaseState> {
  TabsGusteViewModel() : super(TabsGusteIndexState(index: 0));

  void changeIndex(int index) {
    emit(TabsGusteIndexState(index: index));
  }
}
