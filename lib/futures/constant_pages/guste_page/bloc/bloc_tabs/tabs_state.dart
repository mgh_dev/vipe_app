abstract class TabsGusteBaseState {}

class TabsGusteIndexState extends TabsGusteBaseState {
  final int index;

  TabsGusteIndexState({required this.index});
}
