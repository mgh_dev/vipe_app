

import 'package:bloc/bloc.dart';

part 'check_emoji_state.dart';

class CheckEmojiViewModel extends Cubit<CheckEmojiBaseState>{
  CheckEmojiViewModel():super(CheckIsEmojiState(isEmoji: true));

  void updateEmojiState(bool isEmoji){
    emit(CheckIsEmojiState(isEmoji: isEmoji));
  }
}