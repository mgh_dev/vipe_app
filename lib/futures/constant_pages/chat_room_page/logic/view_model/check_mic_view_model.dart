import 'package:bloc/bloc.dart';
import 'package:path_provider/path_provider.dart';

part 'check_mic_state.dart';

class CheckMicViewModel extends Cubit<CheckMicBaseState> {
  CheckMicViewModel() : super(RecordVoiceState(isRecording: false));

  void messageState() {
    emit(SendMessageState());
  }

  void voiceState(bool value) {
    emit(RecordVoiceState(isRecording: value));
  }

    // void _startOrStopRecording() async {
    //   try {
    //    String path = "${(await getApplicationDocumentsDirectory()).path}/recording.m4a";
    //
    //     late final RecorderController
    //     recorderController = RecorderController()
    //       ..androidEncoder = AndroidEncoder.aac
    //       ..androidOutputFormat = AndroidOutputFormat.mpeg4
    //       ..iosEncoder = IosEncoder.kAudioFormatMPEG4AAC
    //       ..sampleRate = 44100;
    //       await recorderController.record(path: path);
    //
    //   } catch (e) {
    //     debugPrint(e.toString());
    //   } finally {
    //     setState(() {
    //       isRecording = !isRecording;
    //     });
    //   }
    // }

}
