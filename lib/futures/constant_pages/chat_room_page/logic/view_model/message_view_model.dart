import 'package:flutter_bloc/flutter_bloc.dart';
import '../model/message_model.dart';

part 'messages_state.dart';

class MessageViewModel extends Cubit<MessagesBaseState> {
  MessageViewModel() : super(MessageInitialState(messages: []));

  void fetchMessage() {
    emit(MessageLoadingState(messages: state.messages));
    emit(MessageSuccessState(messages: [
      MessageModel(
          id: 1,
          senderId: 1,
          receiverId: 2,
          type: MessageType.text,
          value: "The first message")
    ]));
  }

  void addMessage(
      {required int senderId, required int receiverId, required String value, required MessageType type}) {
    emit(MessageLoadingState(messages: state.messages));
    state.messages.add(MessageModel(
        id: 1,
        senderId: senderId,
        receiverId: receiverId,
        type: type,
        value: value));
    emit(MessageSuccessState(messages: state.messages));
  }
}
