part of 'message_view_model.dart';

abstract class MessagesBaseState {
  List<MessageModel> messages;

  MessagesBaseState({required this.messages});
}

class MessageInitialState extends MessagesBaseState {
  MessageInitialState({required super.messages});
}

class MessageLoadingState extends MessagesBaseState {
  MessageLoadingState({required super.messages});
}

class MessageSuccessState extends MessagesBaseState {
  MessageSuccessState({required super.messages});
}

class MessageFailState extends MessagesBaseState {
  final String errorMessage;

  MessageFailState({
    required super.messages,
    required this.errorMessage,
  });
}
