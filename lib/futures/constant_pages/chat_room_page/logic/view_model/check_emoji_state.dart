part of 'check_emoji_view_model.dart';
abstract class CheckEmojiBaseState{}

class CheckIsEmojiState extends CheckEmojiBaseState{
  final bool isEmoji;

  CheckIsEmojiState({required this.isEmoji});
}