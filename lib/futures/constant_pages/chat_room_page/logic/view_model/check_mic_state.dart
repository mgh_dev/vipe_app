part of 'check_mic_view_model.dart';

abstract class CheckMicBaseState {}

class SendMessageState extends CheckMicBaseState {}

class RecordVoiceState extends CheckMicBaseState {
  final bool isRecording;

  RecordVoiceState({required this.isRecording});
}
