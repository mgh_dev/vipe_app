class MessageModel {
  late final int id;
  late final int senderId;
  late final int receiverId;
  late final MessageType type;
  late final String value;

  MessageModel({
    required this.id,
    required this.senderId,
    required this.receiverId,
    required this.type,
    required this.value,
  });

}

enum MessageType { text, image, voice, video, }
