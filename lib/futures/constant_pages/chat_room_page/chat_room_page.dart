import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/widget/custom_image.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/widget/custom_text.dart';
import '../../../core/helper/custom_text_fild.dart';
import 'logic/model/message_model.dart';
import 'logic/view_model/check_emoji_view_model.dart';
import 'logic/view_model/check_mic_view_model.dart';
import 'logic/view_model/message_view_model.dart';

class ChatRoomPage extends StatefulWidget {
  const ChatRoomPage({Key? key}) : super(key: key);

  @override
  State<ChatRoomPage> createState() => _ChatRoomPageState();
}

class _ChatRoomPageState extends State<ChatRoomPage> {
  final _controllerMassageBox = TextEditingController();

  // RecorderController controller = RecorderController();
  final MessageViewModel _messageViewModel = MessageViewModel();
  final CheckMicViewModel _checkMicViewModel = CheckMicViewModel();
  final CheckEmojiViewModel _emojiViewModel = CheckEmojiViewModel();
  FocusNode inputNode = FocusNode();

  Future<File?> pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image != null) {
        print("ok");
        final imageTemporary = File(image.path);
        return imageTemporary;
      }
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  @override
  void initState() {
    _messageViewModel.fetchMessage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.w, top: 20.h, bottom: 22.h),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Messages",
                    style: poppins.copyWith(
                        fontSize: 24.sp,
                        fontWeight: FontWeight.w600,
                        color: AppColor.white),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.transparent,
                  ),
                  child: Column(
                    children: [
                      Expanded(
                        child: BlocBuilder(
                          bloc: _messageViewModel,
                          builder: (context, state) {
                            if (state is MessageSuccessState) {
                              return ListView.builder(
                                  itemCount: state.messages.length,
                                  itemBuilder: (context, index) {
                                    var item = state.messages[index];
                                    if (item.type == MessageType.text) {
                                      return CustomText(
                                        textMassage:
                                            state.messages[index].value,
                                        sender:
                                            state.messages[index].senderId == 1,
                                        msgHour: DateTime.now().hour,
                                        msgMin: DateTime.now().minute,
                                        nameSender: "mohammad",
                                      );
                                    } else if (item.type == MessageType.image) {
                                      return CustomImage(
                                          pathImage: item.value,
                                          sender:
                                              state.messages[index].senderId ==
                                                  1);
                                    }
                                    return const Text("Message not supported");
                                  });
                            }
                            return const SizedBox();
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 19.w, vertical: 24),
                        child: BlocBuilder(
                          bloc: _checkMicViewModel,
                          builder: (context, state) {
                            return Stack(
                              children: [
                                Row(
                                  children: [
                                    Visibility(
                                      visible: state is SendMessageState ||
                                          (state is RecordVoiceState &&
                                              !state.isRecording),
                                      child: Container(
                                        width: 285.w,
                                        height: 54.h,
                                        decoration: BoxDecoration(
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                            borderRadius:
                                                BorderRadius.circular(50.r)),
                                        child: Row(
                                          children: [
                                            SizedBox(width: 16.w),
                                            SvgPicture.asset(ImagesApp.icEmogi),
                                            Expanded(
                                              child: CustomTextField(
                                                controller:
                                                    _controllerMassageBox,
                                                hint: "Type your message",
                                                onChanged: (_) {
                                                  if (_.trim().isNotEmpty) {
                                                    _checkMicViewModel
                                                        .messageState();
                                                  } else {
                                                    _checkMicViewModel
                                                        .voiceState(false);
                                                  }
                                                },
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  right: 17.w, left: 3.w),
                                              child: GestureDetector(
                                                onTap: () {
                                                  showModalBottomSheet(
                                                    isScrollControlled: true,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(
                                                                10.r),
                                                        topRight:
                                                            Radius.circular(
                                                                10.r),
                                                      ),
                                                    ),
                                                    context: context,
                                                    builder: (context) =>
                                                        Container(
                                                      width: double.infinity,
                                                      height: 150.h,
                                                      decoration: BoxDecoration(
                                                          color:
                                                              AppColor.darkBlue,
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10.r),
                                                            topRight:
                                                                Radius.circular(
                                                                    10.r),
                                                          )),
                                                      child: Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                              top: 15.h,
                                                              right: 15.w,
                                                            ),
                                                            child: Align(
                                                              alignment:
                                                                  Alignment
                                                                      .topRight,
                                                              child: GestureDetector(
                                                                onTap: (){
                                                                  Navigator.pop(context);
                                                                },
                                                                child: Container(
                                                                  width: 35.w,
                                                                  height: 35.r,
                                                                  decoration: BoxDecoration(
                                                                    color: Colors.white,
                                                                    borderRadius: BorderRadius.circular(10.r),
                                                                  ),
                                                                  child: const Icon(
                                                                    Icons.close,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              GestureDetector(
                                                                onTap:
                                                                    () async {
                                                                  final img = await pickImage(
                                                                      ImageSource
                                                                          .gallery);
                                                                  if (img !=
                                                                      null) {
                                                                    _messageViewModel
                                                                        .addMessage(
                                                                      senderId:
                                                                          2,
                                                                      receiverId:
                                                                          4,
                                                                      type: MessageType
                                                                          .image,
                                                                      value: img
                                                                          .path,
                                                                    );
                                                                    Navigator.pop(
                                                                        context);
                                                                  } else {
                                                                    print(
                                                                        "no select image");
                                                                  }
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    SizedBox(
                                                                        width: 50
                                                                            .r,
                                                                        height: 50
                                                                            .h,
                                                                        child:
                                                                            const Icon(
                                                                          Icons
                                                                              .image,
                                                                          color:
                                                                              Colors.white,
                                                                        )),
                                                                    Text(
                                                                      "Gallery",
                                                                      style: poppins
                                                                          .copyWith(
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              GestureDetector(
                                                                onTap:
                                                                    () async {
                                                                  final img = await pickImage(
                                                                      ImageSource
                                                                          .camera);
                                                                  if (img !=
                                                                      null) {
                                                                    _messageViewModel
                                                                        .addMessage(
                                                                      senderId:
                                                                          2,
                                                                      receiverId:
                                                                          4,
                                                                      type: MessageType
                                                                          .image,
                                                                      value: img
                                                                          .path,
                                                                    );
                                                                    Navigator.pop(
                                                                        context);
                                                                  } else {
                                                                    print(
                                                                        "no select image");
                                                                  }
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    SizedBox(
                                                                        width: 50
                                                                            .r,
                                                                        height: 50
                                                                            .h,
                                                                        child:
                                                                            const Icon(
                                                                          Icons
                                                                              .camera,
                                                                          color:
                                                                              Colors.white,
                                                                        )),
                                                                    Text(
                                                                      "Camera",
                                                                      style: poppins
                                                                          .copyWith(
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: SvgPicture.asset(
                                                    ImagesApp.icPaperclip),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8.w),
                                    Container(
                                      width: 44.w,
                                      height: 44.h,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 10.h),
                                      decoration: BoxDecoration(
                                          color: AppColor.green3,
                                          shape: BoxShape.circle),
                                      child: GestureDetector(
                                        onTap: () {
                                          _messageViewModel.addMessage(
                                              senderId: 4,
                                              receiverId: 1,
                                              type: MessageType.text,
                                              value:
                                                  _controllerMassageBox.text);
                                          _controllerMassageBox.text = '';
                                          _checkMicViewModel.voiceState(false);
                                        },
                                        child: SvgPicture.asset(
                                            ImagesApp.icSendMsg),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  _cancelVoice() {
    _checkMicViewModel.voiceState(false);
  }
}
