import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

class CustomText extends StatefulWidget {
  final String textMassage;
  final bool sender;
  final int msgHour;
  final int msgMin;
  final String nameSender;

  const CustomText({
    Key? key,
    required this.textMassage,
    required this.sender,
    required this.msgHour,
    required this.msgMin,
    required this.nameSender,
  }) : super(key: key);

  @override
  State<CustomText> createState() => _CustomTextState();
}

class _CustomTextState extends State<CustomText> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: widget.sender ? TextDirection.rtl : TextDirection.ltr,
      child: Padding(
        padding:
            EdgeInsetsDirectional.only(end: 111.w, bottom: 16.h, start: 20.w),
        child: SizedBox(
          child: Align(
            alignment:widget.sender?Alignment.topRight:Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(widget.nameSender,style: poppins.copyWith(fontSize: 14.sp,color: Colors.white),),
                   SizedBox(width: 16.w),
                    Text("${widget.msgHour}:${widget.msgMin}",style: poppins.copyWith(fontSize: 12.sp,color: AppColor.greyTxt),),
                  ],
                ),
                Container(
                  width: 214,
                  padding: EdgeInsetsDirectional.only(
                    top: 14.h,
                    start: 14.w,
                    bottom: 14.h,
                    end: 14.w,
                  ),
                  decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.2),
                    borderRadius: BorderRadiusDirectional.only(
                      bottomStart: Radius.circular(10.r),
                      topEnd: Radius.circular(10.r),
                      bottomEnd: Radius.circular(10.r),
                    ),
                  ),
                  child: Text(
                    widget.textMassage,
                    style: poppins.copyWith(
                      color: widget.sender ? AppColor.greyHint : AppColor.greyHint,
                      fontSize: 13,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
