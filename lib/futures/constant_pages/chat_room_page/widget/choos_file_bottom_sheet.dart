import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/utils/style_text_app.dart';



class ChooseFileBottomSheet extends StatefulWidget {
  const ChooseFileBottomSheet({Key? key}) : super(key: key);

  @override
  State<ChooseFileBottomSheet> createState() => _ChooseFileBottomSheetState();
}

class _ChooseFileBottomSheetState extends State<ChooseFileBottomSheet> {
  Future<File?> pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image != null) {
        print("ok");
        final imageTemporary = File(image.path);
        return imageTemporary;
      }
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 150.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.r),
            topRight: Radius.circular(10.r),
          )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 50.r,
                height: 50.h,
                child: Image.asset(
                  "assets/images/gallery.png",
                  color: Colors.red,
                ),
              ),
              Text(
                "Gallery",
                style: poppins.copyWith(
                  color: Colors.red,
                ),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 50.r,
                height: 50.h,
                child: Image.asset(
                  "assets/images/Camera-Icon.png",
                  color: Colors.red,
                ),
              ),
              Text(
                "Gallery",
                style: poppins.copyWith(
                  color: Colors.red,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
