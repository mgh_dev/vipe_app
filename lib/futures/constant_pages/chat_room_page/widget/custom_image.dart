import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/utils/app_color.dart';

class CustomImage extends StatefulWidget {
  final String pathImage;
  final bool sender;

  const CustomImage({
    required this.pathImage,
    required this.sender,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomImage> createState() => _CustomImageState();
}

class _CustomImageState extends State<CustomImage> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: widget.sender ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        margin:
            EdgeInsetsDirectional.only(bottom: 10.h, end: 140.w, start: 20.w),
        padding: const EdgeInsetsDirectional.all(4),
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.2),
          borderRadius: BorderRadiusDirectional.only(
            topStart: Radius.circular(10.r),
            topEnd: Radius.circular(10.r),
            bottomEnd: Radius.circular(10.r),
          ),
        ),
        child: Center(
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadiusDirectional.only(
                  topStart: Radius.circular(10.r),
                  topEnd: Radius.circular(10.r),
                  bottomStart: Radius.circular(10.r),
                  bottomEnd: Radius.circular(10.r),
                ),
                child: Image.file(File(widget.pathImage), fit: BoxFit.cover),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
