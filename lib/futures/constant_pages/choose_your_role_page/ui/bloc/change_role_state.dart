  abstract class ChangeRoleBaseState {}

class ChangeRoleIndexState extends ChangeRoleBaseState {
  final int index;

  ChangeRoleIndexState({required this.index});
}
