import 'package:bloc/bloc.dart';

import 'change_role_state.dart';

class ChangeRoleViewModel extends Cubit<ChangeRoleBaseState> {
  ChangeRoleViewModel() : super(ChangeRoleIndexState(index: 0));

  void changeIndex(int index) {
    emit(ChangeRoleIndexState(index: index));
  }
}
