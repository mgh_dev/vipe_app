import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../sgin_in_page/ui/sgin_in_page.dart';
import '../../sign_up_page/ui/sign_up_page.dart';
import 'bloc/change_role_state.dart';
import 'bloc/change_role_view_model.dart';

class ChooseYourRolePage extends StatelessWidget {
  ChooseYourRolePage({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100,
              top: -100,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              // Padding(
              //   padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       GestureDetector(
              //           onTap: () {
              //             // Navigator.pop(context);
              //           },
              //           child: SvgPicture.asset(ImagesApp.icBack)),
              //       Image.asset(
              //         ImagesApp.logoApp,
              //         width: 60.w,
              //         height: 60.h,
              //       ),
              //       SvgPicture.asset(ImagesApp.icShared),
              //     ],
              //   ),
              // ),
              SizedBox(
                height: 70.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      BlocBuilder(
                        bloc: changeRoleBloc,
                        builder: (context, state) {
                          if (state is ChangeRoleIndexState) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 22.w),
                                  child: Column(
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Choose your Role",
                                          style: poppins.copyWith(
                                            fontSize: 24.sp,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 6.h,
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Select the option that best describes you.",
                                          style: montserrat.copyWith(
                                            fontSize: 14.sp,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 61.h,
                                ),
                                state.index == 0
                                    ? Container(
                                        width: 340.w,
                                        decoration: BoxDecoration(
                                          color: AppColor.green,
                                          borderRadius:
                                              BorderRadius.circular(16.r),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 14.h),
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 62.h),
                                                child: Align(
                                                    alignment:
                                                        Alignment.bottomRight,
                                                    child: SvgPicture.asset(
                                                        ImagesApp.building)),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 22.w, top: 14.h),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Business or Event\nOrganizer",
                                                      style: poppins.copyWith(
                                                        fontSize: 24.sp,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                    Text(
                                                      "I am a business, venue, promoter\nor event organizer.",
                                                      style: poppins.copyWith(
                                                        fontSize: 14.sp,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 14,
                                                              bottom: 16),
                                                      child: MyCustomButton(
                                                        title: "Sign Up",
                                                        loading: false,
                                                        onTap: () {
                                                          Navigator.pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          SignUpPage(
                                                                            index:
                                                                                1,
                                                                          )));
                                                        },
                                                        color: AppColor.black,
                                                        width: 133.w,
                                                        height: 49.h,
                                                        fontSize: 14.sp,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        borderRadius: 16.sp,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 16.h, bottom: 0.h),
                                        child: GestureDetector(
                                          onTap: () {
                                            changeRoleBloc.changeIndex(0);
                                          },
                                          child: Container(
                                            width: 340.w,
                                            height: 102,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(16.sp),
                                              gradient: const LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                colors: <Color>[
                                                  Color(0xff383A45),
                                                  Color(0xff13151F),
                                                ],
                                              ),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 14.h, left: 22.w),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Business or Event Organizer",
                                                    style: montserrat.copyWith(
                                                        fontSize: 16.sp,
                                                        color: AppColor.white,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(height: 4.h),
                                                  Text(
                                                    "I am a business, venue, promoter\nor event organizer.",
                                                    style: montserrat.copyWith(
                                                      fontSize: 10.sp,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                state.index == 1
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 16.h, bottom: 16.h),
                                        child: Container(
                                          width: 340.w,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(16.r),
                                            color: AppColor.green,
                                          ),
                                          child: Stack(
                                            alignment: Alignment.bottomLeft,
                                            children: [
                                              Align(
                                                  alignment:
                                                      Alignment.bottomRight,
                                                  child: SvgPicture.asset(
                                                      ImagesApp.veArtist)),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                  top: 14.h,
                                                  left: 22.w,
                                                  bottom: 16.h,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    RichText(
                                                      text: TextSpan(children: [
                                                        TextSpan(
                                                            text:
                                                                "Artist or Band",
                                                            style: poppins
                                                                .copyWith(
                                                              fontSize: 24.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            )),
                                                        TextSpan(
                                                            text:
                                                                "\nI am an artist, musician,\n performer, comedian or\n band.",
                                                            style: poppins
                                                                .copyWith(
                                                              fontSize: 14.sp,
                                                              // fontWeight:FontWeight.bold,
                                                            ))
                                                      ]),
                                                    ),
                                                    SizedBox(height: 6.h),
                                                    MyCustomButton(
                                                      title: "Sign Up",
                                                      loading: false,
                                                      onTap: () {
                                                        Navigator.pushReplacement(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        SignUpPage(
                                                                          index:
                                                                              2,
                                                                        )));
                                                      },
                                                      color: AppColor.black,
                                                      width: 133.w,
                                                      height: 49.h,
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      borderRadius: 16.sp,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 16.h, bottom: 16.h),
                                        child: GestureDetector(
                                          onTap: () {
                                            changeRoleBloc.changeIndex(1);
                                          },
                                          child: Container(
                                            width: 340.w,
                                            height: 81,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(16.sp),
                                              gradient: const LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                colors: <Color>[
                                                  Color(0xff383A45),
                                                  Color(0xff13151F),
                                                ],
                                              ),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 14.h, left: 22.w),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Artist or Band",
                                                    style: montserrat.copyWith(
                                                        fontSize: 16.sp,
                                                        color: AppColor.white,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(height: 4.h),
                                                  Text(
                                                    "I am an artist, musician, performer\n, comedian or band.",
                                                    style: montserrat.copyWith(
                                                      fontSize: 10.sp,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                state.index == 2
                                    ? Container(
                                        width: 340.w,
                                        height: 182.h,
                                        decoration: BoxDecoration(
                                          color: AppColor.green,
                                          borderRadius:
                                              BorderRadius.circular(16.r),
                                        ),
                                        child: Stack(
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  top: 14.h, left: 22.w),
                                              child: RichText(
                                                  text: TextSpan(children: [
                                                TextSpan(
                                                  text:
                                                      "Live Entertainment Fan\n",
                                                  style: poppins.copyWith(
                                                    fontSize: 24.sp,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text:
                                                      "I am a music fan looking for local\nlive music & entertainment.",
                                                  style: poppins.copyWith(
                                                    fontSize: 14.sp,
                                                  ),
                                                )
                                              ])),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: 22.w, bottom: 22.h),
                                              child: Align(
                                                alignment: Alignment.bottomLeft,
                                                child: MyCustomButton(
                                                  title: "Sign Up",
                                                  loading: false,
                                                  onTap: () {
                                                    Navigator.pushReplacement(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    SignUpPage(
                                                                      index: 3,
                                                                    )));
                                                  },
                                                  color: AppColor.black,
                                                  width: 133.w,
                                                  height: 49.h,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.bold,
                                                  borderRadius: 16.sp,
                                                ),
                                              ),
                                            ),
                                            Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SvgPicture.asset(
                                                    ImagesApp.vcSignUpFan)),
                                          ],
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(bottom: 16.h),
                                        child: GestureDetector(
                                          onTap: () {
                                            changeRoleBloc.changeIndex(2);
                                          },
                                          child: Container(
                                            width: 340.w,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(16.sp),
                                              gradient: const LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                colors: <Color>[
                                                  Color(0xff383A45),
                                                  Color(0xff13151F),
                                                ],
                                              ),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 19.h,
                                                  left: 22.w,
                                                  bottom: 25),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Live entertainment fan",
                                                    style: montserrat.copyWith(
                                                        fontSize: 16.sp,
                                                        color: AppColor.white,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(height: 4.h),
                                                  Text(
                                                    "I am a music fan looking for local\nlive music & entertainment.",
                                                    style: montserrat.copyWith(
                                                      fontSize: 10.sp,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                              ],
                            );
                          }
                          return const SizedBox();
                        },
                      ),
                      SizedBox(height: 50.h),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignInPage(namePevriosPage: 'role', index: 0,)));
                        },
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Already have an account? ",
                                style: poppins.copyWith(
                                    fontSize: 14.sp, color: AppColor.grey1),
                              ),
                              TextSpan(
                                text: "Sign in",
                                style: poppins.copyWith(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                  color: AppColor.blue,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
