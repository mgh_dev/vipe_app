import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/futures/constant_pages/forget_pass_page/logic/state/forget_pass_state.dart';
import 'package:vibe_app/futures/constant_pages/forget_pass_page/logic/view_model/view_model_forget_pass.dart';
import 'package:vibe_app/futures/constant_pages/new_password_page/ui/new_password_page.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../sgin_in_page/ui/sgin_in_page.dart';
import '../../verification_code_page/ui/verification_code_page.dart';

class ForgetPassPage extends StatefulWidget {
  ForgetPassPage({Key? key}) : super(key: key);

  @override
  State<ForgetPassPage> createState() => _ForgetPassPageState();
}

class _ForgetPassPageState extends State<ForgetPassPage> {
  final _userEmailController = TextEditingController();
  final viewModelForgetPass = ForgetPassViewModel();
  bool a = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 39.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Enter your email to reset\n your password",
                          style: poppins.copyWith(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 32.h, bottom: 8.h),
                          child: Text(
                            "Enter Email",
                            style: poppins.copyWith(
                                fontSize: 13.sp, color: AppColor.greyText),
                          ),
                        ),
                        Container(
                          width: 319.w,
                          height: 49.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            controller: _userEmailController,
                            hint: "Enter email address",
                            icon: ImagesApp.icEmailT,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 36.h, bottom: 58.h),
                          child: BlocConsumer(
                            bloc: viewModelForgetPass,
                            listener: (context, state) {
                              if (state is ForgetPassSuccessState) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            VerificationCodePage(email:_userEmailController.text  ,)));
                              }
                              if (state is ForgetPassFailState) {

                              }
                            },
                            builder: (context, state) {
                              return MyCustomButton(
                                title: "Continue",
                                loading: state is ForgetPassLoadingState,
                                onTap: () {
                                  viewModelForgetPass.sentFoergetPass(
                                      email: _userEmailController.text);
                                },
                                color: AppColor.blue,
                                width: 319.w,
                                height: 49.h,
                                fontSize: 14.sp,
                              );
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignInPage(namePevriosPage: 'forget', index: 0,)));
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Remember password? ",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp, color: AppColor.grey1),
                                  ),
                                  TextSpan(
                                    text: "Sign in",
                                    style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.bold,
                                      color: AppColor.blue,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
