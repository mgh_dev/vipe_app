import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:vibe_app/futures/constant_pages/forget_pass_page/logic/state/forget_pass_state.dart';

import '../../../../../core/config/web_service.dart';

class ForgetPassViewModel extends Cubit<ForgetPassBaseState> {
  ForgetPassViewModel() : super(ForgetPassInitialState());

  void sentFoergetPass({required email}) async {
    emit(ForgetPassLoadingState());
    Map<String, String> body = {"email": email};

    try {
      var response =
          await WebService().dio.post("auth/forgot-password", data: body);
      emit(ForgetPassSuccessState());
    } on DioException catch (e) {
      emit(ForgetPassFailState(message: e.response!.data["message"]));
    }
  }
}
