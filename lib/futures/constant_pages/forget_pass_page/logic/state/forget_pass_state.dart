import 'package:equatable/equatable.dart';

abstract class ForgetPassBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class ForgetPassInitialState extends ForgetPassBaseState {}

class ForgetPassLoadingState extends ForgetPassBaseState {}

class ForgetPassSuccessState extends ForgetPassBaseState {}

class ForgetPassFailState extends ForgetPassBaseState {
  final String message;

  ForgetPassFailState({required this.message});
}
