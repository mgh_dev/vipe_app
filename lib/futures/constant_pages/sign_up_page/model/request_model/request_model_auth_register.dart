class RequestAuthRegisterModel {
  RequestAuthRegisterModel({
    required this.type,
    required this.email,
    required this.password,
    required this.firstName,
    required this.lastName,
  });
  late final int type;
  late final String email;
  late final String password;
  late final String firstName;
  late final String lastName;

  RequestAuthRegisterModel.fromJson(Map<String, dynamic> json){
    type = json['type'];
    email = json['email'];
    password = json['password'];
    firstName = json['firstName'];
    lastName = json['lastName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['type'] = type;
    _data['email'] = email;
    _data['password'] = password;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    return _data;
  }
}