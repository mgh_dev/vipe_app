class ResponseAuthRegisterModel {
  ResponseAuthRegisterModel({
    required this.message,
    required this.data,
  });
  late final String message;
  late final Data data;

  ResponseAuthRegisterModel.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.type,
    required this.email,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
    required this.token,
  });
  late final String type;
  late final String email;
  late final String updatedAt;
  late final String createdAt;
  late final int id;
  late final String token;

  Data.fromJson(Map<String, dynamic> json){
    type = json['type'];
    email = json['email'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
    id = json['id'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['type'] = type;
    _data['email'] = email;
    _data['updatedAt'] = updatedAt;
    _data['createdAt'] = createdAt;
    _data['id'] = id;
    _data['token'] = token;
    return _data;
  }
}