import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/model/response_model/response_auth_register_model.dart';

abstract class SignupBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SignupInitialState extends SignupBaseState {}

class SignupLoadingState extends SignupBaseState {}

class SignupSuccessState extends SignupBaseState {
  final ResponseAuthRegisterModel response;

  SignupSuccessState({required this.response});
}

class SignupFailState extends SignupBaseState {
  final String message;


  SignupFailState({required this.message});
}
