import 'package:bloc/bloc.dart';

import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/model/request_model/request_model_auth_register.dart';

import '../../../../../core/config/web_service.dart';
import '../../model/response_model/response_auth_register_model.dart';
import '../state/auth_register_state.dart';

class LoginSignupViewModel extends Cubit<SignupBaseState> {
  LoginSignupViewModel() : super(SignupInitialState());

  void signup(
      {required RequestAuthRegisterModel requestAuthRegisterModel}) async {
    print("1");
    emit(SignupLoadingState());
    print("2");
    var requestDate = requestAuthRegisterModel.toJson();
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.post("auth/register", data: requestDate);
      print("5");
      ResponseAuthRegisterModel responseAuthRegisterModel =
          ResponseAuthRegisterModel.fromJson(response.data);
      print("6");
      HiveServices.addToken(responseAuthRegisterModel.data.token);
      HiveServices.addTypeUser(responseAuthRegisterModel.data.type);
      print("7");
      emit(SignupSuccessState(response: responseAuthRegisterModel));
      HiveServices.addNameEmail(requestAuthRegisterModel.firstName,responseAuthRegisterModel.data.email);
      print("8");
    } catch (e) {
      print("9");
      emit(SignupFailState(message: e.toString()));
      print("10");
    }
  }

}
// eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9eyJpc3MiOiJodHRwOi8vNDYuMjQ5LjEwMi42NTo4MDgwL2FwaS9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNjkwMTQ4NzIzLCJleHAiOjE2OTM3NDg3MjMsIm5iZiI6MTY5MDE0ODcyMywianRpIjoiWlY5VUFUWHhnR0xaeUJEZCIsInN1YiI6IjkyIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9PbTNaKPpDURHj180UGBPSrmO8Dk5w9jR7p3RUPYpJX4
//