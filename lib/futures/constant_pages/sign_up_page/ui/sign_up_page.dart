import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/logic/state/auth_register_state.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/logic/view_model/auth_register_view_model.dart';
import 'package:vibe_app/futures/constant_pages/sign_up_page/model/request_model/request_model_auth_register.dart';
import 'package:vibe_app/futures/fan_flow/create_profile_fan_page/ui/create_profile_page.dart';
import '../../../../core/helper/custom_tost.dart';
import '../../../fan_flow/bottom_nav_page_fan_flow/ui/bottom_nav_fan.dart';
import '../../../venue_flow/complete_profile_page/ui/message_complete_profile_page.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../sgin_in_page/ui/sgin_in_page.dart';

class SignUpPage extends StatelessWidget {
  final int index;

  SignUpPage({Key? key, required this.index}) : super(key: key);
  final _userFirstNameController = TextEditingController();
  final _userLastNameController = TextEditingController();
  final _userEmailController = TextEditingController();
  final _userPassController = TextEditingController();
  final viewModelRegister = LoginSignupViewModel();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChooseYourRolePage()));
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      children: [
                        Text(
                          "Let’s set up your account.",
                          style: poppins.copyWith(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 28.h, bottom: 8.h),
                          child: Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller: _userFirstNameController,
                              hint: "Enter your first name",
                              prefixIcon: ImagesApp.icName,
                              prefixTitle: "First name   ",
                              prefixTitleStyle: poppins.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                              hintTextDirection: TextDirection.ltr,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 8.h),
                          child: Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller: _userLastNameController,
                              hint: "Enter your last name",
                              prefixIcon: ImagesApp.icName,
                              prefixTitle: "Last name   ",
                              prefixTitleStyle: poppins.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 319.w,
                          height: 49.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            controller: _userEmailController,
                            hint: "Enter email address",
                            prefixIcon: ImagesApp.icEmail,
                            prefixTitle: "Email   ",
                            prefixTitleStyle: poppins.copyWith(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8.h, bottom: 60.h),
                          child: Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(

                              controller: _userPassController,
                              hint: "Enter your password",
                              prefixIcon: ImagesApp.icPass,
                              prefixTitle: "Password   ",
                              prefixTitleStyle: poppins.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        BlocConsumer(
                          bloc: viewModelRegister,
                          listener: (context, state) {
                            if (state is SignupSuccessState) {
                              if (index == 1) {
                                HiveServices.addTypeUser("Venue");
                                print("<typeuser1>");
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const MessageCompleteProfilePage()));
                              } else if (index == 2) {
                                HiveServices.addTypeUser("Artist");
                                print("<typeuser2>");
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                             BottomNavArtistVenusPages(index: 0,)));
                              } else if (index == 3) {
                                HiveServices.addTypeUser("Fan");
                                print("<typeuser3>");
                               if(1==0){
                                 Navigator.pushReplacement(
                                     context,
                                     MaterialPageRoute(
                                         builder: (context) =>
                                          BottomNavFanPages(index: 0,)));
                               }
                               else{
                                 Navigator.pushReplacement(
                                     context,
                                     MaterialPageRoute(
                                         builder: (context) =>
                                          CreateProfileFanPage()));
                               }
                              }
                            }
                            if (state is SignupFailState) {
                              loading = false;
                              print(
                                  "@@@@@@@@@@@@@@@@@@@@########################");

                              print(state.message);
                            }
                          },
                          builder: (context, state) {
                            return MyCustomButton(
                              title: "Continue",
                              loading: state is SignupLoadingState,
                              onTap: () {
                                if (_userLastNameController.text.isNotEmpty ||
                                    _userFirstNameController.text.isNotEmpty ||
                                    _userPassController.text.isNotEmpty) {
                                  viewModelRegister.signup(
                                      requestAuthRegisterModel:
                                          RequestAuthRegisterModel(
                                    type: index,
                                    email: _userEmailController.text,
                                    password: _userPassController.text,
                                    firstName: _userFirstNameController.text,
                                    lastName: _userLastNameController.text,
                                  ));
                                } else {
                                  CustomToast.show(
                                      "Please complete the information");
                                }
                              },
                              color: AppColor.blue,
                              width: 319.w,
                              height: 49.h,
                              fontSize: 14.sp,
                            );
                          },
                        ),

                        // GestureDetector(
                        //   onTap: () {
                        //     if (index == 1) {
                        //       print(
                        //           "_____________________________REGISTER______________________");
                        //       Navigator.pushReplacement(
                        //           context,
                        //           MaterialPageRoute(
                        //               builder: (context) =>
                        //                   const MessageCompleteProfilePage()));
                        //     } else if (index == 2) {
                        //       Navigator.pushReplacement(
                        //           context,
                        //           MaterialPageRoute(
                        //               builder: (context) =>
                        //                   const BottomNavArtistVenusPages()));
                        //     } else if (index == 3) {
                        //       Navigator.pushReplacement(
                        //           context,
                        //           MaterialPageRoute(
                        //               builder: (context) =>
                        //                   const BottomNavPages()));
                        //     }
                        //   },
                        //   child: Container(
                        //     width: 100,
                        //     height: 100,
                        //     color: Colors.red,
                        //   ),
                        // ),
                        SizedBox(height: 52.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              " Connect with",
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(ImagesApp.icMeta),
                                Padding(
                                  padding:
                                      EdgeInsets.only(right: 10.w, left: 10.w),
                                  child: SvgPicture.asset(ImagesApp.icTwitter),
                                ),
                                SvgPicture.asset(ImagesApp.icSpotify),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 65.h, bottom: 15.h),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignInPage(namePevriosPage: 'up', index: index,)));
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Already have an account? ",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp, color: AppColor.grey1),
                                  ),
                                  TextSpan(
                                    text: "Sign in",
                                    style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.bold,
                                      color: AppColor.blue,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
