import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/config/web_service.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'package:vibe_app/futures/constant_pages/guste_page/ui/home_guste_page.dart';
import 'package:vibe_app/futures/fan_flow/bottom_nav_page_fan_flow/ui/bottom_nav_fan.dart';
import 'package:vibe_app/futures/venue_flow/complete_profile_page/ui/message_complete_profile_page.dart';

import '../../../../bottom_sheet_map.dart';
import '../../../../core/config/service/failure.dart';


class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 2),
      () {
        WebService().initialOrUpdate(
          baseUrl: "http://46.249.102.65:8080/api/",
          header: {
            HttpHeaders.contentTypeHeader: 'application/json',
            "accept": "application/json",
            if (HiveServices.getToken != null)
              HttpHeaders.authorizationHeader: "Bearer ${HiveServices.getToken}"
          },
          refreshToken: () async {
            try {
              if(HiveServices.getToken == null){
                Navigator.push(context,MaterialPageRoute(builder: (context)=> HomeGustePage()));
              }
              var response = await WebService().dio.post("auth/refresh");
              HiveServices.addToken(response.data["data"]["token"]);
              return {
                "status": true,
                "token": response.data["data"]["token"],
              };
            } on DioException catch (e) {
              return {
                "status": false,
                "message": ServerFailure.fromJson(e.response?.data).map[Failure.key],
              };
            }
          },
        );
        print(HiveServices.getToken);
        // Navigator.of(context).pushReplacement(
        //           MaterialPageRoute(builder: (context) => BottomSheetMap()));
        print(HiveServices.getTypeUser);
        if (HiveServices.getTypeUser==null) {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => ChooseYourRolePage()));
        } else {
          if (HiveServices.getTypeUser == "Venue") {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                    const MessageCompleteProfilePage()));
          } else if (HiveServices.getTypeUser == "Artist") {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                     BottomNavArtistVenusPages(index: 0,)));
          } else if (HiveServices.getTypeUser == "Fan") {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                     BottomNavFanPages(index: 0,)));
          }
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: AppColor.backgroundApp,
      body: SizedBox(
        width: double.infinity,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Positioned(
                left: -100.w,
                top: -100.h,
                child: ContainerBlur(color: AppColor.purple)),
            Center(
              child: Image.asset(
                ImagesApp.logoApp,
                width: 200.w,
                height: 200.h,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Powered by",
                  style: rajdhani.copyWith(fontSize: 10.sp),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 2.5.h, bottom: 24.h),
                  child: Text(
                    "VIBE OASIS",
                    style: rajdhani,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
