import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/auth_login_state.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/check_box_state.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/view_model/auth_login_view_model.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/view_model/view_model_check_box.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/model/request_model/request_auth_login.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/ui/home_venus_page.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../fan_flow/bottom_nav_page_fan_flow/ui/bottom_nav_fan.dart';
import '../../choose_your_role_page/ui/choose_your_role_page.dart';
import '../../forget_pass_page/ui/forget_pass_page.dart';
import '../../sign_up_page/ui/sign_up_page.dart';

class SignInPage extends StatefulWidget {
  final String namePevriosPage;
  final int index;
  SignInPage({required this.namePevriosPage,required this.index}) : super();

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _userEmailController = TextEditingController();
  final _userPassController = TextEditingController();
  final viewModelLogin = LoginViewModel();
  final checkBoxViewModel = CheckBoxViewModel();
  bool remember = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100,
              top: -100,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          if(widget.namePevriosPage=="up"){
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>SignUpPage(index: widget.index,)));
                          }
                          else if(widget.namePevriosPage=="role"){
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>ChooseYourRolePage()));
                          }

                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 29.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Login to get started",
                          style: poppins.copyWith(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 32.h),
                        Container(
                          width: 319.w,
                          height: 49.h,
                          decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(10.r),
                          ),
                          child: CustomTextField(
                            controller: _userEmailController,
                            hint: "Enter email address",
                            icon: ImagesApp.icEmailT,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8.h, bottom: 11.h),
                          child: Container(
                            width: 319.w,
                            height: 49.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: CustomTextField(
                              controller: _userPassController,
                              hint: "Enter your password",
                              icon: ImagesApp.icPassT,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            BlocBuilder(
                              bloc: checkBoxViewModel,
                              builder: (context, state) {
                                if (state is ChangeState) {
                                  return GestureDetector(
                                    onTap: () {
                                      checkBoxViewModel
                                          .changeState(!state.status);
                                      remember = !state.status;
                                      print("<**>");
                                    },
                                    child: Container(
                                      width: 24,
                                      height: 24,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(7.r),
                                        border: Border.all(
                                            color: state.status
                                                ? Colors.transparent
                                                : AppColor.greyHint),
                                        color: state.status
                                            ? AppColor.greyBlue
                                            : Colors.transparent,
                                      ),
                                      child: state.status
                                          ? Center(
                                              child: Icon(
                                              Icons.check,
                                              color: AppColor.white,
                                              size: 18,
                                            ))
                                          : const SizedBox(),
                                    ),
                                  );
                                }
                                return const SizedBox();
                              },
                            ),
                            SizedBox(width: 8.w),
                            Text(
                              "Remember me?",
                              style: poppins.copyWith(
                                fontSize: 13.sp,
                              ),
                            ),
                            const Spacer(),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ForgetPassPage()));
                              },
                              child: Text(
                                "Forget  password?",
                                style: poppins.copyWith(
                                  fontSize: 13.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 27.h),
                        BlocConsumer(
                          bloc: viewModelLogin,
                          listener: (context, state) {
                            if (state is LoginSuccessState) {
                              print(state.response.data.type);
                              if (state.response.data.type == "Venue") {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            BottomNavVenusPages(
                                              index: 0,
                                            )));
                              } else if (state.response.data.type == "Artist") {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                             BottomNavArtistVenusPages(index: 0,)));
                              } else if (state.response.data.type == "Fan") {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                             BottomNavFanPages(index: 0,)));
                              }
                            }
                            if (state is LoginFailState) {
                              print(
                                  "@@@@@@@@@@@@@@@@@@@@########################");
                              print(state.message);
                            }
                          },
                          builder: (context, state) {
                            return MyCustomButton(
                              title: "Continue",
                              loading: state is LoginLoadingState,
                              onTap: () {
                                if (_userEmailController.text.isNotEmpty ||
                                    _userPassController.text.isNotEmpty) {
                                  viewModelLogin.login(
                                    responseAuthLoginModel: RequestAuthLogin(
                                      email: _userEmailController.text,
                                      password: _userPassController.text,
                                    ),
                                    remember: remember,
                                  );
                                } else {
                                  CustomToast.show(
                                      "Please complete the information");
                                }
                              },
                              color: AppColor.blue,
                              width: 319.w,
                              height: 49.h,
                              fontSize: 14.sp,
                            );
                          },
                        ),
                        SizedBox(height: 52.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              " Connect with",
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(ImagesApp.icMeta),
                                Padding(
                                  padding:
                                      EdgeInsets.only(right: 10.w, left: 10.w),
                                  child: SvgPicture.asset(ImagesApp.icTwitter),
                                ),
                                SvgPicture.asset(ImagesApp.icSpotify),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 52.h, bottom: 15.h),
                          child: Align(
                            alignment: Alignment.center,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ChooseYourRolePage()));
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "Don’t have an account? ",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          color: AppColor.grey1),
                                    ),
                                    TextSpan(
                                      text: "Sign up",
                                      style: poppins.copyWith(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.bold,
                                        color: AppColor.blue,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
