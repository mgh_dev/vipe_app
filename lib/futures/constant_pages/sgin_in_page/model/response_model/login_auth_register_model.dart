class ResponseAuthLoginModel {
  ResponseAuthLoginModel({
    required this.message,
    required this.data,
  });
  late final String message;
  late final Data data;

  ResponseAuthLoginModel.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.firstName,
    required this.lastName,
    required this.phone,
    required this.type,
    required this.email,
    required this.id,
    required this.avatar,
    required this.token,
    required this.vibe,
    required this.file,
    required this.gallery,
    required this.video,
    required this.audio,
    required this.artistry,
    required this.address,
    required this.extraData,
  });
  late final String firstName;
  late final String lastName;
  late final String phone;
  late final String type;
  late final String email;
  late final int id;
  late final String avatar;
  late final String token;
  late final List<Vibe> vibe;
  late final List<File> file;
  late final List<Gallery> gallery;
  late final List<Video> video;
  late final List<Audio> audio;
  late final List<Artistry> artistry;
  late final Address address;
  late final ExtraData extraData;

  Data.fromJson(Map<String, dynamic> json){
    firstName = json['firstName'];
    lastName = json['lastName'];
    // phone = json['phone'];
    type = json['type'];
    email = json['email'];
    id = json['id'];
    // avatar = json['avatar'];
    token = json['token'];
    // vibe = List.from(json['vibe']).map((e)=>Vibe.fromJson(e)).toList();
    // file = List.from(json['file']).map((e)=>File.fromJson(e)).toList();
    // gallery = List.from(json['gallery']).map((e)=>Gallery.fromJson(e)).toList();
    // video = List.from(json['video']).map((e)=>Video.fromJson(e)).toList();
    // audio = List.from(json['audio']).map((e)=>Audio.fromJson(e)).toList();
    // artistry = List.from(json['artistry']).map((e)=>Artistry.fromJson(e)).toList();
    // address = Address.fromJson(json['address']);
    // extraData = ExtraData.fromJson(json['extraData']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['phone'] = phone;
    _data['type'] = type;
    _data['email'] = email;
    _data['id'] = id;
    _data['avatar'] = avatar;
    _data['token'] = token;
    _data['vibe'] = vibe.map((e)=>e.toJson()).toList();
    _data['file'] = file.map((e)=>e.toJson()).toList();
    _data['gallery'] = gallery.map((e)=>e.toJson()).toList();
    _data['video'] = video.map((e)=>e.toJson()).toList();
    _data['audio'] = audio.map((e)=>e.toJson()).toList();
    _data['artistry'] = artistry.map((e)=>e.toJson()).toList();
    _data['address'] = address.toJson();
    _data['extraData'] = extraData.toJson();
    return _data;
  }
}

class Vibe {
  Vibe({
    required this.name,
    required this.id,
    required this.description,
    required this.image,
    required this.icon,
  });
  late final String name;
  late final int id;
  late final String description;
  late final String image;
  late final String icon;

  Vibe.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    description = json['description'];
    image = json['image'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['description'] = description;
    _data['image'] = image;
    _data['icon'] = icon;
    return _data;
  }
}

class File {
  File({
    required this.name,
    required this.id,
    required this.type,
    required this.path,
  });
  late final String name;
  late final int id;
  late final String type;
  late final String path;

  File.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    type = json['type'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['type'] = type;
    _data['path'] = path;
    return _data;
  }
}

class Gallery {
  Gallery({
    required this.name,
    required this.id,
    required this.type,
    required this.path,
  });
  late final String name;
  late final int id;
  late final String type;
  late final String path;

  Gallery.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    type = json['type'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['type'] = type;
    _data['path'] = path;
    return _data;
  }
}

class Video {
  Video({
    required this.name,
    required this.id,
    required this.type,
    required this.path,
  });
  late final String name;
  late final int id;
  late final String type;
  late final String path;

  Video.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    type = json['type'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['type'] = type;
    _data['path'] = path;
    return _data;
  }
}

class Audio {
  Audio({
    required this.name,
    required this.id,
    required this.type,
    required this.path,
  });
  late final String name;
  late final int id;
  late final String type;
  late final String path;

  Audio.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    type = json['type'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['type'] = type;
    _data['path'] = path;
    return _data;
  }
}

class Artistry {
  Artistry({
    required this.name,
    required this.id,
    required this.description,
    required this.image,
    required this.icon,
  });
  late final String name;
  late final int id;
  late final String description;
  late final String image;
  late final String icon;

  Artistry.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    description = json['description'];
    image = json['image'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['description'] = description;
    _data['image'] = image;
    _data['icon'] = icon;
    return _data;
  }
}

class Address {
  Address({
    required this.address,
    required this.longitude,
    required this.latitude,
    required this.city,
    required this.state,
    required this.country,
    required this.postalCode,
  });
  late final String address;
  late final double longitude;
  late final double latitude;
  late final String city;
  late final String state;
  late final String country;
  late final String postalCode;

  Address.fromJson(Map<String, dynamic> json){
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    postalCode = json['postalCode'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['address'] = address;
    _data['longitude'] = longitude;
    _data['latitude'] = latitude;
    _data['city'] = city;
    _data['state'] = state;
    _data['country'] = country;
    _data['postalCode'] = postalCode;
    return _data;
  }
}

class ExtraData {
  ExtraData({
    required this.role,
    required this.about,
    required this.links,
    required this.data,
  });
  late final int role;
  late final String about;
  late final List<Links> links;
  late final Data data;

  ExtraData.fromJson(Map<String, dynamic> json){
    role = json['role'];
    about = json['about'];
    links = List.from(json['links']).map((e)=>Links.fromJson(e)).toList();
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['role'] = role;
    _data['about'] = about;
    _data['links'] = links.map((e)=>e.toJson()).toList();
    _data['data'] = data.toJson();
    return _data;
  }
}

class Links {
  Links({
    required this.title,
    required this.type,
    required this.link,
  });
  late final String title;
  late final String type;
  late final String link;

  Links.fromJson(Map<String, dynamic> json){
    title = json['title'];
    type = json['type'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['type'] = type;
    _data['link'] = link;
    return _data;
  }
}