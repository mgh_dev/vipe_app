abstract class CheckBoxBaseState {}

class ChangeState extends CheckBoxBaseState{
  final bool status;

  ChangeState({required this.status});
}
