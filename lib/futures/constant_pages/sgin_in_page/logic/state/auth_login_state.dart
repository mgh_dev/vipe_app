import 'package:equatable/equatable.dart';

import '../../model/response_model/login_auth_register_model.dart';

abstract class LoginBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginInitialState extends LoginBaseState {}

class LoginLoadingState extends LoginBaseState {}

class LoginSuccessState extends LoginBaseState {
  final ResponseAuthLoginModel response;

  LoginSuccessState({required this.response});
}

class LoginFailState extends LoginBaseState {
  final String message;

  LoginFailState({required this.message});
}
