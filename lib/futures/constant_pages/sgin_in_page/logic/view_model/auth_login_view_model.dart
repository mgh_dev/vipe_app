import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/auth_login_state.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/model/request_model/request_auth_login.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/model/response_model/login_auth_register_model.dart';
import '../../../../../core/config/hive_service/hive_service.dart';
import '../../../../../core/config/web_service.dart';

class LoginViewModel extends Cubit<LoginBaseState> {
  LoginViewModel() : super(LoginInitialState());

  void login({required RequestAuthLogin responseAuthLoginModel,required bool remember}) async {
    emit(LoginLoadingState());

    var requestDate = responseAuthLoginModel.toJson();

    try {
      var response =
          await WebService().dio.post("auth/login", data: requestDate);
      ResponseAuthLoginModel responseAuthLoginModel =
          ResponseAuthLoginModel.fromJson(response.data);
      emit(LoginSuccessState(response: responseAuthLoginModel));
      HiveServices.addToken(responseAuthLoginModel.data.token);
      HiveServices.addNameEmail(responseAuthLoginModel.data.firstName, responseAuthLoginModel.data.email);

      print("02");
      print(remember);
     if(remember){

       HiveServices.addTypeUser(responseAuthLoginModel.data.type);
     }
      print("03");
    } catch (e) {
      emit(LoginFailState(message: e.toString()));
    }
  }
}
