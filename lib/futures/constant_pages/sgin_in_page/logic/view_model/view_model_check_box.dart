import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/check_box_state.dart';





class CheckBoxViewModel extends Cubit<CheckBoxBaseState> {
  CheckBoxViewModel() : super(ChangeState(status: false));

  void changeState(bool status) {
    emit(ChangeState(status: status));
  }
}
