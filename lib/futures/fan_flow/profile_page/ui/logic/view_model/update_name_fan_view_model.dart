import 'package:bloc/bloc.dart';
import 'package:vibe_app/core/config/web_service.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/get_profile_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/update_name_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/model/response_model/response_get_profile_fan_model.dart';

class UpdateNameFanViewModel extends Cubit<UpdateNameFanBaseState> {
  UpdateNameFanViewModel() : super(UpdateNameFanInitialState());

  Future updateName({required String nameUsers,required String lastNameUsers}) async {
    print("1");
    emit(UpdateNameFanLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.patch("auth/update",data: {
            "firstName": nameUsers,
            "lastName": lastNameUsers
          });
      print("5");
      print("6");
      print("7");
      emit(UpdateNameFanSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(UpdateNameFanFailState(message: e.toString()));
      print("10");
    }
  }

}
