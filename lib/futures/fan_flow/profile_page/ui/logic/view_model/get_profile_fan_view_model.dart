import 'package:bloc/bloc.dart';
import 'package:vibe_app/core/config/web_service.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/get_profile_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/model/response_model/response_get_profile_fan_model.dart';

class GetProfileFanViewModel extends Cubit<GetProfileFanBaseState> {
  GetProfileFanViewModel() : super(GetProfileFanInitialState());

  void get() async {
    print("1");
    emit(GetProfileFanLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.get("users/get-profile");
      print("5");
      ResponseGetFanProfile getFanProfile =
      ResponseGetFanProfile.fromJson(response.data);
      print("6");
     
      print("7");
      emit(GetProfileFanSuccessState(response: getFanProfile));
      print("8");
    } catch (e) {
      print("9");
      emit(GetProfileFanFailState(message: e.toString()));
      print("10");
    }
  }

}
