import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/model/response_model/response_get_profile_fan_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/model/response_model/response_get_profile_venus_model.dart';

abstract class UpdateNameFanBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class UpdateNameFanInitialState extends UpdateNameFanBaseState {}

class UpdateNameFanLoadingState extends UpdateNameFanBaseState {}

class UpdateNameFanSuccessState extends UpdateNameFanBaseState {


  UpdateNameFanSuccessState();
}

class UpdateNameFanFailState extends UpdateNameFanBaseState {
  final String message;


  UpdateNameFanFailState({required this.message});
}
