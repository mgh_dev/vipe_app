import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/get_profile_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/view_model/get_profile_fan_view_model.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/Favourite%20Artist_tab.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/shows_tab.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/tabs/venues_tab_profile.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/logic/state/get_profile_venus_state.dart';

import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../venue_flow/profile_venus_page/ui/tabs/tabreview.dart';
import '../../update_profile_page/ui/update_profile_page.dart';

class ProfileFanPage extends StatefulWidget {
  const ProfileFanPage({Key? key}) : super(key: key);

  @override
  State<ProfileFanPage> createState() => _ProfileFanPageState();
}

class _ProfileFanPageState extends State<ProfileFanPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();
  final getProfileFan = GetProfileFanViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileFan.get();
    tabController = TabController(
      length: 3,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    secondController.addListener(() {
      if (secondController.offset <=
          secondController.position.minScrollExtent) {
        firstController.animateTo(0,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      } else if (secondController.offset >=
          secondController.position.minScrollExtent) {
        firstController.animateTo(firstController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      }
    });
    return BlocBuilder(
        bloc: getProfileFan,
        builder: (context, state) {
          if (state is GetProfileFanLoadingState) {
            return const CustomLoading();
          }
          if (state is GetProfileFanSuccessState) {
            return CustomScrollView(
              controller: firstController,
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    height: 395.h,
                    decoration: BoxDecoration(
                      image:state.response.data.avatar==null ?DecorationImage(
                        image: AssetImage(ImagesApp.prof),
                        fit: BoxFit.cover,
                      ):DecorationImage(
                        image: NetworkImage("http://46.249.102.65:8080/${state.response.data.avatar}"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: <Color>[
                            const Color(0xff090B1B),
                            const Color(0xff090B1B).withOpacity(0.2),
                            Colors.transparent,
                            Colors.transparent,
                            Colors.transparent,
                            const Color(0xff090B1B).withOpacity(0.98),
                            const Color(0xff090B1B),
                          ],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 25.w, bottom: 21.h),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "${state.response.data.firstName} ${state.response.data.lastName}",
                                  style: poppins.copyWith(
                                    fontSize: 26.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                             Spacer(),
                                Padding(
                                  padding:  EdgeInsets.only(right: 25.w),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              UpdateProfilePage(),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      width: 40.w,
                                      height: 40.r,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColor.blue,
                                      ),
                                      child: Center(
                                        child: SvgPicture.asset(
                                          ImagesApp.icPen,
                                          color: AppColor.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                const SliverToBoxAdapter(
                  child: SizedBox(height: 20),
                ),
                SliverToBoxAdapter(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.w),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        child: Container(
                          width: double.infinity,
                          height: 1,
                          color: AppColor.greyHint,
                        ),
                      ),
                      TabBar(
                        controller: tabController,
                        // dividerColor: Colors.red,
                        indicatorColor: AppColor.blue,
                        labelColor: AppColor.white,
                        unselectedLabelColor:
                            AppColor.greyHint.withOpacity(0.5),
                        labelStyle: poppins.copyWith(
                          fontSize: 12.sp,
                        ),
                        isScrollable: true,

                        indicatorSize: TabBarIndicatorSize.label,
                        tabs: const [
                          Tab(
                            text: 'Your Events',
                          ),
                          Tab(text: 'Favorite Artists'),
                          Tab(text: 'Favorite Businesses'),
                        ],
                      ),
                    ],
                  ),
                )),
                SliverFillRemaining(
                  child: TabBarView(
                    controller: tabController,
                    children: [
                      ShowsEventTab(scrollController: secondController),
                      VenuesTabProfile(scrollController: secondController),
                      FavoriteArtistTab(scrollController: secondController),
                    ],
                  ),
                ),
              ],
            );
          }
          if (state is GetProfileFanFailState) {
            return Container(
              width: 200,
              height: 100,
              color: Colors.red,
            );
          }

          return const SizedBox();
        });
  }
}
