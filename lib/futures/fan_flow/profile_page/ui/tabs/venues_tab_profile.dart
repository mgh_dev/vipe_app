import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/helper/custom_button.dart';

import '../../../../../core/utils/app_color.dart';
import '../../../../../core/utils/image_app.dart';
import '../../../../../core/utils/style_text_app.dart';

class VenuesTabProfile extends StatelessWidget {
  VenuesTabProfile({required this.scrollController}) : super();
  ScrollController scrollController;
  List<String> ls = [
    ImagesApp.favoriteArtist1,
    ImagesApp.favoriteArtist4,
    ImagesApp.favoriteArtist1,
    ImagesApp.favoriteArtist4,
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: scrollController,
      itemCount: 2,
      itemBuilder: (BuildContext context, int index) {
        return index < 1
            ? GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 19.w,
                  mainAxisSpacing: 20.h,
                ),
                itemCount: ls.length,
                itemBuilder: (context, index) {
                  return Container(
                    width: 162.w,
                    height: 137.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14.r),
                      image: DecorationImage(
                        image: AssetImage(ls[index]),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                })
            : Padding(
                padding: EdgeInsets.only(top: 20.h, bottom: 12.h,left: 38.w,right: 38.w),
                child: Container(
                  width: 298.w,
                  height: 103.h,
                  decoration: BoxDecoration(
                    color: AppColor.greyBlue2.withOpacity(0.05),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  child: Padding(
                    padding:  EdgeInsets.symmetric(vertical: 12.5.h),
                    child: Column(
                      children: [
                        Text(
                          "Invite your friends or artist and venues\nyou don't see on VIBE.",
                          textAlign: TextAlign.center,
                          style: poppins.copyWith(
                            fontSize: 12.sp,
                            color: AppColor.blue,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        SizedBox(height: 7.h),
                        MyCustomButton(
                          width: 132.w,
                          height: 32.h,
                          onTap: (){},
                          title: "Invite",
                          loading: false,
                        ),
                      ],
                    ),
                  ),
                ),
              );
      },
    );
  }
}
