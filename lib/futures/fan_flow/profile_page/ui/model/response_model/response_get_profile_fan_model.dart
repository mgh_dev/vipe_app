class ResponseGetFanProfile {
  ResponseGetFanProfile({
    required this.message,
    required this.data,
  });
  late final String message;
  late final Data data;

  ResponseGetFanProfile.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.type,
    required this.email,
     this.avatar,
  });
  late final int id;
  late final String firstName;
  late final String lastName;
  late final String type;
  late final String email;
   String? avatar;

  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    type = json['type'];
    email = json['email'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['type'] = type;
    _data['email'] = email;
    _data['avatar'] = avatar;
    return _data;
  }
}