abstract class TabsBaseState {}

class TabsIndexState extends TabsBaseState {
  final int index;

  TabsIndexState({required this.index});
}
