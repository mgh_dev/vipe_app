import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/fan_flow/home_page/bloc/bloc_tabs/tabs_state.dart';

class TabsViewModel extends Cubit<TabsBaseState> {
  TabsViewModel() : super(TabsIndexState(index: 0));

  void changeIndex(int index) {
    emit(TabsIndexState(index: index));
  }
}
