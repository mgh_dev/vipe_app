import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/show_helper_app.dart';
import 'package:vibe_app/futures/fan_flow/home_page/ui/tabs/events_tab.dart';
import 'package:vibe_app/futures/fan_flow/home_page/ui/tabs/venues_tab.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_drawer.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';

import '../bloc/bloc_tabs/tabs_state.dart';
import '../bloc/bloc_tabs/tabs_view_model.dart';
import 'tabs/artist_tab.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _tabsBloc = TabsViewModel();
  final List<Widget> lsTabs = [
     EventsPage(),
    ArtistTab(),
    VenuesTab(),
  ];
  bool isHelperApp = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isHelperApp = true;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            BlocBuilder(
                bloc: _tabsBloc,
                builder: (context, state) {
                  if (state is TabsIndexState) {
                    return Expanded(
                      child: Column(
                        children: [
                          Container(
                            width: 240.w,
                            height: 46.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(50.r),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(2),
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      _tabsBloc.changeIndex(0);
                                    },
                                    child: Container(
                                      width: 78.w,
                                      height: 42.h,
                                      decoration: BoxDecoration(
                                        color: state.index == 0
                                            ? AppColor.blue
                                            : Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(50.r),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Events",
                                          style: poppins,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      _tabsBloc.changeIndex(1);
                                    },
                                    child: Container(
                                      width: 78.w,
                                      height: 42.h,
                                      decoration: BoxDecoration(
                                        color: state.index == 1
                                            ? AppColor.blue
                                            : Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(50.r),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Artists",
                                          style: poppins,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      _tabsBloc.changeIndex(2);
                                    },
                                    child: Container(
                                      width: 78.w,
                                      height: 42.h,
                                      decoration: BoxDecoration(
                                        color: state.index == 2
                                            ? AppColor.blue
                                            : Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(50.r),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Venues",
                                          style: poppins,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 39.h,
                          ),
                          Expanded(
                            child: lsTabs[state.index],
                          ),
                        ],
                      ),
                    );
                  }
                  return const SizedBox();
                }),
          ],
        ),
        GestureDetector(
            onTap: () {
              setState(() {
                isHelperApp = !isHelperApp;
              });
            },
            child:
                Visibility(visible: isHelperApp, child: const ShowHelperApp())),
      ],
    );
  }
}
