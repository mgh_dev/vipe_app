import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/ui/sgin_in_page.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/logic/view_model/get_event_view_model.dart';

import '../../../../../core/helper/custom_button.dart';
import '../../../../../core/utils/app_color.dart';
import '../../../../../core/utils/image_app.dart';
import '../../../../../core/utils/style_text_app.dart';
import '../../../../venue_flow/event_details_venus_page/ui/details_event_venus_page.dart';
import '../../../../venue_flow/home_page_venus/logic/state/get_event_state.dart';

class EventsPage extends StatefulWidget {
  EventsPage({Key? key}) : super(key: key);

  @override
  State<EventsPage> createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  List<String> _chipList = [
    'Intimate',
    'Chill',
    'Bluesy',
  ];

  GetEventViewModel getEventViewModel = GetEventViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEventViewModel.getEvent();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: getEventViewModel,
      builder: (context, state) {
        if (state is GetEventLoadingState) {
          return const CustomLoading();
        }
        if (state is GetEventSuccessState) {
          return state.getEventModel.data.isEmpty
              ? Center(
                  child: Text(
                    "No event available",
                    style:
                        poppins.copyWith(fontSize: 20.sp, color: Colors.white),
                  ),
                )
              : SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      CarouselSlider.builder(
                        itemCount: state.getEventModel.data.length,
                        itemBuilder: (BuildContext context, int itemIndex,
                            int pageViewIndex) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DetailsEventVenusPage(
                                    idEvent:
                                        state.getEventModel.data[itemIndex].id,
                                  ),
                                ),
                              );
                            },
                            child: SizedBox(
                              height: 475.h,
                              width: 300.w,
                              child: Stack(
                                alignment: Alignment.topCenter,
                                children: [
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      width: 300.w,
                                      height: 461.h,
                                      decoration: BoxDecoration(
                                        color: AppColor.white.withOpacity(0.1),
                                        borderRadius:
                                            BorderRadius.circular(20.r),
                                      ),
                                      child: SingleChildScrollView(
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              left: 16.w, right: 16.w),
                                          child: Column(
                                            children: [
                                              SizedBox(height: 16.h),
                                              Container(
                                                width: 268.w,
                                                height: 293.h,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: NetworkImage(
                                                          "http://46.249.102.65:8080/storage/${state.getEventModel.data[itemIndex].image}"),
                                                      fit: BoxFit.cover),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.r),
                                                ),
                                                child: Align(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Container(
                                                    height: 45.h,
                                                    // width: 240.w,
                                                    decoration: BoxDecoration(
                                                      // color: Color(0xff090B1B).withOpacity(0.5),
                                                      gradient:
                                                          const LinearGradient(
                                                        begin: Alignment
                                                            .bottomCenter,
                                                        end:
                                                            Alignment.topCenter,
                                                        colors: <Color>[
                                                          Color(0xff090B1B),
                                                          Colors.transparent
                                                        ],
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        bottomLeft:
                                                            Radius.circular(
                                                                20.r),
                                                        bottomRight:
                                                            Radius.circular(
                                                                20.r),
                                                      ),
                                                    ),
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 11.h,
                                                          bottom: 15.h,
                                                          left: 17.w),
                                                      child: Row(
                                                        children: [
                                                          Image.asset(ImagesApp
                                                              .profileMenu),
                                                          SizedBox(width: 4.w),
                                                          Text(
                                                            "Jerry Angel",
                                                            style: poppins.copyWith(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Stack(
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 11.h,
                                                                bottom: 13.h),
                                                        child: Text(
                                                          state
                                                              .getEventModel
                                                              .data[itemIndex]
                                                              .title,
                                                          style:
                                                              poppins.copyWith(
                                                            fontSize: 24,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 150,
                                                        child: Text(
                                                          state
                                                              .getEventModel
                                                              .data[itemIndex]
                                                              .additionalInformation,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style:
                                                              poppins.copyWith(
                                                                  fontSize:
                                                                      14.sp,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 12.h,
                                                                bottom: 6.h),
                                                        child: Text(
                                                          "Friday, Feb 17 at 7:30pm",
                                                          style:
                                                              poppins.copyWith(
                                                                  fontSize:
                                                                      14.sp,
                                                                  color:
                                                                      AppColor
                                                                          .blue,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                        ),
                                                      ),
                                                      // SizedBox(height:6.h),
                                                      // Wrap(
                                                      //   crossAxisAlignment:
                                                      //       WrapCrossAlignment.start,
                                                      //   runSpacing: 7.h,
                                                      //   spacing: 4.w,
                                                      //   runAlignment:
                                                      //       WrapAlignment.start,
                                                      //   alignment:
                                                      //       WrapAlignment.start,
                                                      //   children: _buildChips(),
                                                      // ),
                                                    ],
                                                  ),
                                                  Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          right: 25.w),
                                                      width: 47.w,
                                                      height: 47.h,
                                                      decoration: BoxDecoration(
                                                          gradient: LinearGradient(
                                                              begin: Alignment
                                                                  .topCenter,
                                                              end: Alignment
                                                                  .bottomCenter,
                                                              colors: [
                                                                AppColor.green
                                                                    .withOpacity(
                                                                        0.1),
                                                                AppColor.green
                                                                    .withOpacity(
                                                                        0.35),
                                                                AppColor.green
                                                                    .withOpacity(
                                                                        0.55),
                                                                AppColor.green
                                                                    .withOpacity(
                                                                        0.65),
                                                              ]),
                                                          borderRadius:
                                                              const BorderRadius
                                                                      .only(
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          50),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          50))),
                                                      child: Center(
                                                          child: Text(
                                                        "\$${state.getEventModel.data[itemIndex].ticketPrice}",
                                                        style: inter.copyWith(
                                                            color:
                                                                AppColor.white,
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                        textAlign:
                                                            TextAlign.center,
                                                      )),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: 32.w),
                                    child: Align(
                                      alignment: Alignment.topRight,
                                      child: Container(
                                        width: 49.w,
                                        height: 66.h,
                                        decoration: BoxDecoration(
                                          color:
                                              AppColor.white.withOpacity(0.8),
                                          borderRadius:
                                              BorderRadius.circular(10.r),
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              "17",
                                              style: inter,
                                            ),
                                            Text(
                                              "Feb",
                                              style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                color: AppColor.black,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        options: CarouselOptions(
                          height: 500,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.8,
                          initialPage: 0,
                          enableInfiniteScroll: false,
                          reverse: false,
                          // autoPlay: true,
                          // autoPlayInterval: const Duration(seconds: 3),
                          // autoPlayAnimationDuration: const Duration(milliseconds: 800),
                          // autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          enlargeFactor: 0.3,
                          // onPageChanged: callbackFunction,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      const SizedBox(height: 150),
                    ],
                  ),
                );
        }
        return const SizedBox();
      },
    );
  }

  List<Widget> _buildChips() {
    List<Widget> chips = [];

    for (int i = 0; i < _chipList.length; i++) {
      chips.add(GestureDetector(
        onTap: () {
          _chipList.removeAt(i);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 15.w),
          height: 27.h,
          decoration: BoxDecoration(
            color: AppColor.white.withOpacity(0.1),
            borderRadius: BorderRadius.circular(50.r),
            border:
                Border.all(color: AppColor.white.withOpacity(0.5), width: 0.3),
          ),
          child: Text(
            _chipList[i],
            style: poppins.copyWith(fontSize: 10.sp),
          ),
        ),
      ));
    }

    return chips;
  }
}
