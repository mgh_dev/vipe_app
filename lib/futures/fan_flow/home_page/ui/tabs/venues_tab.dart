import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/logic/state/get_users_state.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/logic/view_model/get_users_view_model.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/fan_flow/show_details_profile_venus/ui/show_profile_details_venus_fan_page.dart';

import '../../../../../core/utils/app_color.dart';
import '../../../../../core/utils/image_app.dart';
import '../../../../venue_flow/show_details_venus_page/ui/show_details_venus_page.dart';

class VenuesTab extends StatefulWidget {
  VenuesTab({Key? key}) : super(key: key);

  @override
  State<VenuesTab> createState() => _VenuesTabState();
}

class _VenuesTabState extends State<VenuesTab> {
  final _searchController = TextEditingController();

  List<String> lsImage = [
    ImagesApp.p2,
    ImagesApp.p1,
  ];

  GetUsersViewModel getUsersViewModel=GetUsersViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsersViewModel.get(1);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: getUsersViewModel,
      builder: (context,state){
        if(state is GetUsersLoadingState){
          return const CustomLoading();
        }
        if (state is GetUsersSuccessState){
          return  Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.w),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      width: 289.w,
                      height: 49.h,
                      decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(14.r),
                      ),
                      child: CustomTextField(
                        controller: _searchController,
                        hint: "Search...",
                        icon: ImagesApp.icSearchBlue,
                        hintTextDirection: TextDirection.ltr,
                        suffixIcon: ImagesApp.icClose,
                      ),
                    ),
                    SizedBox(width: 8.w),
                    Container(
                      width: 44.w,
                      height: 44.h,
                      decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      child: Center(child: SvgPicture.asset(ImagesApp.icFilter)),
                    ),
                  ],
                ),
                Expanded(
                    child: ListView.builder(
                        itemCount: state.response.data.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(bottom: 8.h),
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>const ShowDetailsVenusPage()));
                              },
                              child: Container(
                                width: 335.w,
                                height: 65.h,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(8.r),
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 4.w, top: 4.h, bottom: 4.h),
                                      child: Container(
                                        width: 52.w,
                                        height: 56.h,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8.r),
                                          image: state.response.data[index].avatar==null
                                              ?DecorationImage(
                                            image: AssetImage(ImagesApp.p2),
                                            fit: BoxFit.fill,
                                          ):DecorationImage(
                                            image: NetworkImage("http://46.249.102.65:8080/${state.response.data[index].avatar!}"),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8.w),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              "${state.response.data[index].firstName} ${state.response.data[index].lastName}",
                                              style: poppins.copyWith(
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                            Icon(Icons.check_circle,color: AppColor.greyBlue2,size: 15,)
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              width: 16.w,
                                              height: 16.h,
                                              decoration: BoxDecoration(
                                                color: AppColor.white.withOpacity(0.1),
                                                borderRadius:
                                                BorderRadius.circular(4.r),
                                              ),
                                              child: Center(
                                                  child: SvgPicture.asset(
                                                    ImagesApp.icLocation,
                                                    width: 10.w,
                                                    height: 10.h,
                                                  )),
                                            ),
                                            SizedBox(width: 6.w),
                                            Text(
                                              "Elgin St. Celina, Delaware",
                                              style: inter.copyWith(
                                                fontSize: 10.sp,
                                                color: AppColor.grey2,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          );
                        }))
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
