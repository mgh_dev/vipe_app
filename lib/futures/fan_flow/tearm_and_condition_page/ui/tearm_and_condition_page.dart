import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';


class TearmAandConditionPage extends StatefulWidget {
  const TearmAandConditionPage({super.key});


  @override
  State<TearmAandConditionPage> createState() => _TearmAandConditionPageState();
}

class _TearmAandConditionPageState extends State<TearmAandConditionPage> {
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 51.h, left: 24.w,),
                        child: Text("Tearm and Condition",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 16.h, left: 24.w,bottom: 8.h),
                        child: Text("General Terms and Conditions",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.white),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "a. By creating an account, you agree to abide by our\nterms and conditions, which constitute a legally binding\nagreement between you and our company.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "b. You must be at least 18 years old or have obtained parental\nconsent to use our services.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "c. We reserve the right to modify or update these terms and\nconditions at any time without prior notice.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.h,left: 24.w,),
                        child: Text(
                          "d. You are responsible for keeping your account information,\nsuch as your login credentials, secure and confidential.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 24.w,bottom: 8.h),
                        child: Text("Booking",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.white),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "a. By creating an account, you agree to abide by our\nterms and conditions, which constitute a legally binding\nagreement between you and our company.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "b. You must be at least 18 years old or have obtained parental\nconsent to use our services.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "c. We reserve the right to modify or update these terms and\nconditions at any time without prior notice.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.h,left: 24.w,),
                        child: Text(
                          "d. You are responsible for keeping your account information,\nsuch as your login credentials, secure and confidential.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 24.w,bottom: 8.h),
                        child: Text("User Conduct",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.white),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "a. By creating an account, you agree to abide by our\nterms and conditions, which constitute a legally binding\nagreement between you and our company.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "b. You must be at least 18 years old or have obtained parental\nconsent to use our services.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "c. We reserve the right to modify or update these terms and\nconditions at any time without prior notice.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.h,left: 24.w,),
                        child: Text(
                          "d. You are responsible for keeping your account information,\nsuch as your login credentials, secure and confidential.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 24.w,bottom: 8.h),
                        child: Text("Disclaimer of Warranties and Limitation\nof Liability",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.white),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "a. By creating an account, you agree to abide by our\nterms and conditions, which constitute a legally binding\nagreement between you and our company.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "b. You must be at least 18 years old or have obtained parental\nconsent to use our services.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "c. We reserve the right to modify or update these terms and\nconditions at any time without prior notice.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.h,left: 24.w,),
                        child: Text(
                          "d. You are responsible for keeping your account information,\nsuch as your login credentials, secure and confidential.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 24.w,bottom: 8.h),
                        child: Text("User Conduct",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.white),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "a. By creating an account, you agree to abide by our\nterms and conditions, which constitute a legally binding\nagreement between you and our company.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "b. You must be at least 18 years old or have obtained parental\nconsent to use our services.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 12.h,left: 24.w,),
                        child: Text(
                          "c. We reserve the right to modify or update these terms and\nconditions at any time without prior notice.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 56.h,left: 24.w,),
                        child: Text(
                          "d. You are responsible for keeping your account information,\nsuch as your login credentials, secure and confidential.",
                          style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w400,color: AppColor.white.withOpacity(0.5)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              
              
            ],
          ),
        ],
      ),
    );
  }
}
