abstract class NavBaseState {}

class NavChangeFanState extends NavBaseState{
  final int index;

  NavChangeFanState({required this.index});
}
