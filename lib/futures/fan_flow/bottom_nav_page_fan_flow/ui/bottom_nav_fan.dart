import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_drawer.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/profile_page.dart';
import 'package:vibe_app/futures/fan_flow/search_Fan_page/ui/search_page.dart';
import 'package:vibe_app/test_map.dart';

import '../../../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../constant_pages/splash_page/ui/splash_page.dart';
import '../../home_page/ui/home_page.dart';
import '../logic/state/nav_state.dart';
import '../logic/view_model/view_model_bottom_nav.dart';

class BottomNavFanPages extends StatefulWidget {
  final int index;

  BottomNavFanPages({required this.index});

  @override
  State<BottomNavFanPages> createState() => _BottomNavFanPagesState();
}

class _BottomNavFanPagesState extends State<BottomNavFanPages> {
  NavViewModel viewModel = NavViewModel();
  List<Widget> lsPage = [
    HomePage(),
    SearchFanPage(),
    SizedBox(),
    ProfileFanPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      extendBody: true,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BlocConsumer(
                bloc: viewModel,
                builder: (context, sts) {
                  if (sts is NavChangeFanState) {
                    if (sts.index == 0 || sts.index == 1) {
                      return Padding(
                        padding:
                            EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Builder(
                              builder:
                                  (context) => // Ensure Scaffold is in context
                                      GestureDetector(
                                onTap: () {
                                  Scaffold.of(context).openDrawer();
                                },
                                child: SvgPicture.asset(ImagesApp.icMenu),
                              ),
                            ),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            GestureDetector(
                                onTap: () {},
                                child:
                                    SvgPicture.asset(ImagesApp.icNotification)),
                          ],
                        ),
                      );
                    } else {
                      return SizedBox();
                    }
                  }
                  return const SizedBox();
                },
                listener: (BuildContext context, state) {
                  if (state is NavChangeFanState) {
                    print(state.index);
                  }
                },
              ),
              Expanded(
                child: BlocBuilder(
                  bloc: viewModel..changeNav(widget.index),
                  builder: (context, state) {
                    if (state is NavChangeFanState) {
                      return lsPage[state.index];
                    } else {
                      return const SizedBox();
                    }
                  },
                ),
              )
            ],
          ),
        ],
      ),
      bottomNavigationBar: BlocBuilder(
        bloc: viewModel,
        builder: (context, state) {
          if (state is NavChangeFanState) {
            return SizedBox(
              height: 95.h,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(right: state.index == 0 ? 10.w : 0),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color: Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          bottomLeft: Radius.circular(20.r),
                          topRight: Radius.circular(state.index == 0
                              ? 20.r
                              : state.index == 1
                                  ? 20.r
                                  : 0),
                          bottomRight: Radius.circular(state.index == 0
                              ? 20.r
                              : state.index == 1
                                  ? 20.r
                                  : 0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 0
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(0);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icHome,
                                color: state.index == 0
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: state.index == 1 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color: Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index == 1
                              ? 20.r
                              : state.index == 0
                                  ? 20.r
                                  : 0),
                          topRight: Radius.circular(state.index == 1
                              ? 20.r
                              : state.index == 2
                                  ? 20.r
                                  : 0),
                          bottomLeft: Radius.circular(state.index == 1
                              ? 20.r
                              : state.index == 0
                                  ? 20.r
                                  : 0),
                          bottomRight: Radius.circular(state.index == 1
                              ? 20.r
                              : state.index == 2
                                  ? 20.r
                                  : 0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 1
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(1);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icSearch,
                                color: state.index == 1
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: state.index == 2 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color: Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index == 2
                              ? 20.r
                              : state.index == 1
                                  ? 20.r
                                  : 0),
                          topRight: Radius.circular(state.index == 2
                              ? 20.r
                              : state.index == 3
                                  ? 20.r
                                  : 0),
                          bottomLeft: Radius.circular(state.index == 2
                              ? 20.r
                              : state.index == 1
                                  ? 20.r
                                  : 0),
                          bottomRight: Radius.circular(state.index == 2
                              ? 20.r
                              : state.index == 3
                                  ? 20.r
                                  : 0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 2
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(0);
                                showMap();
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icLocation,
                                color: state.index == 2
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: state.index == 3 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color: Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index == 3
                              ? 20.r
                              : state.index == 2
                                  ? 20.r
                                  : 0),
                          topRight: Radius.circular(20.r),
                          bottomLeft: Radius.circular(state.index == 3
                              ? 20.r
                              : state.index == 2
                                  ? 20.r
                                  : 0),
                          bottomRight: Radius.circular(20.r),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 3
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(3);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icUser,
                                color: state.index == 3
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
          return SizedBox();
        },
      ),
      drawer: CustomDrawer(),
    );
  }
  void showMap() {


    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        child: Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.r),
                                topRight: Radius.circular(20.r)),),
                            height: MediaQuery.of(context).size.height - 101.h,
                            child: MapApp(
                              center: LatLong(33, 54),
                              isVisiblBtn: false,
                              onPicked: (pickedData) {
                                print(pickedData.latLong.latitude);
                                print(pickedData.latLong.longitude);
                                print(pickedData.address);

                                setState(() {
                                });
                              },


                            )
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
