import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

class ShowsTabEventArtist extends StatelessWidget {
  ShowsTabEventArtist({required this.scrollController}) : super();
  ScrollController scrollController;
  List<String> ls = [
    ImagesApp.show1,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
    ImagesApp.show2,
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        controller: scrollController,
        itemCount: 20,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(bottom: 8.h),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20.w),
              width: 336.w,
              decoration: BoxDecoration(
                color: AppColor.white.withOpacity(0.1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 4.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          width: 52.w,
                          height: 56.h,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.r),
                              image: DecorationImage(
                                  image: AssetImage(ImagesApp.detailsArtist))),
                        ),
                        SizedBox(
                          width: 11.w,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Open Mic Night",
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "Thursday, August 24 at 7:30 pm",
                              style: poppins.copyWith(
                                fontSize: 11.sp,
                                fontWeight: FontWeight.w600,
                                color: AppColor.blue,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    Container(
                      height: 78,
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.12),
                          borderRadius: BorderRadius.circular(7),
                          border:
                          Border.all(width: 0.5.w, color: AppColor.blue)),
                      child: Padding(
                        padding: EdgeInsets.only(top: 7.h, left: 9.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "House of Music",
                              style: poppins.copyWith(
                                fontSize: 11.sp,
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 16.w,
                                  height: 16.r,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.r),
                                    color: AppColor.white.withOpacity(0.12),
                                  ),
                                  child: Center(
                                    child: SvgPicture.asset(
                                      ImagesApp.icLocation,
                                      width: 10,
                                      height: 10.r,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 6.w),
                                Text(
                                  "Elgin St. Celina, Delaware",
                                  style: poppins.copyWith(
                                      fontSize: 10.sp,
                                      color: AppColor.greyHint),
                                ),
                              ],
                            ),
                            Padding(
                              padding:  EdgeInsets.only(left: 20.w,top: 4.h),
                              child: Row(
                                children: [
                                 
                                  Stack(
                                    children: [
                                      Container(
                                        width: 19.w,
                                        height: 19.r,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(ImagesApp.icP1),
                                          ),
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Container(
                                          width: 19.w,
                                          height: 19.r,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(ImagesApp.icP2),
                                            ),
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:  EdgeInsets.only(left: 20.w,),
                                        child: Container(
                                          width: 19.w,
                                          height: 19.r,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(ImagesApp.icP3),
                                            ),
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Spacer(),
                                  GenerateChipe(borderColor: AppColor.greyHint,h: 20,borderRadius: 4.r,textStyle:poppins.copyWith(fontSize: 8.sp),texts: ["Chill","Soulful"],),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
