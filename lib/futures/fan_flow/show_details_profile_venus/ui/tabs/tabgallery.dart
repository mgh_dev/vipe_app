import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabGallery extends StatelessWidget {
  TabGallery({required this.scrollController}) : super();
  ScrollController scrollController;
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(top:24.h),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Row  (
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 168.w,
                  height: 276.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.r),
                    image: DecorationImage(
                      image: AssetImage(ImagesApp.p2),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 6.w),
                Container(
                  width: 168.w,
                  height: 109.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.r),
                    image: DecorationImage(
                      image: AssetImage(ImagesApp.profileTest1),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(width: 6.w),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 168.w,
                  height: 139.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.r),
                    image: DecorationImage(
                      image: AssetImage(ImagesApp.p1),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 6.w),
                Container(
                  width: 168.w,
                  height: 246.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.r),
                    image: DecorationImage(
                      image: AssetImage(ImagesApp.prof),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
