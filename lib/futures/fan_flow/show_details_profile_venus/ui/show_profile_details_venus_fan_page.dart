import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/dat.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/shows_tab_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabgallery.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tab_media_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/tabs/tabreview.dart';

import '../../../../core/helper/custom_button.dart';
import '../../../../core/helper/different_size_of_gridview_item.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../../date_create_event_page.dart';
import '../../../../houseofmusic.dart';

class ShowProfileVenusFanPage extends StatefulWidget {
  const ShowProfileVenusFanPage({Key? key}) : super(key: key);

  @override
  State<ShowProfileVenusFanPage> createState() => _ShowProfileVenusFanPageState();
}

class _ShowProfileVenusFanPageState extends State<ShowProfileVenusFanPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();
  var texts = [
    'Intimate',
    'Chill',
    'Bluesy',
    'Eclectic',
    'High Energy',
    'Nostalgic',
    'Folksy',
    'Jazzy',
  ];
  var texts2 = [
    'Intimate',
    'Chill',
    'Bluesy',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(
      length: 4,
      vsync: this,
      initialIndex: 0,
    );

    int tabIndex = 0;

    tabController.addListener(() {
      if (tabController.index == 0) {
        if (tabIndex == 0) {
          return;
        } else {}
      } else {
        if (tabIndex == 1) {
          return;
        } else {}
      }
      tabIndex = tabController.index;
    });


  }

  @override
  Widget build(BuildContext context) {
    secondController.addListener(() {
      if (secondController.offset <=
          secondController.position.minScrollExtent) {
        firstController.animateTo(0,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      } else if (secondController.offset >=
          secondController.position.minScrollExtent) {
        firstController.animateTo(firstController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      }
    });
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: CustomScrollView(
        controller: firstController,
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  height: 427.h,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(ImagesApp.venusProf),
                        fit: BoxFit.cover),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: <Color>[
                          const Color(0xff090B1B),
                          const Color(0xff090B1B).withOpacity(0.2),
                          Colors.transparent,
                          Colors.transparent,
                          Colors.transparent,
                          const Color(0xff090B1B).withOpacity(0.98),
                          const Color(0xff090B1B),
                        ],
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: 53.h, left: 20.w, right: 20.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: SvgPicture.asset(ImagesApp.icBack)),
                              Image.asset(
                                ImagesApp.logoApp,
                                width: 60.w,
                                height: 60.h,
                              ),
                              SvgPicture.asset(ImagesApp.icShared),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Spacer(),
                            Spacer(),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "House of Music",
                                      style: poppins.copyWith(
                                        fontSize: 26.sp,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Icon(
                                      Icons.check_circle,
                                      color: AppColor.greyBlue2,
                                      size: 21,
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "4.5* ",
                                      style: poppins.copyWith(
                                        fontSize: 12.sp,
                                      ),
                                    ),
                                    Text(
                                      ". Concert hall .",
                                      style: poppins.copyWith(
                                        fontSize: 12.sp,
                                      ),
                                    ),
                                    Text(
                                      " Virginia",
                                      style: poppins.copyWith(
                                        fontSize: 12.sp,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 15),
                              ],
                            ),
                            Spacer(),
                            Container(
                              margin: EdgeInsets.only(right: 16.w),
                              width: 40.w,
                              height:40.r,
                              decoration: BoxDecoration(
                                color: AppColor.blue,
                                shape: BoxShape.circle,
                              ),
                              child: Center(child: SvgPicture.asset(ImagesApp.icSaveFavorite)),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 24.h),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.w),
                  child: DescriptionTextWidget(
                    text:
                        "We're a premier event space that caters to a wide range of events. Our space is equipped with state-of-the-art audio and visual technology, customizable lighting options, and a flexible floor plan to accommodate any event layout. Whether you're looking to host an intimate gathering or a large-scale celebration, our venue is the perfect choice for your next event",
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 18.h, bottom: 16.h),
                          child: const DividerWidget(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16.w, top: 4.5.h),
                          child: Text(
                            "vibe",
                            style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w500,
                                color: AppColor.blueText),
                          ),
                        ),
                        SizedBox(
                          height: 12.5.h,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 22.w, bottom: 10),
                          child: GenerateChipe(
                            texts: texts,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 22.h, bottom: 20.h),
                          width: double.infinity,
                          height: 0.5,
                          color: AppColor.greyLine.withOpacity(0.40),
                        ),
                        Row(
                          children: [
                            Text(
                              "Website:  ",
                              style: inter.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.greyTxt),
                            ),
                            Text(
                              "www.houseofnusic.com",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 20.h),
                          width: double.infinity,
                          height: 0.5,
                          color: AppColor.greyLine.withOpacity(0.40),
                        ),
                        Row(
                          children: [
                            Text(
                              "Social Media:  ",
                              style: poppins.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.greyTxt),
                            ),
                            Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white.withOpacity(0.07),
                              ),
                              child: Center(
                                  child: SvgPicture.asset(ImagesApp.icYouTube)),
                            ),
                            SizedBox(width: 8.w),
                            Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white.withOpacity(0.07),
                              ),
                              child: Center(
                                  child:
                                      SvgPicture.asset(ImagesApp.icInstagram)),
                            ),
                            SizedBox(width: 8.w),
                            Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white.withOpacity(0.07),
                              ),
                              child: Center(
                                  child: SvgPicture.asset(ImagesApp.icLinkdin)),
                            ),
                            SizedBox(width: 8.w),
                            Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white.withOpacity(0.07),
                              ),
                              child: Center(
                                  child: SvgPicture.asset(ImagesApp.icFasBook)),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 22.h, bottom: 20.h),
                          width: double.infinity,
                          height: 0.5,
                          color: AppColor.greyLine.withOpacity(0.40),
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Capacity: ",
                                  style: poppins.copyWith(
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w500,
                                      color: AppColor.greyTxt),
                                ),
                                Text(
                                  "300 guests",
                                  style: poppins.copyWith(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Stage number: ",
                                  style: poppins.copyWith(
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w500,
                                      color: AppColor.greyTxt),
                                ),
                                Text(
                                  "1",
                                  style: poppins.copyWith(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Age limit: ",
                                  style: poppins.copyWith(
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w500,
                                      color: AppColor.greyTxt),
                                ),
                                Text(
                                  "No age limit",
                                  style: poppins.copyWith(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20.h, bottom: 16.h),
                          width: double.infinity,
                          height: 0.5,
                          color: AppColor.greyLine.withOpacity(0.40),
                        ),
                        Text(
                          "In-house equipment",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.whiteEB),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: 10.h,
                          ),
                          child: GenerateChipe(
                            texts: texts2,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 22.h,
                          ),
                          width: double.infinity,
                          height: 0.5,
                          color: AppColor.greyLine.withOpacity(0.40),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
              child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.w),
                  child: Container(
                    width: double.infinity,
                    height: 1,
                    color: AppColor.greyHint,
                  ),
                ),
                TabBar(
                  controller: tabController,
                  // dividerColor: Colors.red,
                  indicatorColor: AppColor.blue,
                  labelColor: AppColor.white,
                  unselectedLabelColor: AppColor.greyHint.withOpacity(0.5),
                  labelStyle: poppins.copyWith(
                    fontSize: 12.sp,
                  ),
                  isScrollable: true,

                  indicatorSize: TabBarIndicatorSize.tab,
                  tabs: const [
                    Tab(text: 'Events'),
                    Tab(text: 'Media'),
                    Tab(text: 'Gallery'),
                    Tab(text: 'Review'),
                  ],
                ),
              ],
            ),
          )),
          SliverFillRemaining(
            child: TabBarView(
              controller: tabController,
              children: [
                ShowsTabVenus(scrollController: secondController),
                TabMediaVenus(
                  scrollController: secondController,
                ),
                TabGallery(
                  scrollController: secondController,

                ),
                TabReview(
                  scrollController: secondController,
                ),
              ],
            ),
          ),
        ],
      ),
      // floatingActionButton: Padding(
      //   padding: const EdgeInsets.only(bottom: 100),
      //   child: GestureDetector(
      //     onTap: (){
      //       Navigator.push(context, MaterialPageRoute(builder: C))
      //     },
      //     child: Container(
      //       width: 60.w,
      //       height: 60.r,
      //       decoration: BoxDecoration(
      //         color: AppColor.blue,
      //         shape: BoxShape.circle,
      //       ),
      //       child: Center(
      //         child: Icon(CupertinoIcons.chat_bubble_fill,color: Colors.white,),
      //       ),
      //     ),
      //   ),
      // ),
    );
  }
}
