import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/constant_bloc/logic_edit_filde/state/edit_filde_state.dart';
import 'package:vibe_app/constant_bloc/logic_edit_filde/view_model/view_model_edit_filde.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/futures/fan_flow/bottom_nav_page_fan_flow/ui/bottom_nav_fan.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/get_profile_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/state/update_name_fan_state.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/view_model/get_profile_fan_view_model.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/logic/view_model/update_name_fan_view_model.dart';
import 'package:vibe_app/futures/fan_flow/profile_page/ui/profile_page.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';

class UpdateProfilePage extends StatefulWidget {
  UpdateProfilePage({Key? key}) : super(key: key);

  @override
  State<UpdateProfilePage> createState() => _UpdateProfilePageState();
}

class _UpdateProfilePageState extends State<UpdateProfilePage> {
  File? image;
  UploadAvatarViewModel uploadAvatarViewModel = UploadAvatarViewModel();
  final editNameBloc = ViewModelEditFiled();
  final getProfileFan = GetProfileFanViewModel();
  final updateNameViewModel = UpdateNameFanViewModel();
  final userFirstNameController = TextEditingController();
  final userLastNameController = TextEditingController();

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  FocusNode? _focusNodeName;
  FocusNode? _focusNodeLastName;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileFan.get();
    _focusNodeName = FocusNode();
    _focusNodeLastName = FocusNode();
  }

  @override
  void dispose() {
    _focusNodeName!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: getProfileFan,
        builder: (context, state) {
          if (state is GetProfileFanLoadingState) {
            return Scaffold(
              backgroundColor: AppColor.backgroundApp,
              body: Stack(
                children: [
                  Positioned(
                      left: -100.w,
                      top: -100.h,
                      child: ContainerBlur(color: AppColor.purple)),
                  const Align(
                    alignment: Alignment.center,
                    child: CustomLoading(),
                  ),
                ],
              ),
            );
          }
          if (state is GetProfileFanSuccessState) {
            userFirstNameController.text = state.response.data.firstName;
            userLastNameController.text = state.response.data.lastName;
            return Scaffold(
              backgroundColor: AppColor.backgroundApp,
              body: Stack(
                children: [
                  Positioned(
                      left: -100.w,
                      top: -100.h,
                      child: ContainerBlur(color: AppColor.purple)),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: 53.h, left: 20.w, bottom: 33.h, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              BottomNavFanPages(index: 3)));
                                },
                                child: SvgPicture.asset(ImagesApp.icBack)),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            SvgPicture.asset(ImagesApp.icShared),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 39.h,
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Container(
                                width: 343.w,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(20.r),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, top: 16.h, bottom: 13.h),
                                      child: Text(
                                        "Let’s update your profile",
                                        style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: 349.w,
                                      height: 1.h,
                                      decoration: BoxDecoration(
                                        color: AppColor.white.withOpacity(0.1),
                                        borderRadius:
                                            BorderRadius.circular(20.r),
                                      ),
                                    ),
                                    SizedBox(height: 11.h),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w,
                                          right: 16.w,
                                          bottom: 16.h),
                                      child: Container(
                                        width: 311.w,
                                        height: 232.h,
                                        decoration: BoxDecoration(
                                          color:
                                              AppColor.white.withOpacity(0.1),
                                          borderRadius:
                                              BorderRadius.circular(20.r),
                                        ),
                                        child: Stack(
                                          alignment: Alignment.topRight,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  top: 8.h, right: 8.w),
                                              child: GestureDetector(
                                                onTap: () {
                                                  showModalBottomSheet(
                                                    isScrollControlled: true,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(
                                                                10.r),
                                                        topRight:
                                                            Radius.circular(
                                                                10.r),
                                                      ),
                                                    ),
                                                    context: context,
                                                    builder: (context) =>
                                                        Container(
                                                      width: double.infinity,
                                                      height: 150.h,
                                                      decoration: BoxDecoration(
                                                          color:
                                                              AppColor.darkBlue,
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    10.r),
                                                            topRight:
                                                                Radius.circular(
                                                                    10.r),
                                                          )),
                                                      child: Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                              top: 15.h,
                                                              right: 15.w,
                                                            ),
                                                            child: Align(
                                                              alignment:
                                                                  Alignment
                                                                      .topRight,
                                                              child:
                                                                  GestureDetector(
                                                                onTap: () {
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                child:
                                                                    Container(
                                                                  width: 35.w,
                                                                  height: 35.r,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: Colors
                                                                        .white,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10.r),
                                                                  ),
                                                                  child:
                                                                      const Icon(
                                                                    Icons.close,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              GestureDetector(
                                                                onTap: () {
                                                                  pickImage(
                                                                      ImageSource
                                                                          .gallery);
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    SizedBox(
                                                                        width: 50
                                                                            .r,
                                                                        height: 50
                                                                            .h,
                                                                        child:
                                                                            const Icon(
                                                                          Icons
                                                                              .image,
                                                                          color:
                                                                              Colors.white,
                                                                        )),
                                                                    Text(
                                                                      "Gallery",
                                                                      style: poppins
                                                                          .copyWith(
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              GestureDetector(
                                                                onTap: () {
                                                                  pickImage(
                                                                      ImageSource
                                                                          .camera);
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    SizedBox(
                                                                        width: 50
                                                                            .r,
                                                                        height: 50
                                                                            .h,
                                                                        child:
                                                                            const Icon(
                                                                          Icons
                                                                              .camera,
                                                                          color:
                                                                              Colors.white,
                                                                        )),
                                                                    Text(
                                                                      "Camera",
                                                                      style: poppins
                                                                          .copyWith(
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: SvgPicture.asset(
                                                    ImagesApp.icPen),
                                              ),
                                            ),
                                            Center(
                                              child: Container(
                                                width: 112.w,
                                                height: 112.h,
                                                decoration: BoxDecoration(
                                                  color: AppColor.darkBlue,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                    child: image != null
                                                        ? Container(
                                                            width: 112.w,
                                                            height: 112.h,
                                                            decoration:
                                                                BoxDecoration(
                                                              image: DecorationImage(
                                                                  image:
                                                                      FileImage(
                                                                          image!),
                                                                  fit: BoxFit
                                                                      .fill),
                                                              shape: BoxShape
                                                                  .circle,
                                                            ),
                                                          )
                                                        : Container(
                                                            width: 112.w,
                                                            height: 112.h,
                                                            decoration:
                                                                BoxDecoration(
                                                              image: state
                                                                          .response
                                                                          .data
                                                                          .avatar ==
                                                                      null
                                                                  ? DecorationImage(
                                                                      image: AssetImage(
                                                                          ImagesApp
                                                                              .prof),
                                                                    )
                                                                  : DecorationImage(
                                                                      image: NetworkImage(
                                                                          "http://46.249.102.65:8080/${state.response.data.avatar}"),
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    ),
                                                              shape: BoxShape
                                                                  .circle,
                                                            ),
                                                          )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 10.h, bottom: 0.h),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      children: [
                                        SizedBox(
                                          width: 300.w,
                                          child: CustomTextField(
                                            controller: userFirstNameController,
                                            fontSize: 18.sp,
                                            focusNode: _focusNodeName,
                                          ),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (_focusNodeName!.hasFocus == false) {
                                          FocusScope.of(context)
                                              .requestFocus(_focusNodeName);
                                        } else {
                                          FocusManager.instance.primaryFocus
                                              ?.unfocus();
                                        }
                                      },
                                      child: SvgPicture.asset(
                                        ImagesApp.icPen,
                                        color: AppColor.blue,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.w,
                                    )
                                  ],
                                ),
                              ),
                              const DividerWidget(),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 10.h, bottom: 0.h),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      children: [
                                        SizedBox(
                                          width: 300.w,
                                          child: CustomTextField(
                                            controller: userLastNameController,
                                            fontSize: 18.sp,
                                            focusNode: _focusNodeLastName,
                                          ),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (_focusNodeLastName!.hasFocus ==
                                            false) {
                                          FocusScope.of(context)
                                              .requestFocus(_focusNodeLastName);
                                        } else {
                                          FocusManager.instance.primaryFocus
                                              ?.unfocus();
                                        }
                                      },
                                      child: SvgPicture.asset(
                                        ImagesApp.icPen,
                                        color: AppColor.blue,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.w,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              bottomNavigationBar: Container(
                height: 84.h,
                decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.01),
                ),
                child: Center(
                  child: BlocConsumer(
                    listener: (context, state) {
                      if (state is UpdateNameFanSuccessState) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    BottomNavFanPages(index: 3)));
                      }
                    },
                    bloc: updateNameViewModel,
                    builder: (context, stateNameUpdate) {
                      return BlocConsumer(
                        bloc: uploadAvatarViewModel,
                        listener: (context, state) {
                          if (state is UploadAvatarSuccessState ||
                              stateNameUpdate is UpdateNameFanSuccessState) {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        BottomNavFanPages(index: 3)));
                          }
                        },
                        builder: (context, state) {
                          return MyCustomButton(
                            title: "Update",
                            loading: state is UploadAvatarLoadingState ||
                                stateNameUpdate is UpdateNameFanLoadingState,
                            onTap: () {
                              image != null
                                  ? uploadAvatarViewModel
                                      .uploadAvatar(image!.path)
                                  : null;
                              updateNameViewModel.updateName(
                                nameUsers: userFirstNameController.text,
                                lastNameUsers: userLastNameController.text,
                              );
                            },
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
            );
          }
          if (state is GetProfileFanFailState) {
            return Container();
          }

          return const SizedBox();
        });
  }
}
