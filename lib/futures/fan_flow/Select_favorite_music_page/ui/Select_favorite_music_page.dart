import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:glass/glass.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/futures/fan_flow/bottom_nav_page_fan_flow/ui/bottom_nav_fan.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../home_page/ui/home_page.dart';
import 'contact_model.dart';

class SelectFavoriteMusicPage extends StatefulWidget {
  SelectFavoriteMusicPage({Key? key}) : super(key: key);

  @override
  State<SelectFavoriteMusicPage> createState() =>
      _SelectFavoriteMusicPageState();
}

class _SelectFavoriteMusicPageState extends State<SelectFavoriteMusicPage> {
  List<ContactModel> contacts = [
    ContactModel("assets/png/1.png", "Pop ", false),
    ContactModel("assets/png/2.png", "Rock ", false),
    ContactModel("assets/png/3.png", "Hip hop ", false),
    ContactModel("assets/png/4.png", "Electronic dance ", false),
    ContactModel("assets/png/5.png", "Jazz ", false),
    ContactModel("assets/png/6.png", "Heavy metal ", false),
    ContactModel("assets/png/7.png", "Classical ", false),
    ContactModel("assets/png/8.png", "Country ", false),
    ContactModel("assets/png/9.png", "R&B ", false),
    ContactModel("assets/png/10.png", "Regga ", false),
    ContactModel("assets/png/11.png", "Blues ", false),
    ContactModel("assets/png/12.png", "DJ ", false),
    ContactModel("assets/png/13.png", "Folk ", false),
    ContactModel("assets/png/14.png", "Punk rock ", false),
    ContactModel("assets/png/15.png", "New-age ", false),
  ];
  bool visibleBtn = false;

  List<ContactModel> selectedContacts = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 53.h, left: 4.w, right: 4.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SvgPicture.asset(ImagesApp.icBack),
                      Image.asset(
                        ImagesApp.logoApp,
                        width: 60.w,
                        height: 60.h,
                      ),
                      SvgPicture.asset(ImagesApp.icShared),
                    ],
                  ),
                ),
                SizedBox(height: 20.h),
                Text(
                  "Let’s Vibe!",
                  style: poppins.copyWith(
                    fontSize: 24.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 4.h, bottom: 25.h),
                  child: Text(
                    "Select your favorite music genre.",
                    style: poppins.copyWith(
                        fontSize: 14.sp, color: AppColor.grey2),
                  ),
                ),
                Expanded(
                    child: GridView.builder(
                  itemCount: contacts.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 6.5,
                      mainAxisSpacing: 7),
                  itemBuilder: (context, index) {
                    return contactItem(
                        contacts[index].images,
                        contacts[index].nameMusic,
                        contacts[index].isSelected,
                        index);
                  },
                )),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: 84.h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 165.w,
              height: 49.h,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: AppColor.grey1.withOpacity(0.4),
                ),
                borderRadius: BorderRadius.circular(50.r),
              ),
              child: Center(
                child: Text(
                  "Skip",
                  style: poppins.copyWith(
                    fontSize: 14.sp,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: visibleBtn
                  ? () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BottomNavFanPages(index: 0),
                        ),
                      );
                    }
                  : () {},
              child: Container(
                width: 165.w,
                height: 49.h,
                decoration: BoxDecoration(
                  color: visibleBtn ? AppColor.green : Colors.grey,
                  borderRadius: BorderRadius.circular(50.r),
                ),
                child: Center(
                  child: Text(
                    "Next",
                    style: poppins.copyWith(
                      fontSize: 14.sp,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ).asGlass(),
    );
  }

  Widget contactItem(
      String name, String phoneNumber, bool isSelected, int index) {
    return GestureDetector(
      child: Container(
        width: 111.w,
        height: 111.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.r),
            border: Border.all(
              width: 2,
              color: contacts[index].isSelected
                  ? AppColor.green
                  : Colors.transparent,
            ),
            image: DecorationImage(
              image: AssetImage(
                contacts[index].images,
              ),
            )),
        child: contacts[index].isSelected
            ? Padding(
                padding: EdgeInsets.only(top: 9.h, right: 8.w),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Icon(
                    Icons.check_circle,
                    color: AppColor.green,
                    size: 20,
                  ),
                ),
              )
            : null,
      ),
      onTap: () {
        setState(() {
          contacts[index].isSelected = !contacts[index].isSelected;
          if (contacts[index].isSelected == true) {
            selectedContacts.add(ContactModel(name, phoneNumber, true));
          } else if (contacts[index].isSelected == false) {
            selectedContacts.removeWhere(
                (element) => element.images == contacts[index].images);
          }
          if (selectedContacts.isEmpty) {
            visibleBtn = false;
          } else {
            visibleBtn = true;
          }
        });
      },
    );
  }
}
