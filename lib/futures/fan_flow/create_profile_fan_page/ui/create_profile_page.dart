import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:getwidget/components/progress_bar/gf_progress_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_drawer.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/menu.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../Select_favorite_music_page/ui/Select_favorite_music_page.dart';

class CreateProfileFanPage extends StatefulWidget {
  CreateProfileFanPage({Key? key}) : super(key: key);

  @override
  State<CreateProfileFanPage> createState() => _CreateProfileFanPageState();
}

class _CreateProfileFanPageState extends State<CreateProfileFanPage> {
  File? image;
  UploadAvatarViewModel uploadAvatarViewModel=UploadAvatarViewModel();

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      extendBody: true,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
      Column(
        children: [
          Padding(
            padding:
            EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Builder(
                  builder:
                      (context) => // Ensure Scaffold is in context
                  GestureDetector(
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: SvgPicture.asset(ImagesApp.icMenu),
                  ),
                ),
                Image.asset(
                  ImagesApp.logoApp,
                  width: 60.w,
                  height: 60.h,
                ),
                GestureDetector(
                    onTap: () {},
                    child:
                    SvgPicture.asset(ImagesApp.icNotification)),
              ],
            ),
          ),
          SizedBox(
            height: 39.h,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Container(
                      width: 343.w,
                      decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(20.r),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: 16.w, top: 16.h, bottom: 13.h),
                            child: Text(
                              "Let’s set up your Profile Picture.",
                              style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            width: 349.w,
                            height: 1.h,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(20.r),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 16.w, top: 11.h, bottom: 8.h),
                            child: Text(
                              "Add Profile photo",
                              style: poppins.copyWith(
                                fontSize: 13.sp,
                                color: AppColor.greyText,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 16.w, right: 16.w, bottom: 16.h),
                            child: Align(
                              alignment: Alignment.center,
                              child: Container(
                                width: 311.w,
                                height: 232.h,
                                decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(
                                      20.r),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Stack(
                                      alignment: Alignment.bottomRight,
                                      children: [
                                        Container(
                                          width: 112.w,
                                          height: 112.h,
                                          decoration: BoxDecoration(
                                            color: AppColor.darkBlue,
                                            shape: BoxShape.circle,
                                          ),
                                          child: Center(
                                            child: image != null
                                                ? Container(
                                              width: 112.w,
                                              height: 112.h,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: FileImage(
                                                        image!),
                                                    fit: BoxFit.fill),
                                                shape: BoxShape.circle,
                                              ),
                                            )
                                                : SvgPicture.asset(
                                              ImagesApp.icUser,
                                              width: 22.w,
                                              height: 28.h,
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            pickImage(
                                                ImageSource.gallery);
                                            print("object");
                                          },
                                          child: Container(
                                            width: 42.w,
                                            height: 42.r,
                                            decoration: BoxDecoration(
                                              color: AppColor.black54,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: Container(
                                                width: 35.w,
                                                height: 35.r,
                                                decoration: BoxDecoration(
                                                  color: AppColor
                                                      .darkPurple,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                    child: SvgPicture
                                                        .asset(
                                                        ImagesApp
                                                            .icCamera)),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 17.h),
                                    Text(
                                      "Upload Image ",
                                      style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        color: AppColor.darkPurple,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 21.w,
                                          right: 12.w,
                                          top: 13.h),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 225,
                                            height: 8,
                                            child: GFProgressBar(
                                              percentage: 0.2,
                                              lineHeight: 8,
                                              backgroundColor: AppColor
                                                  .white
                                                  .withOpacity(0.1),
                                              progressBarColor:
                                              AppColor.darkPurple,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Text("10%",
                                              style: poppins.copyWith(
                                                  fontSize: 12)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      )
        ],
      ),
      bottomNavigationBar: Container(
        height: 84.h,
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.01),
        ),
        child: BlocConsumer(
          listener: (context,state){
            if(state is UploadAvatarSuccessState){
              Navigator.pushReplacement(context, MaterialPageRoute(
                  builder: (context) => SelectFavoriteMusicPage()));
            }
          },
          bloc:uploadAvatarViewModel ,
          builder: (context,state){
            return Center(
                child: MyCustomButton(
                  title: "Continue",
                  loading: state is UploadAvatarLoadingState,
                  onTap: () {
                    image!=null
                        ?uploadAvatarViewModel.uploadAvatar(image!.path)
                        :null;

                  },
                ));
          },
        ),
      ),

      drawer: CustomDrawer(),
    );;
  }
}
