import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class MessagesPageArtist extends StatelessWidget {
  MessagesPageArtist({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.h, bottom: 22.h),
                child: Text(
                  "Messages",
                  style: poppins.copyWith(
                      fontSize: 24.sp,
                      fontWeight: FontWeight.w600,
                      color: AppColor.white),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: 15,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ChatRoomPage()));
                      },
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 20.w),
                        padding: EdgeInsets.symmetric(vertical: 14.w),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: AppColor.grey5b,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              ImagesApp.profileMsg,
                              width: 40.w,
                              height: 40.h,
                            ),
                            SizedBox(
                              width: 12.w,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Esther Howard",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                      color: AppColor.white),
                                ),
                                Text(
                                  "Hello! I’d love to play at your venue...",
                                  style: poppins.copyWith(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.grey2),
                                ),
                              ],
                            ),
                            const Spacer(),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "13:45",
                                  style: poppins.copyWith(
                                      fontSize: 11.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.greyHint),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: AppColor.blue),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 4.h, horizontal: 6.5.w),
                                    child: Center(
                                      child: Text(
                                        "2",
                                        style: inter.copyWith(
                                            fontSize: 10.sp,
                                            fontWeight: FontWeight.w500,
                                            color: AppColor.white),
                                      ),
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              /*Expanded(child: SingleChildScrollView(
                child:
              )),*/
            ],
          ),
        ],
      ),
    );
  }
}
