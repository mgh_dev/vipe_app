import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/venue_flow/bottom_nav_page_venus_flow/ui/bottom_nav_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/ui/home_venus_page.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class ComplecteAndShareProfilePage extends StatelessWidget {
  ComplecteAndShareProfilePage({Key? key}) : super(key: key);
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 53.h, left: 20.w, bottom: 0.h, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset(ImagesApp.icMenu),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: 20.h, right: 62.w, left: 62.w, top: 60.h),
                child: Image.asset(
                  ImagesApp.congratulations,
                  width: 250.w,
                  height: 250.h,
                ),
              ),
              Text(
                "Congratulations on \nsetting up your EPK!",
                style: poppins.copyWith(
                    fontSize: 24.sp,
                    fontWeight: FontWeight.w600,
                    color: AppColor.white),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 21.w, vertical: 12.h),
                child: Text(
                  "Your profile is now fully optimized and ready \nfor action.\nyou are now able to request gigs, promote \nyour shows and get paid.",
                  textAlign: TextAlign.center,
                  style: poppins.copyWith(
                    fontSize: 14.sp,
                    color: AppColor.greyHint,
                  ),
                ),
              ),

              Spacer(),
              Padding(
                padding: EdgeInsets.only(right: 16.w,left: 16.w,bottom: 8.h),
                child: MyCustomButton(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => BottomNavArtistVenusPages(
                              index: 0,
                            )));
                  },
                  title: "Back To Home",
                  loading: false,
                  color: AppColor.blue,
                  width: 343.w,
                  height: 49.h,
                  fontSize: 14.sp,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 50.h,right: 16.w,left: 16.w,),
                child: MyCustomButton(
                  onTap: () {

                  },
                  title: "Share your Profile",
                  loading: false,
                  color: AppColor.white,
                  width: 343.w,
                  height: 49.h,
                  fontSize: 14.sp,
                  textColor: AppColor.black,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
