import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/style_text_app.dart';
class CustomTextIconButton extends StatelessWidget {

final String title;
final String icon;
final Function() onTap;
CustomTextIconButton({required this.title,required this.icon,required this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          SvgPicture.asset(icon),
          SizedBox(width: 18.w),
          Text(
            title,
            style: poppins.copyWith(
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Spacer(),
          Icon(
            Icons.arrow_forward_ios_outlined,
            color: AppColor.white,
            size: 22,
          )
        ],
      ),
    );
  }


}
