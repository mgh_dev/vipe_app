import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/futures/artist_flow/notification_setting_artist_page/ui/notification_setting_artist_page.dart';
import 'package:vibe_app/futures/artist_flow/tearm_and_condition_artist_page/ui/tearm_and_condition_artist_page.dart';
import 'package:vibe_app/futures/fan_flow/update_profile_page/ui/update_profile_page.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import '../widget/custom_text_icon_button.dart';

class SettingArtistPage extends StatelessWidget {
  const SettingArtistPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              SizedBox(
                height: 39.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Text(
                          "General",
                          style: poppins.copyWith(
                            fontSize: 16.sp,
                            color: AppColor.grey1,
                          ),
                        ),
                      ),
                      SizedBox(height: 32.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: CustomTextIconButton(
                          title: "Notifications",
                          icon: ImagesApp.icSettingNotification,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const NotificationSettingArtistPage(),
                              ),
                            );
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 24.h, bottom: 35.h),
                        child: const DividerWidget(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: CustomTextIconButton(
                          title: "Privacy",
                          icon: ImagesApp.icSettingLock,
                          onTap: () {},
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 24.h, top: 35.h),
                        child: const DividerWidget(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: CustomTextIconButton(
                          title: "Terms and Condition",
                          icon: ImagesApp.icSettingLock,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const TearmAndConditionArtistPage(),
                              ),
                            );
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: 24.h, top: 100.h, left: 24.w),
                        child: Row(
                          children: [
                            Text(
                              "Account",
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                                color: AppColor.grey1,
                              ),
                            ),
                            const Expanded(child: DividerWidget()),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: CustomTextIconButton(
                          title: "Edit profile",
                          icon: ImagesApp.icSettingProfile,
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => UpdateProfilePage()));
                          },
                        ),
                      ),
                      SizedBox(height: 167.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChooseYourRolePage(),
                              ),
                            );
                            HiveServices.clear();
                          },
                          child: Row(
                            children: [
                              SvgPicture.asset(ImagesApp.icLogout),
                              SizedBox(width: 8.w),
                              Text(
                                "Logout",
                                style: poppins.copyWith(
                                  fontSize: 14.sp,
                                  color: AppColor.white,
                                  // fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
