import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/view_model/upload_avatar_view_model.dart';
import 'package:vibe_app/constant_bloc/upload_file_logic/state/upload_file_state.dart';
import 'package:vibe_app/constant_bloc/upload_file_logic/view_model/upload_file_view_model.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_navigator.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/futures/artist_flow/complect_and_share_profile.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/view_model/send_information_view_model.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/model/request_model/request_profile_artist_model.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/select_artist.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/set_up_your_EPK.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/set_up_your_media.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/setup_your_photo.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/steps/step7.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/send_artist_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_artist_view_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/send_vibe_view_model.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/page_view/stepper_page_view.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../constant_pages/sgin_in_page/model/response_model/login_auth_register_model.dart';
import '../../../venue_flow/create_profile_venue/logic/state/send_vibe_state.dart';
import '../steps/select_your_role_step.dart';

import '../steps/select_vibe.dart';

class CreateProfileArtistPage extends StatefulWidget {
  static int role = 0;
  static List<int> artist = [];
  static List<int> vibe = [];
  static TextEditingController stageName = TextEditingController();
  static TextEditingController aboutYour = TextEditingController();
  static int gigLimit = 10;
  static List<LinksArtist> links = [];
  static List<String> audio = [];
  static List<String> video = [];
  static List<String> gallery = [];
  static String? avatar;
  static List<String> instrument = [];
  static List<String> performanceEquipment = [];
  static TextEditingController detailsCnt = TextEditingController();
  static TextEditingController textCntTiktok = TextEditingController(text: "");
  static TextEditingController textCnTwitter = TextEditingController(text: "");
  static TextEditingController textCntYouTub = TextEditingController(text: "");
  static TextEditingController textCntPress = TextEditingController(text: "");
  static TextEditingController textCntDonation1 =
      TextEditingController(text: "");
  static TextEditingController textCntDonation2 =
      TextEditingController(text: "");
  static TextEditingController textCntDonation3 =
      TextEditingController(text: "");

  static TextEditingController textCntAudio1 = TextEditingController(text: "");
  static TextEditingController textCntAudio2 = TextEditingController(text: "");
  static TextEditingController textCntAudio3 = TextEditingController(text: "");

  static TextEditingController textCntVideo1 = TextEditingController(text: "");
  static TextEditingController textCntVideo2 = TextEditingController(text: "");
  static TextEditingController textCntVideo3 = TextEditingController(text: "");

  const CreateProfileArtistPage({Key? key}) : super(key: key);

  @override
  State<CreateProfileArtistPage> createState() =>
      _CreateProfileArtistPageState();
}

class _CreateProfileArtistPageState extends State<CreateProfileArtistPage> {
  PageController? controller;
  final SendInformationArtistViewModel sendInformationArtistViewModel =
      SendInformationArtistViewModel();
  final SendVibeViewModel sendVibeViewModel = SendVibeViewModel();
  final SendArtistViewModel sendArtistViewModel = SendArtistViewModel();
  final UploadAvatarViewModel avatarViewModel = UploadAvatarViewModel();
  final UploadFileViewModel fileViewModel = UploadFileViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = PageController(initialPage: 0);
  }

  bool isLoadingAvatar = false;
  bool isLoadingFile = false;
  bool isLoadingSendData = false;
  bool isLoadingSendVibe = false;
  bool isLoadingSendArtist = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 53.h, left: 20.w, bottom: 33.h, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Builder(
                      builder: (context) => // Ensure Scaffold is in context
                          GestureDetector(
                        onTap: () {
                          controller?.animateToPage(
                              controller!.page!.toInt() - 1,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack),
                      ),
                    ),
                    Image.asset(
                      ImagesApp.logoApp,
                      width: 60.w,
                      height: 60.h,
                    ),
                    SvgPicture.asset(ImagesApp.icShared),
                  ],
                ),
              ),
              Expanded(
                child: StepperPageView(
                  pageController: controller,
                  physics: const NeverScrollableScrollPhysics(),
                  pageSteps: [
                    PageStep(
                      title: Text('Step 1'),
                      content: SelectRole(),
                    ),
                    PageStep(
                      title: Text('Step 2'),
                      content: SelectArtist(),
                    ),
                    PageStep(
                      title: const Text('Step 3'),
                      content: SelectVibe(),
                    ),
                    PageStep(
                      title: Text('Step 4'),
                      content: SetUpYourEPK(),
                    ),
                    PageStep(
                      title: Text('Step 5'),
                      content: SetUpYourMedia(),
                    ),
                    PageStep(
                      title: Text('Step 6'),
                      content: SetupYourPhoto(),
                    ),
                    PageStep(
                      title: Text('Step 7'),
                      content: Step7(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 15,
            sigmaY: 15,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.07),
                border: BorderDirectional(
                  top: BorderSide(
                    width: 1,
                    color: AppColor.grey1.withOpacity(0.4),
                  ),
                )),
            height: 84.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // Navigator.pushReplacement(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => const ProfileVenusPage()));
                  },
                  child: Container(
                    width: 165.w,
                    height: 49.h,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: AppColor.grey1.withOpacity(0.4),
                      ),
                      borderRadius: BorderRadius.circular(50.r),
                    ),
                    child: Center(
                      child: Text(
                        "Skip",
                        style: poppins.copyWith(
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  ),
                ),
                BlocConsumer(
                    bloc: sendInformationArtistViewModel,
                    listener: (context, state) {
                      if (state is SendInformationArtistLoadingState) {
                        setState(() {
                          isLoadingSendData = true;
                        });
                      }
                      if (state is SendInformationArtistSuccessState) {
                        setState(() {
                          isLoadingSendData = false;
                        });
                        if (!isLoadingSendArtist &&
                            !isLoadingFile &&
                            !isLoadingAvatar &&
                            !isLoadingSendData &&
                            !isLoadingSendVibe) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ComplecteAndShareProfilePage()));
                        }
                      }
                      if (state is SendInformationArtistFailState) {
                        setState(() {
                          isLoadingSendData = false;
                        });
                        CustomToast.show("Error sending information");
                      }
                    },
                    builder: (context, stateInformation) {
                      return BlocConsumer(
                          bloc: sendVibeViewModel,
                          listener: (ctx, sts) {
                            // Navigator.pushReplacement(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) =>
                            //         const BottomNavVenusPages(
                            //           index: 4,
                            //         )));
                            if (sts is SendVibeLoadingState) {
                              setState(() {
                                isLoadingSendVibe = true;
                              });
                            }
                            if (sts is SendVibeSuccessState) {
                              setState(() {
                                isLoadingSendVibe = false;
                              });
                              if (!isLoadingSendArtist &&
                                  !isLoadingFile &&
                                  !isLoadingAvatar &&
                                  !isLoadingSendData &&
                                  !isLoadingSendVibe) {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ComplecteAndShareProfilePage()));
                              }
                            }
                            if (sts is SendVibeFailState) {
                              setState(() {
                                isLoadingSendVibe = false;
                              });
                              CustomToast.show("Vibe Upload Failed");
                            }
                          },
                          builder: (context, stateVibe) {
                            return BlocConsumer(
                                bloc: sendArtistViewModel,
                                listener: (context, state) {
                                  if (state is SendArtistLoadingState) {
                                    setState(() {
                                      isLoadingSendArtist = true;
                                    });
                                  }
                                  if (state is SendArtistSuccessState) {
                                    setState(() {
                                      isLoadingSendArtist = false;
                                    });
                                    if (!isLoadingSendArtist &&
                                        !isLoadingFile &&
                                        !isLoadingAvatar &&
                                        !isLoadingSendData &&
                                        !isLoadingSendVibe) {
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ComplecteAndShareProfilePage()));
                                    }
                                  }
                                  if (state is SendArtistFailState) {
                                    setState(() {
                                      isLoadingSendArtist = false;
                                    });
                                    CustomToast.show("Artistry Upload Failed");
                                  }
                                },
                                builder: (context, stateArtist) {
                                  return BlocConsumer(
                                      bloc: avatarViewModel,
                                      listener: (context, stateAvatar) {
                                        if (stateAvatar
                                            is UploadAvatarLoadingState) {
                                          print("<RIDI>");
                                          setState(() {
                                            isLoadingAvatar = true;
                                          });
                                        }
                                        if (stateAvatar
                                            is UploadAvatarSuccessState) {
                                          setState(() {
                                            print("<RIDI1>");
                                            isLoadingAvatar = false;
                                          });
                                          if (!isLoadingSendArtist &&
                                              !isLoadingFile &&
                                              !isLoadingAvatar &&
                                              !isLoadingSendData &&
                                              !isLoadingSendVibe) {
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ComplecteAndShareProfilePage()));
                                          }
                                        }
                                        if (stateAvatar
                                            is UploadAvatarFailState) {
                                          setState(() {
                                            isLoadingAvatar = false;
                                          });
                                          CustomToast.show(
                                              "Avatar Upload Failed");
                                        }
                                      },
                                      builder: (context, stateAvatar) {
                                        return BlocConsumer(
                                          bloc: fileViewModel,
                                          listener: (context, state) {
                                            if (state
                                                is UploadFileLoadingState) {
                                              print("<NARIDI>");
                                              setState(() {
                                                isLoadingFile = true;
                                              });
                                            }
                                            if (state
                                                is UploadFileSuccessState) {
                                              print("<NARIDI1>");
                                              setState(() {
                                                isLoadingFile = false;
                                              });
                                              if (!isLoadingSendArtist &&
                                                  !isLoadingFile &&
                                                  !isLoadingAvatar &&
                                                  !isLoadingSendData &&
                                                  !isLoadingSendVibe) {
                                                Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ComplecteAndShareProfilePage()));
                                              }
                                            }
                                            if (state is UploadFileFailState) {
                                              setState(() {
                                                isLoadingFile = false;
                                              });
                                              CustomToast.show(
                                                  "Gallery Upload Failed");
                                            }
                                          },
                                          builder: (context, stateFile) {
                                            return MyCustomButton(
                                              onTap: () {
                                                print("##");
                                                print(controller!.page);
                                                // HiveServices.saveStepProf(indexPage:controller!.page!.toInt());
                                                print("##");
                                                if (controller!.page == 5) {
                                                  if (CreateProfileArtistPage
                                                      .textCntTiktok
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "tiktok",
                                                            type: "tiktok",
                                                            link: CreateProfileArtistPage
                                                                .textCntTiktok
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCnTwitter
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "twitter",
                                                            type: "twitter",
                                                            link: CreateProfileArtistPage
                                                                .textCnTwitter
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntYouTub
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "youTub",
                                                            type: "youTub",
                                                            link: CreateProfileArtistPage
                                                                .textCntYouTub
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntPress
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "press",
                                                            type: "press",
                                                            link:
                                                                CreateProfileArtistPage
                                                                    .textCntPress
                                                                    .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntDonation1
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "donation",
                                                            type: "donation",
                                                            link: CreateProfileArtistPage
                                                                .textCntDonation1
                                                                .text));
                                                  }

                                                  if (CreateProfileArtistPage
                                                      .textCntDonation2
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Donation",
                                                            type: "Donation",
                                                            link: CreateProfileArtistPage
                                                                .textCntDonation2
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntDonation3
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Donation",
                                                            type: "Donation",
                                                            link: CreateProfileArtistPage
                                                                .textCntDonation3
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntAudio1
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Audio",
                                                            type: "Audio",
                                                            link: CreateProfileArtistPage
                                                                .textCntAudio1
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntAudio2
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Audio",
                                                            type: "Audio",
                                                            link: CreateProfileArtistPage
                                                                .textCntAudio2
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntAudio3
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Audio",
                                                            type: "Audio",
                                                            link: CreateProfileArtistPage
                                                                .textCntAudio3
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntVideo1
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Video",
                                                            type: "Video",
                                                            link: CreateProfileArtistPage
                                                                .textCntVideo1
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntVideo2
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Video",
                                                            type: "Video",
                                                            link: CreateProfileArtistPage
                                                                .textCntVideo2
                                                                .text));
                                                  }
                                                  if (CreateProfileArtistPage
                                                      .textCntVideo3
                                                      .text
                                                      .isNotEmpty) {
                                                    CreateProfileArtistPage
                                                        .links
                                                        .add(LinksArtist(
                                                            title: "Video",
                                                            type: "Video",
                                                            link: CreateProfileArtistPage
                                                                .textCntVideo3
                                                                .text));
                                                  }
                                                }
                                                if (controller!.page == 6) {
                                                  // print(CreateProfileArtistPage.role);
                                                  // print(CreateProfileArtistPage.artist.length);
                                                  // print(CreateProfileArtistPage.vibe.length);
                                                  // print(CreateProfileArtistPage.stageName.text);
                                                  // print(CreateProfileArtistPage.aboutYour.text);
                                                  // print(CreateProfileArtistPage.gigLimit);
                                                  // print("<<<object>>>");
                                                  // print(CreateProfileArtistPage.links.length);
                                                  // print(CreateProfileArtistPage.textCntTiktok.text);
                                                  // print(CreateProfileArtistPage.textCnTwitter.text);
                                                  // print(CreateProfileArtistPage.textCntYouTub.text);
                                                  // print(CreateProfileArtistPage.textCntPress.text);
                                                  // print(
                                                  //     CreateProfileArtistPage.textCntDonation1.text);
                                                  // print(
                                                  //     CreateProfileArtistPage.textCntDonation2.text);
                                                  // print(
                                                  //     CreateProfileArtistPage.textCntDonation3.text);
                                                  // print(CreateProfileArtistPage.textCntAudio1.text);
                                                  // print(CreateProfileArtistPage.textCntAudio2.text);
                                                  // print(CreateProfileArtistPage.textCntAudio3.text);
                                                  // print(CreateProfileArtistPage.textCntVideo1.text);
                                                  // print(CreateProfileArtistPage.textCntVideo2.text);
                                                  // print(CreateProfileArtistPage.textCntVideo3.text);
                                                  // print("<<<object>>>");
                                                  //
                                                  // print(CreateProfileArtistPage.audio.length);
                                                  // print(CreateProfileArtistPage.video.length);
                                                  // print(CreateProfileArtistPage.gallery.length);
                                                  // print(CreateProfileArtistPage.avatar);
                                                  // print(CreateProfileArtistPage.instrument.length);
                                                  // print(CreateProfileArtistPage
                                                  //     .performanceEquipment.length);
                                                  // print(CreateProfileArtistPage.detailsCnt.text);

                                                  sendInformationArtistViewModel
                                                      .send(
                                                    requestProfileArtistModel:
                                                        RequestProfileArtistModel(
                                                      role:
                                                          CreateProfileArtistPage
                                                              .role,
                                                      about:
                                                          CreateProfileArtistPage
                                                              .aboutYour.text,
                                                      links:
                                                          CreateProfileArtistPage
                                                              .links,
                                                      data: DataArtist(
                                                        additionalDetailedInformation:
                                                            CreateProfileArtistPage
                                                                .detailsCnt
                                                                .text,
                                                        stageName:
                                                            CreateProfileArtistPage
                                                                .stageName.text,
                                                        gigRequestLimit:
                                                            CreateProfileArtistPage
                                                                .gigLimit,
                                                        instrument:
                                                            CreateProfileArtistPage
                                                                .instrument,
                                                        performanceEquipment:
                                                            CreateProfileArtistPage
                                                                .performanceEquipment,
                                                      ),
                                                    ),
                                                  );
                                                  sendVibeViewModel.send(
                                                      CreateProfileArtistPage
                                                          .vibe);
                                                  sendArtistViewModel.send(
                                                      CreateProfileArtistPage
                                                          .artist);

                                                  CreateProfileArtistPage
                                                              .avatar !=
                                                          null
                                                      ? avatarViewModel
                                                          .uploadAvatar(
                                                              CreateProfileArtistPage
                                                                  .avatar!)
                                                      : null;
                                                  CreateProfileArtistPage
                                                          .gallery.isNotEmpty
                                                      ? fileViewModel
                                                          .uploadImageGallery(
                                                              CreateProfileArtistPage
                                                                  .gallery)
                                                      : null;
                                                  CreateProfileArtistPage
                                                          .audio.isNotEmpty
                                                      ? fileViewModel
                                                          .uploadImageGallery(
                                                              CreateProfileArtistPage
                                                                  .audio)
                                                      : null;
                                                } else {
                                                  controller!.nextPage(
                                                      duration: const Duration(
                                                          milliseconds: 500),
                                                      curve: Curves.ease);
                                                }
                                              },
                                              title: "Next",
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.normal,
                                              loading: isLoadingSendData ||
                                                  isLoadingSendVibe ||
                                                  isLoadingAvatar ||
                                                  isLoadingFile ||
                                                  isLoadingSendArtist,
                                              width: 165.w,
                                              height: 49.h,
                                            );
                                          },
                                        );
                                      });
                                });
                          });
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
