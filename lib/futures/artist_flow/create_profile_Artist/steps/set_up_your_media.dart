import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';

class SetUpYourMedia extends StatefulWidget {
  @override
  State<SetUpYourMedia> createState() => _SetUpYourMediaState();
}

class _SetUpYourMediaState extends State<SetUpYourMedia> {
  Future<void> uploadFile(int index) async {
    PlatformFile? file;
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      file = result.files.first;
      String filePath = file.path!;
    }
    if (index == 0) {
      CreateProfileArtistPage.textCntAudio1.clear();
      CreateProfileArtistPage.textCntAudio1.text = file!.path!;
      CreateProfileArtistPage.audio.add(file!.path!);
    }
    if (index == 1) {
      CreateProfileArtistPage.textCntAudio2.clear();
      CreateProfileArtistPage.textCntAudio2.text = file!.path!;
      CreateProfileArtistPage.audio.add(file!.path!);
    }
    if (index == 2) {
      CreateProfileArtistPage.textCntAudio3.clear();
      CreateProfileArtistPage.textCntAudio3.text = file!.path!;
      CreateProfileArtistPage.audio.add(file!.path!);
    }
    if (index == 3) {
      CreateProfileArtistPage.textCntVideo1.clear();
      CreateProfileArtistPage.textCntVideo1.text = file!.path!;
      CreateProfileArtistPage.video.add(file!.path!);
    }
    if (index == 4) {
      CreateProfileArtistPage.textCntVideo2.clear();
      CreateProfileArtistPage.textCntVideo2.text = file!.path!;
      CreateProfileArtistPage.video.add(file!.path!);
    }
    if (index == 5) {
      CreateProfileArtistPage.textCntVideo3.clear();
      CreateProfileArtistPage.textCntVideo3.text = file!.path!;
      CreateProfileArtistPage.video.add(file!.path!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 25.h,
        ),
        Expanded(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.only(right: 16.w, left: 16.w, bottom: 20.h),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.09),
                  borderRadius: BorderRadius.circular(16.r)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(left: 16.w, top: 16.h, bottom: 12.h),
                      child: Text(
                        "Let’s setup your media kit.",
                        style: poppins.copyWith(
                            fontSize: 14.sp, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Container(
                      color: AppColor.white.withOpacity(0.07),
                      height: 1,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 8.h, top: 36.h),
                      child: Text(
                        "Audio Links",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greyTxt),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntAudio1,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.audio.length<3){
                                uploadFile(0);
                              }else{
                              CustomToast.show("Limit3");
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.audioUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntAudio2,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.audio.length<3){
                                uploadFile(1);
                              }else{
                                CustomToast.show("Limit3");
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.audioUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 26.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntAudio3,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.audio.length<3){
                                uploadFile(2);
                              }else{
                                CustomToast.show("Limit3");
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.audioUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: AppColor.white.withOpacity(0.07),
                      height: 1,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 8.h, top: 22.h),
                      child: Text(
                        "Video Links",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greyTxt),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntVideo1,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.video.length<3){
                                uploadFile(3);
                              }else{
                                CustomToast.show("Limit3");
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.videoUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntVideo2,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.video.length<3){
                                uploadFile(4);
                                    }else{
                                    CustomToast.show("Limit3");
                                    }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.videoUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 27.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: CustomTextField(
                                controller:
                                    CreateProfileArtistPage.textCntVideo3,
                                hint: "Youtube.com/jfurjry..",
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Text(
                              "OR",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w500),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if(CreateProfileArtistPage.video.length<3){
                                uploadFile(5);
                              }else{
                                CustomToast.show("Limit3");
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.h, horizontal: 2.w),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  SvgPicture.asset(ImagesApp.dashBorder,
                                      fit: BoxFit.cover),
                                  Column(
                                    children: [
                                      Container(
                                          width: 16.w,
                                          height: 16.r,
                                          child: SvgPicture.asset(
                                              ImagesApp.videoUpload,
                                              color: AppColor.darkPurple,
                                              fit: BoxFit.cover)),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Text(
                                        "Upload",
                                        style: poppins.copyWith(
                                            fontSize: 9.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.darkPurple),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ],
    );
  }
}
