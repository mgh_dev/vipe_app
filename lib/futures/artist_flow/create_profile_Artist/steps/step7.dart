import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';

class Step7 extends StatefulWidget {
  @override
  State<Step7> createState() => _Step7State();
}

class _Step7State extends State<Step7> {
  final controllerAddDetailedInformation = TextEditingController();

  final numberStage = TextEditingController();

  int maxLength = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 25.h,
        ),
        Expanded(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => instrumentsBottomSheet(),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.w, vertical: 12.h),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(12.r)),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.09),
                              shape: BoxShape.circle),
                          height: 44,
                          width: 44,
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.guitarAcoustic,
                                  fit: BoxFit.cover)),
                        ),
                        SizedBox(
                          width: 12.w,
                        ),
                        Text(
                          "Instruments",
                          style: poppins.copyWith(
                              fontSize: 14.sp, fontWeight: FontWeight.w700),
                        ),
                        const Spacer(),
                        Text(
                          "Add",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.blue),
                        ),
                        SizedBox(
                          width: 19.5.w,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 16.h,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 12.h),
                  decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(12.r)),
                  child: GestureDetector(
                    onTap: () => performanceEquipmentBottomSheet(),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.09),
                              shape: BoxShape.circle),
                          height: 44,
                          width: 44,
                          child: Center(
                              child: SvgPicture.asset(ImagesApp.icGuitarHead,
                                  fit: BoxFit.cover)),
                        ),
                        SizedBox(
                          width: 12.w,
                        ),
                        Text(
                          "Performance equipment",
                          style: poppins.copyWith(
                              fontSize: 14.sp, fontWeight: FontWeight.w700),
                        ),
                        const Spacer(),
                        Text(
                          "Add",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.blue),
                        ),
                        SizedBox(
                          width: 19.5.w,
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.w, top: 16.h, bottom: 8.h),
                  child: Text(
                    "Additional Detailed Information\n(Optional)",
                    style: poppins.copyWith(
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w400,
                        color: AppColor.greyText),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                  width: double.infinity,
                  height: 85.h,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  child: CustomTextField(
                    hintTextDirection: TextDirection.ltr,
                    controller: CreateProfileArtistPage.detailsCnt,
                    type: TextInputType.multiline,
                    line: 1000,
                    onChanged: (_) {
                      setState(() {
                        maxLength =
                            CreateProfileArtistPage.detailsCnt.text.length;
                      });
                    },
                    hint: "I can bring my equipment to your venue.",
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      right: 16.w, left: 16.w, top: 8.h, bottom: 20.h),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      "${maxLength.toString()}/300",
                      style: poppins.copyWith(
                          fontSize: 11.sp,
                          fontWeight: FontWeight.w400,
                          color: AppColor.whiteEB),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void instrumentsBottomSheet() {
    final selectItem = SelectItemViewModel();
    final List<String> ls = [
      "Rok",
      "Pop",
      "Jazz",
      "RnB",
      "Hip-hop",
      "Blues",
      "country",
      "folksy",
      "Classical orchestra",
      "Indie",
      "Heavy",
      "punk",
      "other",
    ];
    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 50.h,bottom: 20.h),
                              child: const instruments(),
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void performanceEquipmentBottomSheet() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 50.h,bottom: 20),
                        child: const performanceEquipment(),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class performanceEquipment extends StatefulWidget {
  const performanceEquipment({Key? key}) : super(key: key);

  @override
  State<performanceEquipment> createState() => _performanceEquipmentState();
}

class _performanceEquipmentState extends State<performanceEquipment> {
  final List<String> ls = [
    "dsdsa`",
    "sas",
    "Jazz",
    "RnB",
    "Hip-hop",
    "Blues",
    "country",
    "folksy",
    "Classical s",
    "Indie",
    "Heavy",
    "punk",
    "other",
  ];
  final List<String> select = [

  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 16.h,
      ),
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  left: 16.w, bottom: 13.h, top: 16.h),
              child: Text(
                "Select the instruments you play",
                style: poppins.copyWith(
                    fontSize: 14.sp, fontWeight: FontWeight.w600),
              ),
            ),
            const DividerWidget(),
            ListView.builder(
              padding: EdgeInsets.zero,
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              itemCount: ls.length,
              itemBuilder: (context, index) {
                String item = ls[index];
                return Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: select
                          .contains(item)
                          ? 16.w
                          : 28.w,
                      vertical: 5),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (select
                            .contains(item)) {
                          select
                              .remove(item);
                        } else {
                          select
                              .add(item);
                        }
                      });
                      print("object");
                    },
                    child: Container(
                      height: 68.h,
                      // width: 311,
                      decoration: BoxDecoration(
                          color: select
                              .contains(item)
                              ? AppColor.white.withOpacity(0.09)
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(12.r)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: select
                                .contains(item)
                                ? 12.w
                                : 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: select
                                  .contains(item),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    right: select
                                        .contains(item)
                                        ? 12.w
                                        : 0),
                                child: Container(
                                  width: 44.w,
                                  height: 44.r,
                                  decoration: BoxDecoration(
                                      color: AppColor.white.withOpacity(0.09),
                                      borderRadius:
                                      BorderRadius.circular(10.r)),
                                  child: Center(
                                      child: SvgPicture.asset(
                                          ImagesApp.icTiktook)),
                                ),
                              ),
                            ),
                            Text(
                              ls[index],
                              style: poppins.copyWith(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500),
                            ),
                            const Spacer(),
                            Container(
                              height: 16.6.h,
                              width: 16.6.w,
                              decoration: BoxDecoration(
                                color: select
                                    .contains(item)
                                    ? AppColor.green3
                                    : Colors.transparent,
                                border: Border.all(
                                    width: 1,
                                    color: select
                                        .contains(item)
                                        ? Colors.transparent
                                        : AppColor.grey5b),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Center(
                                child: select
                                    .contains(item)
                                    ? Icon(
                                  Icons.check,
                                  size: 16,
                                  color: AppColor.backgroundApp,
                                )
                                    : const SizedBox(),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
            Padding(
              padding:  EdgeInsets.only(top: 20.h),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Center(
                    child: MyCustomButton(
                      title: "Done",
                      loading: false,
                      onTap: () {
                        CreateProfileArtistPage.performanceEquipment=select;
                        Navigator.pop(context);
                      },
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class instruments extends StatefulWidget {
  const instruments({Key? key}) : super(key: key);

  @override
  State<instruments> createState() => _instrumentsState();
}

class _instrumentsState extends State<instruments> {
  final List<String> ls = [
    "dsdsa`",
    "sas",
    "Jazz",
    "RnB",
    "Hip-hop",
    "Blues",
    "country",
    "folksy",
    "Classical s",
    "Indie",
    "Heavy",
    "punk",
    "other",
  ];
  final List<String> select = [

  ];
  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: EdgeInsets.symmetric(
        vertical: 16.h,
      ),
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  left: 16.w, bottom: 13.h, top: 16.h),
              child: Text(
                "Select the instruments you play",
                style: poppins.copyWith(
                    fontSize: 14.sp, fontWeight: FontWeight.w600),
              ),
            ),
            const DividerWidget(),
            ListView.builder(
              padding: EdgeInsets.zero,
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              itemCount: ls.length,
              itemBuilder: (context, index) {
                String item = ls[index];
                return Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: select
                          .contains(item)
                          ? 16.w
                          : 28.w,
                      vertical: 5),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (select
                            .contains(item)) {
                          select
                              .remove(item);
                        } else {
                          select
                              .add(item);
                        }
                      });
                      print("object");
                    },
                    child: Container(
                      height: 68.h,
                      // width: 311,
                      decoration: BoxDecoration(
                          color: select
                              .contains(item)
                              ? AppColor.white.withOpacity(0.09)
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(12.r)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: select
                                .contains(item)
                                ? 12.w
                                : 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: select
                                  .contains(item),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    right: select
                                        .contains(item)
                                        ? 12.w
                                        : 0),
                                child: Container(
                                  width: 44.w,
                                  height: 44.r,
                                  decoration: BoxDecoration(
                                      color: AppColor.white.withOpacity(0.09),
                                      borderRadius:
                                      BorderRadius.circular(10.r)),
                                  child: Center(
                                      child: SvgPicture.asset(
                                          ImagesApp.icTiktook)),
                                ),
                              ),
                            ),
                            Text(
                              ls[index],
                              style: poppins.copyWith(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500),
                            ),
                            const Spacer(),
                            Container(
                              height: 16.6.h,
                              width: 16.6.w,
                              decoration: BoxDecoration(
                                color:select
                                    .contains(item)
                                    ? AppColor.green3
                                    : Colors.transparent,
                                border: Border.all(
                                    width: 1,
                                    color: select
                                        .contains(item)
                                        ? Colors.transparent
                                        : AppColor.grey5b),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Center(
                                child: select
                                    .contains(item)
                                    ? Icon(
                                  Icons.check,
                                  size: 16,
                                  color: AppColor.backgroundApp,
                                )
                                    : const SizedBox(),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
            Padding(
              padding:  EdgeInsets.only(top: 20.h),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Center(
                    child: MyCustomButton(
                      title: "Done",
                      loading: false,
                      onTap: () {
                        CreateProfileArtistPage.instrument=select;
                        Navigator.pop(context);
                      },
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


