import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/view_model/get_vibe_view_model.dart';

import '../../../../constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import '../../../../core/helper/custom_text_fild.dart';
import '../../../../core/helper/divider.dart';
import '../../../../core/utils/style_text_app.dart';

class SelectVibe  extends StatefulWidget {
  SelectVibe({Key? key}) : super(key: key);

  @override
  State<SelectVibe> createState() => _SelectVibeState();
}

class _SelectVibeState extends State<SelectVibe> {
  final controllerVibeSearch = TextEditingController();
  final getVibe=GetVibeViewModel();



  final selectItem = SelectItemViewModel();


@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getVibe.getVibe();
  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: getVibe,
        builder: (context,stateGetVibe){
      if(stateGetVibe is GetVibeLoadingState){
        return CustomLoading();
      }
      if(stateGetVibe is GetVibeSuccessState){
        return Container(
          margin: EdgeInsets.only(top: 15.h,bottom: 17.h),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.07),
            borderRadius: BorderRadius.circular(16.r),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 23.h, bottom: 12.h),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 2.h),
                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                  width: double.infinity.w,
                  decoration: BoxDecoration(
                    color: AppColor.greyText.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  child: CustomTextField(
                    hintTextDirection: TextDirection.ltr,
                    controller: controllerVibeSearch,
                    icon: ImagesApp.icAddLocation,
                    hintFontSize: 14.sp,
                    hint: "What’s your vibe?",
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 16.h,),
                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  child:   BlocBuilder(
                      bloc: selectItem,
                      builder: (context, state) {
                        if (state is ItemChangeState) {
                          return ListView.builder(
                            padding: EdgeInsets.zero,
                            physics: const BouncingScrollPhysics(),
                            shrinkWrap: true,
                            itemCount:stateGetVibe.response.data.length ,
                            itemBuilder: (context, index) {
                              int item = stateGetVibe.response.data[index].id;
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: CreateProfileArtistPage.vibe.contains(item)? 16.w : 28.w,vertical: 5),
                                child: GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      if (CreateProfileArtistPage.vibe.contains(item)) {
                                        CreateProfileArtistPage.vibe.remove(item);
                                      } else {
                                        CreateProfileArtistPage.vibe.add(item);
                                      }
                                    });
                                  },
                                  child: Container(
                                    height: 68.h,
                                    // width: 311,
                                    decoration: BoxDecoration(
                                        color:CreateProfileArtistPage.vibe.contains(item)
                                            ? AppColor.white.withOpacity(0.09)
                                            : Colors.transparent,
                                        borderRadius:
                                        BorderRadius.circular(12.r)),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal:
                                          CreateProfileArtistPage.vibe.contains(item)? 12.w : 0),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        children: [
                                          Visibility(
                                            visible:CreateProfileArtistPage.vibe.contains(item),
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  right:CreateProfileArtistPage.vibe.contains(item)
                                                      ? 12.w
                                                      : 0),
                                              child: Container(
                                                width: 44.w,
                                                height: 44.r,
                                                decoration: BoxDecoration(
                                                    color: AppColor.white
                                                        .withOpacity(0.09),
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        10.r)),
                                                child: Center(
                                                    child: SvgPicture.asset(
                                                        ImagesApp.icTiktook)),
                                              ),
                                            ),
                                          ),
                                          Text(
                                            stateGetVibe.response.data[index].name,
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          const Spacer(),
                                          Container(
                                            height: 16.6.h,
                                            width: 16.6.w,
                                            decoration: BoxDecoration(
                                              color: CreateProfileArtistPage.vibe.contains(item)
                                                  ? AppColor.green3
                                                  : Colors.transparent,
                                              border: Border.all(
                                                  width: 1,
                                                  color:CreateProfileArtistPage.vibe.contains(item)
                                                      ? Colors.transparent
                                                      : AppColor.grey5b),
                                              borderRadius:
                                              BorderRadius.circular(50),
                                            ),
                                            child: Center(
                                              child: CreateProfileArtistPage.vibe.contains(item)
                                                  ? Icon(
                                                Icons.check,
                                                size: 16,
                                                color: AppColor
                                                    .backgroundApp,
                                              )
                                                  : const SizedBox(),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                        return const SizedBox();
                      }),
                ),
              ),
              SizedBox(
                height: 24.h,
              ),
            ],
          ),
        );
      }
      return SizedBox();
    });
  }
}