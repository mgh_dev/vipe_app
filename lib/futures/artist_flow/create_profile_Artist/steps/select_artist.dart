import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/state/get_artisy_state.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/view_model/get_artisy_view_model.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';

import '../../../../core/utils/image_app.dart';

class SelectArtist extends StatefulWidget {
  SelectArtist({Key? key}) : super(key: key);

  @override
  State<SelectArtist> createState() => _SelectArtistState();
}

class _SelectArtistState extends State<SelectArtist> {
  final selectItem = SelectItemViewModel();
  final getArtis = GetArtistryViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getArtis.getArtist();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: getArtis,
        builder: (context, stateGetArtist) {
          if (stateGetArtist is GetArtistryLoadingState) {
            return const CustomLoading();
          }
          if (stateGetArtist is GetArtistrySuccessState) {
            return Column(
              children: [
                SizedBox(
                  height: 25.h,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 20.h),
                      padding: EdgeInsets.symmetric(vertical: 16.h),
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.09),
                          borderRadius: BorderRadius.circular(16.r)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 16.w, bottom: 24.h),
                            child: Text(
                              "What’s your artistry?",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                            height: 1,
                            width: double.infinity,
                            color: AppColor.white.withOpacity(0.07),
                          ),
                          SizedBox(height: 12.h),
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 16.h,
                            ),
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(10.r),
                            ),
                            child: BlocBuilder(
                                bloc: selectItem,
                                builder: (context, state) {
                                  if (state is ItemChangeState) {
                                    return ListView.builder(
                                      padding: EdgeInsets.zero,
                                      physics: const BouncingScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount:
                                          stateGetArtist.response.data.length,
                                      itemBuilder: (context, index) {
                                        int item = stateGetArtist
                                            .response.data[index].id;
                                        return Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  CreateProfileArtistPage.artist
                                                          .contains(item)
                                                      ? 16.w
                                                      : 28.w,
                                              vertical: 5),
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                if (CreateProfileArtistPage
                                                    .artist
                                                    .contains(item)) {
                                                  CreateProfileArtistPage.artist
                                                      .remove(item);
                                                } else {
                                                  CreateProfileArtistPage.artist
                                                      .add(item);
                                                }
                                              });
                                            },
                                            child: Container(
                                              height: 68.h,
                                              // width: 311,
                                              decoration: BoxDecoration(
                                                  color: CreateProfileArtistPage
                                                          .artist
                                                          .contains(item)
                                                      ? AppColor.white
                                                          .withOpacity(0.09)
                                                      : Colors.transparent,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12.r)),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        CreateProfileArtistPage
                                                                .artist
                                                                .contains(item)
                                                            ? 12.w
                                                            : 0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Visibility(
                                                      visible:
                                                          CreateProfileArtistPage
                                                              .artist
                                                              .contains(item),
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            right: CreateProfileArtistPage
                                                                    .artist
                                                                    .contains(
                                                                        item)
                                                                ? 12.w
                                                                : 0),
                                                        child: Container(
                                                          width: 44.w,
                                                          height: 44.r,
                                                          decoration: BoxDecoration(
                                                              color: AppColor
                                                                  .white
                                                                  .withOpacity(
                                                                      0.09),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10.r)),
                                                          child: Center(
                                                              child: SvgPicture
                                                                  .asset(ImagesApp
                                                                      .icTiktook)),
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      stateGetArtist.response
                                                          .data[index].name,
                                                      style: poppins.copyWith(
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      height: 16.6.h,
                                                      width: 16.6.w,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            CreateProfileArtistPage
                                                                    .artist
                                                                    .contains(
                                                                        item)
                                                                ? AppColor
                                                                    .green3
                                                                : Colors
                                                                    .transparent,
                                                        border: Border.all(
                                                            width: 1,
                                                            color: CreateProfileArtistPage
                                                                    .artist
                                                                    .contains(
                                                                        item)
                                                                ? Colors
                                                                    .transparent
                                                                : AppColor
                                                                    .grey5b),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                      ),
                                                      child: Center(
                                                        child: CreateProfileArtistPage
                                                                .artist
                                                                .contains(item)
                                                            ? Icon(
                                                                Icons.check,
                                                                size: 16,
                                                                color: AppColor
                                                                    .backgroundApp,
                                                              )
                                                            : const SizedBox(),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }
                                  return const SizedBox();
                                }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
          return const SizedBox();
        });
  }
}
