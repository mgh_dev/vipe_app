import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/check_box_state.dart';

class SelectRole extends StatefulWidget {
  SelectRole({Key? key}) : super(key: key);

  @override
  State<SelectRole> createState() => _SelectRoleState();
}

class _SelectRoleState extends State<SelectRole> {
  final selectItem = SelectItemViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
@override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder(
            bloc: selectItem,
            builder: (context, state) {
              if (state is ItemChangeState) {
                return Padding(
                  ///todo
                  padding: EdgeInsets.only(bottom: 20.h, top: 25.h),
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.09),
                        borderRadius: BorderRadius.circular(16.r)),
                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                    padding: EdgeInsets.symmetric(vertical: 16.h),
                    width: double.infinity.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Text(
                            "Select your role",
                            style: poppins.copyWith(
                                fontSize: 14.sp, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                          height: 1.h,
                          width: double.infinity,
                          color: AppColor.white.withOpacity(0.07),
                          margin: EdgeInsets.symmetric(vertical: 12.h),
                        ),

                        Padding(
                          padding: EdgeInsets.symmetric(horizontal:state.index==0 ?16.w:28.w),
                          child: GestureDetector(
                            onTap: () {
                              selectItem.changeItem(0);
                              CreateProfileArtistPage.role=0;
                            },
                            child: Container(
                              height: 68.h,
                              // width: 311,
                              decoration: BoxDecoration(
                                  color: state.index == 0
                                      ? AppColor.white.withOpacity(0.09)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(12.r)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: state.index == 0 ? 12.w : 0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Visibility(
                                      visible: state.index == 0,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            right: state.index == 0 ? 12.w : 0),
                                        child: Container(
                                          width: 44.w,
                                          height: 44.r,
                                          decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.09),
                                              borderRadius:
                                                  BorderRadius.circular(10.r)),
                                          child: Center(
                                              child: SvgPicture.asset(
                                                  ImagesApp.icTiktook)),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "Solo talent",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    const Spacer(),
                                    Container(
                                      height: 16.6.h,
                                      width: 16.6.w,
                                      decoration: BoxDecoration(
                                        color: state.index == 0
                                            ? AppColor.green3
                                            : Colors.transparent,
                                        border: Border.all(
                                            width: 1,
                                            color: state.index == 0
                                                ?  Colors.transparent
                                                :AppColor.grey5b),
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(
                                        child: state.index == 0
                                            ? Icon(
                                                Icons.check,
                                                size: 16,
                                                color: AppColor.backgroundApp,
                                              )
                                            :  const SizedBox(),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal:state.index==1 ?16.w:28.w),
                          child: GestureDetector(
                            onTap: () {
                              selectItem.changeItem(1);
                              CreateProfileArtistPage.role=1;
                            },
                            child: Container(
                              height: 68.h,
                              // width: 311,
                              decoration: BoxDecoration(
                                  color: state.index == 1
                                      ? AppColor.white.withOpacity(0.09)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(12.r)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: state.index == 1 ? 12.w : 0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Visibility(
                                      visible: state.index == 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            right: state.index == 1 ? 12.w : 0),
                                        child: Container(
                                          width: 44.w,
                                          height: 44.r,
                                          decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.09),
                                              borderRadius:
                                              BorderRadius.circular(10.r)),
                                          child: Center(
                                              child: SvgPicture.asset(
                                                  ImagesApp.icTiktook)),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "Solo talent",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    const Spacer(),
                                    Container(
                                      height: 16.6.h,
                                      width: 16.6.w,
                                      decoration: BoxDecoration(
                                        color: state.index == 1
                                            ? AppColor.green3
                                            : Colors.transparent,
                                        border: Border.all(
                                            width: 1,
                                            color: state.index == 1
                                                ?  Colors.transparent
                                                :AppColor.grey5b),
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(
                                        child: state.index == 1
                                            ? Icon(
                                          Icons.check,
                                          size: 16,
                                          color: AppColor.backgroundApp,
                                        )
                                            :  const SizedBox(),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )

                        // Expanded(
                        //   child: ListView.builder(
                        //       itemCount: 10,
                        //       shrinkWrap: true,
                        //       itemBuilder: (context, index) {
                        //         return Padding(
                        //           padding: EdgeInsets.symmetric(
                        //               vertical: 25.h, horizontal: 12.w),
                        //           child: Row(
                        //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //             children: [
                        //               Text(
                        //                 "Solo talent",
                        //                 style: poppins.copyWith(
                        //                     fontSize: 14.sp, fontWeight: FontWeight.w500),
                        //               ),
                        //               Container(
                        //                 height: 16.6.h,
                        //                 width: 16.6.w,
                        //                 decoration: BoxDecoration(
                        //                     border: Border.all(
                        //                         width: 1, color: AppColor.grey5b),
                        //                     borderRadius: BorderRadius.circular(50)),
                        //               )
                        //             ],
                        //           ),
                        //         );
                        //       }),
                        // )
                      ],
                    ),
                  ),
                );
              }
              return const SizedBox();
            })
      ],
    );
  }
}
