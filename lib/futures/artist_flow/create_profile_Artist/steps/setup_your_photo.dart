import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibe_app/core/helper/custom_tost.dart';

import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';

import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';

class SetupYourPhoto extends StatefulWidget {
  @override
  State<SetupYourPhoto> createState() => _SetupYourPhotoState();
}

class _SetupYourPhotoState extends State<SetupYourPhoto> {
  File? image;
  final List<String> _imagePath = [];
  final List<File> _image = [];

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
        print("<<<<IMAGE>>>>");
        CreateProfileArtistPage.avatar = this.image!.path;
      });
    } on PlatformException catch (e) {
      print("Failed to pick Image $e");
    }
  }

  Future getImage(int state) async {
    if (state == 0) {
      final newImage = await ImagePicker()
          .getImage(source: ImageSource.camera, imageQuality: 80);
      if (newImage != null) {
        setState(() {
          _image.add(File(newImage.path));
          _imagePath.add(newImage.path);
          CreateProfileArtistPage.gallery = _imagePath;
        });
      }
    } else {
      final newImage = await ImagePicker()
          .getImage(source: ImageSource.gallery, imageQuality: 80);
      if (newImage != null) {
        setState(() {
          _image.add(File(newImage.path));
          _imagePath.add(newImage.path);
          CreateProfileArtistPage.gallery = _imagePath;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 25.h,
        ),
        Expanded(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.only(right: 16.w, left: 16.w, bottom: 20.h),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColor.white.withOpacity(0.09),
                  borderRadius: BorderRadius.circular(16.r)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(left: 16.w, top: 16.h, bottom: 12.h),
                      child: Text(
                        "Let’s setup your photo gallery",
                        style: poppins.copyWith(
                            fontSize: 14.sp, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Container(
                      color: AppColor.white.withOpacity(0.07),
                      height: 1,
                      width: double.infinity,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 8.h, top: 12.h),
                      child: Text(
                        "Upload profile picture",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greyTxt),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 24.h),
                      padding: EdgeInsets.symmetric(vertical: 39.h),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(15.r)),
                      child: Column(children: [
                        Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            Container(
                              width: 112.w,
                              height: 112.h,
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: image != null
                                    ? Container(
                                        width: 112.w,
                                        height: 112.h,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: FileImage(image!),
                                              fit: BoxFit.fill),
                                          shape: BoxShape.circle,
                                        ),
                                      )
                                    : SvgPicture.asset(
                                        ImagesApp.icUser,
                                        width: 22.w,
                                        height: 28.h,
                                      ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                pickImage(ImageSource.gallery);
                                print("object");
                              },
                              child: Container(
                                width: 42.w,
                                height: 42.r,
                                decoration: BoxDecoration(
                                  color: AppColor.black54,
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: Container(
                                    width: 35.w,
                                    height: 35.r,
                                    decoration: BoxDecoration(
                                      color: AppColor.darkPurple,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                        child: SvgPicture.asset(
                                            ImagesApp.icCamera)),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 17.h,
                        ),
                        Text(
                          "Upload Image ",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w600,
                              color: AppColor.darkPurple),
                        )
                      ]),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, left: 16.w, bottom: 8.h),
                      child: Text(
                        "Upload your best photos (5 max.)",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greyTxt),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: 16.w, left: 16.w, bottom: 24.h),
                      padding: EdgeInsets.symmetric(vertical: 39.h),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(15.r)),
                      child: Column(children: [
                        Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            Container(
                              width: 112.w,
                              height: 112.h,
                              decoration: BoxDecoration(
                                color: AppColor.darkBlue,
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: _image.isNotEmpty
                                    ? Container(
                                        width: 112.w,
                                        height: 112.h,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: FileImage(_image.last),
                                              fit: BoxFit.fill),
                                          shape: BoxShape.circle,
                                        ),
                                      )
                                    : SvgPicture.asset(
                                        ImagesApp.icUser,
                                        width: 22.w,
                                        height: 28.h,
                                      ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                if (_image.length == 5) {
                                  CustomToast.show("Limit5");
                                } else {
                                  _modalBottomSheetMenu();
                                }
                              },
                              child: Container(
                                width: 42.w,
                                height: 42.r,
                                decoration: BoxDecoration(
                                  color: AppColor.black54,
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: Container(
                                    width: 35.w,
                                    height: 35.r,
                                    decoration: BoxDecoration(
                                      color: AppColor.darkPurple,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Center(
                                        child: SvgPicture.asset(
                                            ImagesApp.icCamera)),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 17.h,
                        ),
                        Text(
                          "Upload Image ",
                          style: poppins.copyWith(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w600,
                              color: AppColor.darkPurple),
                        )
                      ]),
                    ),
                    _image.isEmpty
                        ? Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.w),
                            child: SizedBox(
                              height: 69.h,
                              child: ListView.builder(
                                  itemCount: 5,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: EdgeInsets.only(right: 7.w),
                                      child: Container(
                                        width: 69.w,
                                        height: 69.h,
                                        decoration: BoxDecoration(
                                            color: AppColor.white
                                                .withOpacity(0.07),
                                            borderRadius:
                                                BorderRadius.circular(10.r)),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SizedBox(
                                                height: 15.r,
                                                width: 15.w,
                                                child: SvgPicture.asset(
                                                    ImagesApp.audioSquare,
                                                    fit: BoxFit.cover)),
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Text(
                                              "Image",
                                              style: poppins.copyWith(
                                                  fontSize: 13.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.w),
                            child: SizedBox(
                              height: 69.h,
                              child: ListView.builder(
                                  itemCount: _image.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: EdgeInsets.only(right: 7.w),
                                      child: Container(
                                        width: 69.w,
                                        height: 69.h,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: FileImage(
                                                _image[index],
                                              ),
                                              fit: BoxFit.fill
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(10.r)),
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: GestureDetector(
                                            onTap: (){
                                              setState(() {
                                                _image.removeAt(index);
                                              });
                                            },
                                            child: Container(
                                                width: 30,
                                                height: 30,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.circular(10.r),
                                                ),

                                                child: const Icon(
                                                  Icons.close,
                                                  color: Colors.black,
                                                )),
                                          ),
                                        ),
                                      ),
                                    );
                                  }),
                            ),
                          ),
                    SizedBox(
                      height: 16.h,
                    ),
                  ]),
            ),
          ),
        ),
      ],
    );
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (builder) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.15,
            decoration: BoxDecoration(
                color: AppColor.darkBlue,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0))),
            child: Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(right: 15, left: 10),
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      minWidth: MediaQuery.of(context).size.width * 0.45,
                      height: 45,
                      color: AppColor.blue,
                      child: const Text(
                        "camera",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        getImage(0);
                      },
                    )),
                MaterialButton(
                    minWidth: MediaQuery.of(context).size.width * 0.45,
                    height: 45,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: AppColor.blue,
                    child: const Text(
                      "gallery",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      getImage(1);
                    }),
              ],
            ),
          );
        });
  }
}
