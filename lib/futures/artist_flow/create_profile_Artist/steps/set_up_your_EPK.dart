import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import 'package:vibe_app/futures/constant_pages/sgin_in_page/logic/state/check_box_state.dart';
import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';
import 'package:file_picker/file_picker.dart';

class SetUpYourEPK extends StatefulWidget {

  @override
  State<SetUpYourEPK> createState() => _SetUpYourEPKState();
}

class _SetUpYourEPKState extends State<SetUpYourEPK> {
  final selectItemRadioButton = SelectItemViewModel();

  final selectItemCheckBox = SelectItemViewModel();










  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 25.h,
        ),
        Expanded(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Padding(
              padding: EdgeInsets.only(right: 16.w, left: 16.w, bottom: 20.h),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.09),
                        borderRadius: BorderRadius.circular(16.r)),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: 16.w, top: 16.h, bottom: 12.h),
                            child: Text(
                              "Let’s set up your EPK",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                            color: AppColor.white.withOpacity(0.07),
                            height: 1,
                            width: double.infinity,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 16.w, top: 12.h, bottom: 8.h),
                            child: Text(
                              "Stage name",
                              style: poppins.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColor.greyText),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            height: 48.h,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10.r)),
                            child: CustomTextField(
                              controller: CreateProfileArtistPage.stageName,
                              line: 2,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                right: 16.w,
                                left: 16.w,
                                top: 16.h,
                                bottom: 8.h),
                            child: Text(
                              "Tell us about yourself and what makes you unique",
                              style: poppins.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColor.greyText),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                right: 16.w, left: 16.w, bottom: 24.h),
                            height: 138.h,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10.r)),
                            child: CustomTextField(
                              controller: CreateProfileArtistPage.aboutYour,
                              line: 100,
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(
                    height: 13.h,
                  ),
                  Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.09),
                          borderRadius: BorderRadius.circular(16.r)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                right: 16.w,
                                left: 16.w,
                                top: 16.h,
                                bottom: 12.h),
                            child: Text(
                              "How many fans can you bring to your gigs",
                              style: poppins.copyWith(
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColor.greyText),
                            ),
                          ),
                          BlocBuilder(
                            bloc: selectItemRadioButton,
                            builder: (context, state) {
                              if (state is ItemChangeState) {
                                return Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 14.5),
                                      child: GestureDetector(
                                        onTap: () {
                                          selectItemRadioButton.changeItem(0);
                                          CreateProfileArtistPage.gigLimit=10;
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 22.h,
                                              width: 22.w,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: 1,
                                                  color: state.index == 0
                                                      ? AppColor.blue
                                                      : AppColor.grey5b,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                              ),
                                              child: Center(
                                                child: Container(
                                                  height: 14.h,
                                                  width: 14.w,
                                                  decoration: BoxDecoration(
                                                      color: state.index == 0
                                                          ? AppColor.blue
                                                          : Colors.transparent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50)),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 7.w,
                                            ),
                                            Text(
                                              "1 - 10",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 14.5),
                                      child: GestureDetector(
                                        onTap: ()
                                        {
                                          selectItemRadioButton.changeItem(1);
                                          CreateProfileArtistPage.gigLimit=50;
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 22.h,
                                              width: 22.w,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: state.index == 1
                                                        ? AppColor.blue
                                                        : AppColor.grey5b),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                              ),
                                              child: Center(
                                                child: Container(
                                                  height: 14.h,
                                                  width: 14.w,
                                                  decoration: BoxDecoration(
                                                      color: state.index == 1
                                                          ? AppColor.blue
                                                          : Colors.transparent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50)),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 7.w,
                                            ),
                                            Text(
                                              "10 - 50",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 16.w, bottom: 18.5),
                                      child: GestureDetector(
                                        onTap: () {
                                          selectItemRadioButton.changeItem(2);
                                          CreateProfileArtistPage.gigLimit=100;
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 22.h,
                                              width: 22.w,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: state.index == 2
                                                        ? AppColor.blue
                                                        : AppColor.grey5b),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                              ),
                                              child: Center(
                                                child: Container(
                                                  height: 14.h,
                                                  width: 14.w,
                                                  decoration: BoxDecoration(
                                                      color: state.index == 2
                                                          ? AppColor.blue
                                                          : Colors.transparent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50)),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 7.w,
                                            ),
                                            Text(
                                              "50+",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              }
                              return const SizedBox();
                            },
                          ),
                        ],
                      )),
                  SizedBox(
                    height: 12.h,
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.09),
                        borderRadius: BorderRadius.circular(16.r)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              right: 16.w, left: 16.w, top: 16.h, bottom: 11.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Additional Details",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              BlocBuilder(
                                bloc: selectItemCheckBox,
                                builder: (context, sts) {
                                  if(sts is ItemChangeState){
                                    return  GestureDetector(
                                      onTap: (){
                                        if(sts.index==0){
                                          selectItemCheckBox.changeItem(1);
                                        }
                                        else{
                                          selectItemCheckBox.changeItem(0);
                                        }
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                width: 1,
                                                color: sts.index == 0
                                                    ? Colors.transparent
                                                    : AppColor.grey5b),
                                            color:sts.index==0?AppColor.greyBlue:Colors.transparent,
                                            borderRadius: BorderRadius.circular(4.r)),
                                        width: 24.w,
                                        height: 24.h,
                                        child:sts.index==0? Icon(
                                          Icons.check,
                                          color: AppColor.white,
                                          size: 19,
                                        ):const SizedBox(),
                                      ),
                                    );
                                  }
                                  return const SizedBox();
                                },
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: AppColor.white.withOpacity(0.07),
                          height: 1,
                          width: double.infinity,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              right: 16.w, left: 16.w, top: 13.h, bottom: 8.h),
                          child: Text(
                            "Press Links",
                            style: poppins.copyWith(
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.greyText),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(
                              right: 16.w, left: 16.w, bottom: 16.h),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child: CustomTextField(
                            controller: CreateProfileArtistPage.textCntPress,
                            prefixIcon: ImagesApp.icLink,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Container(
                          color: AppColor.white.withOpacity(0.07),
                          height: 1,
                          width: double.infinity,
                        ),
                        Container(

                          margin: EdgeInsets.only(
                              right: 16.w, left: 16.w, bottom: 12.h, top: 8.h),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child:CustomTextField(
                            controller: CreateProfileArtistPage.textCntTiktok,
                            prefixIcon: ImagesApp.tiktok,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(
                              right: 16.w, left: 16.w, bottom: 12.h),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child:CustomTextField(
                            controller: CreateProfileArtistPage.textCnTwitter,
                            prefixIcon: ImagesApp.icTiwett,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(
                              right: 16.w, left: 16.w, bottom: 24.h),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child: CustomTextField(
                            controller: CreateProfileArtistPage.textCntYouTub,
                            prefixIcon: ImagesApp.icWhatsapp,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16.w, bottom: 1.h),
                          child: Text(
                            "Donation Links",
                            style: poppins.copyWith(
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.greyText),
                          ),
                        ),
                        Container(
                          color: AppColor.white.withOpacity(0.07),
                          height: 1,
                          width: double.infinity,
                        ),
                        Container(

                          margin: EdgeInsets.only(
                              right: 16.w, left: 16.w, bottom: 12.h, top: 8.h),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child:CustomTextField(
                            controller: CreateProfileArtistPage.textCntDonation1,
                            prefixIcon: ImagesApp.icWeb,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(
                            right: 16.w,
                            left: 16.w,
                            bottom: 12.h,
                          ),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child: CustomTextField(
                            controller: CreateProfileArtistPage.textCntDonation2,
                            prefixIcon: ImagesApp.icWeb,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(
                            right: 16.w,
                            left: 16.w,
                            bottom: 16.h,
                          ),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r)),
                          child: CustomTextField(
                            controller: CreateProfileArtistPage.textCntDonation3,
                            prefixIcon: ImagesApp.icWeb,
                            hint: "Youtube.com/jfurjry4h/ltot",
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
