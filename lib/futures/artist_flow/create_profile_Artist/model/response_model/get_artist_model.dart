class GetResponseArtistryModel {
  GetResponseArtistryModel({
    required this.message,
    required this.data,
  });
  late final String message;
  late final List<Data> data;

  GetResponseArtistryModel.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = List.from(json['data']).map((e)=>Data.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class Data {
  Data({
    required this.name,
    required this.id,
    required this.description,
    required this.image,
    required this.icon,
  });
  late final String name;
  late final int id;
  late final String description;
  late final String image;
   String? icon;

  Data.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    description = json['description'];
    image = json['image'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['description'] = description;
    _data['image'] = image;
    _data['icon'] = icon;
    return _data;
  }
}