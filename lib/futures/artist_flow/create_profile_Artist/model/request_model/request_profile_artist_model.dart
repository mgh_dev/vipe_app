class RequestProfileArtistModel {
  RequestProfileArtistModel({
    required this.role,
    required this.about,
    required this.links,
    required this.data,
  });
  late final int role;
  late final String about;
  late final List<LinksArtist> links;
  late final DataArtist data;

  RequestProfileArtistModel.fromJson(Map<String, dynamic> json){
    role = json['role'];
    about = json['about'];
    links = List.from(json['links']).map((e)=>LinksArtist.fromJson(e)).toList();
    data = DataArtist.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['role'] = role;
    _data['about'] = about;
    _data['links'] = links.map((e)=>e.toJson()).toList();
    _data['data'] = data.toJson();
    return _data;
  }
}

class LinksArtist {
  LinksArtist({
    required this.title,
    required this.type,
    required this.link,
  });
  late final String title;
  late final String type;
  late final String link;

  LinksArtist.fromJson(Map<String, dynamic> json){
    title = json['title'];
    type = json['type'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['type'] = type;
    _data['link'] = link;
    return _data;
  }
}

class DataArtist {
  DataArtist({

    required this.additionalDetailedInformation,
    required this.stageName,
    required this.gigRequestLimit,
    required this.instrument,
    required this.performanceEquipment,
  });

  late final String additionalDetailedInformation;
  late final String stageName;
  late final int gigRequestLimit;
  late final List<String> instrument;
  late final List<String> performanceEquipment;

  DataArtist.fromJson(Map<String, dynamic> json){

    additionalDetailedInformation = json['additionalDetailedInformation'];
    stageName = json['stageName'];
    gigRequestLimit = json['gigRequestLimit'];
    instrument = List.castFrom<dynamic, String>(json['instrument']);
    performanceEquipment = List.castFrom<dynamic, String>(json['performanceEquipment']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};

    _data['additionalDetailedInformation'] = additionalDetailedInformation;
    _data['stageName'] = stageName;
    _data['gigRequestLimit'] = gigRequestLimit;
    _data['instrument'] = instrument;
    _data['performanceEquipment'] = performanceEquipment;
    return _data;
  }
}