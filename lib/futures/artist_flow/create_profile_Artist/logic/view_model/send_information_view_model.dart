import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/state/send_information_state.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/model/request_model/request_profile_artist_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';





import '../../../../../core/config/web_service.dart';


class SendInformationArtistViewModel extends Cubit<SendInformationArtistBaseState> {
  SendInformationArtistViewModel() : super(SendInformationArtistInitialState());

  Future send({required RequestProfileArtistModel requestProfileArtistModel}) async {
    print("1");
    emit(SendInformationArtistLoadingState());
    print("3");
    var requestData=requestProfileArtistModel.toJson();
    try {
      print("4");
      var response =
          await WebService().dio.patch("users/extra-data",data: requestData);
      print("5");

      print("6");
     
      print("7");
      emit(SendInformationArtistSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(SendInformationArtistFailState(message: e.toString()));
      print("10");
    }
  }

}
