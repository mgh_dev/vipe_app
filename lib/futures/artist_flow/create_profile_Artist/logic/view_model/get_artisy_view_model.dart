import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/state/get_artisy_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../../../core/config/web_service.dart';
import '../../model/response_model/get_artist_model.dart';


class GetArtistryViewModel extends Cubit<GetArtistryBaseState> {
  GetArtistryViewModel() : super(GetArtistryInitialState());

  void getArtist() async {
    print("1");
    emit(GetArtistryLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.get("artistry");
      print("5");
      GetResponseArtistryModel getResponseArtistryModel =
      GetResponseArtistryModel.fromJson(response.data);
      print("6");
     
      print("7");
      emit(GetArtistrySuccessState(response: getResponseArtistryModel));
      print("8");
    } catch (e) {
      print("9");
      emit(GetArtistryFailState(message: e.toString()));
      print("10");
    }
  }

}
