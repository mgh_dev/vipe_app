import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class SendInformationArtistBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class SendInformationArtistInitialState extends SendInformationArtistBaseState {}

class SendInformationArtistLoadingState extends SendInformationArtistBaseState {}

class SendInformationArtistSuccessState extends SendInformationArtistBaseState {

}

class SendInformationArtistFailState extends SendInformationArtistBaseState {
  final String message;


  SendInformationArtistFailState({required this.message});
}
