import 'package:equatable/equatable.dart';
import '../../model/response_model/get_artist_model.dart';

abstract class GetArtistryBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetArtistryInitialState extends GetArtistryBaseState {}

class GetArtistryLoadingState extends GetArtistryBaseState {}

class GetArtistrySuccessState extends GetArtistryBaseState {
  final GetResponseArtistryModel response;

  GetArtistrySuccessState({required this.response});
}

class GetArtistryFailState extends GetArtistryBaseState {
  final String message;


  GetArtistryFailState({required this.message});
}
