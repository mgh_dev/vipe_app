import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/ui/create_profile_artist_page.dart';
import 'package:vibe_app/futures/artist_flow/home_page_artist/ui/tabs/events_tab_artist.dart';
import 'package:vibe_app/futures/artist_flow/profile_artist_page_message_setup/ui/profile_artist_page_message_setup.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_tabs/tabs_state_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_tabs/tabs_view_model_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/ui/tabs/artist_tab_venus.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/ui/tabs/venues_tab_venus.dart';
import 'package:vibe_app/futures/venue_flow/profile_page_message_setup/ui/profile_page_message_setup.dart';

import '../../../../core/helper/container_blur.dart';
import '../../../../core/helper/custom_button.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';
import '../../../../core/utils/style_text_app.dart';
import '../../../../menu.dart';

class HomeArtistPage extends StatefulWidget {
  HomeArtistPage({Key? key}) : super(key: key);

  @override
  State<HomeArtistPage> createState() => _HomeArtistPageState();
}

class _HomeArtistPageState extends State<HomeArtistPage> {
  final _tabsBlocVenus = TabsViewModelVenus();
  final List<Widget> lsTabs = [
     EventsArtistTab(),
    ArtistTabVenus(),
    HomeVenuesTab(),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(HiveServices.getToken);
  }

  @override
  Widget build(BuildContext context) {
    print("object");
    return  Column(
      children: [

        Padding(
          padding: EdgeInsets.only(bottom: 12.h),
          child: Container(
            height: 50.h,
            decoration: BoxDecoration(
              color: AppColor.white.withOpacity(0.1),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: Column(
                children: [
                  SizedBox(height: 6.h),
                  SizedBox(
                    height: 3.5,
                    child: ListView.builder(
                      itemCount: 7,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: EdgeInsets.only(right: 1.w),
                          child: Container(
                            width: 47.w,
                            height: 2.h,
                            decoration: BoxDecoration(
                              color: index <= 1
                                  ? AppColor.blue
                                  : AppColor.black24,
                              borderRadius: BorderRadius.circular(50.r),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 6.h),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.warning,
                          color: AppColor.yellow,
                          size: 15,
                        ),
                        SizedBox(width: 4.w),
                        Text(
                          "Click ‘Continue’ to complete your\nprofile & get noticed!",
                          style: poppins.copyWith(fontSize: 10.sp),
                        ),
                        const Spacer(),
                        MyCustomButton(
                          title: "Continue",
                          loading: false,
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    const ProfileArtistPageMessageSetup()));
                          },
                          color: AppColor.white,
                          width: 83.w,
                          height: 26.h,
                          fontSize: 10.sp,
                          textColor: AppColor.blue,
                          fontWeight: FontWeight.normal,
                          borderRadius: 50.r,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: BlocBuilder(
              bloc: _tabsBlocVenus,
              builder: (context, state) {
                if (state is TabsIndexStateVenus) {
                  return Column(
                    children: [
                      Container(
                        width: 240.w,
                        height: 46.h,
                        decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(50.r),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2),
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  _tabsBlocVenus.changeIndex(0);
                                },
                                child: Container(
                                  width: 78.w,
                                  height: 42.h,
                                  decoration: BoxDecoration(
                                    color: state.index == 0
                                        ? AppColor.blue
                                        : Colors.transparent,
                                    borderRadius:
                                    BorderRadius.circular(50.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Events",
                                      style: poppins,
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  _tabsBlocVenus.changeIndex(1);
                                },
                                child: Container(
                                  width: 78.w,
                                  height: 42.h,
                                  decoration: BoxDecoration(
                                    color: state.index == 1
                                        ? AppColor.blue
                                        : Colors.transparent,
                                    borderRadius:
                                    BorderRadius.circular(50.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Artists",
                                      style: poppins,
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  _tabsBlocVenus.changeIndex(2);
                                },
                                child: Container(
                                  width: 78.w,
                                  height: 42.h,
                                  decoration: BoxDecoration(
                                    color: state.index == 2
                                        ? AppColor.blue
                                        : Colors.transparent,
                                    borderRadius:
                                    BorderRadius.circular(50.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Venues",
                                      style: poppins,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      Expanded(
                        child: lsTabs[state.index],
                      ),
                    ],
                  );
                }
                return const SizedBox();
              }),
        ),
      ],
    );
  }
}
