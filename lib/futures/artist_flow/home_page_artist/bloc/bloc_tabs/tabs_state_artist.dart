abstract class TabsArtistBase {}

class TabsArtistIndexState extends TabsArtistBase {
  final int index;

  TabsArtistIndexState({required this.index});
}
