import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_tabs/tabs_state_venus.dart';

class TabsArtistViewModel extends Cubit<TabsBaseVenus> {
  TabsArtistViewModel() : super(TabsIndexStateVenus(index: 0));

  void changeArtistIndex(int index) {
    emit(TabsIndexStateVenus(index: index));
  }
}
