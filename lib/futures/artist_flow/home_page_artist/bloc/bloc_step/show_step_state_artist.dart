abstract class ShowStepArtistBaseState {}

class StatusStepArtist extends ShowStepArtistBaseState {
  final int index;

  StatusStepArtist({required this.index});
}
