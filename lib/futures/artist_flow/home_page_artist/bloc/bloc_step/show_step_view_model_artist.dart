import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/venue_flow/home_page_venus/bloc/bloc_step/show_step_state_venus.dart';

class ShowStepArtistViewModelVenus extends Cubit<ShowStepBaseVenus> {
  ShowStepArtistViewModelVenus() : super(StatusStep(index: 0));

  void changeArtistIndex(int index) {
    emit(StatusStep(index: index));
  }
}
