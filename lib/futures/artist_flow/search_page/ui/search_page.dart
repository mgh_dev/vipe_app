import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';

import '../../../../core/helper/custom_text_fild.dart';
import '../../../../core/utils/app_color.dart';
import '../../../../core/utils/image_app.dart';


class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController searchController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(left: 15.w,right: 15.w,top: 35.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(

            height: 49.h,
            decoration: BoxDecoration(
              color: AppColor.white.withOpacity(0.1),
              borderRadius: BorderRadius.circular(14.r),
            ),
            child: CustomTextField(
              controller:searchController,
              hint: "Search...",
              icon: ImagesApp.icSearchBlue,
              hintTextDirection: TextDirection.ltr,
              suffixIcon: ImagesApp.icClose,
            ),
          ),
          SizedBox(height:8.h),
          GenerateChipe(texts:["Vibe","Location","Dates"],borderColor: AppColor.greyHint,padding: EdgeInsets.only(left: 5.w,right: 8.w),)
        ],
      ),
    );
  }
}
