import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/nav_state.dart';



class NavArtistViewModel extends Cubit<NavArtistBaseState> {
  NavArtistViewModel() : super(NavArtistChangeState(index: 0));

  void changeNav(int i) {
    emit(NavArtistChangeState(index: i));
  }
}
