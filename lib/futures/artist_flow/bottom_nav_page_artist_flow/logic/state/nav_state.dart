abstract class NavArtistBaseState {}

class NavArtistChangeState extends NavArtistBaseState{
  final int index;

  NavArtistChangeState({required this.index});
}
