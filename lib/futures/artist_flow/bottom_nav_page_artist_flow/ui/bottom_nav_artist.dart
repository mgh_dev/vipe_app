import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/config/web_service.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/calender_artist_page.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/calender_artist_with_not_bottom_nav_page.dart';
import 'package:vibe_app/futures/artist_flow/home_page_artist/ui/home_Artist_page.dart';
import 'package:vibe_app/futures/artist_flow/profile_artist_page/ui/profile_artist_page.dart';
import 'package:vibe_app/futures/venue_flow/notifications_list/notifications_list.dart';
import 'package:vibe_app/menu.dart';
import 'package:vibe_app/menu_artist.dart';

import '../../../../core/config/service/failure.dart';
import '../../../../core/helper/container_blur.dart';
import '../../../../test_map.dart';
import '../../search_page/ui/search_page.dart';
import '../logic/state/nav_state.dart';
import '../logic/view_model/view_model_bottom_nav.dart';

class BottomNavArtistVenusPages extends StatefulWidget {
   BottomNavArtistVenusPages({required this.index});
final int index;
  @override
  State<BottomNavArtistVenusPages> createState() =>
      _BottomNavArtistVenusPagesState();
}

class _BottomNavArtistVenusPagesState extends State<BottomNavArtistVenusPages> {
  NavArtistViewModel viewModel = NavArtistViewModel();
  List<Widget> lsPage = [
    HomeArtistPage(),
    const SearchPage(),
    const SizedBox(),
    const CalenderArtistPageWithNotBottom(),
   const ProfileArtistPage()
  ];
  void initState() {
    // TODO: implement initState
    super.initState();
    WebService().initialOrUpdate(
      baseUrl: "http://46.249.102.65:8080/api/",
      header: {
        HttpHeaders.contentTypeHeader: 'application/json',
        "accept": "application/json",
        if (HiveServices.getToken != null)
          HttpHeaders.authorizationHeader: "Bearer ${HiveServices.getToken}"
      },
      refreshToken: () async {
        try {
          // if(HiveServices.getToken == null){
          //   Navigator.push(context,MaterialPageRoute(builder: (context)=> HomeGustePage()));
          // }
          var response = await WebService().dio.post("auth/refresh");
          HiveServices.addToken(response.data["data"]["token"]);
          return {
            "status": true,
            "token": response.data["data"]["token"],
          };
        } on DioException catch (e) {
          return {
            "status": false,
            "message": ServerFailure.fromJson(e.response?.data).map[Failure.key],
          };
        }
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      extendBody: true,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BlocConsumer(
                bloc: viewModel,

                builder: (context, sts) {
                  if(sts is NavArtistChangeState){
                    if(sts.index==0||sts.index==1){
                      return  Padding(
                        padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Builder(
                              builder: (context) => // Ensure Scaffold is in context
                              GestureDetector(
                                onTap: () {
                                  Scaffold.of(context).openDrawer();
                                },
                                child: SvgPicture.asset(ImagesApp.icMenu),
                              ),
                            ),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            GestureDetector(
                                onTap: (){

                                },
                                child: SvgPicture.asset(ImagesApp.icNotification)),
                          ],
                        ),
                      );
                    }
                    else{
                      return  const SizedBox();
                    }
                  }
                  return const SizedBox();
                }, listener: (BuildContext context,state) {
                if(state is NavArtistChangeState){
                  print(state.index);
                }
              },
              ),
              Expanded(
                child: BlocBuilder(
                  bloc: viewModel..changeNav(widget.index),
                  builder: (context, state) {
                    if (state is NavArtistChangeState) {
                      return lsPage[state.index];
                    } else {
                      return const SizedBox();
                    }
                  },
                ),
              )
            ],
          )
        ],

      ),
      bottomNavigationBar: BlocBuilder(
        bloc: viewModel,
        builder: (context, state) {
          if (state is NavArtistChangeState) {
            return SizedBox(
              height: 95.h,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(right: state.index == 0 ? 10.w : 0),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color:const Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          bottomLeft: Radius.circular(20.r),
                          topRight:
                              Radius.circular(state.index == 0 ? 20.r :state.index==1?20.r:0),
                          bottomRight:
                              Radius.circular(state.index == 0 ? 20.r :state.index==1?20.r:0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 0
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(0);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icHome,
                                color: state.index == 0
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: state.index == 1 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color:const Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index==1?20.r:state.index==0?20.r:0),
                          topRight: Radius.circular(state.index==1?20.r:state.index==2?20.r:0),
                          bottomLeft: Radius.circular(state.index==1?20.r:state.index==0?20.r:0),
                          bottomRight: Radius.circular(state.index==1?20.r:state.index==2?20.r:0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 1
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(1);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icSearch,
                                color: state.index == 1
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: state.index == 2 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color:const Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index==2?20.r:state.index==1?20.r:0),
                          topRight: Radius.circular(state.index==2?20.r:state.index==3?20.r:0),
                          bottomLeft: Radius.circular(state.index==2?20.r:state.index==1?20.r:0),
                          bottomRight: Radius.circular(state.index==2?20.r:state.index==3?20.r:0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 2
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(0);
                                showMap();
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icLocation,
                                color: state.index == 2
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: state.index == 3 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color:const Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index==3?20.r:state.index==2?20.r:0),
                          topRight: Radius.circular(state.index==3?20.r:state.index==4?20.r:0),
                          bottomLeft: Radius.circular(state.index==3?20.r:state.index==2?20.r:0),
                          bottomRight: Radius.circular(state.index==3?20.r:state.index==4?20.r:0),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 3
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                            onTap: () {
                              viewModel.changeNav(3);
                            },
                            child: SvgPicture.asset(
                              ImagesApp.icCalendar,
                              color: state.index == 3
                                  ? AppColor.blue
                                  : AppColor.iconColor,
                            ),
                          ), // Default: 2
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: state.index == 4 ? 10.w : 0.w),
                    child: Container(
                      width: 59.w,
                      height: 59.h,
                      decoration: BoxDecoration(
                        color:const Color(0xff222738),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(state.index==4?20.r:state.index==3?20.r:0),
                          topRight: Radius.circular(20.r),
                          bottomLeft: Radius.circular(state.index==4?20.r:state.index==3?20.r:0),
                          bottomRight: Radius.circular(20.r),
                        ),
                      ),
                      child: Center(
                        child: SimpleShadow(
                          opacity: 0.9,
                          color: state.index == 4
                              ? AppColor.blue
                              : Colors.transparent,
                          offset: const Offset(0, 1),
                          sigma: 9,
                          child: GestureDetector(
                              onTap: () {
                                viewModel.changeNav(4);
                              },
                              child: SvgPicture.asset(
                                ImagesApp.icUser,
                                color: state.index == 4
                                    ? AppColor.blue
                                    : AppColor.iconColor,
                              )), // Default: 2
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
          return const SizedBox();
        },
      ),
      drawer: MenuArtist(),
    );
  }
  void showMap() {


    ScrollController scrollController = ScrollController();
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r), topRight: Radius.circular(20.r)),
      ),
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height - 85.h,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: AppColor.darkBlue,
                  height: MediaQuery.of(context).size.height - 100.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.r),
                            topRight: Radius.circular(20.r)),
                        child: Image.asset(
                          ImagesApp.ellipse,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SingleChildScrollView(
                        physics: const NeverScrollableScrollPhysics(),
                        child: Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.r),
                                topRight: Radius.circular(20.r)),),
                            height: MediaQuery.of(context).size.height - 101.h,
                            child: MapApp(
                              center: LatLong(33, 54),
                              isVisiblBtn: false,
                              onPicked: (pickedData) {
                                print(pickedData.latLong.latitude);
                                print(pickedData.latLong.longitude);
                                print(pickedData.address);

                                setState(() {
                                });
                              },


                            )
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 45.w),
                    width: 35.w,
                    height: 35.r,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r),
                    ),
                    child: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
