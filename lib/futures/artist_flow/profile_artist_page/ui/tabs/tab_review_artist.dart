import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/profile_venus_page/ui/widgets/bottom_sheets.dart';

import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabReviewArtist extends StatelessWidget {
  TabReviewArtist({required this.scrollController}) : super();
  final changeRoleBloc = ChangeRoleViewModel();
  ScrollController scrollController;
  TextEditingController textCommentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 12.w, left: 15.w),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: 22.h, bottom: 23.h, right: 26.47.w, left: 26.47.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "4.5",
                            style: poppins.copyWith(
                                fontSize: 32.sp, fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 4.5.w,
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 6.h),
                            child: Text(
                              "/5",
                              style: poppins.copyWith(
                                  fontSize: 14.sp, fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                      Text("Based on 120 Reviews",
                          style: poppins.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w400,
                              color: AppColor.grey2)),
                      SizedBox(
                        height: 15.h,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.star_rounded,
                            color: AppColor.green3,
                            size: 25,
                          ),
                          Icon(
                            Icons.star_rounded,
                            color: AppColor.green3,
                            size: 25,
                          ),
                          Icon(
                            Icons.star_rounded,
                            color: AppColor.green3,
                            size: 25,
                          ),
                          Icon(
                            Icons.star_rounded,
                            color: AppColor.green3,
                            size: 25,
                          ),
                          Icon(
                            Icons.star_rounded,
                            color: AppColor.green3,
                            size: 25,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    width: 110.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "5 Star",
                              style: poppins.copyWith(
                                  fontSize: 10.sp, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: 10.w,
                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.black54),
                                ),
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.green3),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 7.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "4 Star",
                              style: poppins.copyWith(
                                  fontSize: 10.sp, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: 10.w,
                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.black54),
                                ),
                                Container(
                                  height: 6.h,
                                  width: 44.31.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.green3),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 7.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "3 Star",
                              style: poppins.copyWith(
                                  fontSize: 10.sp, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: 10.w,
                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.black54),
                                ),
                                Container(
                                  height: 6.h,
                                  width: 28.2.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.green3),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 7.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "2 Star",
                              style: poppins.copyWith(
                                  fontSize: 10.sp, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: 10.w,
                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.black54),
                                ),
                                Container(
                                  height: 6.h,
                                  width: 20.14.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.green3),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 7.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "1 Star",
                              style: poppins.copyWith(
                                  fontSize: 10.sp, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: 10.w,
                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 6.h,
                                  width: 61.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.black54),
                                ),
                                Container(
                                  height: 6.h,
                                  width: 9.4.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppColor.green3),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 1,
              color: AppColor.white.withOpacity(0.07),
              margin: EdgeInsets.symmetric(vertical: 16.h),
            ),
            GestureDetector(
              onTap: () {
                ProfileVenusBottomSheets.sendComment(
                  context,
                  textCommentController,
                );
              },
              child: Row(
                children: [
                  SvgPicture.asset(ImagesApp.addReview),
                  SizedBox(
                    width: 12.w,
                  ),
                  Text(
                    "Add Review",
                    style: poppins.copyWith(
                        fontSize: 14.sp, fontWeight: FontWeight.w400),
                  ),
                  Spacer(),
                  SvgPicture.asset(
                    ImagesApp.arrowForward,
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 1,
              color: AppColor.white.withOpacity(0.07),
              margin: EdgeInsets.symmetric(vertical: 16.h),
            ),
            Row(
              children: [
                Text(
                  "User reviews",
                  style: poppins.copyWith(
                      fontSize: 14.sp, fontWeight: FontWeight.w500),
                ),
                Spacer(),
                SvgPicture.asset(ImagesApp.sort),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.w),
                  child: Text(
                    "Most useful",
                    style: poppins.copyWith(
                        fontSize: 14.sp, fontWeight: FontWeight.w500),
                  ),
                ),
                SvgPicture.asset(ImagesApp.arrowDown),
              ],
            ),
            SizedBox(
              height: 16.h,
            ),
            ListView.builder(
              shrinkWrap: true,
              // controller: scrollController,
              itemCount: 13,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.only(
                      left: 16.w, top: 16.h, bottom: 24.h, right: 48.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 24.w,
                            height: 24.h,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage(
                                      ImagesApp.venusProf,
                                    ),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Text(
                            "Arman Rokni",
                            style: poppins.copyWith(
                                fontSize: 14.sp, fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                          Text(
                            "2h ago",
                            style: poppins.copyWith(
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.white.withOpacity(0.5)),
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.star_rounded,
                                color: AppColor.green3,
                                size: 15,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: AppColor.green3,
                                size: 15,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: AppColor.green3,
                                size: 15,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: AppColor.green3,
                                size: 15,
                              ),
                              Icon(
                                Icons.star_rounded,
                                color: AppColor.green3,
                                size: 15,
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Text(
                        "“It was a Vibe! Eric Jones delivered an\noutstanding performance at the open\nmic night!”",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.grey73),
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
