import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../../core/utils/app_color.dart';
import '../../../../../../../../core/utils/image_app.dart';
import '../../../../constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class TabGalleryArtist extends StatelessWidget {
  TabGalleryArtist({required this.scrollController,required this.gallery}) : super();
  ScrollController scrollController;
  final List<String> gallery;
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding:  EdgeInsets.only(top:24.h),
      child: Stack(
        children: [
          SingleChildScrollView(
            controller: scrollController,
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal:16.w),
              child: Column(
                children: [
                  Row  (
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: gallery.length>=1,
                            child: Container(
                              width: 168.w,
                              height: 276.h,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                image: DecorationImage(
                                  image: NetworkImage("http://46.249.102.65:8080/storage/${gallery[0]}"),
                                  // image: AssetImage(ImagesApp.profileTest1),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 6.w),
                          Visibility(
                            visible:gallery.length>=2,
                            child: Container(
                              width: 168.w,
                              height: 109.h,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                image: DecorationImage(
                                  image: NetworkImage("http://46.249.102.65:8080/storage/${gallery[1]}"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 6.w),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: gallery.length>=3,
                            child: Container(
                              width: 168.w,
                              height: 139.h,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                image: DecorationImage(
                                  image: NetworkImage("http://46.249.102.65:8080/storage/${gallery[2]}"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 6.w),
                          Visibility(
                            visible: gallery.length>=4,
                            child: Container(
                              width: 168.w,
                              height: 246.h,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.r),
                                image: DecorationImage(
                                  image:NetworkImage("http://46.249.102.65:8080/storage/${gallery[3]}"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                  SizedBox(height: 6.h),
                  Visibility(
                    visible: gallery.length>=5,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 150.h,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.r),
                        image: DecorationImage(
                          image:NetworkImage("http://46.249.102.65:8080/storage/${gallery[3]}"),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
