class GetResponseProfileArtistModels {
  GetResponseProfileArtistModels({
    required this.message,
    required this.data,
  });

  late final String message;
  late final Data data;

  GetResponseProfileArtistModels.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.type,
    required this.email,
    required this.avatar,
    required this.vibe,
    required this.artistry,
    required this.gallery,
    required this.audio,
    required this.video,
    required this.extraData,
  });

  late final int id;
  late final String firstName;
  late final String lastName;
  late final String type;
  late final String email;
  late final String avatar;
  late final List<Vibe> vibe;
  late final List<dynamic> artistry;
  late final List<Gallery> gallery;
  late final List<dynamic> audio;
  late final List<dynamic> video;
  late final ExtraData extraData;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    type = json['type'];
    email = json['email'];
    avatar = json['avatar'];
    vibe = List.from(json['vibe']).map((e) => Vibe.fromJson(e)).toList();
    artistry = List.castFrom<dynamic, dynamic>(json['artistry']);
    gallery =
        List.from(json['gallery']).map((e) => Gallery.fromJson(e)).toList();
    audio = List.castFrom<dynamic, dynamic>(json['audio']);
    video = List.castFrom<dynamic, dynamic>(json['video']);
    extraData = ExtraData.fromJson(json['extraData']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['type'] = type;
    _data['email'] = email;
    _data['avatar'] = avatar;
    _data['vibe'] = vibe.map((e) => e.toJson()).toList();
    _data['artistry'] = artistry;
    _data['gallery'] = gallery.map((e) => e.toJson()).toList();
    _data['audio'] = audio;
    _data['video'] = video;
    _data['extraData'] = extraData.toJson();
    return _data;
  }
}

class Vibe {
  Vibe({
    required this.id,
    required this.name,
    required this.description,
    required this.image,
    // this.icon,
  });

  late final int id;
  late final String name;
  late final String description;
  late final String image;

  // late final Null icon;

  Vibe.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    // icon = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['image'] = image;
    // _data['icon'] = icon;
    return _data;
  }
}

class Gallery {
  Gallery({
    required this.id,
    required this.name,
    required this.type,
    required this.path,
  });

  late final int id;
  late final String name;
  late final String type;
  late final String path;

  Gallery.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['type'] = type;
    _data['path'] = path;
    return _data;
  }
}

class ExtraData {
  ExtraData({
    required this.role,
    required this.links,
    required this.exteData,
    required this.about,
  });

  late final int role;
  late final List<Links> links;
  late final ExteData exteData;
  late final String about;

  ExtraData.fromJson(Map<String, dynamic> json) {
    role = json['role'];
    links = List.from(json['links']).map((e) => Links.fromJson(e)).toList();
    exteData = ExteData.fromJson(json['data']);
    about = json['about'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['role'] = role;
    _data['links'] = links.map((e) => e.toJson()).toList();
    _data['data'] = exteData.toJson();
    _data['about'] = about;
    return _data;
  }
}

class Links {
  Links({
    required this.link,
    required this.type,
    required this.title,
  });

  late final String link;
  late final String type;
  late final String title;

  Links.fromJson(Map<String, dynamic> json) {
    link = json['link'];
    type = json['type'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['link'] = link;
    _data['type'] = type;
    _data['title'] = title;
    return _data;
  }
}

class ExteData {
  ExteData({
    required this.instrument,
    required this.stageName,
    required this.gigRequestLimit,
    required this.performanceEquipment,
    required this.additionalDetailedInformation,
  });

  late final List<String> instrument;
  late final String stageName;

  late final int gigRequestLimit;

  late final List<String> performanceEquipment;
  late final String additionalDetailedInformation;

  ExteData.fromJson(Map<String, dynamic> json) {
    instrument = List.castFrom<dynamic, String>(json['instrument']);
    stageName = json['stageName'];
    gigRequestLimit = json['gigRequestLimit'];
    performanceEquipment =
        List.castFrom<dynamic, String>(json['performanceEquipment']);
    additionalDetailedInformation = json['additionalDetailedInformation'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};

    _data['instrument'] = instrument;
    _data['stageName'] = stageName;

    _data['gigRequestLimit'] = gigRequestLimit;

    _data['performanceEquipment'] = performanceEquipment;
    _data['additionalDetailedInformation'] = additionalDetailedInformation;
    return _data;
  }
}
