import 'package:bloc/bloc.dart';
import 'package:vibe_app/futures/artist_flow/create_profile_Artist/logic/state/get_artisy_state.dart';
import 'package:vibe_app/futures/artist_flow/profile_artist_page/model/get_profile_artist_model.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../../../core/config/web_service.dart';
import '../state/get_profile_artist_state.dart';


class GetArtistProfileViewModel extends Cubit<GetArtistProfileBaseState> {
  GetArtistProfileViewModel() : super(GetArtistProfileInitialState());

  void getArtist() async {
    print("1");
    emit(GetArtistProfileLoadingState());
    print("3");
    try {
      print("4");
      var response =
          await WebService().dio.get("users/get-profile?vibe=1&artistry=1&extraData=1&video=1&audio=1&gallery=1");
      print("5");
      GetResponseProfileArtistModels getResponseProfileArtistModels =
      GetResponseProfileArtistModels.fromJson(response.data);
      print("6");
     
      print("7");
      emit(GetArtistProfileSuccessState(response: getResponseProfileArtistModels));
      print("8");
    } catch (e) {
      print("9");
      emit(GetArtistProfileFailState(message: e.toString()));
      print("10");
    }
  }

}
