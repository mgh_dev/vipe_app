import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/artist_flow/profile_artist_page/model/get_profile_artist_model.dart';

abstract class GetArtistProfileBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetArtistProfileInitialState extends GetArtistProfileBaseState {}

class GetArtistProfileLoadingState extends GetArtistProfileBaseState {}

class GetArtistProfileSuccessState extends GetArtistProfileBaseState {
  final GetResponseProfileArtistModels response;

  GetArtistProfileSuccessState({required this.response});
}

class GetArtistProfileFailState extends GetArtistProfileBaseState {
  final String message;


  GetArtistProfileFailState({required this.message});
}
