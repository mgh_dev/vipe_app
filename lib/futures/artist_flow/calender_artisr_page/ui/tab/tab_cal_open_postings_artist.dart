import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

import '../../../../../core/helper/different_size_of_gridview_item.dart';

class TabCalOpenPostingsArtist extends StatefulWidget {
   TabCalOpenPostingsArtist({required this.scrollController}) : super();
  ScrollController scrollController;
  @override
  State<TabCalOpenPostingsArtist> createState() => _TabCalOpenPostingsArtistState();
}

class _TabCalOpenPostingsArtistState extends State<TabCalOpenPostingsArtist> {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding:  EdgeInsets.symmetric(horizontal:16.w  ),
      child: Container(
        width: 332,
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(18.r),
        ),
        child: Column(
          children: [
            SizedBox(height: 27.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.h),
              child: Text(
                "View and manage all incoming requests from artists.",
                style: poppins.copyWith(
                    fontSize: 11.sp,
                    fontWeight: FontWeight.w500,
                    color: AppColor.greyHint),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: ListView.builder(
                controller: widget.scrollController,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.09),
                        borderRadius: BorderRadius.circular(8.r)),
                    margin:
                    EdgeInsets.only( bottom: 8.h),
                    padding: EdgeInsets.only(left: 8.w,right: 8.w,top: 7.h),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8.r),
                          child: Image.asset(
                            ImagesApp.showsHouseofmusic,
                            width: 52.w,
                            height: 56.h,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(
                          width: 8.w,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Open Mic Night",
                              style: poppins.copyWith(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w600),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 5.h),
                              child: Text(
                                "Guarantee: \$700",
                                style: inter.copyWith(
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.greenText2,
                                ),
                              ),
                            ),
                            Text(
                              "Type of artist needed: Jazz Band",
                              style: inter.copyWith(
                                fontSize: 10.sp,
                                fontWeight: FontWeight.w500,
                                color: AppColor.white,
                              ),
                            ),
                            SizedBox(
                              height:9.h,
                            ),
                            GenerateChipe(
                              texts: const ["Nostalgic", "Bluesy", "Jazzy"],
                              h: 20.h,
                              borderRadius: 4.r,
                              borderColor: Colors.white,
                              padding: EdgeInsets.only(right:5.w),
                            ),
                            SizedBox(
                              height:9.h,
                            ),
                          ],
                        ),
                        const Spacer(),
                        Container(
                          width: 52,
                          height: 62,
                          decoration: BoxDecoration(
                              color: AppColor.black12,
                              borderRadius: BorderRadius.circular(8.r)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "03",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "Aug",
                                style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.grey81),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

}
