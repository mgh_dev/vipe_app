import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/container_blur.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/bottom_nav_page_artist_flow/ui/bottom_nav_artist.dart';
import 'package:vibe_app/futures/artist_flow/gig_request_artist/ui/tab/tab_incoming_equest_artist.dart';
import 'package:vibe_app/futures/artist_flow/gig_request_artist/ui/tab/tab_outgoing_requests_artist.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';
import 'package:vibe_app/futures/venue_flow/payment_method_page/ui/payment_method_page.dart';

class GigRequestArtist extends StatefulWidget {
  const GigRequestArtist({Key? key}) : super(key: key);

  @override
  State<GigRequestArtist> createState() => _GigRequestArtistState();
}

class _GigRequestArtistState extends State<GigRequestArtist> {
  final changeRoleBloc = ChangeRoleViewModel();
  SelectItemViewModel changeItemBloc = SelectItemViewModel();
final List<Widget> lsTabGigPage=[
  const TabIncomingRequestArtist(),
  const TabOutgoingRequestsArtist(),
];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 53.h, bottom: 25.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Gig Requests",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    GestureDetector(
                        onTap: () {},
                        child: SvgPicture.asset(ImagesApp.icNotification)),
                  ],
                ),
              ),
              BlocBuilder(
                bloc: changeItemBloc,
                builder: (context, state) {
                  if (state is ItemChangeState) {
                    return Container(
                      width: 343.w,
                      height: 54.h,
                      decoration: BoxDecoration(
                          color: AppColor.white.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(50)),
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: () => changeItemBloc.changeItem(0),
                            child: Container(
                              width: 180,
                              height: 45.h,
                              decoration: BoxDecoration(
                                  color: state.index == 0
                                      ? AppColor.white.withOpacity(0.05)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Center(
                                child: Text(
                                  "Incoming Request",
                                  style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColor.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(),
                          GestureDetector(
                            onTap: () => changeItemBloc.changeItem(1),
                            child: Container(
                              width: 180,
                              height: 45.h,
                              decoration: BoxDecoration(
                                  color: state.index == 1
                                      ? AppColor.white.withOpacity(0.05)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Center(
                                child: Text(
                                  "Outgoing Request",
                                  style: poppins.copyWith(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColor.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                        ],
                      ),
                    );
                  }
                  return const SizedBox();
                },
              ),
              SizedBox(height: 13.h),
              Expanded(
                child: BlocBuilder(
                  bloc: changeItemBloc,
                  builder: (context, state) {
                    if(state is ItemChangeState){
                      return lsTabGigPage[state.index];
                    }
                    return const SizedBox();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
