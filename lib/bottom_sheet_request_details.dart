
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetRequestDetailsw extends StatelessWidget {
  BottomSheetRequestDetailsw({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 54.h,
      child: Stack(
        alignment: Alignment.topLeft,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.r),
                topRight: Radius.circular(20.r)),
            child: Image.asset(
              ImagesApp.ellipse,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding:
                  EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 16.w),
                          child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Cancel",
                                style: poppins.copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.blue),
                              )),
                        ),
                      ),
                      Expanded(
                          child: Text(
                            "Request Details",
                            style: poppins.copyWith(
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w500),
                          )),
                      Expanded(
                        child: Container(),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1,
                  width: double.infinity,
                  color: AppColor.white.withOpacity(0.07),
                ),
                SizedBox(
                  height: 25.h,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(50.r),
                  child: Image.asset(
                    ImagesApp.profileRequestDetails,
                    width: 72.w,
                    height: 72.r,
                    fit: BoxFit.fill,
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(top: 16.h, bottom: 4.h),
                  child: Text(
                    "Eric Johns",
                    style: poppins.copyWith(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: 12.w,
                        height: 12.h,
                        child: SvgPicture.asset(
                          ImagesApp.icLocation,
                          color: AppColor.grey97,
                          fit: BoxFit.cover,
                        )),
                    SizedBox(
                      width: 7.5.w,
                    ),
                    Text(
                      "Virginia Singer",
                      style: poppins.copyWith(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          color: AppColor.grey97),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 57.w,right: 36.w),
                  child: Row(
                    children: [
                      Text(
                        "Thursday ",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight:
                            FontWeight.w400,
                            color:
                            AppColor.grey81),
                      ),
                      Text(
                        "at 7:30pm",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight:
                            FontWeight.w400,
                            color: AppColor.blue),
                      ),
                      Spacer(),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 12.5.w,
                            vertical: 6.h),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(14.r),
                          color: AppColor.black12,
                        ),
                        child: Column(
                          children: [
                            Text(
                              "24",
                              style: poppins.copyWith(
                                  fontSize: 18.sp,
                                  fontWeight:
                                  FontWeight.w600,
                                  color: AppColor.white),
                            ),
                            Text(
                              "Dec",
                              style: poppins.copyWith(
                                  fontSize: 16.sp,
                                  fontWeight:
                                  FontWeight.w500,
                                  color: AppColor.grey2),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(4.r),
                  margin: EdgeInsets.only(
                      top: 21.h, bottom: 12.h,right: 27.w,left: 27.w),
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(8.r),
                    color: AppColor.blackBack,
                  ),
                  width: double.infinity,
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius:
                        BorderRadius.circular(8.r),
                        child: Image.asset(
                          width: 52.w,
                          height: 56.8.h,
                          ImagesApp.backHouseofmusic,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "House of Music",
                                style: poppins.copyWith(
                                    fontSize: 15.sp,
                                    fontWeight:
                                    FontWeight.w500),
                              ),
                              SizedBox(
                                width: 6.w,
                              ),
                              Icon(
                                Icons.check_circle,
                                color: AppColor.greyBlue2,
                                size: 16.r,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          Row(
                            children: [
                              Container(
                                padding:
                                EdgeInsets.all(4.5.r),
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius
                                      .circular(4.r),
                                  color: AppColor.white
                                      .withOpacity(0.09),
                                ),
                                child: Container(
                                    height: 7.h,
                                    width: 7.w,
                                    child:
                                    SvgPicture.asset(
                                      ImagesApp
                                          .icLocation,
                                      color:
                                      AppColor.white,
                                      fit: BoxFit.cover,
                                    )),
                              ),
                              SizedBox(
                                width: 6.w,
                              ),
                              Text(
                                "273-296 Geary St, Richmond, VA 09584",
                                style: inter.copyWith(
                                    fontSize: 10.sp,
                                    fontWeight:
                                    FontWeight.w400,
                                    color:
                                    AppColor.greyTxt),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(
                    horizontal: 11.w,
                    vertical: 14.h
                  ),
                  margin: EdgeInsets.only(
                      right: 29.w,
                      left: 27.w,
                      bottom: 14.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.r),
                    color: AppColor.white.withOpacity(0.07),
                  ),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Price",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        "\$ 250",
                        style: poppins.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColor.greenText2),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 4.h,left: 37.w),
                  child: Row(
                    children: [
                      Text(
                        "Message",
                        style: poppins.copyWith(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500,
                            color: AppColor.grey7E),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 122.h,
                  padding: EdgeInsets.symmetric(
                      horizontal: 10.w, vertical: 8.h),
                  margin: EdgeInsets.only(
                      right: 27.w,left: 32.w),
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(10.r),
                    color:
                    AppColor.white.withOpacity(0.07),
                  ),
                  width: double.infinity,
                  child: Text(
                   "I’ll be in California this weekend and would love to play at your venue. I bring soulful vibes and have a following of 50K. Would you be open to \$400?",
                    style: poppins.copyWith(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 16.w,left: 16.w,top: 40.h,bottom: 28.h),
                  width: double.infinity,
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.r),
                  color: AppColor.blackBack2,
                ),
                child: Column(children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 18.h),
                    child: MyCustomButton(
                      title: "Accept",
                      onTap: () {},
                      loading: false,
                      color: AppColor.green3,
                      width: double.infinity,
                      height: 50.h,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Container(width: double.infinity,color: AppColor.white.withOpacity(0.3),height: 0.5,),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 18.h),
                    child: MyCustomButton(
                      title: "Message",
                      onTap: () {},
                      borderColor: AppColor.white,
                      loading: false,
                      color: AppColor.darkBlue,
                      width: double.infinity,
                      height: 50.h,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ]),
                ),
                Text("Decline",style: poppins.copyWith(color: AppColor.redText,fontWeight: FontWeight.w400,fontSize: 15.sp),),
                SizedBox(height: 96.h,),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
