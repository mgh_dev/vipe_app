import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class EventDetailsPage extends StatelessWidget {
  EventDetailsPage({Key? key}) : super(key: key);
  final _eventNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Calendar",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(right: 16.w,left: 16.w,top: 31.9.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Event Details",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w600),),
                      Container(
                        margin: EdgeInsets.only(top: 28.h),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.r),
                          color: AppColor.white.withOpacity(0.07),
                        ),
                        child: CustomTextField(
                          controller: _eventNameController,
                          hint: "Event Name*",
                          hintTextDirection:TextDirection.ltr,
                          hintStyle: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),
                          contentPadding: EdgeInsets.symmetric(horizontal: 11.w.h,vertical: 15.5.h),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 12.h),
                        padding: EdgeInsets.symmetric(horizontal: 11.w.h,vertical: 15.5.h),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.r),
                            color: AppColor.white.withOpacity(0.07)
                        ),
                        child: Row(children: [
                          Container(
                              height: 17.r,
                              width: 17.w,
                              child: SvgPicture.asset(ImagesApp.icClock,fit: BoxFit.cover,)),
                          SizedBox(width: 8.w,),
                          Text("Date and time",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                          Spacer(),
                          Text("9 pm",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),),
                          SizedBox(width: 6.w,),
                          Container(
                              height: 17.r,
                              width: 17.w,
                              child: SvgPicture.asset(ImagesApp.arrowForward,fit: BoxFit.cover,)),
                        ],),),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 11.w.h,vertical: 15.5.h),
                        margin: EdgeInsets.only(bottom: 12.h),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.r),
                            color: AppColor.white.withOpacity(0.07)
                        ),
                        child: Row(children: [
                          Container(
                              height: 17.r,
                              width: 17.w,
                              child: SvgPicture.asset(ImagesApp.icLocation,fit: BoxFit.cover)
                          ),
                          SizedBox(width: 8.w,),
                          Text("Location",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                          Spacer(),
                          Text("9 Galaction N kore stress..",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),),
                          SizedBox(width: 6.w,),
                          Container(
                              height: 17.r,
                              width: 17.w,
                              child: SvgPicture.asset(ImagesApp.arrowForward,fit: BoxFit.cover,)),
                        ],),),
                      Text("About this event",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey7E),),
                      Container(
                        margin: EdgeInsets.only(top: 5.h,bottom: 16.h),
                        padding: EdgeInsets.symmetric(horizontal: 12.w,vertical: 10.h),
                        width: double.infinity,
                        height: 149,
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(10.r)
                        ),
                        child: Text(
                          "Join us at \"Music Fest\" and let the music take you on a journey. This show is all about exploring the various genres and styles of music and finding the connections that bring them together.",
                          style: poppins.copyWith(fontSize: 13.sp,fontWeight: FontWeight.w400,color: AppColor.grey81),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 5.w,bottom: 8.h),
                        child: Text("Age limit",style: poppins.copyWith(fontSize: 13.sp,fontWeight: FontWeight.w400,color: AppColor.greyText),),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 13.w,right: 76.w),
                        child: Row(children: [
                          Container(height: 15.r,width: 15.w,color: Colors.red,),
                          SizedBox(width: 8.w,),
                          Text("No Age Limit",style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w500),),
                          Spacer(),
                          Container(height: 15.r,width: 15.w,color: Colors.red,),
                          SizedBox(width: 8.w,),
                          Text("18+",style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w500),),
                          SizedBox(width: 15.w,),
                          Container(height: 15.r,width: 15.w,color: Colors.red,),
                          SizedBox(width: 8.w,),
                          Text("21+",style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w500),),
                        ],),
                      )
                    ],
                  )
              )
            ],
          ),

        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
        width: double.infinity,
        color: AppColor.white.withOpacity(0.07),
        child: MyCustomButton(
          title: "Continue",
          onTap: () {},
          loading: false,
          color: AppColor.blue,
          width: double.infinity,
          height: 49.h,
          fontSize: 14.sp,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
