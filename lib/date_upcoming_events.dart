import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class DateUpcomingEvents extends StatelessWidget {
  DateUpcomingEvents({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                    },
                    child: SvgPicture.asset(ImagesApp.icBack)),
                Text(
                  "Gig Requests",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                SvgPicture.asset(ImagesApp.icNotification),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                ),
                backgroundColor: AppColor.darkBlue,
                isScrollControlled: true,
                builder: (BuildContext context) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 54.h,
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.r),
                              topRight: Radius.circular(20.r)),
                          child: Image.asset(
                            ImagesApp.ellipse,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 19.h, bottom: 14.h),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 16.w),
                                        child: GestureDetector(
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              "Cancel",
                                              style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: AppColor.blue),
                                            )),
                                      ),
                                    ),
                                    Expanded(
                                        child: Center(
                                            child: Text(
                                      "Calendar",
                                      style: poppins.copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500),
                                    ))),
                                    Expanded(
                                      child: Container(),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                width: double.infinity,
                                color: AppColor.white.withOpacity(0.07),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 16.h, left: 26.w),
                                child: Text(
                                  "Upcoming Events",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(
                                    right: 16.w,
                                    left: 16.w,
                                    top: 12.h,
                                    bottom: 15.h),
                                padding: EdgeInsets.symmetric(
                                  horizontal: 4.w,
                                ),
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.07),
                                    borderRadius: BorderRadius.circular(18.r)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 7.h,
                                    ),
                                    Container(
                                      height: 1.5,
                                      width: 21.5,
                                      color: AppColor.white,
                                    ),
                                    SizedBox(
                                      height: 4.h,
                                    ),
                                    Container(
                                        height: 1.5,
                                        width: 9.5,
                                        color: AppColor.white),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16.w),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Sat, Aug 24",
                                            style: inter.copyWith(
                                                fontSize: 13.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.grey7),
                                          ),
                                          SizedBox(
                                            width: 15.w,
                                          ),
                                          Expanded(
                                              child: Container(
                                                  height: 1,
                                                  color: AppColor.white
                                                      .withOpacity(0.2))),
                                        ],
                                      ),
                                    ),
                                    ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: 2,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 6.h),
                                          padding: EdgeInsets.all(4.r),
                                          decoration: BoxDecoration(
                                              color: AppColor.white
                                                  .withOpacity(0.09),
                                              borderRadius:
                                                  BorderRadius.circular(8.r)),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.r),
                                                    child: Image.asset(
                                                      ImagesApp.profileTest1,
                                                      width: 52.w,
                                                      height: 56.8.r,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 11.8.w,
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        "Open Mic Night",
                                                        style: poppins.copyWith(
                                                            fontSize: 16.sp,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                      SizedBox(
                                                        height: 6.h,
                                                      ),
                                                      Text(
                                                        "Thursday, August 24 at 7:30 pm",
                                                        style: inter.copyWith(
                                                            fontSize: 11.sp,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color:
                                                                AppColor.blue),
                                                      )
                                                    ],
                                                  )
                                                ],
                                              ),
                                              Container(
                                                width: double.infinity,
                                                margin:
                                                    EdgeInsets.only(top: 8.h),
                                                decoration: BoxDecoration(
                                                    color: AppColor.blackBack3,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.r),
                                                    border: Border.all(
                                                        width: 1,
                                                        color: AppColor.blue)),
                                                child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 9.w,
                                                                top: 8.h),
                                                        child: Text(
                                                          "House of Music",
                                                          style: inter.copyWith(
                                                              fontSize: 11.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              color: AppColor
                                                                  .white),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 9.w,
                                                                top: 6.h),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(4.5
                                                                          .r),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            4.r),
                                                                color: AppColor
                                                                    .white
                                                                    .withOpacity(
                                                                        0.09),
                                                              ),
                                                              child: Container(
                                                                  height: 7.r,
                                                                  width: 7.w,
                                                                  child:
                                                                      SvgPicture
                                                                          .asset(
                                                                    ImagesApp
                                                                        .icLocation,
                                                                    color: AppColor
                                                                        .white,
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )),
                                                            ),
                                                            SizedBox(
                                                              width: 6.w,
                                                            ),
                                                            Text(
                                                              "Elgin St. Celina, Delaware",
                                                              style: inter.copyWith(
                                                                  fontSize:
                                                                      10.sp,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  color: AppColor
                                                                      .greyHint),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.h,
                                                                right: 11.w,
                                                                bottom: 14.h),
                                                        child: Row(
                                                          children: [
                                                            Spacer(),
                                                            Container(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          8.w,
                                                                      vertical:
                                                                          4.h),
                                                              decoration: BoxDecoration(
                                                                  color: AppColor
                                                                      .white
                                                                      .withOpacity(
                                                                          0.05),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(4
                                                                              .r),
                                                                  border: Border.all(
                                                                      width:
                                                                          0.5,
                                                                      color: AppColor
                                                                          .white
                                                                          .withOpacity(
                                                                              .2))),
                                                              child: Text(
                                                                "Chill",
                                                                style: poppins.copyWith(
                                                                    fontSize:
                                                                        8.sp,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 4.w,
                                                            ),
                                                            Container(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          8.w,
                                                                      vertical:
                                                                          4.h),
                                                              decoration: BoxDecoration(
                                                                  color: AppColor
                                                                      .white
                                                                      .withOpacity(
                                                                          0.05),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(4
                                                                              .r),
                                                                  border: Border.all(
                                                                      width:
                                                                          0.5,
                                                                      color: AppColor
                                                                          .white
                                                                          .withOpacity(
                                                                              .2))),
                                                              child: Text(
                                                                "Soulful",
                                                                style: poppins.copyWith(
                                                                    fontSize:
                                                                        8.sp,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ]),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: AppColor.white.withOpacity(0.07),width: double.infinity,
                                padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                                child: MyCustomButton(
                                  title: "Book me",
                                  loading: false,
                                  onTap: () {},
                                  color: AppColor.blue,
                                  width: double.infinity,
                                  height: 49.h,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500,
                                  borderRadius: 48.sp,
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            },
            child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16)),
                child: Text(
                  "Open Bottom Sheet",
                  style: poppins.copyWith(fontSize: 14),
                )),
          ),
        ],
      ),
    );
  }
}
