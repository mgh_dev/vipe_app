import 'package:cell_calendar/cell_calendar.dart';
import 'package:flutter/material.dart';

List<CalendarEvent> sampleEvents() {
  final today = DateTime.now();
  const eventTextStyle = TextStyle(
    fontSize: 9,
    color: Colors.blue,
  );
  final sampleEvents = [
    CalendarEvent(
      eventName: "a.a",
      eventDate: today.add(const Duration(days: 3)),
      eventTextStyle: eventTextStyle,
    ),
    CalendarEvent(
      eventName: "a.b",
      eventDate: today.add(const Duration(days: 3)),
      eventTextStyle: eventTextStyle,
    ),
    CalendarEvent(
      eventName: "a.c",
      eventDate: today.add(const Duration(days: 3)),
      eventTextStyle: eventTextStyle,
    ),
    CalendarEvent(
      eventName: "a.d",
      eventDate: today.add(const Duration(days: 3)),
      eventTextStyle: eventTextStyle,
    ),
    /*CalendarEvent(
      eventName: "a.e",
      eventDate: today.add(const Duration(days: 3)),
      eventTextStyle: eventTextStyle,
    ),*/

    CalendarEvent(
      eventName: "b.b",
      eventDate: today.add(const Duration(days: 0)),
      eventTextStyle: eventTextStyle,
    ),
    /*CalendarEvent(
      eventName: "b.c",
      eventDate: today.add(const Duration(days: 0)),
      eventTextStyle: eventTextStyle,
    ),*/
  ];
  return sampleEvents;
}
