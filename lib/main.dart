import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/bottom_sheet_details.dart';
import 'package:vibe_app/bottom_sheet_review_request.dart';
import 'package:vibe_app/futures/constant_pages/splash_page/ui/splash_page.dart';
import 'bottom_sheet_edit_request.dart';
import 'bottom_sheet_request_details.dart';
import 'core/config/hive_service/hive_service.dart';
import 'core/config/service/failure.dart';
import 'core/config/web_service.dart';

void main()async {
  WidgetsFlutterBinding();
  await HiveServices.init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void initState() {
    WebService().initialOrUpdate(
        baseUrl: "http://46.249.102.65:8080/api/",
      header: {
        HttpHeaders.contentTypeHeader: 'application/json',
        "accept": "application/json",
        if (HiveServices.getToken != null)
          HttpHeaders.authorizationHeader: "Bearer ${HiveServices.getToken}"
      },
      refreshToken: () async {
        try {
          if(HiveServices.getToken == null){
            Navigator.push(context,MaterialPageRoute(builder: (context)=>const SplashPage()));
          }
          var response = await WebService().dio.post("auth/refresh");
          HiveServices.addToken(response.data["data"]["token"]);
          return {
            "status": true,
            "token": response.data["data"]["token"],
          };
        } on DioException catch (e) {
          return {
            "status": false,
            "message": ServerFailure.fromJson(e.response?.data).map[Failure.key],
          };
        }
      },
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    //Set the fit size (Find your UI design, look at the dimensions of the device screen and fill it in,unit in dp)
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return  MaterialApp(
          debugShowCheckedModeBanner: false,
          home: BottomSheetReviewRequest(),
        );
      },
    );
  }
}
