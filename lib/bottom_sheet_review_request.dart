
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetReviewRequest extends StatefulWidget {
  const BottomSheetReviewRequest({Key? key}) : super(key: key);

  @override
  State<BottomSheetReviewRequest> createState() => _BottomSheetReviewRequestState();
}

class _BottomSheetReviewRequestState extends State<BottomSheetReviewRequest> {
  @override

  final date = TextEditingController();
  final time = TextEditingController();
  final performanceLength = TextEditingController();
  final guaranteePrice = TextEditingController();
  final responseTime = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    date.text="Dec 24";
    time.text="7:30 pm";
    performanceLength.text="1.5 hrs";
    guaranteePrice.text="\$300";
    responseTime.text="48 hrs";
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          topRight: Radius.circular(20.r)),
                    ),
                    backgroundColor: AppColor.darkBlue,
                    isScrollControlled: true,
                    builder: (BuildContext context) {
                      return Container(
                        height: MediaQuery.of(context).size.height - 54.h,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.r),
                                  topRight: Radius.circular(20.r)),
                              child: Image.asset(
                                ImagesApp.ellipse,
                                fit: BoxFit.fill,
                              ),
                            ),
                            SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.only(left: 16.w),
                                            child: GestureDetector(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  "Cancel",
                                                  style: poppins.copyWith(
                                                      fontSize: 16.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: AppColor.blue),
                                                )),
                                          ),
                                        ),
                                        Text(
                                          "Review your request",
                                          style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Expanded(child: Container()),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 1,
                                    width: double.infinity,
                                    color: AppColor.white.withOpacity(0.07),
                                  ),
                                  SizedBox(
                                    height: 25.h,
                                  ),
                                  ClipRRect(
                                    child: Image.asset(
                                      ImagesApp.profileRequestDetails,
                                      width: 72.w,
                                      height: 72.r,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    EdgeInsets.only(top: 16.h, bottom: 4.h),
                                    child: Text(
                                      "Eric Johns",
                                      style: poppins.copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          width: 12.w,
                                          height: 12.h,
                                          child: SvgPicture.asset(
                                            ImagesApp.icLocation,
                                            color: AppColor.grey97,
                                            fit: BoxFit.cover,
                                          )),
                                      SizedBox(
                                        width: 7.5.w,
                                      ),
                                      Text(
                                        "Virginia Singer",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.grey97),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(12.r),
                                    margin: EdgeInsets.only(
                                        top: 23.h, bottom: 10.h,right: 16.w,left: 16.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(6.r),
                                          child: Image.asset(
                                            width: 44.w,
                                            height: 43.h,
                                            ImagesApp.backHouseofmusic,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Location",
                                              style: poppins.copyWith(
                                                  fontSize: 11.sp,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  color: AppColor.white.withOpacity(0.5)
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Text(
                                              "House of Music",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    color: AppColor.white.withOpacity(0.07),
                                    height: 1,
                                    width: double.infinity,
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10.h, bottom: 12.h,right: 15.5.w,left: 15.5.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 12.h,left: 12.w),
                                          child: Text(
                                            "Request Details",
                                            style: poppins.copyWith(
                                                fontSize: 16.sp,
                                                fontWeight:
                                                FontWeight.w500,
                                                color: AppColor.white
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.symmetric(vertical: 12.h),
                                          color: AppColor.white.withOpacity(0.07),
                                          height: 1,
                                          width: double.infinity,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 13.w,bottom: 8.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: date,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Date   ",
                                                    prefixIcon: ImagesApp.icCalendar,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 13.w,bottom: 8.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: time,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Time   ",
                                                    prefixIcon: ImagesApp.icClock,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 13.w,bottom: 8.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: performanceLength,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Performance length   ",
                                                    prefixIcon: ImagesApp.icClock,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(right: 12.w,left: 12.w,bottom: 12.h),
                                          decoration: BoxDecoration(
                                            color: AppColor.white.withOpacity(0.07),
                                            borderRadius: BorderRadius.circular(10.r),
                                          ),
                                          padding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 13.5.h),
                                          child: Row(children: [
                                            Container(width: 17.w,height: 17.r,child: SvgPicture.asset(ImagesApp.locationRound,color: AppColor.white,fit: BoxFit.cover)),
                                            SizedBox(width: 8.w,),
                                            Text("Location",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                                            Spacer(),
                                            Text("273-296 Geary St, Rich...",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),),
                                            SizedBox(width: 6.w,),
                                            Container(width: 16.w,height: 16.r,child: SvgPicture.asset(ImagesApp.arrowForward,color: AppColor.white,fit: BoxFit.cover)),
                                          ],),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 12.w,bottom: 2.h),
                                          child: Text("Message*",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey7E),),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 12.w,right: 12,bottom: 68.h),
                                          padding: EdgeInsets.only(right: 10.w,left: 10.w,top: 10.h,bottom: 20.h),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10.r),
                                              color: AppColor.white.withOpacity(0.07)
                                          ),
                                          child: Text(
                                            "We are interested in having you performing at our venue. I believe that your music would be a perfect fit for our audience.",
                                            style: inter.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(
                                        bottom: 12.h,right: 15.5.w,left: 15.5.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 13.w,bottom: 10.h,top: 18.h,),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: guaranteePrice,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Guarantee Price   ",
                                                    prefixIcon: ImagesApp.icUser,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 13.w,bottom: 14.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: responseTime,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Response Time   ",
                                                    prefixIcon: ImagesApp.icUser,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(
                                        right: 16.w,left: 16.w),
                                    padding: EdgeInsets.symmetric(horizontal: 11.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 15.5.h),
                                        child: Text("Offer Food & Beverage?",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,),),
                                      ),
                                      Spacer(),
                                      CustomSwitch(
                                          onToggled: (isToggled) {},
                                          isToggled: true)
                                    ],),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(top: 40.h,),
                                    color: AppColor.black48,
                                    height: 1,
                                    width: double.infinity,
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                                    width: double.infinity,
                                    color: AppColor.white.withOpacity(0.07),
                                    child: Column(
                                      children: [
                                        MyCustomButton(
                                          title: "Publish",
                                          onTap: () {},
                                          loading: false,
                                          color: AppColor.blue,
                                          width: double.infinity,
                                          height: 49.h,
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical: 23.h,),
                                          child: Text("Cancel event",style: poppins.copyWith(fontSize: 15.sp,fontWeight: FontWeight.w400,color: AppColor.redText),),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(16)
                    ),
                    child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

              ),
            ],
          ),
        ],
      ),
    );

  }
}
