import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:flutter_open_street_map/widgets/custom_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:latlong2/latlong.dart';
import 'package:vibe_app/core/helper/custom_button.dart';

import 'dart:async';
import 'dart:convert';

import 'package:flutter_map/plugin_api.dart';
import 'package:vibe_app/core/helper/custom_text_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/widget/create_event_bottom_sheet.dart';

class MapApp extends StatefulWidget {
  static var lat;
  static var long;
  static double? lati;
  static double? longi;

  final LatLong center;
  final void Function(PickedData pickedData) onPicked;
  final Future<LatLng> Function() onGetCurrentLocationPressed;
  final IconData zoomInIcon;
  final IconData zoomOutIcon;
  final IconData currentLocationIcon;
  final Color buttonColor;
  final Color buttonTextColor;
  final Color locationPinIconColor;
  final String buttonText;
  final String hintText;
  final bool isVisiblBtn;

  static Future<LatLng> nopFunction() {
    throw Exception("");
  }

  const MapApp({
    Key? key,
    required this.center,
    required this.onPicked,
    this.zoomOutIcon = Icons.zoom_out_map,
    this.zoomInIcon = Icons.zoom_in_map,
    this.currentLocationIcon = Icons.my_location,
    this.onGetCurrentLocationPressed = nopFunction,
    this.buttonColor = Colors.blue,
    this.locationPinIconColor = Colors.blue,
    this.buttonTextColor = Colors.white,
    this.buttonText = 'Set Current Location',
    this.hintText = 'Search Location',
    required this.isVisiblBtn,
  }) : super(key: key);

  @override
  State<MapApp> createState() => _MapAppState();
}

class _MapAppState extends State<MapApp> {
  late final Future<LatLng> Function() onGetCurrentLocationPressed;
  late final LatLong center;

  final TextEditingController _searchController = TextEditingController();

  final PopupController _popupController = PopupController();

  late List markers;

  var client = http.Client();

  MapController _mapController = MapController();

  var start_latitude = 42.839145508940945;
  var start_longitude = 50.36190067490672;

  List<LatLng> routpoints = [];
  bool isVisible = false;

  var _mylocationlatitude;
  var _mylocationlongitude;

  var select_latitude;

  var select_longitude;

  Future<Position> getUserCurrentLocation() async {
    await Geolocator.requestPermission()
        .then(
      (value) {},
    )
        .onError(
      (error, stackTrace) {
        print("error");
      },
    );
    return await Geolocator.getCurrentPosition();
  }

  final FocusNode _focusNode = FocusNode();
  List<OSMdata> _options = <OSMdata>[];
  Timer? _debounce;

  void setNameCurrentPos() async {
    double latitude = _mapController.center.latitude;
    double longitude = _mapController.center.longitude;
    if (kDebugMode) {
      print(latitude);
    }
    if (kDebugMode) {
      print(longitude);
    }
    String url =
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=$latitude&lon=$longitude&zoom=18&addressdetails=1';

    var response = await client.post(Uri.parse(url));
    var decodedResponse =
        jsonDecode(utf8.decode(response.bodyBytes)) as Map<dynamic, dynamic>;

    _searchController.text =
        decodedResponse['display_name'] ?? "MOVE TO CURRENT POSITION";
    setState(() {});
  }

  void setNameCurrentPosAtInit() async {
    double latitude = widget.center.latitude;
    double longitude = widget.center.longitude;
    if (kDebugMode) {
      print(latitude);
    }
    if (kDebugMode) {
      print(longitude);
    }
    String url =
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=$latitude&lon=$longitude&zoom=18&addressdetails=1';

    var response = await client.post(Uri.parse(url));
    var decodedResponse =
        jsonDecode(utf8.decode(response.bodyBytes)) as Map<dynamic, dynamic>;

    // _searchController.text =
    //     decodedResponse['display_name'] ?? "MOVE TO CURRENT POSITION";
    setState(() {});
  }

  @override
  void initState() {
    _mapController = MapController();

    setNameCurrentPosAtInit();

    _mapController.mapEventStream.listen((event) async {
      if (event is MapEventMoveEnd) {
        var client = http.Client();
        String url =
            'https://nominatim.openstreetmap.org/reverse?format=json&lat=${event.center.latitude}&lon=${event.center.longitude}&zoom=18&addressdetails=1';

        var response = await client.post(Uri.parse(url));
        var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes))
            as Map<dynamic, dynamic>;

        // _searchController.text = decodedResponse['display_name'];
        setState(() {});
      }
    });

    super.initState();

    getUserCurrentLocation().then((value) async {
      print('my location');
      print(value.latitude.toString() + " " + value.longitude.toString());
      setState(() {
        _mylocationlatitude = value.latitude;
        _mylocationlongitude = value.longitude;
        _mapController.move(LatLng(_mylocationlatitude, _mylocationlongitude),
            _mapController.zoom);
      });
    });
  }

  @override
  void dispose() {
    _mapController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            child: Stack(
              alignment: Alignment.bottomRight,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                  child: FlutterMap(
                    options: MapOptions(
                      center: LatLng(start_latitude, start_longitude),
                      minZoom: 4,
                      plugins: [MarkerClusterPlugin()],
                      onTap: (tapPosition, point) =>
                          _popupController.hideAllPopups(),
                    ),
                    mapController: _mapController,
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        subdomains: ['a', 'b', 'c'],
                        tilesContainerBuilder: darkModeTilesContainerBuilder,
                      ),
                      if (_mylocationlatitude != null)
                        MarkerClusterLayerOptions(
                          maxClusterRadius: 120,
                          disableClusteringAtZoom: 6,
                          size: Size(40, 10),
                          anchor: AnchorPos.align(AnchorAlign.center),
                          fitBoundsOptions:
                              FitBoundsOptions(padding: EdgeInsets.all(50)),
                          markers: [
                            Marker(
                              anchorPos: AnchorPos.align(AnchorAlign.center),
                              height: 30,
                              width: 30,
                              point: LatLng(
                                  _mylocationlatitude, _mylocationlongitude),
                              builder: (ctx) => Icon(
                                Icons.my_location,
                                color: AppColor.blue,
                              ),
                            ),
                            if (select_latitude != null)
                              Marker(
                                anchorPos: AnchorPos.align(AnchorAlign.center),
                                height: 30,
                                width: 30,
                                point:
                                    LatLng(select_latitude, select_longitude),
                                builder: (ctx) => Icon(
                                  Icons.pin_drop,
                                  color: AppColor.red,
                                ),
                              ),
                          ],
                          builder: (context, markers) {
                            return FloatingActionButton(
                              onPressed: null,
                              child: Text(markers.length.toString()),
                            );
                          },
                        ),
                    ],
                  ),
                ),
                Positioned.fill(
                    child: IgnorePointer(
                  child: Center(
                    child: Icon(
                      Icons.location_pin,
                      size: 50,
                      color: widget.locationPinIconColor,
                    ),
                  ),
                )),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    margin: EdgeInsets.all(15.h),
                    padding: EdgeInsets.symmetric(
                      horizontal: 14.w,
                    ),
                    decoration: BoxDecoration(
                      color: AppColor.white.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(9.r),
                    ),
                    child: Column(
                      children: [
                        TextFormField(
                            style: poppins.copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: AppColor.whiteE1),
                            textAlign: TextAlign.left,
                            controller: _searchController,
                            // focusNode: _focusNode,
                            decoration: InputDecoration(
                              hintText: widget.hintText,
                              hintStyle: poppins.copyWith(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColor.whiteE1),
                            ),
                            onChanged: (String value) {
                              if (_debounce?.isActive ?? false)
                                _debounce?.cancel();

                              _debounce = Timer(
                                  const Duration(milliseconds: 2000), () async {
                                if (kDebugMode) {
                                  print(value);
                                }
                                var client = http.Client();
                                try {
                                  String url =
                                      'https://nominatim.openstreetmap.org/search?q=$value&format=json&polygon_geojson=1&addressdetails=1';
                                  if (kDebugMode) {
                                    print(url);
                                  }
                                  var response =
                                      await client.post(Uri.parse(url));
                                  var decodedResponse = jsonDecode(
                                          utf8.decode(response.bodyBytes))
                                      as List<dynamic>;
                                  if (kDebugMode) {
                                    print(decodedResponse);
                                  }
                                  _options = decodedResponse
                                      .map((e) => OSMdata(
                                          displayname: e['display_name'],
                                          lat: double.parse(e['lat']),
                                          lon: double.parse(e['lon'])))
                                      .toList();
                                  setState(() {});
                                } finally {
                                  client.close();
                                }

                                setState(() {});
                              });
                            }),
                        Container(
                          margin: EdgeInsets.only(bottom: 15.h),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(9.r),
                                bottomRight: Radius.circular(9.r)),
                            color: AppColor.white.withOpacity(0.07),
                          ),
                          child: StatefulBuilder(builder: ((context, setState) {
                            return ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount:
                                    _options.length > 5 ? 5 : _options.length,
                                itemBuilder: (context, index) {
                                  return ListTile(
                                    title: Text(
                                      _options[index].displayname,
                                      style: poppins.copyWith(
                                          color: AppColor.white),
                                    ),
                                    subtitle: Text(
                                      '${_options[index].lat},${_options[index].lon}',
                                      style: poppins.copyWith(
                                          color: AppColor.grey7),
                                    ),
                                    onTap: () {
                                      _mapController.move(
                                          LatLng(_options[index].lat,
                                              _options[index].lon),
                                          15.0);

                                      _focusNode.unfocus();
                                      _options.clear();
                                      setState(() {});
                                    },
                                  );
                                });
                          })),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 22.h, right: 18.w),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          _mapController.move(_mapController.center,
                              _mapController.zoom - 0.25);
                        },
                        child: Container(
                            width: 36.r,
                            height: 36.r,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r),
                            ),
                            child: Icon(
                              size: 24,
                              Icons.remove,
                              color: AppColor.white,
                            )),
                      ),
                      SizedBox(
                        height: 18.h,
                      ),
                      GestureDetector(
                        onTap: () {
                          _mapController.move(_mapController.center,
                              _mapController.zoom + 0.25);
                        },
                        child: Container(
                            width: 36.r,
                            height: 36.r,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r),
                            ),
                            child: Container(
                                height: 24.r,
                                width: 24.r,
                                child: Icon(
                                  size: 24,
                                  Icons.add,
                                  color: AppColor.white,
                                ))),
                      ),
                      SizedBox(
                        height: 18.h,
                      ),
                      GestureDetector(
                        onTap: () async {
                          getUserCurrentLocation().then((value) async {
                            print('my location');
                            print(value.latitude.toString() +
                                " " +
                                value.longitude.toString());
                            setState(() {
                              _mylocationlatitude = value.latitude;
                              _mylocationlongitude = value.longitude;
                              _mapController.move(
                                  LatLng(_mylocationlatitude,
                                      _mylocationlongitude),
                                  _mapController.zoom);
                            });
                            var urlrouting = Uri.parse(
                                'http://router.project-osrm.org/route/v1/driving/$_mylocationlongitude,$_mylocationlatitude;$start_longitude,$start_latitude?steps=true&annotations=true&geometries=geojson&overview=full');
                            var response = await http.get(urlrouting);
                            print(response.body);
                            setState(() {
                              routpoints = [];
                              var ruter = jsonDecode(response.body)['routes'][0]
                                  ['geometry']['coordinates'];
                              for (int i = 0; i < ruter.length; i++) {
                                var reep = ruter[i].toString();
                                reep = reep.replaceAll("[", "");
                                reep = reep.replaceAll("]", "");
                                var lat1 = reep.split(',');
                                var long1 = reep.split(",");
                                routpoints.add(LatLng(double.parse(lat1[1]),
                                    double.parse(long1[0])));
                              }
                              isVisible = !isVisible;
                              print(routpoints);
                            });
                          });
                        },
                        child: Container(
                            width: 36.r,
                            height: 36.r,
                            decoration: BoxDecoration(
                              color: AppColor.white.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(8.r),
                            ),
                            child: Container(
                                width: 24.r,
                                height: 24.r,
                                child: Center(
                                    child: SvgPicture.asset(ImagesApp.icSend,
                                        fit: BoxFit.cover)))),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Visibility(
              visible: widget.isVisiblBtn,
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: AppColor.white.withOpacity(0.07),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: 16.w, vertical: 17.5.h),
                    color: AppColor.black48,
                    child: MyCustomButton(
                      title: "Set",
                      loading: false,
                      onTap: () async {

                        pickData().then((pickedData) {
                          // widget.onPicked;

                          setState(() {
                            select_latitude = pickedData.latLong.latitude;
                            select_longitude = pickedData.latLong.longitude;

                          });
print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
print(select_latitude);
print(select_longitude);
print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<object>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        });
                        Navigator.pop(context);

                        CreateEventBottomSheets.showDetailsMap(
                          context: context,
                          lat: 55,
                          long:55,
                        );
                      },
                      color: AppColor.blue,
                      width: double.infinity,
                      height: 49.h,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                      borderRadius: 48.sp,
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  Future<PickedData> pickData() async {
    LatLong center = LatLong(
        _mapController.center.latitude, _mapController.center.longitude);
    var client = http.Client();
    String url =
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=${_mapController.center.latitude}&lon=${_mapController.center.longitude}&zoom=18&addressdetails=1';

    var response = await client.post(Uri.parse(url));
    var decodedResponse =
        jsonDecode(utf8.decode(response.bodyBytes)) as Map<dynamic, dynamic>;
    String displayName = decodedResponse['display_name'];

    return PickedData(center, displayName);
  }
}

class OSMdata {
  final String displayname;
  final double lat;
  final double lon;

  OSMdata({required this.displayname, required this.lat, required this.lon});

  @override
  String toString() {
    return '$displayname, $lat, $lon';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is OSMdata && other.displayname == displayname;
  }

  @override
  int get hashCode => Object.hash(displayname, lat, lon);
}

class LatLong {
  final double latitude;
  final double longitude;

  LatLong(this.latitude, this.longitude);
}

class PickedData {
  final LatLong latLong;
  final String address;

  PickedData(this.latLong, this.address);
}
