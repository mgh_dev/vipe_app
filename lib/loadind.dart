import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_loading.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:vibe_app/futures/venue_flow/payment_failed_message_page/ui/payment_failed_message_page.dart';
import 'package:vibe_app/futures/venue_flow/payment_method_page/ui/payment_method_page.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';
import 'futures/constant_pages/choose_your_role_page/ui/bloc/change_role_view_model.dart';

class Loading extends StatefulWidget {
  Loading({Key? key}) : super(key: key);

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  final changeRoleBloc = ChangeRoleViewModel();

  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 2),
      () {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => PaymentFailedMessagePage()));
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>PaymentMethodPage()));
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              const Spacer(),
            const CustomLoading(),
              Text(
                "Processing Payment",
                style: poppins.copyWith(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w500,
                    color: AppColor.greyHint),
              ),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }
}
