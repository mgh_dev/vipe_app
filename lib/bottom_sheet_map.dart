
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/map.dart';
import 'package:vibe_app/test_map.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetMap extends StatefulWidget {
  BottomSheetMap({Key? key}) : super(key: key);

  @override
  State<BottomSheetMap> createState() => _BottomSheetMapState();
}

class _BottomSheetMapState extends State<BottomSheetMap> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                    },
                    child: SvgPicture.asset(ImagesApp.icBack)),
                Text(
                  "Subscription",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                SvgPicture.asset(ImagesApp.icNotification),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {

              showModalBottomSheet(
                enableDrag: false,
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                ),
                backgroundColor: AppColor.darkBlue,
                isScrollControlled: true,
                builder: (BuildContext context) {
                  return SizedBox(
                    height: MediaQuery.of(context).size.height - 101.h,
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.r),
                              topRight: Radius.circular(20.r)),
                          child: Image.asset(
                            ImagesApp.ellipse,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.r),
                                  topRight: Radius.circular(20.r)),),
                              height: MediaQuery.of(context).size.height - 101.h,
                              child: MapApp(
                                center: LatLong(33, 54),
                                isVisiblBtn: true,
                                onPicked: (pickedData) {
                                  print(pickedData.latLong.latitude);
                                  print(pickedData.latLong.longitude);
                                  print(pickedData.address);

                                  setState(() {
                                  });
                                },


                              )
                          ),
                        ),

                      ],
                    ),
                  );
                },
              );
            },
            child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16)
                ),
                child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

          ),
        ],
      ),
    );
  }
}
