import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/utils/app_color.dart';



bool showItems = false;

class DescriptionTextWidget extends StatefulWidget {
  final String text;
  final Widget child;

  DescriptionTextWidget({required this.text, required this.child});

  @override
  _DescriptionTextWidgetState createState() => _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  late String firstHalf;
  late String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 138) {
      firstHalf = widget.text.substring(0, 138);
      secondHalf = widget.text.substring(150, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16.5.w,
      ),
      child: secondHalf.isEmpty
          ? Text(firstHalf)
          : SingleChildScrollView(
              child: Column(
                children: [
                  Wrap(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 4.w),
                        child: Text(
                          flag ? ("$firstHalf...") : (firstHalf + secondHalf),
                          style: poppins.copyWith(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      InkWell(
                        child: Text(
                          flag ? "Read more" : " See Less",
                          style: poppins.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w400,
                              color: AppColor.blue),
                        ),
                        onTap: () {
                          setState(() {
                            flag = !flag;
                            showItems = !showItems;
                          });
                        },
                      ),
                      showItems
                          ? AnimatedContainer(
                              duration: Duration(seconds: showItems ? 3 : 2),
                              curve: Curves.bounceInOut,
                              child: widget.child,
                            )
                          : Container(),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
