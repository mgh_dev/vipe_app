
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/map.dart';
import 'package:vibe_app/test_map.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetDetailsMap extends StatefulWidget {
  BottomSheetDetailsMap({Key? key}) : super(key: key);

  @override
  State<BottomSheetDetailsMap> createState() => _BottomSheetDetailsMapState();
}

class _BottomSheetDetailsMapState extends State<BottomSheetDetailsMap> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                    },
                    child: SvgPicture.asset(ImagesApp.icBack)),
                Text(
                  "Subscription",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                SvgPicture.asset(ImagesApp.icNotification),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              String address = 'بلواردانشگاه, دانشگاه, شهر یزد, بخش مرکزی, شهرستان یزد, استان یزد, 89168-38576, ایران';
              List<String> addressParts = address.split(', ');

              String street = addressParts[0];
              String building = addressParts[1];
              String city = addressParts[2];
              String district = addressParts[3];
              String county = addressParts[4];
              String province = addressParts[5];
              String postalCode = addressParts[6];
              String country = addressParts[7];

              print(street); // بلواردانشگاه
              print(building); // دانشگاه
              print(city); // شهر یزد
              print(district); // بخش مرکزی
              print(county); // شهرستان یزد
              print(province); // استان یزد
              print(postalCode); // 89168-38576
              print(country); // ایران
              showModalBottomSheet(
                enableDrag: false,
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                ),
                backgroundColor: AppColor.darkBlue,
                isScrollControlled: true,
                builder: (BuildContext context) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 101.h,
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.r),
                              topRight: Radius.circular(20.r)),
                          child: Image.asset(
                            ImagesApp.ellipse,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: Column(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: AppColor.white.withOpacity(0.07),
                                        borderRadius: BorderRadius.circular(10.r),
                                        border: Border.all(color: AppColor.white.withOpacity(0.07),width: 1,)
                                      ),
                                      width: double.infinity,
                                      margin: EdgeInsets.only(right: 16.w,left: 16.w,top: 33.h,bottom: 27.5.h),
                                      padding: EdgeInsets.symmetric(vertical: 11.h),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(left: 12.w,bottom: 3.h),
                                            child: Text("Street",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 12.w,bottom: 10.h),
                                            child: Text("$street",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                          ),
                                          Container(width: double.infinity,height: 1,color: AppColor.white.withOpacity(0.07),),
                                          Padding(
                                            padding: EdgeInsets.only(left: 10.w,bottom: 19.5.h,top: 19.5.h),
                                            child: Text("Apt, suite, etc. (Optional)",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                          ),
                                          Container(width: double.infinity,height: 1,color: AppColor.white.withOpacity(0.07),),
                                          Padding(
                                            padding: EdgeInsets.only(left: 12.w,bottom: 3.h,top: 11.h),
                                            child: Text("City",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 12.w,bottom: 10.h),
                                            child: Text("$city",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                          ),
                                          Container(width: double.infinity,height: 1,color: AppColor.white.withOpacity(0.07),),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border(
                                                        right: BorderSide(color: AppColor.white.withOpacity(0.07),width: 1))
                                                  ),
                                                  child: Padding(
                                                    padding: EdgeInsets.only(left: 12.w,top: 11.h,bottom: 11.h),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text("State",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                                        SizedBox(height: 3.h,),
                                                        Text("CA",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              //Container(color: AppColor.white.withOpacity(0.07),width: 1,height: 163.h,),
                                              Expanded(
                                                child: Padding(
                                                  padding: EdgeInsets.only(left: 12.w,top: 11.h,bottom: 11.h),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text("Zip code",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                                      SizedBox(height: 3.h,),
                                                      Text("09584",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(width: double.infinity,height: 1,color: AppColor.white.withOpacity(0.07),),
                                          Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 12.w,bottom: 3.h,top: 11.h),
                                                    child: Text("Country / Region",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 12.w),
                                                    child: Text("$country",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                                  ),
                                                ],
                                              ),
                                              SvgPicture.asset(ImagesApp.arrowDown,fit: BoxFit.cover,color: AppColor.greyTxt,)
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(width: double.infinity,height: 1,color: AppColor.black48,margin: EdgeInsets.symmetric(horizontal: 23.75.w),),
                                    Padding(
                                      padding: EdgeInsets.only(top: 25.5.h,bottom: 21.75.h,right: 21.w,left: 16.w),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            children: [
                                              Text("Show your specific location",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.whiteEB),),
                                              SizedBox(height: 5.h,),
                                              Text(
                                                "Make it more clear to guests where your\nplace is located. We’ll never shere your\naddress untill a guest is booked",
                                                style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),
                                              ),

                                            ],
                                          ),
                                          CustomSwitch(onToggled: (isToggled) {}, isToggled: true)
                                        ],
                                      ),
                                    )
                                  ],
                                )
                              ),
                            ),
                            Column(
                              children: [
                                Container(width: double.infinity,height: 1,color: AppColor.black48,),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                                  color: AppColor.white.withOpacity(0.07),
                                  child: MyCustomButton(
                                    title: "Next",
                                    loading: false,
                                    onTap: () {},
                                    color: AppColor.blue,
                                    width: double.infinity,
                                    height: 49.h,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    borderRadius: 48.r,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),

                      ],
                    ),
                  );
                },
              );
            },
            child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16)
                ),
                child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

          ),
        ],
      ),
    );
  }
}
