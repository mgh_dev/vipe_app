import 'package:cell_calendar/cell_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/sample_event.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class ManageCalendar extends StatefulWidget {
  const ManageCalendar({super.key});

  @override
  State<ManageCalendar> createState() => _ManageCalendarState();
}

class _ManageCalendarState extends State<ManageCalendar> {
  @override
  final events = sampleEvents();
  final cellCalendarPageController = CellCalendarPageController();

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Manage Calendar",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding:
                        EdgeInsets.symmetric(horizontal: 24.w, vertical: 16.h),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Block/Open all days",
                                  style: poppins.copyWith(
                                      fontSize: 14.sp, fontWeight: FontWeight.w400),
                                ),
                                Text(
                                  "Show your availability by selecting the specific\ndate(s) you would like to open or close.",
                                  style: poppins.copyWith(
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.grey2),
                                ),
                              ],
                            ),
                            Spacer(),
                            CustomSwitch(
                              isToggled: true,
                              onToggled: (isToggled) {},
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 12.h,
                      ),

                      Container(
                        height: 584.h,
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(18.r)),
                        child: CellCalendar(
                          todayTextColor: AppColor.white,
                          todayMarkColor: AppColor.white.withOpacity(0.2),
                          dateTextStyle: poppins.copyWith(
                              fontSize: 10.sp, fontWeight: FontWeight.w400),
                          cellCalendarPageController: cellCalendarPageController,
                          events: events,
                          daysOfTheWeekBuilder: (dayIndex) {
                            final labels = [
                              "SU",
                              "MON",
                              "TUE",
                              "WED",
                              "THU",
                              "FRI",
                              "SAT"
                            ];
                            return Padding(
                              padding: EdgeInsets.only(bottom: 6.5.h),
                              child: Text(
                                labels[dayIndex],
                                style: poppins.copyWith(
                                    fontSize: 11.sp,
                                    fontWeight: FontWeight.w400,
                                    color: AppColor.grey2),
                                textAlign: TextAlign.center,
                              ),
                            );
                          },
                          monthYearLabelBuilder: (datetime) {
                            final year = datetime!.year.toString();
                            final month = datetime.month.monthName;
                            return Padding(
                              padding: EdgeInsets.symmetric(vertical: 19.h),
                              child: Row(
                                children: [
                                  SizedBox(width: 16.h),
                                  Text(
                                    "$month  $year",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  const Spacer(),
                                  GestureDetector(
                                    onTap: (){
                                      cellCalendarPageController.animateToDate(
                                        DateTime.now(),
                                        curve: Curves.linear,
                                        duration:
                                        const Duration(milliseconds: 300),
                                      );
                                    },
                                    child: Container(
                                      height: 40.h,
                                        width: 40.h,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10.r),
                                        color: AppColor.white.withOpacity(0.07)
                                      ),
                                      child: Center(
                                        child: SvgPicture.asset(
                                            ImagesApp.icCalendar,
                                            color: AppColor.white,
                                            fit: BoxFit.cover,
                                          height: 20.h,
                                          width: 20.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10.h),
                                  /*IconButton(
                                    padding: EdgeInsets.zero,
                                    icon: const Icon(Icons.calendar_today),
                                    onPressed: () {
                                      cellCalendarPageController.animateToDate(
                                        DateTime.now(),
                                        curve: Curves.linear,
                                        duration:
                                        const Duration(milliseconds: 300),
                                      );
                                    },
                                  )*/
                                ],
                              ),
                            );
                          },
                          onCellTapped: (date) {
                            final eventsOnTheDate = events.where((event) {
                              final eventDate = event.eventDate;
                              return eventDate.year == date.year &&
                                  eventDate.month == date.month &&
                                  eventDate.day == date.day;
                            }).toList();
                            showDialog(
                                context: context,
                                builder: (_) => Container(
                                  child: AlertDialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.r)),
                                    backgroundColor: AppColor.darkBlue,
                                    title: Text(
                                      "${date.month.monthName} ${date.day}",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.purple),
                                    ),
                                    content: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: eventsOnTheDate
                                          .map(
                                            (event) => Container(
                                              decoration: BoxDecoration(
                                                color: AppColor.blue,
                                                borderRadius: BorderRadius.circular(6.r)
                                              ),
                                          width: double.infinity,
                                          padding: EdgeInsets.all(4.h),
                                          margin: EdgeInsets.only(
                                              bottom: 6.h),
                                          child: Text(
                                            event.eventName,
                                            style: poppins,
                                          ),
                                        ),
                                      )
                                          .toList(),
                                    ),
                                  ),
                                ));
                          },
                            onPageChanged: (firstDate, lastDate) {
                              print("This is the first date of the new page: $firstDate");
                              print("This is the last date of the new page: $lastDate");
                            }
                          /*onPageChanged: (firstDate, lastDate) {
                            /// Called when the page was changed
                            /// Fetch additional events by using the range between [firstDate] and [lastDate] if you want
                           /// CalendarDatePicker(initialDate: DateTime.now(), firstDate: DateTime.now().add(const Duration(days: -3)), lastDate: DateTime.now().add(const Duration(days: 2)), onDateChanged: (value) {},);
                          },*/
                        ),
                      ),

                      SizedBox(
                        height: 20.h,
                      ),
                    ],
                  ),
                ),
              ),

              Column(
                children: [
                  Container(
                    color: AppColor.black48,
                    height: 1,
                    width: double.infinity,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: 16.w, vertical: 17.5.h),
                    width: double.infinity,
                    color: AppColor.white.withOpacity(0.07),
                    child: MyCustomButton(
                      title: "Block",
                      onTap: () {},
                      loading: false,
                      color: AppColor.grey8F,
                      width: double.infinity,
                      height: 49.h,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
