import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import 'core/utils/app_color.dart';
import 'core/utils/image_app.dart';
import 'core/utils/style_text_app.dart';


class IncomingRequestsTab extends StatefulWidget {
   IncomingRequestsTab({required this.scrollController}) : super();
  ScrollController scrollController;

  @override
  State<IncomingRequestsTab> createState() => _IncomingRequestsTabState();
}

class _IncomingRequestsTabState extends State<IncomingRequestsTab> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal:16.w  ),
      child: Container(
        width: 332,
        decoration: BoxDecoration(
          color: AppColor.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(18.r),
        ),
        child: Column(
          children: [
            SizedBox(height: 27.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.h),
              child: Text(
                "View and manage all incoming requests from artists.",
                style: poppins.copyWith(
                    fontSize: 11.sp,
                    fontWeight: FontWeight.w500,
                    color: AppColor.greyHint),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: ListView.builder(
                controller: widget.scrollController,
                physics: const BouncingScrollPhysics(),
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Container(
                    width: double.infinity,
                    height: 180.h,
                    margin: EdgeInsets.only(
                        bottom: 8.h, left: 5.w, right: 5.w),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(12.r)),
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {},
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 22.w,
                                    top: 17.5.h,
                                    bottom: 14.5.h),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                        ImagesApp.icClock),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      "24 Aug",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight:
                                          FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                width: double.infinity,
                                color:
                                AppColor.white.withOpacity(0.07),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 46.6.w,
                                    top: 15.5.h,
                                    bottom: 14.5.h),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      ImagesApp.profileRequest,
                                      width: 48.w,
                                      height: 48.r,
                                    ),
                                    SizedBox(
                                      width: 16.w,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .spaceBetween,
                                      children: [
                                        Text(
                                          "Audrey Soul",
                                          style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight:
                                              FontWeight.w500),
                                        ),
                                        Text(
                                          "\$599",
                                          style: poppins.copyWith(
                                              fontSize: 14.sp,
                                              fontWeight:
                                              FontWeight.w400,
                                              color: AppColor.grey2),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                width: double.infinity,
                                color:
                                AppColor.white.withOpacity(0.20),
                              ),
                              SizedBox(
                                height: 46.h,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    GestureDetector(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 41.w,
                                            top: 12.h,
                                            bottom: 13.h),
                                        child: Row(
                                          children: [
                                            SvgPicture.asset(
                                                ImagesApp.icMessage),
                                            SizedBox(
                                              width: 8.w,
                                            ),
                                            Text(
                                              "Message",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  color: AppColor
                                                      .whiteF8),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      child: Container(
                                        color: AppColor.white
                                            .withOpacity(0.20),
                                        width: 1,
                                      ),
                                    ),
                                    GestureDetector(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: 12.h,
                                            bottom: 13.h,
                                            right: 41.w),
                                        child: Row(
                                          children: [
                                            SvgPicture.asset(
                                                ImagesApp.check),
                                            SizedBox(
                                              width: 8.w,
                                            ),
                                            Text(
                                              "Decline",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  color: AppColor
                                                      .blueText2),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
