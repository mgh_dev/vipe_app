import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class UploadFileBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class UploadFileInitialState extends UploadFileBaseState {}

class UploadFileLoadingState extends UploadFileBaseState {}

class UploadFileSuccessState extends UploadFileBaseState {}

class UploadFileFailState extends UploadFileBaseState {
  final String message;

  UploadFileFailState({required this.message});
}
