import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:vibe_app/constant_bloc/upload_file_logic/state/upload_file_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../core/config/web_service.dart';


class UploadFileViewModel extends Cubit<UploadFileBaseState> {
  UploadFileViewModel() : super(UploadFileInitialState());



  void uploadImageGallery(List<String> image) async {
    print("1");
    emit(UploadFileLoadingState());
    print("3");
    try {

      image.forEach((element) async {
        emit(UploadFileLoadingState());
        print("{${element}}");
        FormData formData = FormData.fromMap({
          'file': await MultipartFile.fromFile(element),
        });
        var response =
        await WebService().dio.post("files/upload",data: formData);
        emit(UploadFileSuccessState());
      });

      print("4");

      //

      // print("5");
      // print("6");

      print("7");
      emit(UploadFileSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(UploadFileFailState(message: e.toString()));
      print("10");
    }
  }

}
