import 'package:bloc/bloc.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/logic/state/get_users_state.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/model/response_model/response_get_profile_fan_model.dart';
import 'package:vibe_app/core/config/web_service.dart';

class GetUsersViewModel extends Cubit<GetUsersBaseState> {
  GetUsersViewModel() : super(GetUsersInitialState());

  void get(int typeUsers) async {
    print("1");
    emit(GetUsersLoadingState());
    print("3");
    try {
      print("4");
      var response = await WebService().dio.get("users?type=$typeUsers");
      print("5");
      GetUsers getUsers = GetUsers.fromJson(response.data);
      print("6");

      print("7");
      emit(GetUsersSuccessState(response: getUsers));
      print("8");
    } catch (e) {
      print("9");
      emit(GetUsersFailState(message: e.toString()));
      print("10");
    }
  }
}
