import 'package:equatable/equatable.dart';
import 'package:vibe_app/constant_bloc/get_artist_users/model/response_model/response_get_profile_fan_model.dart';

abstract class GetUsersBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class GetUsersInitialState extends GetUsersBaseState {}

class GetUsersLoadingState extends GetUsersBaseState {}

class GetUsersSuccessState extends GetUsersBaseState {
  final GetUsers response;

  GetUsersSuccessState({required this.response});
}

class GetUsersFailState extends GetUsersBaseState {
  final String message;


  GetUsersFailState({required this.message});
}
