class GetUsers {
  GetUsers({
    required this.data,
    required this.meta,
  });
  late final List<Data> data;
  late final Meta meta;

  GetUsers.fromJson(Map<String, dynamic> json){
    data = List.from(json['data']).map((e)=>Data.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.map((e)=>e.toJson()).toList();
    _data['meta'] = meta.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.type,
    required this.email,
    this.avatar,
  });
  late final int id;
  late final String firstName;
  late final String lastName;
  late final String type;
  late final String email;
  late final String? avatar;

  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    type = json['type'];
    email = json['email'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['type'] = type;
    _data['email'] = email;
    _data['avatar'] = avatar;
    return _data;
  }
}

class Meta {
  Meta({
    required this.pagination,
  });
  late final Pagination pagination;

  Meta.fromJson(Map<String, dynamic> json){
    pagination = Pagination.fromJson(json['pagination']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pagination'] = pagination.toJson();
    return _data;
  }
}

class Pagination {
  Pagination({
    required this.total,
    required this.count,
    required this.perPage,
    required this.currentPage,
    required this.totalPages,
    required this.links,
  });
  late final int total;
  late final int count;
  late final int perPage;
  late final int currentPage;
  late final int totalPages;
  late final Links links;

  Pagination.fromJson(Map<String, dynamic> json){
    total = json['total'];
    count = json['count'];
    perPage = json['perPage'];
    currentPage = json['currentPage'];
    totalPages = json['totalPages'];
    links = Links.fromJson(json['links']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total'] = total;
    _data['count'] = count;
    _data['perPage'] = perPage;
    _data['currentPage'] = currentPage;
    _data['totalPages'] = totalPages;
    _data['links'] = links.toJson();
    return _data;
  }
}

class Links {
  Links();

  Links.fromJson(Map json);

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    return _data;
  }
}