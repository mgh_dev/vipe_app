import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/select_item_state.dart';



class SelectItemViewModel extends Cubit<SelectItemBaseState> {
  SelectItemViewModel() : super(ItemChangeState(index: 0));

  void changeItem(int i) {
    emit(ItemChangeState(index: i));
  }
}
