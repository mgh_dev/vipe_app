abstract class SelectItemBaseState {}

class ItemChangeState extends SelectItemBaseState{
  final int index;

  ItemChangeState({required this.index});
}
