import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/validate_state.dart';



class ViewModelValidate extends Cubit<ValidateBaseState> {
  ViewModelValidate() : super(checkValueState(value:false));

  void check(bool value) {
    emit(checkValueState(value: value));
  }
}
