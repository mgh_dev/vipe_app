import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/logic/state/get_vibe_state.dart';
import 'package:vibe_app/constant_bloc/upload_avatat_logic/state/upload_avatar_state.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';




import '../../../core/config/web_service.dart';


class UploadAvatarViewModel extends Cubit<UploadAvatarBaseState> {
  UploadAvatarViewModel() : super(UploadAvatarInitialState());

  Future uploadAvatar(String  avatar) async {
    print("1");
    emit(UploadAvatarLoadingState());
    print("3");
    try {
      print("4");
      FormData formData = FormData.fromMap({
        'avatar': await MultipartFile.fromFile(avatar),
      });
      var response =
          await WebService().dio.post("auth/upload-avatar",data: formData);
      print("5");
      print("6");
     
      print("7");
      emit(UploadAvatarSuccessState());
      print("8");
    } catch (e) {
      print("9");
      emit(UploadAvatarFailState(message: e.toString()));
      print("10");
    }
  }

  // void uploadImageGallery(List<String> image) async {
  //   print("1");
  //   emit(UploadFileLoadingState());
  //   print("3");
  //   try {
  //
  //     image.forEach((element) async {
  //       print("{${element}}");
  //       FormData formData = FormData.fromMap({
  //         'avatar': await MultipartFile.fromFile(element),
  //       });
  //       var response =
  //       await WebService().dio.post("files/upload",data: formData);
  //     });
  //
  //     print("4");
  //
  //     //
  //
  //     // print("5");
  //     // print("6");
  //
  //     print("7");
  //     emit(UploadFileSuccessState());
  //     print("8");
  //   } catch (e) {
  //     print("9");
  //     emit(UploadFileFailState(message: e.toString()));
  //     print("10");
  //   }
  // }

}
