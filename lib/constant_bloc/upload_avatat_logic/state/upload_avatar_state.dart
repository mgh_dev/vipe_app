import 'package:equatable/equatable.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

abstract class UploadAvatarBaseState extends Equatable {
  @override
  List<Object> get props => [];
}

class UploadAvatarInitialState extends UploadAvatarBaseState {}

class UploadAvatarLoadingState extends UploadAvatarBaseState {}

class UploadAvatarSuccessState extends UploadAvatarBaseState {}

class UploadAvatarFailState extends UploadAvatarBaseState {
  final String message;

  UploadAvatarFailState({required this.message});
}
