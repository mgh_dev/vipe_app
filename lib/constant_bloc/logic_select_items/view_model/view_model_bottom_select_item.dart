import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/select_item_state.dart';



class SelectListItemViewModel extends Cubit<SelectItemListBaseState> {
  SelectListItemViewModel() : super(ItemChangeListState(index: []));

  void changeItem(int i) {
    List<int> ls=[];
    ls.add(i);
    emit(ItemChangeListState(index:ls));
  }
}
