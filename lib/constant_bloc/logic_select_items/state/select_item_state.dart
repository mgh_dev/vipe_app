abstract class SelectItemListBaseState {}

class ItemChangeListState extends SelectItemListBaseState{
  final List<int> index;

  ItemChangeListState({required this.index});
}
