import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/edit_filde_state.dart';



class ViewModelEditFiled extends Cubit<EditFiledBaseState> {
  ViewModelEditFiled() : super(EditFiledValueState(value:false));

  void editFiled(bool value) {
    emit(EditFiledValueState(value: value));
  }
}
