
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';



import 'core/helper/time_picker.dart';

class ChooseDeliveryType extends StatefulWidget {
  const ChooseDeliveryType({Key? key}) : super(key: key);
  static const routeName = 'ChooseDeliveryType';

  @override
  State<ChooseDeliveryType> createState() => _ChooseDeliveryTypeState();
}

class _ChooseDeliveryTypeState extends State<ChooseDeliveryType> {
  DateTime? _chosenDateTime;

  void change() {
    setState(() {});
  }

  late String birthdayData = "2001/08/08";
  final DateTime _selectedDateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text('choose_delivery_type'),
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: const Icon(
            Icons.arrow_back_ios,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            const SizedBox(
              height: 3,
            ),
            GestureDetector(
              onTap: () {

              },
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                child: ListTile(
                  title: Text('scheduled_delivery'),
                  trailing: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.arrow_forward_ios_rounded,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 53,
            ),

          ],
        ),
      ),
    );
  }


}
