
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/state/select_item_state.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetEventDetails extends StatefulWidget {
  BottomSheetEventDetails({Key? key}) : super(key: key);

  @override
  State<BottomSheetEventDetails> createState() => _BottomSheetEventDetailsState();
}

class _BottomSheetEventDetailsState extends State<BottomSheetEventDetails> {
  @override

  final viewChaneCheckBox = SelectItemViewModel();

  final openMicNight = TextEditingController();
  final date = TextEditingController();
  final time = TextEditingController();
  final ticketLink = TextEditingController();
  final ticketPrice = TextEditingController();
  final responseTime = TextEditingController();
  final eventPhoto = TextEditingController();


  List<String> texts = [
    "Energetic",
    "Nostalgic",
    "Electric"
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    openMicNight.text="";
    date.text="Dec 24";
    time.text="7:30 pm";
    ticketLink.text="7Eventbrite.com/jfurjry4h/ltot";
    ticketPrice.text="\$55";
    responseTime.text="48 hrs";
    eventPhoto.text="";
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          topRight: Radius.circular(20.r)),
                    ),
                    backgroundColor: AppColor.darkBlue,
                    isScrollControlled: true,
                    builder: (BuildContext context) {
                      return Container(
                        height: MediaQuery.of(context).size.height - 54.h,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.r),
                                  topRight: Radius.circular(20.r)),
                              child: Image.asset(
                                ImagesApp.ellipse,
                                fit: BoxFit.fill,
                              ),
                            ),
                            SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.only(left: 16.w),
                                            child: GestureDetector(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  "Cancel",
                                                  style: poppins.copyWith(
                                                      fontSize: 16.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: AppColor.blue),
                                                )),
                                          ),
                                        ),
                                        Text(
                                          "Review your request",
                                          style: poppins.copyWith(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Expanded(child: Container()),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 1,
                                    width: double.infinity,
                                    color: AppColor.white.withOpacity(0.07),
                                  ),

                                  SizedBox(height: 17.h),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 32.w),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                      Text("Performance details",style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w500,color: AppColor.greyHint),),
                                      Container(width: 20.w,height: 20.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                    ],),
                                  ),
                                  SizedBox(height: 15.h),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 12.w),
                                    child: SizedBox(
                                      height: 120.h,
                                      child: ListView.builder(
                                        physics: AlwaysScrollableScrollPhysics(),
                                        padding: EdgeInsets.zero,
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: 3,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              Container(
                                                height: 120.h,
                                                child: Padding(
                                                  padding: EdgeInsets.only(right: 17.5.w,left: 17.5.w,bottom: 14.h),
                                                  child: Column(
                                                    children: [
                                                      ClipRRect(
                                                        borderRadius: BorderRadius.circular(100.r),
                                                        child: Image.asset(
                                                          ImagesApp.profileRequestDetails,
                                                          width: 62.r,
                                                          height: 62.r,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                      SizedBox(height: 4.h,),
                                                      Text("Eric Johns",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400),),
                                                      SizedBox(height: 2.h,),
                                                      Text("Rock & Pop",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w500,color: AppColor.grey81),),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 4.w,)
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.all(12.r),
                                    margin: EdgeInsets.only(
                                        top: 12.h, bottom: 10.h,right: 16.w,left: 16.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(6.r),
                                          child: Image.asset(
                                            width: 44.w,
                                            height: 43.h,
                                            ImagesApp.backHouseofmusic,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Location",
                                              style: poppins.copyWith(
                                                  fontSize: 11.sp,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  color: AppColor.white.withOpacity(0.5)
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Text(
                                              "House of Music",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),

                                  Container(
                                    color: AppColor.white.withOpacity(0.07),
                                    height: 1,
                                    width: double.infinity,
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10.h,right: 15.5.w,left: 15.5.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 8.h,left: 12.w,bottom: 8.h),
                                          child: Text(
                                            "Event Details",
                                            style: poppins.copyWith(
                                                fontSize: 24.sp,
                                                fontWeight:
                                                FontWeight.w400,
                                                color: AppColor.white
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 12.h),
                                          color: AppColor.white.withOpacity(0.07),
                                          height: 1,
                                          width: double.infinity,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 11.w,bottom: 6.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: openMicNight,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Open Mic Night   ",
                                                    prefixIcon: ImagesApp.icUser,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 11.w,bottom: 6.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: date,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Date   ",
                                                    prefixIcon: ImagesApp.icCalendar,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 11.w,bottom: 6.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: time,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Time   ",
                                                    prefixIcon: ImagesApp.icClock,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),

                                        Container(
                                          margin: EdgeInsets.only(right: 13.w,left: 11.w,bottom: 6.h),
                                          decoration: BoxDecoration(
                                            color: AppColor.white.withOpacity(0.07),
                                            borderRadius: BorderRadius.circular(10.r),
                                          ),
                                          padding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 13.5.h),
                                          child: Row(children: [
                                            Container(width: 17.w,height: 17.r,child: SvgPicture.asset(ImagesApp.locationRound,color: AppColor.white,fit: BoxFit.cover)),
                                            SizedBox(width: 8.w,),
                                            Text("Location",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                                            Spacer(),
                                            Text("273-296 Geary St, Rich...",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),),
                                            SizedBox(width: 6.w,),
                                            Container(width: 16.w,height: 16.r,child: SvgPicture.asset(ImagesApp.arrowForward,color: AppColor.white,fit: BoxFit.cover)),
                                          ],),
                                        ),

                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 11.w,bottom: 6.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: ticketLink,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Ticket Link   ",
                                                    prefixIcon: ImagesApp.piano,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(right: 22.w,left: 11.w,bottom: 16.h),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: AppColor.white.withOpacity(0.07),
                                                    borderRadius: BorderRadius.circular(10.r),
                                                  ),
                                                  child: CustomTextField(
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                    controller: ticketPrice,
                                                    align:TextAlign.end,
                                                    prefixTitle: "Ticket price   ",
                                                    prefixIcon: ImagesApp.ticketStar,
                                                  ),

                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: 21.w),
                                                child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                              ),
                                            ],
                                          ),
                                        ),

                                        Padding(
                                          padding: EdgeInsets.only(left: 11.w,bottom: 8.h),
                                          child: Text("About this event",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey7E),),
                                        ),
                                        Container(
                                          //height: 73.h,
                                          margin: EdgeInsets.only(left: 11.w,right: 13,bottom: 13.h),
                                          padding: EdgeInsets.only(right: 10.w,left: 10.w,top: 10.h,bottom: 10.h),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10.r),
                                            color: AppColor.white.withOpacity(0.07)
                                          ),
                                          child: Text(
                                            "We are interested in having you performing at our venue. I believe that your music would be a perfect fit for our audience.",
                                            style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w400,color: AppColor.grey2),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),

                                  Padding(
                                    padding: EdgeInsets.only(right: 37.5.w,left: 15.5.w,bottom: 8.h,top: 16.h,),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: AppColor.white.withOpacity(0.07),
                                              borderRadius: BorderRadius.circular(10.r),
                                            ),
                                            child: CustomTextField(
                                              contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                              controller: responseTime,
                                              align:TextAlign.end,
                                              prefixTitle: "Response Time   ",
                                              prefixIcon: ImagesApp.icUser,
                                              //hint: "Response Time",
                                              //prefixIcon: ,

                                              //icon: ImagesApp.icPassT,
                                            ),

                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 21.w),
                                          child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                        ),
                                      ],
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(
                                        right: 16.w,left: 16.w),
                                    padding: EdgeInsets.symmetric(horizontal: 11.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 15.5.h),
                                        child: Text("Offer Food & Beverage?",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,),),
                                      ),
                                      Spacer(),
                                      CustomSwitch(
                                          onToggled: (isToggled) {},
                                          isToggled: true)
                                    ],),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: 37.5.w,left: 15.5.w,bottom: 11.h,top: 10.h,),
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 11.w,),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Vibe",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500),),
                                          Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 15.5.w),
                                    child: GenerateChipe(
                                      texts: texts,
                                      ///borderChip
                                      // border: Border.all(width: 0.5,color: AppColor.white.withOpacity(0.2)),
                                      padding: EdgeInsets.symmetric(horizontal: 15.w,vertical: 6.h),
                                    ),
                                  ),

                                  Padding(
                                    padding: EdgeInsets.only(right: 37.5.w,left: 15.5.w,bottom: 6.h,top: 3.h,),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: AppColor.white.withOpacity(0.07),
                                              borderRadius: BorderRadius.circular(10.r),
                                            ),
                                            //padding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                            child: CustomTextField(
                                                contentPadding: EdgeInsets.symmetric(horizontal: 11.w,vertical: 15.5.h),
                                                controller: eventPhoto,
                                                align:TextAlign.end,
                                                prefixTitle: "Event photo   ",
                                              prefixIcon: ImagesApp.icUser,
                                            ),

                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 21.w),
                                          child: Container(width: 20.w,height: 20.7.r,child: SvgPicture.asset(ImagesApp.editText,fit: BoxFit.cover)),
                                        ),
                                      ],
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                                    padding: EdgeInsets.symmetric(horizontal: 11.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 15.5.h),
                                        child: Text("Offer Food & Beverage?",style: poppins.copyWith(fontSize: 14.sp,fontWeight: FontWeight.w500,),),
                                      ),
                                      Spacer(),
                                      CustomSwitch(
                                          onToggled: (isToggled) {},
                                          isToggled: false)
                                    ],),
                                  ),
                                  SizedBox(height: 6.h,),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 27.5.w),
                                    child: Text("Age limit",style: poppins.copyWith(fontSize: 13.sp,fontWeight: FontWeight.w400,color: AppColor.greyText),),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 27.5.w,right: 65.5.w,top: 5.5.h),
                                    child: BlocBuilder(
                                        bloc: viewChaneCheckBox,
                                        builder: (context, state) {
                                          if (state is ItemChangeState) {
                                            return Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () =>
                                                      viewChaneCheckBox.changeItem(0),
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: 22.w,
                                                        height: 22.r,
                                                        decoration: BoxDecoration(
                                                          border: Border.all(
                                                            width: 1.w,
                                                            color: state.index == 0
                                                                ? AppColor.greyBlue3
                                                                : AppColor.black48,
                                                          ),
                                                          shape: BoxShape.circle,
                                                        ),
                                                        child: Center(
                                                          child: Container(
                                                            width: 14.w,
                                                            height: 14.r,
                                                            decoration: BoxDecoration(
                                                              color: state.index == 0
                                                                  ? AppColor.greyBlue3
                                                                  : Colors.transparent,
                                                              shape: BoxShape.circle,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 8.w,
                                                      ),
                                                      Text(
                                                        "No Age Limit",
                                                        style: poppins.copyWith(
                                                          fontSize: 16.sp,
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                const Spacer(),
                                                Row(
                                                  children: [
                                                    GestureDetector(
                                                      onTap: () =>
                                                          viewChaneCheckBox.changeItem(1),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            width: 22.w,
                                                            height: 22.r,
                                                            decoration: BoxDecoration(
                                                              border: Border.all(
                                                                width: 1.w,
                                                                color: state.index == 1
                                                                    ? AppColor.greyBlue3
                                                                    : AppColor.black48,
                                                              ),
                                                              shape: BoxShape.circle,
                                                            ),
                                                            child: Center(
                                                              child: Container(
                                                                width: 14.w,
                                                                height: 14.r,
                                                                decoration: BoxDecoration(
                                                                  color: state.index == 1
                                                                      ? AppColor.greyBlue3
                                                                      : Colors.transparent,
                                                                  shape: BoxShape.circle,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 8.w,
                                                          ),
                                                          Text(
                                                            "18+",
                                                            style: poppins.copyWith(
                                                              fontSize: 16.sp,
                                                              fontWeight: FontWeight.bold,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 8.w,
                                                    ),
                                                    GestureDetector(
                                                      onTap: () =>
                                                          viewChaneCheckBox.changeItem(2),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            width: 22.w,
                                                            height: 22.r,
                                                            decoration: BoxDecoration(
                                                              border: Border.all(
                                                                width: 1.w,
                                                                color: state.index == 2
                                                                    ? AppColor.greyBlue3
                                                                    : AppColor.black48,
                                                              ),
                                                              shape: BoxShape.circle,
                                                            ),
                                                            child: Center(
                                                              child: Container(
                                                                width: 14.w,
                                                                height: 14.r,
                                                                decoration: BoxDecoration(
                                                                  color: state.index == 2
                                                                      ? AppColor.greyBlue3
                                                                      : Colors.transparent,
                                                                  shape: BoxShape.circle,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 8.w,
                                                          ),
                                                          Text(
                                                            "21+",
                                                            style: poppins.copyWith(
                                                              fontSize: 16.sp,
                                                              fontWeight: FontWeight.bold,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            );
                                          }
                                          return const SizedBox();
                                        }),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(top: 18.5.h,),
                                    color: AppColor.black48,
                                    height: 1,
                                    width: double.infinity,
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                                    width: double.infinity,
                                    color: AppColor.white.withOpacity(0.07),
                                    child: Column(
                                      children: [
                                        MyCustomButton(
                                          title: "Save & Submit",
                                          onTap: () {},
                                          loading: false,
                                          color: AppColor.blue,
                                          width: double.infinity,
                                          height: 49.h,
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical: 23.h,),
                                          child: Text("Cancel event",style: poppins.copyWith(fontSize: 15.sp,fontWeight: FontWeight.w400,color: AppColor.redText),),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(16)
                    ),
                    child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

              ),
            ],
          ),
        ],
      ),
    );

  }
}