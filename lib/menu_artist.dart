import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/core/helper/divider.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/artist_flow/calender_artisr_page/ui/calender_artist_page.dart';
import 'package:vibe_app/futures/artist_flow/gig_request_artist/ui/gig_request_artist_page.dart';
import 'package:vibe_app/futures/artist_flow/message_artist_page/ui/messages_page_artist.dart';
import 'package:vibe_app/futures/artist_flow/settimgs_artist_page/ui/settings_artist_page.dart';
import 'package:vibe_app/futures/venue_flow/request_list_page/request_list_page.dart';
import 'package:vibe_app/futures/venue_flow/status_request_page/status_request_page.dart';
import 'package:vibe_app/incoming_requests_tab.dart';

import 'date_create_event_page.dart';
import 'futures/artist_flow/live_gig_opportunities_artist_page/ui/live_gig_opportunities_artist_page.dart';
import 'futures/venue_flow/buy_subscription_page/ui/buy_subscription_page.dart';
import 'futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';
import 'futures/venue_flow/messages_page_venus/ui/messages_page_venus.dart';


class MenuArtist extends StatefulWidget {

  var btn;

  MenuArtist({@required this.btn});

  @override
  State<MenuArtist> createState() => _MenuArtistState();
}


class _MenuArtistState extends State<MenuArtist> {


  @override
  Widget build(BuildContext context) {


    return Drawer(
      width: 270.w,
      backgroundColor: AppColor.white.withOpacity(00),
      child:

      ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 15,
            sigmaY: 15,
          ),
          child: Container(
            width: 228.0,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.3),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 63.h, bottom: 53.h, left: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 63.h, left: 16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          ImagesApp.profileMenu,
                          width: 48.w,
                          height: 48.h,
                        ),
                        SizedBox(height: 12.h,),
                        Row(
                          children: [
                            Text(HiveServices.getNameUser.toString(),
                              style: poppins.copyWith(fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: AppColor.white),),
                            SizedBox(width: 5.w,),
                            Icon(Icons.check_circle, color: AppColor.greyBlue2,)
                          ],
                        ),
                        SizedBox(height: 4.h,),
                        Text(HiveServices.getEmailUser.toString(),
                          style: poppins.copyWith(fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: AppColor.white),),
                      ],
                    ),
                  ),
                 const DividerWidget(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const GigRequestArtist()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuRequests),
                          SizedBox(width: 8.w,),
                          Text(
                            "Gig Requests",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => const CalenderArtistPage()));
                        },
                        child: Row(
                          children: [
                            SvgPicture.asset(ImagesApp.menuCalendar),
                            SizedBox(width: 8.w,),
                            Text(
                              "Calendar",
                              style: poppins.copyWith(
                                  fontSize: 14, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => MessagesPageArtist()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuMessage),
                          SizedBox(width: 8.w,),
                          Text(
                            "Messages",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => BuySubscriptionPage()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuSubscription),
                          SizedBox(width: 8.w,),
                          Text(
                            "Subscription",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const LiveGigOpportunitiesArtistPage()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.icMic),
                          SizedBox(width: 8.w,),
                          Text(
                            "Gig opportunities",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const SettingArtistPage()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuSetting),
                          SizedBox(width: 8.w,),
                          Text(
                            "Settings",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DividerWidget(),
                  const Spacer(),
                  GestureDetector(
                    onTap: () {
                      HiveServices.clear();
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20.w, top: 16.h, bottom: 16.h),
                      margin: EdgeInsets.only(bottom: 53.h),
                      child: Row(
                        children: [
                          SvgPicture.asset(ImagesApp.menuLogout),
                          SizedBox(width: 8.w,),
                          Text(
                            "Logout",
                            style: poppins.copyWith(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),


    );
  }
}
