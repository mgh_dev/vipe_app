import 'dart:async';
import 'dart:convert';


import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:flutter_open_street_map/flutter_open_street_map.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:latlong2/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_map/plugin_api.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';





class SeetMap extends StatefulWidget {
  late final Future<LatLng> Function() onGetCurrentLocationPressed;
  late final LatLong center;

  @override
  State<SeetMap> createState() => _SeetMapState();
}

class _SeetMapState extends State<SeetMap> {

  final Completer _completer =Completer();

  final TextEditingController _searchController = TextEditingController();


  final PopupController _popupController = PopupController();

  late List markers;

  List<OSMdata> _options = <OSMdata>[];

  var client = http.Client();


  MapController _mapController = MapController();

  var start_latitude = 42.839145508940945;
  var start_longitude = 50.36190067490672;

  List<LatLng> routpoints = [];
  bool isVisible =false;

  var _mylocationlatitude;
  var _mylocationlongitude;


  Future<Position> getUserCurrentLocation() async{
    await Geolocator.requestPermission().then((value) {

    },).onError((error, stackTrace) {
      print("error");
    },);
    return await Geolocator.getCurrentPosition();
  }
@override
  void initState() {
    // TODO: implement initState
    super.initState();

    getUserCurrentLocation().then((value) async{
      print('my location');
      print(value.latitude.toString() +" "+value.longitude.toString());
      setState(() {
        _mylocationlatitude = value.latitude;
        _mylocationlongitude = value.longitude;
        _mapController.move(
            LatLng(_mylocationlatitude, _mylocationlongitude),_mapController.zoom);

      });
    });


  }
  @override

  Widget build(BuildContext context) {
    //final LocationService locationService = LocationService();
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 70.h),
          child: FlutterMap(
            options: MapOptions(
              center: LatLng(start_latitude, start_longitude),
              minZoom: 4,

              plugins: [MarkerClusterPlugin()],
              onTap: (tapPosition, point) =>
                  _popupController.hideAllPopups(),
            ),
            mapController: _mapController,
            layers: [
              TileLayerOptions(
                urlTemplate:
                    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 22.h,right: 18.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [

              GestureDetector(
                onTap: () {
                  _mapController.move(_mapController.center, _mapController.zoom -  0.25);
                },
                child: Container(
                    width: 36.r,
                    height: 36.r,
                    decoration: BoxDecoration(
                      color: AppColor.red.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(8.r),
                    ),
                    child: Icon(size: 24,Icons.remove,color: AppColor.white,)),
              ),
              SizedBox(height: 18.h,),
              GestureDetector(
                onTap: () {
                  _mapController.move(_mapController.center, _mapController.zoom + 0.25);
                },
                child: Container(
                    width: 36.r,
                    height: 36.r,
                    decoration: BoxDecoration(
                      color: AppColor.red.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(8.r),
                    ),
                    child: Container(height: 24.r,width: 24.r,child: Icon(size: 24,Icons.add,color: AppColor.white,))
                ),
              ),
              SizedBox(height: 18.h,),
              GestureDetector(
                onTap: () async{
                  getUserCurrentLocation().then((value) async{
                    print('my location');
                    print(value.latitude.toString() +" "+value.longitude.toString());
                    setState(() {
                      _mylocationlatitude = value.latitude;
                      _mylocationlongitude = value.longitude;
                      _mapController.move(
                          LatLng(_mylocationlatitude, _mylocationlongitude),_mapController.zoom);

                    });
                    var urlrouting =Uri.parse('http://router.project-osrm.org/route/v1/driving/$_mylocationlongitude,$_mylocationlatitude;$start_longitude,$start_latitude?steps=true&annotations=true&geometries=geojson&overview=full');
                    var response = await http.get(urlrouting);
                    print(response.body);
                    setState(() {
                      routpoints = [];
                      var ruter = jsonDecode(response.body)['routes'][0]['geometry']['coordinates'];
                      for(int i=0; i< ruter.length; i++){
                        var reep = ruter[i].toString();
                        reep = reep.replaceAll("[","");
                        reep = reep.replaceAll("]","");
                        var lat1 = reep.split(',');
                        var long1 = reep.split(",");
                        routpoints.add(LatLng( double.parse(lat1[1]), double.parse(long1[0])));
                      }
                      isVisible = !isVisible;
                      print(routpoints);
                    });
                  });
                },
                child: Container(
                    width: 36.r,
                    height: 36.r,
                    decoration: BoxDecoration(
                      color: AppColor.red.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(8.r),
                    ),
                    child: Container(width: 24.r,height: 24.r,child: Center(child: SvgPicture.asset(ImagesApp.icSend,fit: BoxFit.cover)))),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
