
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/bottom_sheet_review_request.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class BottomSheetEditRequest2 extends StatelessWidget {
  BottomSheetEditRequest2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          topRight: Radius.circular(20.r)),
                    ),
                    backgroundColor: AppColor.darkBlue,
                    isScrollControlled: true,
                    builder: (BuildContext context) {
                      return Container(
                        height: MediaQuery.of(context).size.height - 54.h,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.r),
                                  topRight: Radius.circular(20.r)),
                              child: Image.asset(
                                ImagesApp.ellipse,
                                fit: BoxFit.fill,
                              ),
                            ),
                            SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.only(left: 16.w),
                                            child: GestureDetector(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  "Cancel",
                                                  style: poppins.copyWith(
                                                      fontSize: 16.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: AppColor.blue),
                                                )),
                                          ),
                                        ),
                                        Expanded(
                                            child: Text(
                                              "Request Details",
                                              style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w500),
                                            )),
                                        Expanded(
                                          child: Container(),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 1,
                                    width: double.infinity,
                                    color: AppColor.white.withOpacity(0.07),
                                  ),

                                  Padding(
                                    padding: EdgeInsets.only(top: 30.h,bottom: 20.h),
                                    child: Text("Review your request",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w500,color: AppColor.blue),),
                                  ),
                                  ClipRRect(
                                    child: Image.asset(
                                      ImagesApp.profileRequestDetails,
                                      width: 72.w,
                                      height: 72.r,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    EdgeInsets.only(top: 16.h, bottom: 4.h),
                                    child: Text(
                                      "Eric Johns",
                                      style: poppins.copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          width: 12.w,
                                          height: 12.h,
                                          child: SvgPicture.asset(
                                            ImagesApp.icLocation,
                                            color: AppColor.grey97,
                                            fit: BoxFit.cover,
                                          )),
                                      SizedBox(
                                        width: 7.5.w,
                                      ),
                                      Text(
                                        "Virginia Singer",
                                        style: poppins.copyWith(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.grey97),
                                      ),
                                    ],
                                  ),

                                  Container(
                                    padding: EdgeInsets.all(12.r),
                                    margin: EdgeInsets.only(
                                        top: 27.h,right: 16.w,left: 16.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(14.r),
                                      color: AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(6.r),
                                          child: Image.asset(
                                            width: 44.w,
                                            height: 43.h,
                                            ImagesApp.backHouseofmusic,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Business Location",
                                              style: poppins.copyWith(
                                                  fontSize: 11.sp,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  color: AppColor.white.withOpacity(0.5)
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4.h,
                                            ),
                                            Text(
                                              "House of Music",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 11.w, vertical: 15.5.h),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 16.w, vertical: 12.h),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.r),
                                      color:
                                      AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Row(

                                      children: [
                                        Text(
                                          "Price",
                                          style: poppins.copyWith(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Spacer(),
                                        Text(
                                          "\$ 350",
                                          style: poppins.copyWith(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: AppColor.greyHint),
                                        ),
                                        SizedBox(width: 6.w,),
                                        Container(width: 16.w,height: 16.r,child: SvgPicture.asset(ImagesApp.arrowForward,color: AppColor.white,fit: BoxFit.cover)),
                                      ],
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10.w, vertical: 10.h),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 16.w),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.r),
                                      color:
                                      AppColor.white.withOpacity(0.07),
                                    ),
                                    width: double.infinity,
                                    child: Text(
                                      "I am a musician interested in performing at your venue. I believe that my music would be a perfect fit for your audience.",
                                      style: poppins.copyWith(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                        color: AppColor.grey2
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  SizedBox(height: 165.h,),

                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                                    child: MyCustomButton(
                                      title: "Edit request",
                                      onTap: () {},
                                      loading: false,
                                      color: AppColor.blue,
                                      width: double.infinity,
                                      height: 49.h,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: AppColor.white.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(16)
                    ),
                    child: Text("Open Bottom Sheet",style: poppins.copyWith(fontSize: 14),)),

              ),
            ],
          ),
        ],
      ),
    );

  }
}