import 'dart:io';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

class HiveServices {
  static late Box _myBox;

  static const String _myBoxKey = "myBoxKey";

  static const String _tokenKey = "tokenUser";
  static const String _typeUsersKey = "typeUser";
  static const String _nameUserKey = "nameUser";
  static const String _emailUserKey = "emailUser";
  static const String _statusHelperKey = "statusHelper";

  // static const String _saveTimeKey = "tokenUser";

  static Future<void> init() async {
    Directory directory = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(directory.path);
    _myBox = await Hive.openBox(_myBoxKey);
  }

  static void addToken(String token) {
    _myBox.put(_tokenKey, token);
  }

  static String? get getToken {
    return _myBox.get(_tokenKey);
  }

  ////
  static void addTypeUser(String indexType) {
    _myBox.put(_typeUsersKey, indexType);
  }

  static String? get getTypeUser {
    return _myBox.get(_typeUsersKey);
  }

  ///////////

  static void addNameEmail(String name, String email) {
    _myBox.put(_nameUserKey, name);
    _myBox.put(_emailUserKey, email);
  }

  static String? get getNameUser {
    return _myBox.get(_nameUserKey);
  }

  static String? get getEmailUser {
    return _myBox.get(_emailUserKey);
  }

  static clear() {
    _myBox.clear();
  }

// static void saveTime(dynamic time){
//   _myBox.put(_saveTimeKey, time);
// }
//
//
// static dynamic? get getTime {
//   return _myBox.get(_saveTimeKey);
// }
}
