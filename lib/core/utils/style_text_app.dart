import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vibe_app/core/utils/app_color.dart';

TextStyle get rajdhani => GoogleFonts.rajdhani(
      color: AppColor.white,
      fontSize: 15.sp,
      fontWeight: FontWeight.bold,
    );

TextStyle get poppins => GoogleFonts.poppins(
      color: AppColor.white,
      fontSize: 12.sp,
      fontWeight: FontWeight.normal,
    );

TextStyle get inter => GoogleFonts.inter(
      color: AppColor.black,
      fontSize: 20.sp,
      fontWeight: FontWeight.bold,
    );

TextStyle get montserrat => GoogleFonts.montserrat(
      color: AppColor.grey1,
      fontSize: 10.sp,
      fontWeight: FontWeight.bold,
    );
