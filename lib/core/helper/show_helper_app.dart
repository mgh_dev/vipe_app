import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

class ShowHelperApp extends StatelessWidget {
  const ShowHelperApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child:
    Stack(
      children: [
        Container(
          height: 500.h,
          width: 300.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21.r),
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: <Color>[
                AppColor.blue.withOpacity(0.90),
                AppColor.blue.withOpacity(0.4),
              ],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 101.r,
                width: 101.w,
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.2),
                    shape: BoxShape.circle
                ),
                child: Center(
                  child: Container(
                    height: 69.r,
                    width: 69.w,
                    decoration: BoxDecoration(
                        color: AppColor.white,
                        shape: BoxShape.circle
                    ),
                    child: Center(
                      child: Container(
                          height: 30.r,
                          width: 30.w,
                          child: SvgPicture.asset(ImagesApp.icswipe,fit: BoxFit.cover)),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 27.h,),
              Text("Swip Left or Right",style: poppins.copyWith(fontSize: 24.sp,fontWeight: FontWeight.w600),),
              SizedBox(height: 4.h,),
              Text("Swipe Left or Right to explore",style: poppins.copyWith(fontSize: 16.sp,fontWeight: FontWeight.w400),),
            ],
          ),
        ),
      ],
    )
    );
  }
}
