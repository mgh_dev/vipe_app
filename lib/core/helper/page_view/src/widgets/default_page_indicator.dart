import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vibe_app/core/helper/page_view/src/utils/list_utils.dart';
import 'package:vibe_app/core/utils/app_color.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../stepper_page_view.dart';
import '../models/page_controls_details.dart';

/// A default page indicator with dots and circle pages
class DefaultPageIndicator extends StatelessWidget {
  const DefaultPageIndicator({
    super.key,
    required this.pageSteps,
    required this.pageControlsDetails,
    required this.pageProgress,
    this.numberPage,
  });

  final List<PageStep> pageSteps;
  final int? numberPage;

  final PageControlsDetails pageControlsDetails;

  final ValueListenable<double> pageProgress;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final currentStep = pageControlsDetails.currentStep;
    final onStepSelect = pageControlsDetails.onStepSelect;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        color: Colors.transparent,
        elevation: 0.0,
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: ValueListenableBuilder<double>(
            valueListenable: pageProgress,
            builder: (context, progress, _) {
              // progress goes up to pageSteps.length

              return Column(
                children: [
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: pageSteps.mapIndexed<Widget>(
                        (index, step) {
                          final maybeIcon = step.icon;

                          Widget iconChild;

                          if (maybeIcon == null) {
                            iconChild = Text('$index');
                          } else {
                            iconChild = maybeIcon;
                          }

                          final isPrevious = index < currentStep;
                          final isNext = index > currentStep;
                          final Color backgroundColor;
                          final Color iconColor;
                          final double elevation;

                          if (isPrevious) {
                            backgroundColor = AppColor.blue;
                            iconColor = theme.colorScheme.onPrimary;
                            elevation = 0.0;
                          } else if (isNext) {
                            backgroundColor = AppColor.black24.withOpacity(0.5);
                            iconColor =
                                theme.colorScheme.onSurface.withAlpha(140);
                            elevation = 0.0;
                          } else /* current */ {
                            backgroundColor = AppColor.blue;
                            iconColor = theme.colorScheme.onSurface;
                            elevation = 12.0;
                          }

                          return GestureDetector(
                            onTap: onStepSelect == null
                                ? null
                                : () => onStepSelect(index),
                            child: Padding(
                              padding:  EdgeInsets.only(right: 1.5.w),
                              child: Container(
                                width: 45,
                                height: 3,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: backgroundColor,
                                ),
                              ),
                            ),
                          );
                        },
                      ).intersperseIndexed(
                        (int index) {
                          final elementIndex = (index ~/ 2);
                          final currentProgress = progress - elementIndex;

                          return const SizedBox();
                        },
                      ).toList(),
                    ),
                  ),
                  SizedBox(height: 12.h),
                  Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        "Step ${numberPage.toString()} of 7",
                        style:GoogleFonts.inter(
                          color: Colors.white,
                          fontSize: 12.sp,
                        ),
                      ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
