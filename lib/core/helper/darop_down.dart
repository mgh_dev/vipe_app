import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vibe_app/constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'package:vibe_app/core/utils/image_app.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/venue_flow/create_event_details_venue_page/ui/create_event_details_venue_page.dart';
import 'package:vibe_app/futures/venue_flow/create_profile_venue/model/response_model/get_vibe_model.dart';

import '../../constant_bloc/logic_change_items/state/select_item_state.dart';
import '../utils/app_color.dart';

class ClassDropDown extends StatefulWidget {
  const ClassDropDown({
    Key? key,
    required this.data,
    required this.callBack,
    this.scrollController,
  }) : super(key: key);

  final List<GetDataVibe> data;
  final Function(int) callBack;
  final ScrollController? scrollController;

  @override
  State<ClassDropDown> createState() => _ClassDropDownState();
}

class _ClassDropDownState extends State<ClassDropDown> {
  int selectedItem = -1;

  double chooseLangHeight = 0;
  SelectItemViewModel changeItemBloc = SelectItemViewModel();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AnimatedContainer(
          margin: EdgeInsets.only(top: 60.h),
          duration: const Duration(milliseconds: 350),
          height: chooseLangHeight,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppColor.white.withOpacity(0.1),
            border: Border.all(
              color: chooseLangHeight > 50
                  ? Colors.transparent
                  : Colors.transparent,
              width: 1,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 0, bottom: 12, right: 15),
            child: BlocBuilder(
                bloc: changeItemBloc,
                builder: (context, state) {
                  if (state is ItemChangeState) {
                    return ListView.builder(
                      controller: widget.scrollController,
                      physics: const BouncingScrollPhysics(),
                      itemCount: widget.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        int item = index;
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedItem = index;
                              if (CreateEventDetailsVenuePage.selectItemVibe.contains(item)) {
                                CreateEventDetailsVenuePage.selectItemVibe.remove(item);
                              } else {
                                CreateEventDetailsVenuePage.selectItemVibe.add(item);
                              }
                              // changeExpanded();
                              widget.callBack(index);
                              changeItemBloc.changeItem(index);
                            });
                            // selectedItem = index;
                            // // changeExpanded();
                            // widget.callBack(index);
                            // changeItemBloc.changeItem(index);
                          },
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 8, top: 8),
                              child: Row(
                                children: [
                                  SizedBox(width: 24.w),
                                  Text(
                                    widget.data[index].name,
                                    maxLines: 1,
                                    style: poppins.copyWith(
                                      fontSize: 14.sp,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    height: 16.6.h,
                                    width: 16.6.w,
                                    decoration: BoxDecoration(
                                        color: CreateEventDetailsVenuePage.selectItemVibe.contains(item)
                                            ? AppColor.green
                                            : Colors.transparent,
                                        border: Border.all(
                                            width: 1,
                                            color: CreateEventDetailsVenuePage.selectItemVibe.contains(item)
                                                ? AppColor.green
                                                : AppColor.grey5b),
                                        shape: BoxShape.circle),
                                    child: Center(
                                      child: Icon(
                                        Icons.check,
                                        size: 14,
                                        color: CreateEventDetailsVenuePage.selectItemVibe.contains(item)
                                            ? AppColor.backgroundApp
                                            : Colors.transparent,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 24.w),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                  return const SizedBox();
                }),
          ),
        ),
        GestureDetector(
          onTap: widget.data.length > 1 ? changeExpanded : null,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: chooseLangHeight > 50
                    ? Colors.transparent
                    : Colors.transparent,
                width: 1,
              ),
              color: Colors.white.withOpacity(0.1),
            ),
            height: 50,
            child: Center(
              child: Row(
                children: [
                  const SizedBox(width: 20),
                  Row(
                    children: [
                      SvgPicture.asset(ImagesApp.icAddLocation),
                      SizedBox(width: 10.w),
                      Text(
                        "What’s the vibe for this event?",
                        style: poppins.copyWith(
                            fontSize: 14.sp, color: Colors.white),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  const Spacer(),
                  Visibility(
                    visible: widget.data.length > 1,
                    child: AnimatedSwitcher(
                      duration: const Duration(milliseconds: 320),
                      transitionBuilder: (child, anim) => RotationTransition(
                        turns: child.key == const ValueKey('icon1')
                            ? Tween<double>(begin: 0, end: 1).animate(anim)
                            : Tween<double>(begin: 1, end: 0).animate(anim),
                        child: ScaleTransition(scale: anim, child: child),
                      ),
                      child: chooseLangHeight > 0
                          ? const Icon(
                              Icons.keyboard_arrow_up_outlined,
                              color: Colors.white,
                              size: 20,
                              key: ValueKey('icon1'),
                            )
                          : const Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.white,
                              size: 20,
                              key: ValueKey('icon2'),
                            ),
                    ),
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void changeExpanded() {
    setState(() {
      if (chooseLangHeight == 0) {
        chooseLangHeight = MediaQuery.of(context).size.height / 1.65;
      } else {
        chooseLangHeight = 0;
      }
    });
  }
}
