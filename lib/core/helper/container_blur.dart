import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:vibe_app/core/utils/app_color.dart';

class ContainerBlur extends StatelessWidget {
  final double? width;
  final double? height;
  final double? sigmaX;
  final double? sigmaY;
  final Color color;
  final Widget? child;

  const ContainerBlur(
      {this.width,
      this.height,
      this.sigmaX,
      this.sigmaY,
      required this.color,
      this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 250,
      height: height ?? 250,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: sigmaX ?? 150,
          sigmaY: sigmaY ?? 150,
        ),
        child: Container(
          color: Colors.transparent,
          child: child ?? const SizedBox(),
        ),
      ),
    );
  }
}
