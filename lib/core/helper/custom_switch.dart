import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vibe_app/core/utils/app_color.dart';

// ignore: must_be_immutable
class CustomSwitch extends StatefulWidget {
  final void Function(bool isToggled) onToggled;
  late bool isToggled;
  final double? width;
  final double? height;
  final int? duration;
  final double? borderRadius;
  final double? toggledBorderRadius;
  final double? innerPadding;
  final Color? toggledActiveColor;
  final Color? toggledInActiveColor;
  final Color? activeColor;
  final Color? inactiveColor;


  CustomSwitch({
    Key? key,
    required this.onToggled,
    required this.isToggled,
    this.width,
    this.height,
    this.duration,
    this.borderRadius,
    this.toggledBorderRadius,
    this.innerPadding,
    this.toggledActiveColor,
    this.toggledInActiveColor,
    this.activeColor,
    this.inactiveColor
  })
      : super(key: key);

  @override
  State<CustomSwitch> createState() => _CustomSwitchState();

}


class _CustomSwitchState extends State<CustomSwitch> {
  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: ()  {
        setState(() => widget.isToggled = !widget.isToggled);
        widget.onToggled(widget.isToggled);
      },
      onPanEnd: (b) {
        setState(() => widget.isToggled = !widget.isToggled);
        widget.onToggled(widget.isToggled);
      },
      child: AnimatedContainer(
        height: widget.height??24.h,
        width: widget.width??45.w,
        padding: EdgeInsets.all(widget.innerPadding??3),
        alignment: widget.isToggled ? Alignment.centerRight : Alignment.centerLeft,
        duration: Duration(milliseconds: widget.duration??300,),
        curve: Curves.easeOut,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.borderRadius??100),
          color: widget.isToggled ? widget.activeColor??AppColor.blueSwitch : widget.inactiveColor??AppColor.blackSwitch,
        ),
        child: Container(
          width: widget.height??24.h - ((widget. innerPadding??3)*2),
          height: widget.height??24.h -((widget. innerPadding??3)*2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.toggledBorderRadius??100),
            color: widget.isToggled ? widget.toggledActiveColor??AppColor.white : widget.toggledInActiveColor??AppColor.white,
          ),
        ),
      ),
    );
  }
}