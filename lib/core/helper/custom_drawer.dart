import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/config/hive_service/hive_service.dart';
import 'package:vibe_app/futures/constant_pages/choose_your_role_page/ui/choose_your_role_page.dart';

import '../../futures/fan_flow/settimgs_page/ui/settings_page.dart';
import '../utils/app_color.dart';
import '../utils/image_app.dart';
import '../utils/style_text_app.dart';
import 'divider.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 15,
          sigmaY: 15,
        ),
        child: Container(
          width: 228.0,
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.3),
          ),
          child: Padding(
            padding: EdgeInsets.only(top: 63.h, bottom: 53.h, left: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 48.w,
                  height: 48.h,
                  decoration: BoxDecoration(
                    // color: AppColor.darkBlue,
                    image: DecorationImage(image: AssetImage(ImagesApp.prof)),
                    shape: BoxShape.circle,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 12.h, bottom: 4.h),
                  child: Row(
                    children: [
                      Text(
                        HiveServices.getNameUser.toString(),
                        style: poppins.copyWith(
                          fontSize: 24.sp,
                          color: AppColor.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 4.w),
                      Icon(
                        Icons.check_circle,
                        color: AppColor.greyBlue,
                        size: 21,
                      )
                    ],
                  ),
                ),
                Text(
                  HiveServices.getEmailUser.toString(),
                  style: poppins.copyWith(
                    fontSize: 13.sp,
                    color: AppColor.white,
                    // fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h, bottom: 32.h),
                  child: const DividerWidget(),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SettingPage()));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(ImagesApp.icSetting),
                      SizedBox(width: 8.w),
                      Text(
                        "Settings",
                        style: poppins.copyWith(
                          fontSize: 14.sp,
                          color: AppColor.white,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16.h),
                const DividerWidget(),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChooseYourRolePage(),
                      ),
                    );
                    HiveServices.clear();
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(ImagesApp.icLogout),
                      SizedBox(width: 8.w),
                      Text(
                        "Logout",
                        style: poppins.copyWith(
                          fontSize: 14.sp,
                          color: AppColor.white,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
