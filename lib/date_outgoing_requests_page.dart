import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';

class DateOutgoingRequestsPage extends StatelessWidget {
  DateOutgoingRequestsPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: SvgPicture.asset(ImagesApp.icBack)),
                    Text(
                      "Subscription",
                      style: poppins.copyWith(
                          fontSize: 16.sp, fontWeight: FontWeight.w500),
                    ),
                    SvgPicture.asset(ImagesApp.icNotification),
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:[
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(
                            right: 12.w,
                            left: 20.w,
                            top: 8.h,),
                        decoration: BoxDecoration(
                            color: AppColor.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(18.r)),
                        child: Column(
                          children: [

                            Container(
                              margin: EdgeInsets.only(top: 7.h,bottom: 4.h,),
                              height: 1.5,
                              width: 21.5,
                              color: AppColor.white,
                            ),
                            Container(
                                height: 1.5,
                                width: 9.5,
                                color: AppColor.white),
                            Padding(
                              padding: EdgeInsets.only(top: 11.h,bottom: 11.h,right: 17.w,left: 15.w),
                              child: Text("View and manage all outgoing requests you’ve created.",
                                textAlign: TextAlign.center,
                                style: poppins.copyWith(fontSize: 12.sp,fontWeight: FontWeight.w400,color: AppColor.greyHint),
                              ),
                            ),
                            ListView.builder(
                              padding: EdgeInsets.zero,
                              shrinkWrap: true,
                              itemCount: 2,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return Stack(
                                  alignment: Alignment.topCenter,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: 9.h),
                                      padding: EdgeInsets.only(right: 6.w,left: 8.w,bottom: 6.h,top: 8.h),
                                      decoration: BoxDecoration(
                                          color: AppColor.white
                                              .withOpacity(0.09),
                                          borderRadius:
                                          BorderRadius.circular(8.r)),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                            BorderRadius.circular(
                                                8.r),
                                            child: Image.asset(
                                              ImagesApp.profileTest1,
                                              width: 52.w,
                                              height: 56.8.r,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          SizedBox(
                                            width:7.w,
                                          ),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            crossAxisAlignment:
                                            CrossAxisAlignment
                                                .start,
                                            children: [
                                              SizedBox(
                                                height: 1.5.h,
                                              ),
                                              Text(
                                                "R&B Flow",
                                                style: poppins.copyWith(
                                                    fontSize: 16.sp,
                                                    fontWeight:
                                                    FontWeight
                                                        .w500),
                                              ),
                                              SizedBox(
                                                height: 0.5.h,
                                              ),
                                              Text(
                                                "Expired",
                                                style: inter.copyWith(
                                                    fontSize: 10.sp,
                                                    fontWeight:
                                                    FontWeight.w500,
                                                    color:
                                                    AppColor.redError),
                                              ),
                                              SizedBox(
                                                height: 5.h,
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(
                                                    50.r),
                                                child: Image.asset(
                                                  ImagesApp.profileTest1,
                                                  width: 24.w,
                                                  height: 24.r,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Spacer(),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 13.w,
                                                left: 13.w,
                                                top: 4.h,
                                                bottom: 17.h,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(14.r),
                                              color: AppColor.black12,
                                            ),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "14",
                                                  style: poppins.copyWith(
                                                      fontSize: 18.sp,
                                                      fontWeight:
                                                      FontWeight.w600,
                                                      color: AppColor.white),
                                                ),
                                                Text(
                                                  "Jun",
                                                  style: poppins.copyWith(
                                                      fontSize: 16.sp,
                                                      fontWeight:
                                                      FontWeight.w500,
                                                      color: AppColor.grey2),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                        decoration: BoxDecoration(
                                            color: AppColor.black38,
                                            borderRadius: BorderRadius.circular(6.r)
                                        ),
                                        margin: EdgeInsets.only(top: 3.h),
                                        padding: EdgeInsets.all(6.r),
                                        child: Text("Event",style: poppins.copyWith(fontSize: 9.sp,fontWeight: FontWeight.w400,color: AppColor.grey1),)),
                                  ],
                                );
                              },
                            ),
                            SizedBox(height: 26.h,)
                          ],
                        ),
                      ),
                      SizedBox(height: 342.h,),
                      Container(
                        color: AppColor.black48,
                        height: 1,
                        width: double.infinity,
                      ),
                      Container(
                        color: AppColor.white.withOpacity(0.07),width: double.infinity,
                        padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 17.5.h),
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            MyCustomButton(
                              title: "Manage Calendar",
                              loading: false,
                              onTap: () {},
                              color: Colors.transparent,
                              borderColor: AppColor.white,
                              width: 165.w,
                              height: 49.h,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              borderRadius: 48.sp,
                            ),
                            MyCustomButton(
                              title: "Create a Event",
                              loading: false,
                              onTap: () {},
                              color: AppColor.green3,
                              width: 165.w,
                              height: 49.h,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              borderRadius: 48.sp,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
