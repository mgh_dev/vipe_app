import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import 'constant_bloc/logic_change_items/state/select_item_state.dart';
import 'constant_bloc/logic_change_items/view_model/view_model_bottom_select_item.dart';
import 'core/utils/app_color.dart';
import 'core/utils/image_app.dart';
import 'core/utils/style_text_app.dart';

class Da extends StatefulWidget {
  static String years = DateTime.now().year.toString();
  static String month = DateFormat('MM').format(DateTime.now());
  static String day = DateFormat('dd').format(DateTime.now());

  const Da({Key? key}) : super(key: key);

  @override
  State<Da> createState() => _DaState();
}

class _DaState extends State<Da> {
  late StreamController<DateTime> _weekDatesController;
  SelectItemViewModel selectItemViewModel = SelectItemViewModel();
  SelectItemViewModel selectDate = SelectItemViewModel();
  SelectItemViewModel viewDate = SelectItemViewModel();

  final ScrollController firstController = ScrollController();
  final ScrollController secondController = ScrollController();
  List<DateTime> weekDates = [];
  int currentYear = DateTime.now().year;
  int currentMonth = DateTime.now().month;

  List<String> lsDay = [
    "M",
    "T",
    "W",
    "T",
    "F",
    "S",
    "S",
  ];
  List<String> months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDate;

  Map<String, List<String>> mySelectedEvents = {};

  loadPreviousEvents() {
    mySelectedEvents = {
      "2023-07-05": [
        "cdcd",
      ],
      "2023-07-19": [
        "cdcd",
      ],
      "2023-07-29": [
        "cdcd",
      ]
    };
  }

  List _listOfDayEvents(DateTime dateTime) {
    if (mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)] != null) {
      return mySelectedEvents[DateFormat('yyyy-MM-dd').format(dateTime)]!;
    } else {
      return [];
    }
  }

  @override
  void initState() {
    super.initState();

    print(currentMonth);
    _selectedDate = _focusedDay;
    loadPreviousEvents();
    print(months[currentMonth - 1]);
    DateTime now = DateTime.now();
    int weekday = now.weekday;

    // Calculate the start date of the current week
    DateTime startDate = now.subtract(Duration(days: weekday - 1));
    _weekDatesController = StreamController<DateTime>.broadcast();

    Timer.periodic(const Duration(days: 1), (timer) {
      DateTime currentDate = DateTime.now();
      if (currentDate.weekday == 1 && currentDate.day != startDate.day) {
        startDate = currentDate;
        _weekDatesController.add(startDate);
      }
    });

    // Generate a list of dates for the week
    for (int i = 0; i < 7; i++) {
      DateTime date = startDate.add(Duration(days: i));
      weekDates.add(date);
    }
  }

  @override
  void dispose() {
    _weekDatesController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: viewDate,
      builder: (context, state) {
        if (state is ItemChangeState) {
          if (state.index == 0) {
            return Container(
              padding: EdgeInsets.only(
                  left: 13.w, right: 13.w, top: 27.h, bottom: 10.h),
              width: 344.w,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            months[currentMonth - 1],
                            style: poppins.copyWith(
                              fontSize: 16.sp,
                            ),
                          ),
                          Text(
                            currentYear.toString(),
                            style: poppins.copyWith(
                              fontSize: 16.sp,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 40.w,
                            height: 40.r,
                            decoration: BoxDecoration(
                                color: AppColor.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10.r)),
                            child: Center(
                                child: SvgPicture.asset(ImagesApp.calendar)),
                          ),
                          SizedBox(width: 7.w),
                          GestureDetector(
                            onTap: () => viewDate.changeItem(1),
                            child: Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(10.r)),
                              child: Center(
                                  child: SvgPicture.asset(
                                      ImagesApp.icDoubleArrow)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 35.h),
                  BlocBuilder(
                      bloc: selectDate,
                      builder: (context, state) {
                        if (state is ItemChangeState) {
                          return SizedBox(
                            height: 60,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: const BouncingScrollPhysics(),
                              itemCount: weekDates.length,
                              itemBuilder: (context, index) {
                                DateTime date = weekDates[index];
                                String formattedDate =
                                    DateFormat('dd').format(date);
                                if (formattedDate ==
                                    DateFormat('dd').format(DateTime.now())) {
                                  print(formattedDate);
                                }
                                return Padding(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: GestureDetector(
                                    onTap: () {
                                      selectDate.changeItem(index);
                                      Da.day = formattedDate;
                                      Da.month=DateFormat('MM').format(DateTime.now());
                                      print("object");
                                      print(Da.years);
                                      print(Da.month);
                                      print(Da.day);
                                    },
                                    child: Container(
                                      width: 45,
                                      // height: 60,
                                      decoration: BoxDecoration(
                                        color: state.index == index
                                            ? AppColor.green3
                                            : Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(16.r),
                                      ),
                                      child: Column(
                                        children: [
                                          SizedBox(height: 2.5.h),
                                          Text(
                                            lsDay[index],
                                            style: poppins.copyWith(
                                                fontSize: 12.sp,
                                                color: AppColor.white),
                                          ),
                                          SizedBox(height: 10.h),
                                          Text(
                                            formattedDate,
                                            style: poppins.copyWith(
                                              fontSize: 16.sp,
                                            ),
                                          ),
                                          SizedBox(height: 2.5.h),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                        }
                        return const SizedBox();
                      })
                ],
              ),
            );
          } else {
            return Container(
              margin: EdgeInsets.only(
                left: 13.w,
                right: 13.w,
                bottom: 10.h,
              ),
              decoration: BoxDecoration(
                color: AppColor.white.withOpacity(0.1),
                borderRadius: BorderRadius.circular(20.r),
              ),
              child: Column(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(top: 15.h, left: 13.w, right: 13.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Text(
                              months[currentMonth - 1],
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                              ),
                            ),
                            Text(
                              currentYear.toString(),
                              style: poppins.copyWith(
                                fontSize: 16.sp,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              width: 40.w,
                              height: 40.r,
                              decoration: BoxDecoration(
                                  color: AppColor.white.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(10.r)),
                              child: Center(
                                  child: SvgPicture.asset(ImagesApp.calendar)),
                            ),
                            SizedBox(width: 7.w),
                            GestureDetector(
                              onTap: () => viewDate.changeItem(0),
                              child: Container(
                                width: 40.w,
                                height: 40.r,
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.07),
                                    borderRadius: BorderRadius.circular(10.r)),
                                child: Center(
                                    child: SvgPicture.asset(
                                        ImagesApp.icDoubleArrow)),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 17.5.h,
                      bottom: 19.h,
                      left: 16.w,
                      right: 16.w,
                    ),
                    child: TableCalendar(
                      headerVisible: false,
                      startingDayOfWeek: StartingDayOfWeek.monday,
                      daysOfWeekHeight: 20,
                      daysOfWeekStyle: DaysOfWeekStyle(
                        weekdayStyle: TextStyle(color: AppColor.grey2),
                        weekendStyle: TextStyle(color: AppColor.grey2),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom:
                                BorderSide(color: AppColor.grey2, width: 0.5.w),
                          ),
                        ),
                      ),
                      firstDay: DateTime.utc(2010, 10, 16),
                      lastDay: DateTime.utc(2030, 3, 14),
                      focusedDay: _focusedDay,
                      calendarStyle: CalendarStyle(
                        markerDecoration: BoxDecoration(
                            color: AppColor.green3, shape: BoxShape.circle),
                        selectedDecoration: const BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        todayDecoration: const BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        outsideDaysVisible: false,
                        defaultTextStyle: TextStyle(color: AppColor.white),
                        selectedTextStyle: TextStyle(color: AppColor.white),
                        todayTextStyle: TextStyle(color: AppColor.white),
                        weekendTextStyle: TextStyle(color: AppColor.white),
                      ),
                      calendarFormat: CalendarFormat.month,
                      onDaySelected: (selectedDay, focusedDay) {
                        print("<<<<${selectedDay}>>>>>>");
                        print("dnaksdjksabdjksa");
                        Da.years = DateFormat('yyyy').format(selectedDay);
                        Da.month = DateFormat('MM').format(selectedDay);
                        Da.day = DateFormat('dd').format(selectedDay);
                        print(Da.years);
                        print(Da.month);
                        print(Da.day);
                        if (!isSameDay(_selectedDate, selectedDay)) {
                          setState(() {
                            _selectedDate = selectedDay;
                            _focusedDay = focusedDay;
                          });
                        }
                      },
                      selectedDayPredicate: (day) {

                        return isSameDay(_selectedDate, day);
                      },
                      onFormatChanged: (format) {
                        if (_calendarFormat != format) {
                          setState(() {
                            _calendarFormat = format;
                          });
                        }
                      },
                      onPageChanged: (focusedDay) {
                        _focusedDay = focusedDay;
                      },
                      eventLoader: _listOfDayEvents,
                    ),
                  ),
                ],
              ),
            );
          }
        }
        return const SizedBox();
      },
    );
  }
}
