import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/helper/different_size_of_gridview_item.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';
import 'package:vibe_app/futures/constant_pages/chat_room_page/chat_room_page.dart';

import '../../../../../../../core/helper/container_blur.dart';
import '../../../../../../../core/utils/app_color.dart';
import '../../../../../../../core/utils/image_app.dart';

class EventsDetailspageArtist2 extends StatelessWidget {
  EventsDetailspageArtist2({Key? key}) : super(key: key);

  final typeArtist = TextEditingController();

  List<String> texts = ["Energetic", "Nostalgic", "Electric"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundApp,
      body: Stack(
        children: [
          Positioned(
              left: -100.w,
              top: -100.h,
              child: ContainerBlur(color: AppColor.purple)),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(20.r),
                          bottomLeft: Radius.circular(20.r)),
                      child: Image.asset(
                        ImagesApp.backEvent,
                        width: double.infinity,
                        height: 383.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: Padding(
                        padding:
                            EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColor.white.withOpacity(0.18),
                                        shape: BoxShape.circle),
                                    child: SvgPicture.asset(ImagesApp.icBack))),
                            Image.asset(
                              ImagesApp.logoApp,
                              width: 60.w,
                              height: 60.h,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                    color: AppColor.white.withOpacity(0.18),
                                    shape: BoxShape.circle),
                                child: SvgPicture.asset(ImagesApp.icShared)),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 12.w,
                        bottom: 11.h,
                      ),
                      child: Container(
                        width: 40.w,
                        height: 40.r,
                        decoration: BoxDecoration(
                            color: AppColor.blue, shape: BoxShape.circle),
                        child: Center(
                          child: SvgPicture.asset(
                            ImagesApp.editText,
                            width: 17.14.w,
                            height: 17.14.r,
                            color: AppColor.white,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 27.w,bottom: 0.h,),
                      child: Text("Music Fest",
                          style: poppins.copyWith(
                              fontSize: 20.sp, fontWeight: FontWeight.w500)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 34.w,bottom: 5.h),
                      child: Container(
                        padding: EdgeInsets.only(
                            top: 21.h, bottom: 12.h, right: 12.w, left: 12.w),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(50),
                              bottomRight: Radius.circular(50)),
                          gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: <Color>[
                              AppColor.green,
                              AppColor.green.withOpacity(
                                0.0,
                              ),
                            ],
                          ),
                        ),
                        child: Text(
                          "\$55",
                          style: inter.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w600,
                              color: AppColor.white),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 27.w, bottom: 6.h),
                  child: Text("Thursday December 24th at 7:30pm",
                      style: poppins.copyWith(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          color: AppColor.grey81)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 27.w),
                  child: GenerateChipe(
                    texts: texts,
                    ///borderChip
                    // border: Border.all(
                    //     width: 0.5, color: AppColor.white.withOpacity(0.2)),
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 6.h),
                  ),
                ),
                SizedBox(
                  height: 13.h,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24.w),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.r),
                      border:
                          Border.all(color: AppColor.blackBorder2, width: 1)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(4.r),
                        margin: EdgeInsets.only(
                            top: 4.h, bottom: 16.h, right: 4.w, left: 4.w),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.r),
                          color: AppColor.white.withOpacity(0.09),
                        ),
                        width: double.infinity,
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.r),
                              child: Image.asset(
                                width: 52.w,
                                height: 56.8.h,
                                ImagesApp.backHouseofmusic,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "House of Music",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 4.h,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(4.5.r),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(4.r),
                                        color: AppColor.white.withOpacity(0.09),
                                      ),
                                      child: Container(
                                          height: 7.h,
                                          width: 7.w,
                                          child: SvgPicture.asset(
                                            ImagesApp.icLocation,
                                            color: AppColor.white,
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                    SizedBox(
                                      width: 6.w,
                                    ),
                                    Text(
                                      "Elgin St. Celina, Delaware",
                                      style: inter.copyWith(
                                          fontSize: 10.sp,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.greyTxt),
                                    )
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 12.w,bottom: 8.h),
                        child: Text(
                          "Description",
                          style: poppins.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.greyHint),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.w),
                        child: Text(
                          "House of Music presents ‘Music Fest’ featuring your favorite local musicians. See more",
                          style: poppins.copyWith(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),


                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12.w,vertical: 16.h),
                        width: double.infinity,
                        height: 1,
                          color: AppColor.white.withOpacity(0.1),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 12.w,bottom: 16.h),
                        child: Text(
                          "Lineup",
                          style: poppins.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              color: AppColor.greyHint),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.w),
                        child: SizedBox(
                          height: 125.h,
                          child: ListView.builder(
                            physics: AlwaysScrollableScrollPhysics(),
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: 3,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  Container(
                                    height: 125.h,
                                    padding: EdgeInsets.only(right: 17.5.w,left: 17.5.w),
                                    child: Column(
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(100.r),
                                          child: Image.asset(
                                            ImagesApp.profileRequestDetails,
                                            width: 62.r,
                                            height: 62.r,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        SizedBox(height: 4.h,),
                                        Text("Eric Johns",style: poppins.copyWith(fontSize: 11.sp,fontWeight: FontWeight.w400),),
                                        SizedBox(height: 2.h,),
                                        Text("Rock & Pop",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w500,color: AppColor.grey81),),
                                        Spacer(),
                                        Text("Confirmed",style: poppins.copyWith(fontSize: 10.sp,fontWeight: FontWeight.w500,color: AppColor.greenText2),),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 4.w,)
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12.w,vertical: 16.h),
                        width: double.infinity,
                        height: 1,
                          color: AppColor.white.withOpacity(0.1),
                      ),

                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.w,vertical: 8.h),
                        child: Row(
                          children: [
                            Container(
                              padding: EdgeInsets.all(13.r),
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(12.r),
                                color: AppColor.white.withOpacity(0.09),
                              ),
                              child: SvgPicture.asset(
                                height: 18.r,
                                width: 18.w,
                                ImagesApp.icLocation,
                                color: AppColor.white,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              width: 12.w,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Address",
                                  style: poppins.copyWith(
                                      fontSize: 11.sp,
                                      fontWeight: FontWeight.w400,
                                    color: AppColor.greyHint
                                  ),
                                ),
                                SizedBox(height: 8.h,),
                                Text(
                                  "273-296 Geary St, Richmond, VA 09584",
                                  style: poppins.copyWith(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 14.h,bottom: 24.h,left: 12.w,right: 12.w),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(9.r),
                          child: Image.asset(
                            ImagesApp.openMap,
                            width: double.infinity,
                            /*height: 156.h*/
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 12.w,bottom: 8.h),
                        child: Text(
                          "Age limit:",
                          style: poppins.copyWith(
                              fontSize: 11.sp,
                              fontWeight: FontWeight.w400,
                              color: AppColor.greyHint
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 12.w,bottom: 0.h),
                        child: Text(
                          "21+",
                          style: poppins.copyWith(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.w),
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                  height: 49.h,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: AppColor.whiteBorder,
                                    ),
                                    borderRadius: BorderRadius.circular(50.r),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Share",
                                      style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 8.w,),
                            Expanded(
                              child: MyCustomButton(
                                title: "Buy Now",
                                color: AppColor.greenText2,
                                loading: false,
                                onTap: () {},
                              ),
                            ),
                            
                          ],
                        ),
                      ),
                      SizedBox(height: 46.h,)


                    ],
                  ),
                ),
                SizedBox(height: 47.h,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
