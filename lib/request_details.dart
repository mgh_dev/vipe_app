import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vibe_app/core/helper/custom_button.dart';
import 'package:vibe_app/core/helper/custom_switch.dart';
import 'package:vibe_app/core/helper/custom_text_fild.dart';
import 'package:vibe_app/core/utils/style_text_app.dart';

import '../../../core/helper/container_blur.dart';
import '../../../core/utils/app_color.dart';
import '../../../core/utils/image_app.dart';

class RequestDetails extends StatelessWidget {
  RequestDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.grey7E,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 53.h, left: 20.w, right: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                    },
                    child: SvgPicture.asset(ImagesApp.icBack)),
                Text(
                  "Subscription",
                  style: poppins.copyWith(
                      fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                SvgPicture.asset(ImagesApp.icNotification),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.r),
                      topRight: Radius.circular(20.r)),
                ),
                backgroundColor: AppColor.darkBlue,
                isScrollControlled: true,
                builder: (BuildContext context) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 54.h,
                    child: Stack(
                      alignment: Alignment.topLeft,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.r),
                              topRight: Radius.circular(20.r)),
                          child: Image.asset(
                            ImagesApp.ellipse,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 18.5.h, bottom: 13.h),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 16.w),
                                        child: GestureDetector(
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              "Cancel",
                                              style: poppins.copyWith(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: AppColor.blue),
                                            )),
                                      ),
                                    ),
                                    Expanded(
                                        child: Text(
                                      "Request Details",
                                      style: poppins.copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500),
                                    )),
                                    Expanded(
                                      child: Container(),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                width: double.infinity,
                                color: AppColor.white.withOpacity(0.07),
                              ),
                              SizedBox(
                                height: 25.h,
                              ),
                              ClipRRect(
                                child: Image.asset(
                                  ImagesApp.profileRequestDetails,
                                  width: 72.w,
                                  height: 72.r,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(top: 16.h, bottom: 4.h),
                                child: Text(
                                  "Eric Johns",
                                  style: poppins.copyWith(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      width: 12.w,
                                      height: 12.h,
                                      child: SvgPicture.asset(
                                        ImagesApp.icLocation,
                                        color: AppColor.grey97,
                                        fit: BoxFit.cover,
                                      )),
                                  SizedBox(
                                    width: 7.5.w,
                                  ),
                                  Text(
                                    "Virginia Singer",
                                    style: poppins.copyWith(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: AppColor.grey97),
                                  ),
                                ],
                              ),
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(
                                    top: 36.h, right: 14.w, left: 18.w),
                                padding: EdgeInsets.only(
                                    right: 10.w,
                                    left: 12.w,
                                    top: 12.h,
                                    bottom: 12.h),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(14.r),
                                  color: AppColor.white.withOpacity(0.07),
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 4.w),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Open Mic Night",
                                                  style: poppins.copyWith(
                                                      fontSize: 20.sp,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                SizedBox(
                                                  height: 4.h,
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Thursday ",
                                                      style: poppins.copyWith(
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color:
                                                              AppColor.grey81),
                                                    ),
                                                    Text(
                                                      "at 7:30pm",
                                                      style: poppins.copyWith(
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: AppColor.blue),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 12.5.w,
                                                  vertical: 6.h),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(14.r),
                                                color: AppColor.black12,
                                              ),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "24",
                                                    style: poppins.copyWith(
                                                        fontSize: 18.sp,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: AppColor.white),
                                                  ),
                                                  Text(
                                                    "Dec",
                                                    style: poppins.copyWith(
                                                        fontSize: 16.sp,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: AppColor.grey2),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(4.r),
                                        margin: EdgeInsets.only(
                                            top: 8.h, bottom: 16.h),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8.r),
                                          color: AppColor.blackBack,
                                        ),
                                        width: double.infinity,
                                        child: Row(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8.r),
                                              child: Image.asset(
                                                width: 52.w,
                                                height: 56.8.h,
                                                ImagesApp.backHouseofmusic,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 8.w,
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      "House of Music",
                                                      style: poppins.copyWith(
                                                          fontSize: 15.sp,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    SizedBox(
                                                      width: 6.w,
                                                    ),
                                                    Icon(
                                                      Icons.check_circle,
                                                      color: AppColor.greyBlue2,
                                                      size: 16.r,
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 4.h,
                                                ),
                                                Row(
                                                  children: [
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(4.5.r),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(4.r),
                                                        color: AppColor.white
                                                            .withOpacity(0.09),
                                                      ),
                                                      child: Container(
                                                          height: 7.h,
                                                          width: 7.w,
                                                          child:
                                                              SvgPicture.asset(
                                                            ImagesApp
                                                                .icLocation,
                                                            color:
                                                                AppColor.white,
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    SizedBox(
                                                      width: 6.w,
                                                    ),
                                                    Text(
                                                      "273-296 Geary St, Richmond, VA 09584",
                                                      style: inter.copyWith(
                                                          fontSize: 10.sp,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color:
                                                              AppColor.greyTxt),
                                                    )
                                                  ],
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Performance length:",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400),
                                          ),
                                          Text(
                                            "1.5 hrs",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.greenText),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 12.h, bottom: 8.h),
                                        color: AppColor.white.withOpacity(0.07),
                                        height: 1,
                                        width: double.infinity,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Guarantee amount:",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400),
                                          ),
                                          Text(
                                            " \$300",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.greenText),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 12.h, bottom: 8.h),
                                        color: AppColor.white.withOpacity(0.07),
                                        height: 1,
                                        width: double.infinity,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Food and beverage included: ",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400),
                                          ),
                                          Text(
                                            "Yes",
                                            style: poppins.copyWith(
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400,
                                                color: AppColor.greenText),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.symmetric(vertical: 8.h),
                                        color: AppColor.white.withOpacity(0.07),
                                        height: 1,
                                        width: double.infinity,
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 11.w, vertical: 12.h),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.r),
                                          color:
                                              AppColor.white.withOpacity(0.07),
                                        ),
                                        width: double.infinity,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Response time for this request: ",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                            Text(
                                              "48 hrs",
                                              style: poppins.copyWith(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: AppColor.blueText),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 14.h, bottom: 6.h),
                                        child: Text(
                                          "Message",
                                          style: poppins.copyWith(
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w500,
                                              color: AppColor.grey7E),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.w, vertical: 8.h),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.r),
                                          color:
                                              AppColor.white.withOpacity(0.07),
                                        ),
                                        width: double.infinity,
                                        child: Text(
                                          "We are interested in having you performing at our venue. I believe that your music would be a perfect fit for our audience. Are you interested?",
                                          style: poppins.copyWith(
                                              fontSize: 11.sp,
                                              fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ]),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    right: 16.w,
                                    left: 16.w,
                                    bottom: 41.h,
                                    top: 58.h),
                                child: MyCustomButton(
                                  title: "Go back home",
                                  onTap: () {},
                                  loading: false,
                                  color: AppColor.blue,
                                  width: double.infinity,
                                  height: 50.h,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            },
            child: Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: AppColor.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(16)),
                child: Text(
                  "Open Bottom Sheet",
                  style: poppins.copyWith(fontSize: 14),
                )),
          ),
        ],
      ),
    );
  }
}
